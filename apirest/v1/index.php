<?php
/*Los headers permiten acceso desde otro dominio (CORS) a nuestro REST API o desde un cliente remoto via HTTP
* Removiendo las lineas header() limitamos el acceso a nuestro RESTfull API a el mismo dominio
* Nótese los métodos permitidos en Access-Control-Allow-Methods. Esto nos permite limitar los métodos de consulta a nuestro RESTfull API
* Mas información: https://developer.mozilla.org/en-US/docs/Web/HTTP/Access_control_CORS
**/
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS');
header("Access-Control-Allow-Headers: X-Requested-With");
header('Content-Type: text/html; charset=utf-8');
header('P3P: CP="IDC DSP COR CURa ADMa OUR IND PHY ONL COM STA"');

include '../include/ConfigMysql.php';
//include '../include/ConfigFirebird.php';

//LLAMADO A LA LIBRERIA SLIM
require '../libs/Slim/Slim.php';

\Slim\Slim::registerAutoloader();
$app = new \Slim\Slim();

//////////////////METODO GET///////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///LA LECTURA DE LOS DATOS DE RESPUESTA ESTA SOLICITANDO UN API-KEY
	
	$app->get('/auto', 'authenticate', function () {
		include '../include/ConfigFirebird.php';
		include 'GET/local.php';

	echoResponse(200, $response);
	});
///////////////////////////////////////////////////


/////////////////////////METODO POST///////////////////////////
	$app->post('/auto', 'authenticate', function() use ($app) {
	// check for required params
	
	include '../include/ConfigFirebird.php';
	include 'POST/afiliados.php';	
	
	echoResponse(201, $response);
	});
//////////////////////////////////////////////////////////////////


//////////////METODO GET PARA INACTIVAR RECEPCION POR VENCIMIENTO DE LICENCIA ////////////////////////
	$app->get('/lock', 'authenticate', function () {
		include '../include/ConfigFirebird.php';
		include 'GET/lockSohos.php';

	echoResponse(200, $response);
	});
///////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////METODO PARA INACTIVAR RECEPCION POR VENCIMIENTO DE LICENCIA ////////////////////////
	$app->post('/lock', 'authenticate', function() use ($app) {
	// check for required params
	
	include '../include/ConfigFirebird.php';
	include 'POST/lockSohosActivate.php';	
	
	echoResponse(201, $response);
	});
//////////////////////////////////////////////////////////////////




/*EXCECT APLICACION  app*/
$app->run();

/*******************  FUNCIONES *******************************/
/**
* Verificando los parametros requeridos en el metodo o endpoint
*/
function verifyRequiredParams($required_fields) {
	$error = false;
	$error_fields = "";
	$request_params = array();
	$request_params = $_REQUEST;
	// Handling PUT request params
		if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
			$app = \Slim\Slim::getInstance();
			parse_str($app->request()->getBody(), $request_params);
		}
	foreach ($required_fields as $field) {
		if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
			$error = true;
			$error_fields .= $field . ', ';
		}
	}

		if ($error) {
			// Required field(s) are missing or empty
			// echo error json and stop the app
			$response = array();
			$app = \Slim\Slim::getInstance();
			$response["error"] = true;
			$response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
			echoResponse(400, $response);

			$app->stop();
		}
	}

/**
* Validando parametro email si necesario; un Extra ;)
*/
function validateEmail($email) {
	$app = \Slim\Slim::getInstance();
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$response["error"] = true;
			$response["message"] = 'Email address is not valid';
			echoResponse(400, $response);

			$app->stop();
		}
	}

/**
* Mostrando la respuesta en formato json al cliente o navegador
* @param String $status_code Http response code
* @param Int $response Json response
*/
function echoResponse($status_code, $response) {
		$app = \Slim\Slim::getInstance();
		// Http response code
		$app->status($status_code);

		// setting response content type to json
		$app->contentType('application/json');

		echo json_encode($response);
	}

/**
* Agregando un leyer intermedio e autenticación para uno o todos los metodos, usar segun necesidad
* Revisa si la consulta contiene un Header "Authorization" para validar
*/
function authenticate(\Slim\Route $route) {
			// Getting request headers
			$headers = apache_request_headers();
			$response = array();
			$app = \Slim\Slim::getInstance();

			// Verifying Authorization Header
			if (isset($headers['Authorization'])) {
					//$db = new DbHandler(); //utilizar para manejar autenticacion contra base de datos

					// get the api key
					$token = $headers['Authorization'];

					// validating api key
						if (!($token == API_KEY)) { //API_KEY declarada en Config.php

						// api key is not present in users table
						$response["error"] = true;
						$response["message"] = "Acceso denegado. Token inválido";
						echoResponse(401, $response);

						$app->stop(); //Detenemos la ejecución del programa al no validar

						} else {
					//procede utilizar el recurso o metodo del llamado
						}
			} 
			else {
			// api key is missing in header
			$response["error"] = true;
			$response["message"] = "Falta token de autorización";
			echoResponse(400, $response);

			$app->stop();
			}
			}


function utf8_converter($array){
			      array_walk_recursive($array, function(&$item){
			          $item = utf8_encode( $item ); 
			      });
			      return json_encode( $array );
			}


?>