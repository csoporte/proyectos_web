<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get("/", "AdminController@login");
Route::get('logout', 'AdminController@logout');
//Route::get("Clients/prueba", ['as' => 'prueba', 'uses' => 'ClientController@prueba']);

/*funciones diferentes del recurso*/
Route::get("Clients/prueba", "ClientController@prueba");
Route::get("Clients/sincronizar", "ClientController@sincronizar");
Route::get('Pricelists/crearlista','PricelistController@crearlista');
Route::post('Pricelists/generarlista','PricelistController@generarlista')->name('generarlista');
Route::get("Pedidos/PedidosCrudos","PedidosController@PedidosCrudos");
//Route::post('Pedidos/actualizarorderdetails','PedidosController@actualizarorderdetails')->name('Pedidos.actualizarorderdetails');
Route::post('Pedidos/actualizarorderdetails','PedidosController@actualizarorderdetails')->name('Pedidos.actualizarorderdetails');
Route::get('Pedidos/mostrar/{id}/{referencia}','PedidosController@mostrar');


/*Sincronizaciones*/
Route::get("SyncroController/SincronizarGrupos","SyncroController@SincronizarGrupos");
Route::get("SyncroController/SincronizarSubgrupos","SyncroController@SincronizarSubgrupos");
Route::get("SyncroController/SincronizarBodegas","SyncroController@SincronizarBodegas");
Route::get("SyncroController/SincronizarVendedores","SyncroController@SincronizarVendedores");
Route::get("Pedidos/SincronizarPedidos","PedidosController@SincronizarPedidos");
/******************/

/*Rutas datatables*/
Route::get('/tasks', 'ClientController@getTasks')->name('datatable.tablaclientes');
Route::get('/tasksref', 'ReferenciaController@getTasks')->name('datatable.tablareferencias');
Route::get('/taskscolor', 'ColorController@getTasks')->name('datatable.tablacolores');
Route::get('/tasksgrupo', 'GroupsupplyController@getTasks')->name('datatable.tablagrupo');
Route::get('/taskssubgrupo', 'SubGroupsupplyController@getTasks')->name('datatable.tablasubgrupo');
Route::get('/tasksbodega', 'BodegaController@getTasks')->name('datatable.tablabodegas');
Route::get('/tasksilueta', 'SiluetaController@getTasks')->name('datatable.tablasiluetas');
Route::get('/taskvendedor', 'SellerController@getTasks')->name('datatable.tablavendedores');
Route::get('/taskpedidoscrudos','PedidosController@getTasks')->name('datatable.tablapedidoscrudos');
Route::get('/taskordenes','PedidosController@getOrdenes')->name('datatable.tablaordenes');
/**********************/

/*----------Rutas ajax--------------*/
Route::get('/JsonCities', 'CityController@jsonCities');
Route::get('/JsonCity', 'CityController@jsoncity');
Route::get('/JsonTypes', 'ClientController@jsontypes');
Route::get('/Traertallas','ReferenciaController@Traertallas');
Route::get('/JsonCodGrupo','SubGroupsupplyController@jsoncodgrupo');
Route::get('/datosReferencia','ReferenciaController@datosReferencia');
Route::get('/GuardarVendedoReferencia', 'PricelistController@GuardarVendedoReferencia');
Route::get('/BorrarVendedoReferencia', 'PricelistController@BorrarVendedoReferencia');
Route::get('/llenarmatriz','PedidosController@llenarmatriz');
/*********************************************/

/*Recursos*/
Route::resource("Admin", "AdminController");
Route::resource("Bodegas","BodegaController");
Route::resource("Clients", "ClientController");
Route::resource("Colores", "ColorController");
Route::resource("Users", "UserController");
Route::resource("Supplies", "SupplyController");
Route::resource("Groupsupplies", "GroupsupplyController");
Route::resource("SubGroupsupplies", "SubGroupsupplyController");
Route::resource("Pricelists", "PricelistController");
Route::resource("Providers", "ProviderController");
Route::resource("Active", "ActiveController");
Route::resource("Cifin", "CifinController");
Route::resource("City", "CityController");
Route::resource("CivilStatus", "CivilStatusController");
Route::resource("Clasification", "ClasificationController");
Route::resource("Client", "ClientController");
Route::resource("CodePai", "CodePaiController");
Route::resource("Creevtas", "CreevtasController");
Route::resource("Gender", "GenderController");
Route::resource("List", "ListController");
Route::resource("Pedidos","PedidosController");
Route::resource("ProductionOrder","ProductionOrderController");
Route::resource("Profession", "ProfessionController");
Route::resource("Referencia", "ReferenciaController");
Route::resource("Regimen", "RegimenController");
Route::resource("Sellers", "SellerController");
Route::resource("Silueta", "SiluetaController");
Route::resource("State", "StateController");
Route::resource("TypeDocument", "TypeDocumentController");
Route::resource("Zone", "ZoneController");

