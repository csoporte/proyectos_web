<?php

namespace naga\Http\Controllers;

use Illuminate\Http\Request;
use naga\Pricelist;
use naga\Reference;
use naga\Seller;
use naga\Silueta;
use naga\Price;
use naga\ReferenciaTalla;
use naga\Groupsupply;
use naga\ThereAreUpdate;
use naga\ThereAreHistoricUpdate;
use Session;
use Redirect;
use DB;
use Auth;
use Illuminate\Support\Facades\Input;
use Response;

class PricelistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    //Función que guarda el registro de la lista de precios. Donde se asocia la referencia con el vendedor.
    public function GuardarVendedoReferencia()
    {
        //Se capturan los arreglos que vienen por ajax, de las referencias con los vendedores asociados.
        $referenciaaux = Input::get('referencias');
        $vendedoraux = Input::get('vendedores');
        
        //Las referencias y los vendedores se construyen como una variable get desde la vista, al ser mas de uno entonces el automaticamente lo manda con una ,. Por tal motivo se separa en un arreglo donde cada posicion seria una referencia para el arreglo referencia igualmente con los vendedores.
        $referencias = explode(',',$referenciaaux);
        $vendedores = explode(',',$vendedoraux);

        //Se recorren los dos arreglos creados generando un registro por referencia y vendedor.
        for($i=0;$i<count($vendedores);$i++)
        {
            for($j=0;$j<count($referencias);$j++)
            {
                //Se buscan todos los datos de la referencia
                $refer=Reference::find($referencias[$j]);
                $silueta = Silueta::find($refer->silueta);
                if($refer->muestra==0)
                    $muestra="No";
                else
                    $muestra="Si";

                //Se filtran los precios de la referencia.
                $precios=Price::where('refprice','=',$refer->price)->get();

                //Se obtienen todas las tallas que maneja la referencia.
                $tallas = ReferenciaTalla::where('idreferencia','=',$refer->tallas)->get();

                //se obtiene el numero de tallas de la referencia.
                $numeroregistrotallas=count($tallas);
                
                //Se crea una cadena donde se concatena la primera talla y la ultima. Por ejemplo si la referencia maneja las tallas desde la 6 hasta la 20 entonces la cadena quedaria asi: 6 - 20
                $arreglotalla=$tallas[0]->talla."-".$tallas[$numeroregistrotallas-1]->talla;

                $nombregrupo = Groupsupply::where('group','=',$refer->grupo)->get();
                //dd($nombregrupo);
                
                //se verifica si ya existen la referencia en la lista de precios, de ser asi no se hace nada. Sino existe genera el registro.
                $pricelistverificar = Pricelist::where('code','=',$refer->codigo)
                                                ->where('seller','=',$vendedores[$i])
                                                ->get();

                //Se valida si es surtido o xcolor, si llega a ser surtido se guarda en la lista de precios con 1 si es xcolor se guarda con cero. 
                if($refer->formaventa == "surtido")
                    $vende=1;
                else
                    $vende=0;

                //Proceso inicial para verificar el numero de precios creado al momento de parametrizar las referencias. La lista esta enfocada en solo tres precios. Por ese motivo se cuenta cuantos precios se guardaron si es uno, los otros precios son ceros. Si es dos entonces el precio tres es cero. Si es tres entonces los precios estan completos y se asocian a las variables, si por casualidad se agregan mas de tres precios, como la lista esta limitada a solo tres, se asocian nuevamente solo los precios 1,2,3.
                $cuanto=count($precios);
                $precio1=$precios[0]->price;
                if($cuanto == 1)
                {
                    $precio2=0;
                    $precio3=0;
                }
                elseif($cuanto == 2)
                {
                    $precio2=$precios[1]->price;
                    $precio3=0;
                }
                elseif($cuanto == 3)
                {
                    $precio2=$precios[1]->price;
                    $precio3=$precios[2]->price;
                }
                elseif($cuanto > 3)
                {
                    $precio2=$precios[1]->price;
                    $precio3=$precios[2]->price;
                }

                if(count($pricelistverificar)>0)
                {
                    $existe=1;
                }
                else
                {
                    $pricelist = 
                    [
                        'reference'=>$refer->codigo,
                        'code'=>$refer->codigo,
                        'colection'=>$refer->categoria,
                        'season_id'=>$refer->temporada,
                        'shape'=>$silueta->name,
                        'producttype_id'=>$refer->subgrupo,
                        'physicalsample'=>$muestra,
                        'composition'=>$refer->composition,
                        'vende'=>$vende,
                        'price1'=>$precio1,
                        'price2'=>$precio2,
                        'pwarehouse'=>0,
                        'price3'=>$precio3,
                        'M'=>'SP',
                        'seller'=>$vendedores[$i],
                        'sizes'=>$arreglotalla,
                        'comments'=>$refer->description,
                        'deliverydate'=>$refer->fecharef,
                        'ticket'=>$refer->precio_tiquete,
                        'estado'=>'1',
                        'sync'=>'0',
                        'grupo'=>$nombregrupo[0]->groupName,
                        'nro_colores'=>$refer->colores,
                    ];

                    Pricelist::create($pricelist);
                }
            }
            //Actualiza el estado para que el app movil sincronice un cambio.
            Pricelist::where('seller',$vendedores[$i])
                     ->update(['sync' => '0']);

            //codigo para guardar en la tabla de sincronización del aplicativo movil, para obligar a que la informacion se actualice.
            $vendedor=Seller::where('code','=',$vendedores[$i])->get();
            $verificarupdate = ThereAreUpdate::where('id_vendedor','=',$vendedor[0]->id)->get();

            $hayactualizaciones = 
            [
                'id_vendedor'=>$vendedor[0]->id,
                'identification'=>$vendedor[0]->identification,
                'cambio_cliente'=>0,
                'cambio_talla'=>0,
                'cambio_vendedores'=>0,
                'cambio_listap'=>1,
                'fecha_cambio'=>date("Y/m/d"),
                'usuario_registra_cambio'=>Auth::user()->id
            ];
            
            if(count($verificarupdate) == 0)
            {

                ThereAreUpdate::create($hayactualizaciones);
                ThereAreHistoricUpdate::create($hayactualizaciones);
            }
            else
            {
                $verificarupdate[0]->cambio_listap = 1;
                $verificarupdate[0]->fecha_cambio = date("Y/m/d");
                $verificarupdate[0]->usuario_registra_cambio = Auth::user()->id;
                $verificarupdate[0]->save();
                ThereAreHistoricUpdate::create($hayactualizaciones);
            }
            //fin codigo para guardar sincronización
        }

        //Se obtienen los datos de la lista que estan activos y correctos.
        $priceslist = Pricelist::where('estado','=','1')->get();
        //dd($priceslist);
        return Response::json($priceslist);
    }

    //Función que elimina la asociación de la referencia con el vendedor.
    public function BorrarVendedoReferencia()
    {
        //Se capturan los arreglos que vienen por ajax, de las referencias con los vendedores asociados.
        $refer_vende_aux = Input::get('refer_vende');
        $refer_vende = explode(',',$refer_vende_aux);
        for($i=0;$i<count($refer_vende);$i++)
        {
            $pricelist=Pricelist::find($refer_vende[$i]);
            Pricelist::where('seller',$pricelist->seller)
                     ->update(['sync' => '0']);
            $pricelist->delete();

            
            //codigo para guardar en la tabla de sincronización del aplicativo movil, para obligar a que la informacion se actualice.
            $vendedor=Seller::where('code','=',$pricelist->seller)->get();
            $verificarupdate = ThereAreUpdate::where('id_vendedor','=',$vendedor[0]->id)->get();
            $hayactualizaciones = 
            [
                'id_vendedor'=>$vendedor[0]->id,
                'identification'=>$vendedor[0]->identification,
                'cambio_cliente'=>0,
                'cambio_talla'=>0,
                'cambio_vendedores'=>0,
                'cambio_listap'=>1,
                'fecha_cambio'=>date("Y/m/d"),
                'usuario_registra_cambio'=>Auth::user()->id
            ];
            
            if(count($verificarupdate) == 0)
            {

                ThereAreUpdate::create($hayactualizaciones);
                ThereAreHistoricUpdate::create($hayactualizaciones);
            }
            else
            {
                $verificarupdate[0]->cambio_listap = 1;
                $verificarupdate[0]->fecha_cambio = date("Y/m/d");
                $verificarupdate[0]->usuario_registra_cambio = Auth::user()->id;
                $verificarupdate[0]->save();
                ThereAreHistoricUpdate::create($hayactualizaciones);
            }
            //fin codigo para guardar sincronización
        }

        $priceslist = Pricelist::where('estado','=','1')->get();
        //dd($priceslist);
        return Response::json($priceslist);
    }


    public function index()
    {
        $pricelists = Pricelist::paginate(15);
        return view('PriceList.index', compact('pricelists'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('PriceList.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Pricelist::create($request->all());
        Session::flash('message', 'Lista de precios creado correctamente');
        return ['url'=>'Pricelists/'];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Pricelist = Pricelist::find($id);
        return view('PriceList.edit', ['pricelist'=>$Pricelist]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Pricelist = Pricelist::find($id);
        $Pricelist->fill($request->all());
        $Pricelist->save();
        Session::flash('message', 'Lista de precios modificado correctamente');
        return ['url'=>'Pricelists/'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Pricelist::destroy($id);
        Session::flash('message', 'Lista de precios eliminado correctamente');
        return ['url'=>'Pricelists/'];
    }


    
    public function crearlista()
    {
        $referencias = Reference::pluck('codigo','id');
        $vendedores = Seller::where('status_seller','=','A')->get();
        $priceslist = Pricelist::where('estado','=','1')->get();
        //dd($priceslist);
        return view('PriceList.crearlista', compact('referencias','vendedores','priceslist'));
    }

    
    
    public function generarlista()
    {
        dd("si llega");
    }
}
