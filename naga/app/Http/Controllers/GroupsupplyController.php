<?php

namespace naga\Http\Controllers;

use Illuminate\Http\Request;
use naga\Groupsupply;
use Session;
use Redirect;
use DB;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Input;
use Response;

class GroupsupplyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function getTasks()
    {
        $grupos = Groupsupply::select(['id','group','groupName']);
 
        return Datatables::of($grupos)

        ->make(true);
    }

    public function index()
    {
        //$groupsupplies = Groupsupply::paginate(15);
        $groupsupplies = Groupsupply::all();
        //dd($groupsupplies);
        return view('Groupsupply.index', compact('groupsupplies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Groupsupply.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Groupsupply::create($request->all());
        Session::flash('message', 'grupo de suministro creado correctamente');
        return ['url'=>'Groupsupplies/'];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Groupsupply = Groupsupply::find($id);
        return view('Groupsupply.edit', ['groupsupply'=>$Groupsupply]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Groupsupply = Groupsupply::find($id);
        $Groupsupply->fill($request->all());
        $Groupsupply->save();
        Session::flash('message', 'grupo de suministro modificado correctamente');
        return ['url'=>'Groupsupplies/'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Groupsupply::destroy($id);
        Session::flash('message', 'grupo de suministro eliminado correctamente');
        return ['url'=>'Groupsupplies/'];
    }
}
