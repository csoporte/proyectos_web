<?php

namespace naga\Http\Controllers;

use Illuminate\Http\Request;
use naga\reference;
use naga\ReferenciaTalla;
use naga\ReferenciaColor;
use Yajra\Datatables\Datatables;
use naga\talla;
use naga\color;
use naga\ProOrder;
use naga\CutProportion;
use naga\OrderProductionColor;
use Illuminate\Support\Facades\Input;
use Response;
use DB;

class ProductionOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $colores = color::All()->pluck('nombre','id');
        $proorder = ProOrder::All()->last();

        if($proorder==null)
            $idproorder=1;
        else
            $idproorder = $proorder->id + 1;

        //dd($colores);
        return view('ProOrder.create',compact('colores','idproorder'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
