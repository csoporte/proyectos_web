<?php

namespace naga\Http\Controllers;

use Illuminate\Http\Request;
use naga\Silueta;
use Session;
use Redirect;
use DB;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Input;
use Response;

class SiluetaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $siluetas = Silueta::all();
        return view('Silueta.index', compact('siluetas'));
    }

    public function getTasks()
    {
        $siluetas = Silueta::select(['id','name','description']);
 
        return Datatables::of($siluetas)

        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Silueta.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Silueta::create($request->all());
        Session::flash('message', 'grupo de suministro creado correctamente');
        return ['url'=>'Silueta/'];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $silueta = Silueta::find($id);
        return view('Silueta.edit', ['silueta'=>$silueta]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $silueta = Silueta::find($id);
        $silueta->fill($request->all());
        $silueta->save();
        Session::flash('message', 'grupo de suministro modificado correctamente');
        return ['url'=>'Silueta/'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
