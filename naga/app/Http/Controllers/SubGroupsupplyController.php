<?php

namespace naga\Http\Controllers;

use Illuminate\Http\Request;
use naga\SubGroupsupply;
use naga\Groupsupply;
use Session;
use Redirect;
use DB;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Input;
use Response;

class SubGroupsupplyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function getTasks()
    {
        $subgrupos = SubGroupsupply::select(['id','subgroup','subgroupName','group_id']);
 
        return Datatables::of($subgrupos)

        ->make(true);
    }

    public function jsoncodgrupo()
    {
        $grupo_id = Input::get('grupo_id');
        
        //$grupo_cod =DB::table('groupsupplies')->where('groupsupplies.id', '=', $grupo_id)->select('groupsupplies.group')->get();
        $grupo_cod = Groupsupply::where('id',$grupo_id)->get();
        return Response::json($grupo_cod);
    }

    public function index()
    {
        $subgroupsupplies = SubGroupsupply::paginate(15);
        return view('SubGroupsupply.index', compact('subgroupsupplies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $groupsupplies = Groupsupply::pluck('groupName','id');
        return view('SubGroupsupply.create',compact('groupsupplies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        SubGroupsupply::create($request->all());
        Session::flash('message', 'subgrupo de suministro creado correctamente');
        return ['url'=>'SubGroupsupplies/'];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subgroupsupplies = SubGroupsupply::find($id);
        $groupsupplies = Groupsupply::pluck('groupName','id');
        return view('SubGroupsupply.edit', ['subgroupsupply'=>$subgroupsupplies], compact('groupsupplies'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Groupsupply = SubGroupsupply::find($id);
        $Groupsupply->fill($request->all());
        $Groupsupply->save();
        Session::flash('message', 'subgrupo de suministro modificado correctamente');
        return ['url'=>'SubGroupsupplies/'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Groupsupply::destroy($id);
        Session::flash('message', 'subgrupo de suministro eliminado correctamente');
        return ['url'=>'SubGroupsupplies/'];
    }
}
