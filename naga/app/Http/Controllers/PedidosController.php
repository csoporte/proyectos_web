<?php

namespace naga\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use naga\Seller;
use naga\Client;
use naga\Reference;
use naga\ReferenciaTalla;
use naga\State;
use naga\City;
use naga\Order;
use naga\OrdersDetail;
use naga\tallaswebmobile;
use naga\Pricelist;
use naga\OrdersDetailsFinal;
use naga\talla;
use Yajra\Datatables\Datatables;
use Redirect;
use Illuminate\Support\Facades\Input;
use Response;
use Session;
use Alert;
use PDF;

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

class PedidosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    

    /* codigo de la construccion de la matriz, con ejemplo funcional
    $orden="1";
        $referencia="01010626";
        $pedidos = DB::table('orders_details')
            ->where('orders_details.id_order','=',$orden)
            ->where('orders_details.referencia','=',$referencia)
            ->select('*')
            ->orderBy('orders_details.codcolor')
            ->get();

        $pedidosagrupxcolor = DB::table('orders_details')
            ->where('orders_details.id_order','=',$orden)
            ->where('orders_details.referencia','=',$referencia)
            ->groupBy('orders_details.codcolor')
            ->select('orders_details.codcolor')
            ->get();

        $coloresobj = Pricelist::where('reference','=',$referencia)->get();
        $numcolores = $coloresobj[0]->nro_colores;

        for($i=0;$i<$numcolores;$i++)
        {
            $tempo[]=array(
                0=>0,
                1=>0,
                2=>0,
                3=>0,
                4=>0,
                5=>0,
                6=>0,
                7=>0,
                8=>0,
                9=>0
            );
        }

        foreach($pedidos as $pedido)
        {
            $tallapedido = $pedido->talla;
            $tallawebmobile = tallaswebmobile::where('mobilekey',$tallapedido)->get();
            $indicetalla = $tallawebmobile[0]->id;

            $tempo[$pedido->codcolor][$indicetalla-1]=$pedido->cantidad;
        }

        dd($tempo);

     */
    // funcion ajax que llena la matriz con el pedido sincronizado desde el dispositivo movil.
    public function llenarmatriz()
    {
        // se capturan los datos enviados: la referencia, el numero de orden y el id del pedido movil
        $referencia = Input::get('ref');
        $orden = Input::get('orden');
        $codpedidomovil = Input::get('codpedidomovil');

        //consulta para verificar si ya se ha realizado balanceo de esa referencia. Si ya se realizó entonces los datos salen de la tabla donde se registran los balanceos, sino se ha realizado entonces la información sale de la tabla de las ordenes. Adicionalmente se envia una bandera para que en la vista se muestre o no el boton de guardar.
        $validacion = OrdersDetail::where('id_order','=',$orden)
            ->where('referencia','=',$referencia)
            ->where('balanceado','=',1)
            ->get();

        if(count($validacion) > 0)
        {
            // se seleccionan todos los elementos de la orden detallada de acuerdo a la orden y referencia seleccionada en la vista.
            $pedidos = DB::table('orders_details_finals')
                ->where('orders_details_finals.id_order','=',$orden)
                ->where('orders_details_finals.referencia','=',$referencia)
                ->select('*')
                ->orderBy('orders_details_finals.codcolor')
                ->get();
            $notaaux = DB::table('orders_details_finals')
                ->where('orders_details_finals.id_order','=',$orden)
                ->where('orders_details_finals.referencia','=',$referencia)
                ->groupBy('orders_details_finals.referencia','orders_details_finals.nota','orders_details_finals.vlrUnitario')
                ->select('orders_details_finals.nota','orders_details_finals.vlrUnitario',DB::raw('SUM(cantidad) as cantidadxref'))
                ->get();
            $bandera=1;
        }
        else
        {
            // se seleccionan todos los elementos de la orden detallada de acuerdo a la orden y referencia seleccionada en la vista.
            $pedidos = DB::table('orders_details')
                ->where('orders_details.id_order','=',$orden)
                ->where('orders_details.referencia','=',$referencia)
                ->select('*')
                ->orderBy('orders_details.codcolor')
                ->get();

            $notaaux = DB::table('orders_details')
                ->where('orders_details.id_order','=',$orden)
                ->where('orders_details.referencia','=',$referencia)
                ->groupBy('orders_details.referencia','orders_details.nota','orders_details.vlrUnitario')
                ->select('orders_details.nota','orders_details.vlrUnitario',DB::raw('SUM(cantidad) as cantidadxref'))
                ->get();
            $bandera=0;
        }
        
        $nota=$notaaux[0]->nota;
        $cantidadxref = $notaaux[0]->cantidadxref;
        $vlrUnitario = $notaaux[0]->vlrUnitario;

        // se selecciona la cantidad de colores de la referencia seleccionada en la vista.
        $coloresobj = Pricelist::where('reference','=',$referencia)->get();
        $numcolores = $coloresobj[0]->nro_colores;

        //se crea un arreglo vacio con el numero de colores de la referencia por filas y la cantidad de tallas existentes en la matriz por columnas.
        for($i=0;$i<$numcolores;$i++)
        {
            $tempo[]=array(
                0=>null,
                1=>null,
                2=>null,
                3=>null,
                4=>null,
                5=>null,
                6=>null,
                7=>null,
                8=>null,
                9=>null
            );
        }

        //se recorren los registros de cada orden detallada y se actualiza la cantidad de la referencia de acuerdo al color y tallas correspondientes.
        foreach($pedidos as $pedido)
        {
            $tallapedido = $pedido->talla;
            $tallawebmobile = tallaswebmobile::where('mobilekey',$tallapedido)->get();
            $indicetalla = $tallawebmobile[0]->id;

            $tempo[$pedido->codcolor][$indicetalla-1]=$pedido->cantidad;
        }

        return Response::json(['pedidos'=>$pedidos,'numcolores'=>$numcolores,'datosmatriz'=>$tempo,'notarefer'=>$nota,'cantidadxref'=>$cantidadxref,'vlrunitario'=>$vlrUnitario,'bandera'=>$bandera]);
        //return Response::json($pedidos);
    }

    public function index()
    {
        //$orders = Order::all();
        return view('Ordenes.index',compact('orders'));
    }

    // funcion que devuelve todos los pedidos realizados por los vendedores desde el aplicativo movil y lo muestra en una tabla.
    public function getTasks()
    {
        $pedidoscrudos = DB::table('pedidos_sync_mysql')
            ->select('id', 'clienteid', 'nroOrden', 'referenciaId', 'clasificacion_producto', 'cantidadItems', 'vendedorId')
            ->get();

        //dd($pedidoscrudos);

        return Datatables::of($pedidoscrudos)

        ->make(true);
    }

    //Funcion que muestra todas las ordenes sincronizadas.
    public function getOrdenes()
    {
        $orders = DB::table('orders')
            ->join('clients','clients.codeTer','=','orders.codcliente')
            ->join('sellers','sellers.code','=','orders.codvendedor')
            ->select('orders.id', 'clients.nameTer', 'orders.totalprendas', 'orders.dirPedido', 'orders.fechaDespacho', DB::raw('SUBSTRING(orders.notapedido,1,30) as notapedido'), 'sellers.name')
            ->get();

        //dd($pedidoscrudos);

        return Datatables::of($orders)
        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        dd("llega aqui");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        dd("llega aqui");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        dd("Estoy en la función show.");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    //Funcion que edita la orden y muestra los detalles de la orden.
    public function edit($id)
    {
        $orden = Order::find($id);

        $referencias = DB::table('orders_details')
            ->where('id_order','=',$orden->id)
            ->groupBy('referencia')
            ->pluck('referencia','referencia');

        //dd($referencias);

        $itemsxorden = OrdersDetail::where('id_order','=',$orden->id)->orderBy('id')->get();

        $clientes = Client::where('codeTer','=',$orden->codcliente)->get();
        $cliente=$clientes[0];

        Alert::message('Robots are working!');

        //dd($itemsxorden);
        return view('Ordenes.edit',['orden'=>$orden],compact('referencias','itemsxorden','cliente'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function PedidosCrudos()
    {
        /*$pedidos = DB::table('pedidos_sync_mysql')
            ->select('*')
            ->get();

        return view('Pedidos.MostrarPedidosRemotos',compact('pedidos'));*/

        return view('Pedidos.main');
    }

    //Función que sincroniza los pedidos realizados por el app movil y crea los registros en el aplicativo web. Creando dos tablas una para la orden y otra para los detalles.
    public function SincronizarPedidos()
    {
        //Se obtienen todos los datos de la tabla donde se guardan los pedidos realizados por el app movil pero se agrupan, para obtener los datos principales del pedido como el total de prendas, la fecha de despacho, el id del vendedor y la nota del pedido.
        $pedidoscrudosagrupado = DB::table('pedidos_sync_mysql')
            ->select('clienteid','nroOrden','fecha_despacho',DB::raw('SUM(cantidadItems) as cantidadItems'),DB::raw('SUM(vlrTotal) as vlrTotal'),'emailConfirm','vendedorId','nroParciales','recibeDesde','notaFinal','fechaPedido','otraDirEnvio')
            ->groupBy('nroOrden','clienteid','fecha_despacho','emailConfirm','vendedorId','nroParciales','recibeDesde','notaFinal','fechaPedido','otraDirEnvio')
            ->orderBy('nroOrden')
            ->get();
        
        //Se recorren los registros obtenidos en el proceso anterior.
        foreach($pedidoscrudosagrupado as $pedido)
        {
            //Dado que el aplicativo movil guarda el vendedor con su codigo avansis, entonces se captura el id del vendedor cruzando las tablas con el codigo avansis como filtro.
            $vendedores = DB::table('sellers')
                ->join('seller_users','seller_users.id','=','sellers.iduser')
                ->where('sellers.code','=',$pedido->vendedorId)
                ->select('sellers.code','sellers.id')
                ->get();

            //Se obtiene toda la información del cliente.
            $cliente = Client::where('codeTer',$pedido->clienteid)->get();

            // Se verifica si el vendedor asocio otra dirección de envio para el pedido. De ser asi pasa a auditoria y se asocia la nueva dirección. Sino se asocia la dirección orginal
            if($pedido->otraDirEnvio <> null)
            {
                $direccion = $pedido->otraDirEnvio;
                $cliente[0]->auditoria='T';
            }
            else
            {
                $direccion = trim($cliente[0]->dirTer);
            }

            $fecaux = explode("/",$pedido->recibeDesde);
            if($fecaux[1]<10)
                $fecaux[1]="0".$fecaux[1];
            $recibedesde=$fecaux[2]."-".$fecaux[1]."-".$fecaux[0];

            //Se crea el registro de la orden de pedido general. Guarda el cliente, el vendedor, el codigo del pedido movil, el total de prendas, el costo total, el numero de parciales, la fecha de despacho, la fecha que reciba, la nota del pedido, la fecha del pedido, la direccion de envio, la ciudad y el email del cliente.
            $order = 
            [
                'codcliente'=>$pedido->clienteid,
                'codvendedor'=>$vendedores[0]->code,
                'codpedidomovil'=>$pedido->nroOrden,
                'totalprendas'=>$pedido->cantidadItems,
                'costototal'=>$pedido->vlrTotal,
                'precio'=>0,
                'numparciales'=>$pedido->nroParciales,
                'fechaDespacho'=>$pedido->fecha_despacho,
                'fechaRecibe'=>$recibedesde,
                'notapedido'=>$pedido->notaFinal,
                'fechapedido'=>$pedido->fechaPedido,
                'dirPedido'=>$direccion,
                'codciudad'=>$cliente[0]->city_id,
                'emailConfirmacion'=>$cliente[0]->email
            ];

            //$neworder = order::create($order); Desbloquear para generar la orden-------------------NO OLVIDAR DESBLOQUEAR

            //Se obtienen los registros por id del pedido movil.
            $pedidoscrudos=DB::table('pedidos_sync_mysql')
                ->where('nroOrden',$pedido->nroOrden)
                ->select('*')
                ->get();

            $tallasequivalentes=null;

            // se recorren los registros obtenidos con el fin de generar los registros especificos de la orden.
            foreach($pedidoscrudos as $pedidoxitem)
            {
                //Dado que la app movil guarda una unica talla sin importar la referencia, se crea una tabla de equivalencias donde por ejemplo el app movil guarda la talla XS, pero dependiendo de la referencia puede ser la talla XS o puede ser la talla 34 para caballeros y 12 para damas. Asi que se obtienen las equivalencias.
                $tallawebmobile = tallaswebmobile::where('mobilekey',$pedidoxitem->nroTalla)->get();

                //Como el registro de las equivalencias se guarda de la siguiente manera por ejemplo: XS->34-12. Entonces se toma la equivalencia y se separa con la funcion explode usando el caracter '-'. 
                $tallasequivalentes = explode("-",$tallawebmobile[0]->datoasociado);

                //Si la equivalencia es una sola por ejemplo 32->10 entonces se hace una consulta con un OR donde se obtiene el codigo referencia-talla de esa talla.  Si la equivalencia es mayor que uno por ejemplo XS->34-12. Entonces se hace la consulta de modo raw para concatenar mediante un foreach todas las tallas asociadas tambien con un OR y obtener todos los id de referencias-tallas dependiendo de la talla revisada.
                if(count($tallasequivalentes) > 1)
                {

                    $consulta = "SELECT * FROM referencia_tallas WHERE codreferencia='$pedidoxitem->referenciaId' AND (talla='$pedidoxitem->nroTalla'";

                    foreach($tallasequivalentes as $tallaequi)
                    {
                        $consulta = $consulta." OR talla='$tallaequi'";
                    }
                    $consulta = $consulta.")";

                    $tallareferencia = DB::select(DB::raw($consulta));


                    //Falta hacer la validacion para las tallas cuando sean mas de una equivalente. 
                }
                else
                {
                    $tallareferencia = ReferenciaTalla::where('codreferencia',$pedidoxitem->referenciaId)
                        ->where('talla',$pedidoxitem->nroTalla)
                        ->orWhere('talla',$tallasequivalentes[0])
                        ->get();

                    //Verificación para validar que la talla pedida exista o no. Si no existe dentro de las tallas validas, se busca la talla en la tabla de tallas haciendo un cruce con el tipo de prenda y el sexo, todo para generar el registgro en la orden ya que, se debe permitir elingreso de tallas no existentes. 
                    if(count($tallareferencia)==0)
                    {
                        //se busca la referencia de acuerdo al codigo.
                        $referenciageneral=Reference::where('codigo','=',$pedidoxitem->referenciaId)->get();
                        
                        //variables auxiliares que se utilizan para enviar a la funcion que agrupa las condicione de la talla en la consulta de busqueda de la tabla tallas. 
                        $t=$pedidoxitem->nroTalla;
                        $t2=$tallasequivalentes[0];

                        //Consulta que busca las tallas existentes que cumplan las condiciones de la prenda. El tipo ya sea M o F, la pieza ya sea superior o inferior y las tallas. 
                        $tallaaux = talla::where('sexo','=',$referenciageneral[0]->tipo)
                                ->where('area','=',$referenciageneral[0]->pieza)
                                ->where(function($query) use ($t,$t2){
                                    $query->where('talla','=',$t)
                                          ->orWhere('talla','=',$t2);
                                })
                                ->get();

                        //usando DB::table1
                        /*$tallaaux=DB::table('tallas')
                                ->where('sexo','=',$referenciageneral[0]->tipo)
                                ->where('area','=',$referenciageneral[0]->pieza)
                                ->where(function($query) use ($t,$t2){
                                    $query->where('talla','=',$t)
                                          ->orWhere('talla','=',$t2);
                                })
                                ->select('tallas.*')
                                ->get();*/
                        //asociacion a las variables intermedias que luego se asociaran al objeto orderdetail.
                        $talla=$tallaaux[0]->talla;
                        $idtalla=$tallaaux[0]->id;

                        dd($talla,$idtalla);
                    }
                    else
                    {
                        $talla=$tallareferencia[0]->talla;
                        $idtalla=$tallareferencia[0]->talla_id;
                    }
                }
                //Se genera el registro detallado del pedido, por cada referencia con su color y su talla. Y se asocia al id de la orden creada antes de este ciclo.
                $orderdetail = 
                [
                    'id_order'=>$neworder->id,
                    'codpedidomovil'=>$pedidoxitem->nroOrden,
                    'referencia'=>$pedidoxitem->referenciaId,
                    'codcolor'=>$pedidoxitem->nroColor,
                    'color'=>'',
                    'codtalla'=>$tallareferencia[0]->talla_id,
                    'talla'=>$tallareferencia[0]->talla,
                    'cantidad'=>$pedidoxitem->cantidadItems,
                    'vlrUnitario'=>$pedidoxitem->vlrUnitario,
                    'vlrTotal'=>$pedidoxitem->vlrTotal,
                    'nota'=>$pedidoxitem->nota
                ];

                OrdersDetail::create($orderdetail);
            }
        }
        //Se eiminan todos los registros de la tabla de sincronización para indicar que el app web ya sincronizo los datos creados por la app movil.
        $consulta = DB::delete("DELETE FROM pedidos_sync_mysql");
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    //esta funcion actualiza la matrtiz de los pedidos por referencia. 
    public function actualizarorderdetails(Request $request)
    {
        //se busca la cantidad de colores de la referencia, de acuerdo a como se contruyo la matriz. Ya que es muy posible que se hayan balanceado a otros colores y tallas.
        $coloresobj = Pricelist::where('reference','=',$request->referencia)->get();
        $numcolores = $coloresobj[0]->nro_colores;

        //se hace un ciclo con el numero de colores y la cantidad de tallas, en este caso se deja la constante del numero de tallas permitidos entre 28-6 hasta 3XL-46-24. Esto da un total de diez tallas posibles. 
        for($i=0;$i<$numcolores;$i++)
        {
            for($j=0;$j<10;$j++)
            {
                $aux=0;
                //se captura el valor del input cantidad de la matriz de balanceo.
                $aux = $request->input('cantidad_'.$i.'_'.$j);
                //condicion para verificar que venga o no venga vacio. De venir vacio no se hace nada, pero si viene con datos validos se genera un nuevo registro en la tabla de las ordenes finales balanceadas.
                if($aux === null)
                    $cambio=0;
                else
                {   
                    //Dado que la app movil guarda una unica talla sin importar la referencia, se crea una tabla de equivalencias donde por ejemplo el app movil guarda la talla XS, pero dependiendo de la referencia puede ser la talla XS o puede ser la talla 34 para caballeros y 12 para damas. Asi que se obtienen las equivalencias.
                    $tallawebmobile = tallaswebmobile::where('id',($j+1))->get();
                    $talla = $tallawebmobile[0]->mobilekey;

                    //Como el registro de las equivalencias se guarda de la siguiente manera por ejemplo: XS->34-12. Entonces se toma la equivalencia y se separa con la funcion explode usando el caracter '-'. 
                    $tallasequivalentes = explode("-",$tallawebmobile[0]->datoasociado);

                    //Si la equivalencia es una sola por ejemplo 32->10 entonces se hace una consulta con un OR donde se obtiene el codigo referencia-talla de esa talla.  Si la equivalencia es mayor que uno por ejemplo XS->34-12. Entonces se hace la consulta de modo raw para concatenar mediante un foreach todas las tallas asociadas tambien con un OR y obtener todos los id de referencias-tallas dependiendo de la talla revisada.
                    if(count($tallasequivalentes) > 1)
                    {

                        $consulta = "SELECT *  FROM referencia_tallas WHERE codreferencia='$request->referencia' AND (talla='$talla'";

                        foreach($tallasequivalentes as $tallaequi)
                        {
                            $consulta = $consulta." OR talla='$tallaequi'";
                        }
                        $consulta = $consulta.")";

                        $tallareferencia = DB::select(DB::raw($consulta));
                    }
                    else
                    {
                        $tallareferencia = ReferenciaTalla::where('codreferencia',$request->referencia)
                            ->where('talla',$talla)
                            ->orWhere('talla',$tallasequivalentes[0])
                            ->get();
                    }

                    //capturo la informacion de las ordenes de la tabla orders_detail, de acuerdo al color, a la talla y a la referencia.
                    $orderdetailoriginal = OrdersDetail::where('id_order','=',$request->id)
                        ->where('referencia','=',$request->referencia)
                        ->where('talla','=',$talla)
                        ->where('codcolor','=',$i)
                        ->get();

                    //si existe el dato, es decir si existe el registro, entonces se toma el id y se actualiza su campo balanceo para indicar que fue balanceado. Sino existe el dato entonces significa que se balanaceo a una celda nueva por lo cual es necesario generar un nuevo registro, donde el id de la orden a balancear es null ya que no hay registro previo.
                    if(count($orderdetailoriginal) > 0)
                    {
                        $orderdetailfinal = 
                        [
                            'id_detail_modificado'=>$orderdetailoriginal[0]->id,
                            'id_order'=>$request->id,
                            'codpedidomovil'=>$request->codpedidomovil,
                            'referencia'=>$request->referencia,
                            'codcolor'=>$i,
                            'codtalla'=>$tallareferencia[0]->talla_id,
                            'talla'=>$talla,
                            'cantidad'=>$aux,
                            'vlrUnitario'=>$orderdetailoriginal[0]->vlrUnitario,
                            'vlrTotal'=>($aux * $orderdetailoriginal[0]->vlrUnitario),
                            'nota'=>$request->notaref
                        ];

                        $orderdetailoriginal[0]->balanceado = 1;
                        $orderdetailoriginal[0]->save();
                    }
                    else
                    {
                        $orderdetailfinal = 
                        [
                            'id_order'=>$request->id,
                            'codpedidomovil'=>$request->codpedidomovil,
                            'referencia'=>$request->referencia,
                            'codcolor'=>$i,
                            'codtalla'=>$tallareferencia[0]->talla_id,
                            'talla'=>$talla,
                            'cantidad'=>$aux,
                            'vlrUnitario'=>$request->vlrunitario,
                            'vlrTotal'=>($aux * $request->vlrunitario),
                            'nota'=>$request->notaref
                        ];
                    }
                    OrdersDetailsFinal::create($orderdetailfinal);
                }
            }
        }

        //dd($cantidad,$tallawebmobile);

        return redirect('Pedidos');
        
        //return ['url'=>'Pedidos/'];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function mostrar($id,$referencia)
    {
        //proceso para la cabecera del informe o impresion
        $orden = Order::find($id);

        $referencias = DB::table('orders_details')
            ->where('id_order','=',$orden->id)
            ->groupBy('referencia')
            ->pluck('referencia','referencia');


        $itemsxorden = OrdersDetail::where('id_order','=',$orden->id)->orderBy('id')->get();

        $clientes = Client::where('codeTer','=',$orden->codcliente)->get();
        $cliente=$clientes[0];

        //proceso para la matriz
        $validacion = OrdersDetail::where('id_order','=',$id)
            ->where('referencia','=',$referencia)
            ->where('balanceado','=',1)
            ->get();

        if(count($validacion) > 0)
        {
            // se seleccionan todos los elementos de la orden detallada de acuerdo a la orden y referencia seleccionada en la vista.
            $pedidos = DB::table('orders_details_finals')
                ->where('orders_details_finals.id_order','=',$id)
                ->where('orders_details_finals.referencia','=',$referencia)
                ->select('*')
                ->orderBy('orders_details_finals.codcolor')
                ->get();
            $notaaux = DB::table('orders_details_finals')
                ->where('orders_details_finals.id_order','=',$id)
                ->where('orders_details_finals.referencia','=',$referencia)
                ->groupBy('orders_details_finals.referencia','orders_details_finals.nota','orders_details_finals.vlrUnitario')
                ->select('orders_details_finals.nota','orders_details_finals.vlrUnitario',DB::raw('SUM(cantidad) as cantidadxref'))
                ->get();
            $bandera=1;
        }
        else
        {
            // se seleccionan todos los elementos de la orden detallada de acuerdo a la orden y referencia seleccionada en la vista.
            $pedidos = DB::table('orders_details')
                ->where('orders_details.id_order','=',$id)
                ->where('orders_details.referencia','=',$referencia)
                ->select('*')
                ->orderBy('orders_details.codcolor')
                ->get();

            $notaaux = DB::table('orders_details')
                ->where('orders_details.id_order','=',$id)
                ->where('orders_details.referencia','=',$referencia)
                ->groupBy('orders_details.referencia','orders_details.nota','orders_details.vlrUnitario')
                ->select('orders_details.nota','orders_details.vlrUnitario',DB::raw('SUM(cantidad) as cantidadxref'))
                ->get();
            $bandera=0;
        }
        
        $nota=$notaaux[0]->nota;
        $cantidadxref = $notaaux[0]->cantidadxref;
        $vlrUnitario = $notaaux[0]->vlrUnitario;

        // se selecciona la cantidad de colores de la referencia seleccionada en la vista.
        $coloresobj = Pricelist::where('reference','=',$referencia)->get();
        $numcolores = $coloresobj[0]->nro_colores;

        //se crea un arreglo vacio con el numero de colores de la referencia por filas y la cantidad de tallas existentes en la matriz por columnas.
        for($i=0;$i<$numcolores;$i++)
        {
            $tempo[]=array(
                0=>null,
                1=>null,
                2=>null,
                3=>null,
                4=>null,
                5=>null,
                6=>null,
                7=>null,
                8=>null,
                9=>null
            );
        }

        //se recorren los registros de cada orden detallada y se actualiza la cantidad de la referencia de acuerdo al color y tallas correspondientes.
        foreach($pedidos as $pedido)
        {
            $tallapedido = $pedido->talla;
            $tallawebmobile = tallaswebmobile::where('mobilekey',$tallapedido)->get();
            $indicetalla = $tallawebmobile[0]->id;

            $tempo[$pedido->codcolor][$indicetalla-1]=$pedido->cantidad;
        }

        Alert::message('Robots are working!');

        $pdf = PDF::loadView('Pedidos.mostrar', compact('orden','referencias','itemsxorden','cliente','numcolores','tempo'));
        
        return $pdf->stream('pedido.pdf');

        //return view('Pedidos.mostrar',compact('orden','referencias','itemsxorden','cliente','numcolores','tempo'));
    }
}
