<?php

namespace naga\Http\Controllers;

use Illuminate\Http\Request;
use naga\Bodega;
use Session;
use Redirect;
use DB;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Input;
use Response;

class BodegaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function getTasks()
    {
        $bodegas = Bodega::select(['id','Codigo','Nombre']);
 
        return Datatables::of($bodegas)

        ->make(true);
    }

    public function index()
    {
        $bodegas = Bodega::all();
        return view('Bodega.index', compact('bodegas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Bodega.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Bodega::create($request->all());
        Session::flash('message', 'Bodega creada correctamente');
        return ['url'=>'Bodegas/'];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bodega = Bodega::find($id);
        return view('Bodega.edit', ['bodega'=>$bodega]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $bodega = Bodega::find($id);
        $bodega->fill($request->all());
        $bodega->save();
        Session::flash('message', 'Bodega modificada correctamente');
        return ['url'=>'Bodegas/'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Bodega::destroy($id);
        Session::flash('message', 'Bodega eliminada correctamente');
        return ['url'=>'Bodegas/'];
    }
}
