<?php

namespace naga\Http\Controllers;

use Illuminate\Http\Request;
use naga\Groupsupply;
use naga\SubGroupsupply;
use naga\Bodega;
use naga\Seller;
use Session;
use Redirect;
use DB;

class SyncroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function SincronizarReferencia()
    {

    }

    public function SincronizarCiudad()
    {

    }

    public function SincronizarDpto()
    {

    }

    public function SincronizarGrupos()
    {
        $dbgrupos=dbase_open('C:\Users\M3MEDIOS_DESARROLLO\Desktop\Proceso naga\DBF_AVANSIS\tablas\INV_GRUP.DBF', 0);
        if ($dbgrupos) 
        {
            $numero_registros = dbase_numrecords($dbgrupos);
            $numcampos = dbase_numfields($dbgrupos);
            for ($i = 1; $i <= $numero_registros; $i++) 
            {
                if ($i==0) 
                    $i++;
                // procesar cada uno de los registros
                $fila = dbase_get_record_with_names($dbgrupos, $i);
                $grupos[$i-1]=$fila;
                $fila['NOMBRE']=utf8_encode($fila['NOMBRE']);
                $fila['NOMBRE']=str_replace("à","Ó", $fila['NOMBRE']);
                $fila['NOMBRE']=str_replace("¥","Ñ", $fila['NOMBRE']);
                $grupo=
                [
                    'group'=>$fila['CODIGO'],
                    'groupName'=>$fila['NOMBRE']
                    //'groupName'=>recode_string("lat1..iso646-de",$fila['NOMBRE'])
                    //'groupName'=>utf8_encode($fila['NOMBRE'])
                    //'groupName'=>iconv("UTF-8", "ASCII//TRANSLIT", $fila['NOMBRE'])
                    //'groupName'=>iconv("UTF-8", "ISO-8859-1", utf8_encode($fila['NOMBRE']))
                    //'groupName'=>iconv("ISO-8859-1", "UTF-8", $fila['NOMBRE'])
                ];
                Groupsupply::create($grupo);
            }
        }

        $groupsupplies = Groupsupply::all();
        return view('Groupsupply.index', compact('groupsupplies'));
    }

    public function SincronizarSubgrupos()
    {
        $dbsubgrupos=dbase_open('C:\Users\M3MEDIOS_DESARROLLO\Desktop\Proceso naga\DBF_AVANSIS\tablas\INV_TALL.DBF', 0);
        if ($dbsubgrupos) 
        {
            $numero_registros = dbase_numrecords($dbsubgrupos);
            $numcampos = dbase_numfields($dbsubgrupos);
            for ($i = 1; $i <= $numero_registros; $i++) 
            {
                if ($i==0) 
                    $i++;
                // procesar cada uno de los registros
                $fila = dbase_get_record_with_names($dbsubgrupos, $i);
                $subgrupos[$i-1]=$fila;
                $fila['NOMBRE']=utf8_encode($fila['NOMBRE']);
                $fila['NOMBRE']=str_replace("à","Ó", $fila['NOMBRE']);
                $fila['NOMBRE']=str_replace("¥","Ñ", $fila['NOMBRE']);
                if($fila['CODIGO']=="91FO")
                    $groupcod="91";
                else
                    $groupcod = substr($fila['CODIGO'],0,2);
                
                $idgrupo =DB::table('groupsupplies')->where('groupsupplies.group', '=', $groupcod)->select('groupsupplies.id')->get();

                if($idgrupo[0]->id==0 or $idgrupo[0]->id==null)
                    $idgrupo[0]->id=1;

                //if($idgrupo==0 or $idgrupo==null)
                    //$idgrupo=1;

                //$idgrupo = (int)Groupsupply::where('group', $groupcod)->get()->pluck('id');
                //$idgrupo = (int) DB::select(DB::raw("SELECT groupsupplies.id from groupsupplies where groupsupplies.group = '05'"));
                //$idgrupo = DB::table('groupsupplies')->where('groupsupplies.group', '=', $groupcod)->select('groupsupplies.id')->get();
                $subgrupo=
                [
                    'subgroup'=>$fila['CODIGO'],
                    'subgroupName'=>$fila['NOMBRE'],
                    'group_id'=>$idgrupo[0]->id
                    //'group_id'=>$idgrupo
                ];
                SubGroupsupply::create($subgrupo);
            }
        }

        $subgroupsupplies = SubGroupsupply::all();
        return view('SubGroupsupply.index', compact('subgroupsupplies'));
    }

    public function SincronizarBodegas()
    {
        $bodegasavan=dbase_open('C:\Users\M3MEDIOS_DESARROLLO\Desktop\Proceso naga\DBF_AVANSIS\tablas\INV_ALMA.DBF', 0);
        if ($bodegasavan) 
        {
            $numero_registros = dbase_numrecords($bodegasavan);
            $numcampos = dbase_numfields($bodegasavan);
            for ($i = 1; $i <= $numero_registros; $i++) 
            {
                if ($i==0) 
                    $i++;
                // procesar cada uno de los registros
                $fila = dbase_get_record_with_names($bodegasavan, $i);
                //$grupos[$i-1]=$fila;
                $fila['NOMBRE']=utf8_encode($fila['NOMBRE']);
                $fila['NOMBRE']=str_replace("à","Ó", $fila['NOMBRE']);
                $fila['NOMBRE']=str_replace("¥","Ñ", $fila['NOMBRE']);
                $bodega=
                [
                    'Codigo'=>$fila['CODIGO'],
                    'Nombre'=>$fila['NOMBRE']
                ];
                Bodega::create($bodega);
            }
        }

        $bodegas = Bodega::all();
        return view('Bodega.index', compact('bodegas'));
    }

    public function SincronizarVendedores()
    {
        $vendedores = DB::table('contater_avansis')
            ->where('TIPO_TER','=','V')
            ->orwhere('TIPO_TER','=','W')
            ->where('ACTIVO','=','A')
            ->select('*')
            ->get();

        $contador=0;
        $bandera=0;
        foreach($vendedores as $vendedor)
        {
            $resultado = DB::table('sellers')
                ->where('code','=',$vendedor->CODIGO_TER)
                ->select('*')
                ->get();
            if(count($resultado)>0)
            {
                $bandera=$bandera+1;
            }
            else
            {
                $vendedornaga = 
                [
                    'code'=>$vendedor->CODIGO_TER,
                    'warehouse'=>'56',
                    'identification'=>$vendedor->NIT_TER,
                    'name'=>$vendedor->NOMBRE_TER,
                    'address'=>$vendedor->DIR_TER,
                    'phone'=>$vendedor->FAX_TER,
                    'cellphone'=>$vendedor->TEL_TER
                ];
                Seller::create($vendedornaga);
            }
            $contador=$contador+1;
        }

        $vendedores = Seller::all();
        return view('Seller.index', compact('vendedores'));
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
