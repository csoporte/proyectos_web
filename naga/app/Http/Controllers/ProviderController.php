<?php

namespace naga\Http\Controllers;

use Illuminate\Http\Request;
use naga\Provider;
use Session;
use Redirect;
use DB;

class ProviderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $providers = Provider::paginate(15);
        return view('Provider.index', compact('providers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Provider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Provider::create($request->all());
        Session::flash('message', 'Proveedor creado correctamente');
        return ['url'=>'Providers/'];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Provider = Provider::find($id);
        return view('Provider.edit', ['provider'=>$Provider]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Provider = Provider::find($id);
        $Provider->fill($request->all());
        $Provider->save();
        Session::flash('message', 'Proveedor modificado correctamente');
        return ['url'=>'Providers/'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Provider::destroy($id);
        Session::flash('message', 'Proveedor eliminado correctamente');
        return ['url'=>'Providers/'];
    }
}
