<?php

namespace naga\Http\Controllers;

use Illuminate\Http\Request;
use naga\Seller;
use naga\Typeuser;
use naga\ThereAreUpdate;
use naga\ThereAreHistoricUpdate;
use naga\sellerUser;
use Session;
use Redirect;
use DB;
use Auth;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Input;
use naga\Custom\MyEncrypt;
use naga\Custom\AesCipher;

class SellerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sellers = Seller::paginate(15);
        return view('Seller.index', compact('sellers'));
    }

    public function getTasks()
    {
        $vendedores = Seller::select(['id','code','identification','name','address','phone','cellphone','email']);
 
        return Datatables::of($vendedores)

        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        //Se llama la vista de la creación del vendedor.
        $nombreusuario=null;
        $typeuser=Typeuser::where('id',2)->orWhere('id',4)->pluck('typeuser','id');
        return view('Seller.create',compact('typeuser','nombreusuario'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //se crea el usuario de la tabla sellerUser. Se crea una tabla diferente por si se necesita que el vendedor ingrese al aplicativo desde afuera, se valida a esta tabla.
        $usuario=
        [
            'name'=>$request->name,
            'email'=>$request->email,
            'user'=>$request->username,
            'identification'=>$request->identification,
            'typeuser_id'=>$request->rol,
            'password'=>\Hash::make($request->clave)
        ];
        
        $usuariosave = sellerUser::create($usuario);

        //proceso para encriptar la contraseña con mcrypt, ya no se usa en el php 7. 
        /*$mcrypt = new MyEncrypt();
        #Encrypt
        $encrypted = $mcrypt->encrypt($request->clave);
        dd($encrypted);
        #Decrypt
        //$decrypted = $mcrypt->decrypt($encrypted);
        */

        //Encriptación con AES de 16 bits para guardar el campo secreto, que se valida con la clave ingresada por el vendedor en el aplicativo móvil.
        $secretKey = 'M3medios104872*@';
        $text = $request->clave;
        $encrypted = AesCipher::encrypt($secretKey, $text);
        /*$decrypted = AesCipher::decrypt($secretKey, $encrypted);
        $encrypted->hasError(); // TRUE if operation failed, FALSE otherwise
        $encrypted->getData(); // Encoded/Decoded result
        $encrypted->getInitVector(); // Get used (random if encode) init vector
        */

        //Se verifica si el vendedor ya existe, de ser asi se notifica para que se edite, sino se crea y se actualiza los cambios para que los vendedores sincronicen.
        $verificarseller = Seller::where('code','=',$request->code)->get();
        if(count($verificarseller)==0)
        {
            $seller = 
            [
                'code'=>$request->code,
                'warehouse'=>$request->warehouse,
                'identification'=>$request->identification,
                'name'=>$request->name,
                'address'=>$request->address,
                'phone'=>$request->phone,
                'cellphone'=>$request->cellphone,
                'email'=>$request->email,
                'zone_id'=>$request->zone_id,
                'status_seller'=>'A',
                'rol'=>$request->rol,
                'secreto'=>$encrypted,
                'iduser'=>$usuariosave->id
            ];
            $vendedor=Seller::create($seller);
            Session::flash('message', 'vendedor creado correctamente');

            //codigo para guardar en la tabla de sincronización del aplicativo movil, para obligar a que la informacion se actualice.
            $hayactualizaciones = 
            [
                'id_vendedor'=>$vendedor->id,
                'identification'=>$vendedor->identification,
                'cambio_cliente'=>0,
                'cambio_talla'=>0,
                'cambio_vendedores'=>1,
                'cambio_listap'=>0,
                'fecha_cambio'=>date("Y/m/d"),
                'usuario_registra_cambio'=>Auth::user()->id
            ];
            
            ThereAreUpdate::create($hayactualizaciones);
            ThereAreHistoricUpdate::create($hayactualizaciones);
            //fin codigo para guardar sincronización:
        }
        else
        {
            Session::flash('message', 'Vendedor ya existe en la base, por favor edítelo');
        }

        return ['url'=>'Sellers/'];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Seller = Seller::find($id);
        $usuario = sellerUser::where('identification','=',$Seller->identification)->get();
        if(count($usuario) > 0)
            $nombreusuario=$usuario[0]->user;
        else
            $nombreusuario=null;
        $typeuser=Typeuser::where('id',2)->orWhere('id',4)->pluck('typeuser','id');
        return view('Seller.edit', ['seller'=>$Seller],compact('typeuser','nombreusuario'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //se busca el vendedor de acuerdo a su id.
        $Seller = Seller::find($id);
        
        //se actualizan los registros del vendedor, por este motivo se encripta nuevamente la contraseña del vendedor y se guarda el secreto para que coincida con la contraseña digitada en el aplicativo movil.
        $secretKey = 'M3medios104872*@';
        $text=$request->clave;
        $encrypted = AesCipher::encrypt($secretKey, $text);

        $Seller->fill($request->all());
        $Seller->secreto = $encrypted;

        //Se verifica si el usuario del vendedor ya existe, de ser asi se actualizan sus campos sino existe se crea. 
        $selleruser = sellerUser::where('identification','=',$Seller->identification)->get();
        if(count($selleruser) > 0)
        {
            $selleruser[0]->name=$request->name;
            $selleruser[0]->email=$request->email;
            $selleruser[0]->user=$request->username;
            $selleruser[0]->identification=$request->identification;
            $selleruser[0]->typeuser_id=$request->rol;
            $selleruser[0]->password=\Hash::make($request->clave);
            $selleruser[0]->save();

            if($Seller->iduser==null)
                $Seller->iduser = $selleruser[0]->id;
        }
        else
        {
            $usuario=
            [
                'name'=>$request->name,
                'email'=>$request->email,
                'user'=>$request->username,
                'identification'=>$request->identification,
                'typeuser_id'=>$request->rol,
                'password'=>\Hash::make($request->clave)
            ];
            
            $usuariosave = sellerUser::create($usuario);
            $Seller->iduser = $usuariosave->id;
        }
        $Seller->save();
        
        //codigo para guardar en la tabla de sincronización del aplicativo movil, para obligar a que la informacion se actualice.
        $verificarupdate = ThereAreUpdate::where('id_vendedor','=',$Seller->id)->get();

        $hayactualizaciones = 
        [
            'id_vendedor'=>$Seller->id,
            'identification'=>$Seller->identification,
            'cambio_cliente'=>0,
            'cambio_talla'=>0,
            'cambio_vendedores'=>1,
            'cambio_listap'=>0,
            'fecha_cambio'=>date("Y/m/d"),
            'usuario_registra_cambio'=>Auth::user()->id
        ];
        
        if(count($verificarupdate) == 0)
        {

            ThereAreUpdate::create($hayactualizaciones);
            ThereAreHistoricUpdate::create($hayactualizaciones);
        }
        else
        {
            $verificarupdate[0]->cambio_vendedores = 1;
            $verificarupdate[0]->fecha_cambio = date("Y/m/d");
            $verificarupdate[0]->usuario_registra_cambio = Auth::user()->id;
            $verificarupdate[0]->save();
            ThereAreHistoricUpdate::create($hayactualizaciones);
        }
        //fin codigo para guardar sincronización

        Session::flash('message', 'vendedor modificado correctamente');
        return ['url'=>'Sellers/'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Seller::destroy($id);
        Session::flash('message', 'vendedor eliminado correctamente');
        return ['url'=>'Sellers/'];
    }
}
