<?php

namespace naga\Http\Controllers;

use Illuminate\Http\Request;
use naga\Supply;
use naga\measure;
use Session;
use Redirect;
use DB;

class SupplyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $supplies = Supply::paginate(15);
        return view('Supply.index', compact('supplies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $measures = measure::pluck('name','id');
        return view('Supply.create',compact('measures'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Supply::create($request->all());
        Session::flash('message', 'suministro creado correctamente');
        return ['url'=>'Supplies/'];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Supply = Supply::find($id);
        return view('Supply.edit', ['supply'=>$Supply]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Supply = Supply::find($id);
        $Supply->fill($request->all());
        $Supply->save();
        Session::flash('message', 'suministro modificado correctamente');
        return ['url'=>'Supplies/'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Supply::destroy($id);
        Session::flash('message', 'suministro eliminado correctamente');
        return ['url'=>'Supplies/'];
    }
}
