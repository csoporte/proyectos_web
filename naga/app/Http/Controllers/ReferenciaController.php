<?php

namespace naga\Http\Controllers;

use Illuminate\Http\Request;
use naga\reference;
use naga\ReferenciaTalla;
use naga\ReferenciaColor;
use Yajra\Datatables\Datatables;
use naga\talla;
use naga\color;
use naga\Bodega;
use naga\Temporada;
use naga\Groupsupply;
use naga\subgroupsupply;
use naga\Silueta;
use naga\Price;
use naga\Pricelist;
use naga\ThereAreUpdate;
use naga\ThereAreHistoricUpdate;
use Illuminate\Support\Facades\Input;
use Response;
use DB;
use Session;

class ReferenciaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function Traertallas()
    {
        $tipo = Input::get('tipo');    
        $pieza = Input::get('pieza');

        //$tallas = DB::select(DB::raw("SELECT tallas.id, tallas.talla from tallas where tallas.sexo = '$tipo' and tallas.area='$pieza'"));
        $tallas = DB::table('tallas')
            ->where('tallas.sexo','=',$tipo)
            ->where('tallas.area','=',$pieza)
            ->select('tallas.id','tallas.talla')
            ->get();

        return Response::json($tallas);
    }

    public function datosReferencia()
    {
        $referenciacod = Input::get('idreferencia');    

        $referencia = DB::table('inv_mae_avansis')
            ->where('CODIGO','=',$referenciacod)
            ->where('BORRAR','<>','*')
            ->where('ALMACEN','=','00')
            ->select('*')
            ->get();
       
        return Response::json($referencia);
    }

    public function index()
    {
        //$datos=ReferenciaTalla::();
        //return view('Referencia.index', compact('datos'));
        return view('Referencia.index');
    }

    public function getTasks()
    {
        $referencias = reference::select(['id','codigo','nombre']);
 
        return Datatables::of($referencias)

        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //$colores = color::All();
        $bodegas = Bodega::All()->pluck('Nombre','Codigo');
        $temporadas = Temporada::All()->pluck('Name','id');
        $grupos = Groupsupply::All()->pluck('groupName','group');
        $subgrupos = subgroupsupply::All()->pluck('subgroupName','subgroup');
        $siluetas = Silueta::All()->pluck('name','id');
        $bandera = 0;
        //return view('Referencia.create',compact('colores','bodegas','temporadas','grupos','subgrupos','siluetas'));
        return view('Referencia.create',compact('bodegas','temporadas','grupos','subgrupos','siluetas','bandera'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $reftalla = ReferenciaTalla::all()->last();
        if($reftalla==null)
            $idref=1;
        else
            $idref=$reftalla->idreferencia+1;
        for ($i=0; $i < count($request->talla) ; $i++)
        {
            $tallatalla =DB::table('tallas')->where('tallas.id', '=', $request->talla[$i])->select('tallas.talla')->get();
            //dd($tallatalla->talla);
            $referenciatalla=
            [
                'idreferencia'=>$idref,
                'codreferencia'=>$request->codigo,
                'talla_id'=>$request->talla[$i],
                'talla'=>$tallatalla[0]->talla
            ];
            ReferenciaTalla::create($referenciatalla);
        }
        /*Por ahora no lo necesito, ya que el color se convierte en un numero de colores.*/
        /*
        $refcolor = ReferenciaColor::all()->last();
        if($refcolor==null)
            $idcol=1;
        else
            $idcol=$refcolor->idcolor+1;
        for ($i=0; $i < count($request->colores) ; $i++)
        {
            $referenciacolor=
            [
                'idcolor'=>$idcol,
                'codreferencia'=>$request->codigo,
                'color'=>$request->colores[$i]
            ];
            ReferenciaColor::create($referenciacolor);
        }*/

        $refprice = Price::all()->last();
        if($refprice==null)
            $idprice=1;
        else
            $idprice=$refprice->refprice+1;
        for($i=0; $i < count($request->precio); $i++)
        {
            $referenciaprecio=[
                'referencia'=>$request->codigo,
                'refprice'=>$idprice,
                'pricenumber'=>$i+1,
                'price'=>$request->precio[$i]
            ];
            Price::create($referenciaprecio);
        }
        $referencia=
        [
            'codigo'=>$request->codigo,
            'nombre'=>$request->nombre,
            'almacen'=>$request->almacen,
            'temporada'=>$request->temporada,
            'categoria'=>$request->categoria,
            'tipo'=>$request->tipo,
            'pieza'=>$request->pieza,
            'grupo'=>$request->grupo,
            'subgrupo'=>$request->subgrupo,
            'silueta'=>$request->silueta,
            'colores'=>$request->colores,
            'tallas'=>$idref,
            'formaventa'=>$request->formaventa,
            'description'=>$request->description,
            'muestra'=>$request->muestra,
            'fecharef'=>$request->fecharef,
            'composition'=>$request->composition,
            'costohisto'=>$request->costohisto,
            'costoinve'=>$request->costoinve,
            'precio_tiquete'=>$request->precio_tiquete,
            'price'=>$idprice,
            'observation'=>$request->observation,
        ];
        reference::create($referencia);


        //codigo para guardar en la tabla de sincronización del aplicativo movil, para obligar a que la informacion se actualice.
        /*$lista = Pricelist::where('reference','=',$request->codigo)->get()->last();
        $seller_code = $lista[0]->seller;
        $vendedor=Seller::where('code','=',$seller_code)->get();
        $verificarupdate = ThereAreUpdate::where('id_vendedor','=',$vendedor[0]->id)->get();
        $hayactualizaciones = 
        [
            'id_vendedor'=>$vendedor[0]->id,
            'identification'=>$vendedor[0]->identification,
            'cambio_cliente'=>0,
            'cambio_talla'=>1,
            'cambio_vendedores'=>0,
            'cambio_listap'=>0,
            'fecha_cambio'=>date("Y/m/d"),
            'usuario_registra_cambio'=>Auth::user()->id
        ];
        
        if(count($verficarupdate) == 0)
        {

            ThereAreUpdate::create($hayactualizaciones);
            ThereAreHistoricUpdate::create($hayactualizaciones);
        }
        else
        {
            $verificarupdate[0]->cambio_talla = 1;
            $verificarupdate[0]->fecha_cambio = date("Y/m/d");
            $verificarupdate[0]->usuario_registra_cambio = Auth::user()->id;
            $verificarupdate[0]->save();
            ThereAreHistoricUpdate::create($hayactualizaciones);
        }*/
        //fin codigo para guardar sincronización
        return ['url'=>'Referencia/'];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $referencia = reference::find($id);

        $bodegas = Bodega::All()->pluck('Nombre','Codigo');
        $temporadas = Temporada::All()->pluck('Name','id');
        $grupos = Groupsupply::All()->pluck('groupName','group');
        $subgrupos = subgroupsupply::All()->pluck('subgroupName','subgroup');
        $siluetas = Silueta::All()->pluck('name','id');
        
        //proceso para capturar las tallas dependiendo del tipo y la pieza
        $tallas = DB::table('tallas')
            ->where('tallas.sexo','=',$referencia->tipo)
            ->where('tallas.area','=',$referencia->pieza)
            ->select('tallas.id','tallas.talla')
            ->get();

        //ciclo para capturar los identificadores de las tallas y se guardan en una arreglo temporal, para posteriormente hacer una busqueda sobre el
        foreach($tallas as $talla)
        {
            $temporal[]=$talla->id;
        }

        //dd($temporal);

        //proceso para capturar las tallas seleccionadas de la referencia que se esta editando.
        $tallascheck = DB::table('referencia_tallas')
            ->where('referencia_tallas.idreferencia','=',$referencia->tallas)
            ->select('referencia_tallas.talla_id','referencia_tallas.talla')
            ->get();

        //variable que determinará dentro de la plantilla si se esta editando o se esta creando.
        $bandera = 1;

        //dd($tallascheck);

        //Arreglo de arreglos manual que se construye con la información basica de la talla(el id de la talla, la talla en si, y un valor para determinar si esta seleccionado o no en el registro que se esta editando)
        $check=false;
        foreach($tallas as $talla)
        {
            $tallasfin[]=array($talla->id,$talla->talla,$check);
        }

        //dd($tallasfin);
        
        //proceso para comparar las tallas seleccionadas en la referencia que se esta editando, con las tallas dependiendo del tipo y la pieza. Array_search devuelve el indice donde hubo concordancia, por tal motivo se genera un vector donde cada espacio guarda la posición de las tallas iguales en ambos .
        foreach($tallascheck as $tallacheck)
        {
            $clave[] = array_search($tallacheck->talla_id,$temporal);
        }

        //proceso en el cual se cambia el estado de acuerdo a la concordancia del proceso anterior, por lo cual si una talla de la referencia fue seleccionada esta se volverá true, sino lo fue será false. 
        for($i=0;$i<count($clave);$i++)
        {
            $tallasfin[$clave[$i]][2]=true;
        }

        //proceso para capturar los precios de la referencia editada.
        $precios = DB::table('prices')
            ->where('prices.refprice','=',$referencia->price)
            ->select('prices.pricenumber','prices.price')
            ->get();

        //dd($precios);
        return view('Referencia.edit', ['referencia'=>$referencia], compact('bodegas','temporadas','grupos','subgrupos','siluetas','tallasfin','precios','bandera'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request,
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $referencia = reference::find($id);
        $referencia->
        fill([
            'codigo'=>$request->codigo,
            'nombre'=>$request->nombre,
            'almacen'=>$request->almacen,
            'temporada'=>$request->temporada,
            'categoria'=>$request->categoria,
            'tipo'=>$request->tipo,
            'pieza'=>$request->pieza,
            'grupo'=>$request->grupo,
            'subgrupo'=>$request->subgrupo,
            'silueta'=>$request->silueta,
            'colores'=>$request->colores,
            'formaventa'=>$request->formaventa,
            'description'=>$request->description,
            'muestra'=>$request->muestra,
            'fecharef'=>$request->fecharef,
            'composition'=>$request->composition,
            'costohisto'=>$request->costohisto,
            'costoinve'=>$request->costoinve,
            'precio_tiquete'=>$request->precio_tiquete,
            'observation'=>$request->observation,
        ]);
        $referencia->save();
        
        Session::flash('message', 'Referencia guardada correctamente');
        
        /*Aqui proceso para guardar las tallas y los precios. Hay que validar si son nuevas tallas, si se aumentan o disminuyen y lo mismo con los precios creados.*/

        /*$reftallas = DB::table('referencia_tallas')
            ->where('referencia_tallas.idreferencia','=',$referencia->tallas)
            ->select('referencia_tallas.*')
            ->get();
        
        if(count($reftallas)==count($request->talla))
        {
            for($i=0;$i<count($reftallas);$i++)
            {
                $tallatalla =DB::table('tallas')->where('tallas.id', '=', $request->talla[$i])->select('tallas.talla')->get();

                $reftallas[$i]->talla_id=$request->talla[$i];
                $reftallas[$i]->talla=$tallatalla[0]->talla;
                $reftallas[$i]->save();
                $i=$i+1;
            }
            Session::flash('message', 'Son iguales');
        }
        if(count($reftallas)>count($request->talla))
            Session::flash('message', 'La cantidad seleccionada es menor');
        if(count($reftallas)<count($request->talla))
            Session::flash('message', 'La cantidad seleccionada es mayor de la que hay en base de datos');
        
        $price = Price::find($referencia->price);*/


        //codigo para guardar en la tabla de sincronización del aplicativo movil, para obligar a que la informacion se actualice.
        $lista = Pricelist::where('reference','=',$request->codigo)->get()->last();
        $seller_code = $lista[0]->seller;
        $vendedor=Seller::where('code','=',$seller_code)->get();
        $verificarupdate = ThereAreUpdate::where('id_vendedor','=',$vendedor[0]->id)->get();
        $hayactualizaciones = 
        [
            'id_vendedor'=>$vendedor[0]->id,
            'identification'=>$vendedor[0]->identification,
            'cambio_cliente'=>0,
            'cambio_talla'=>1,
            'cambio_vendedores'=>0,
            'cambio_listap'=>0,
            'fecha_cambio'=>date("Y/m/d"),
            'usuario_registra_cambio'=>Auth::user()->id
        ];
        
        if(count($verficarupdate) == 0)
        {

            ThereAreUpdate::create($hayactualizaciones);
            ThereAreHistoricUpdate::create($hayactualizaciones);
        }
        else
        {
            $verificarupdate[0]->cambio_talla = 1;
            $verificarupdate[0]->fecha_cambio = date("Y/m/d");
            $verificarupdate[0]->usuario_registra_cambio = Auth::user()->id;
            $verificarupdate[0]->save();
            ThereAreHistoricUpdate::create($hayactualizaciones);
        }
        //fin codigo para guardar sincronización
        

        return ['url'=>'Referencia/'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
