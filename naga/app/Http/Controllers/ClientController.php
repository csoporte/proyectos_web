<?php

namespace naga\Http\Controllers;

use Illuminate\Http\Request;
use naga\Client;
use naga\State;
use naga\typedocument;
use naga\City;
use naga\Profession;
use naga\Civilstatus;
use naga\Clasification;
use naga\FormaPago;
use naga\Lista;
use naga\Seller;
use naga\Creevtas;
use naga\Cifin;
use naga\Price;
use Session;
use Redirect;
use DB;
use naga\ThereAreUpdate;
use naga\ThereAreHistoricUpdate;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Input;
use Response;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /*public function prueba()
    {
        $dbclient=dbase_open('C:\Users\M3MEDIOS_DESARROLLO\Desktop\Proceso naga\DBF_AVANSIS\CONTATER.DBF', 0);
        if ($dbclient) 
        {
            $numero_registros = dbase_numrecords($dbclient);
            $temp = dbase_get_record($dbclient, 0);
            /*for ($i = 1; $i <= $numero_registros; $i++) 
            {
                // procesar cada uno de los registros
                $temp = dbase_get_record($dbclient, $i);
                echo $temp[0];
                echo $temp[1];
                echo $temp[2];
            }
        }
        return view('Client.mostrar', compact('temp'));
    }*/

    /*public function prueba()
    {
        $dbclient=dbase_open('C:\Users\M3MEDIOS_DESARROLLO\Desktop\Proceso naga\DBF_AVANSIS\CONTATER.DBF', 0);
        if ($dbclient) 
        {
            $numero_registros = dbase_numrecords($dbclient);
            $aux = dbase_get_record($dbclient, 1);
            $numcampos=dbase_numfields($dbclient);
            

            for ($i = 1; $i <= 100; $i++) 
            {
                $indice=0;
                // procesar cada uno de los registros
                $temp = dbase_get_record($dbclient, $i);
                while ($indice < $numcampos)
                {
                    $arreglo[$i-1][$indice]=$temp[$indice];
                    $indice++;
                }
                //echo $temp[0];
            }
        }
        return view('Client.mostrar', compact('arreglo','numcampos'));
        //return view('Client.mostrar', compact('numcampos'));
    }*/ 

    public function prueba()
    {
        $dbclient=dbase_open('C:\Users\M3MEDIOS_DESARROLLO\Desktop\Proceso naga\DBF_AVANSIS\CONTATER.DBF', 0);
        if ($dbclient) 
        {
            $numero_registros = dbase_numrecords($dbclient);
            $numcampos = dbase_numfields($dbclient);
            for ($i = 0; $i <= 500; $i++) 
            {
                if ($i==0) 
                    $i++;
                // procesar cada uno de los registros
                $fila = dbase_get_record_with_names($dbclient, $i);
                $clients[$i-1]=$fila;
            }
        }
        return view('Client.mostrar', compact('clients','numcampos'));
        
        //$clients = Client::All();
        //return view('Client.mostrar2',compact('clients'));
    }

    public function jsontypes()
    {
        $typedocument_id = Input::get('type_id');    

        //$cities = City::where('state_id', $state_id)->pluck('Name', 'id');
        //$type = DB::select(DB::raw("SELECT typedocuments.typedocument from typedocuments where typedocuments.id = $typedocument_id"));
        $type = typedocument::where('id',$typedocument_id)->get();
        return Response::json($type);
    }

    public function sincronizar()
    {
        $dbclient=dbase_open('C:\Users\M3MEDIOS_DESARROLLO\Desktop\Proceso naga\DBF_AVANSIS\CONTATER.DBF', 0);
        if ($dbclient) 
        {
            $numero_registros = dbase_numrecords($dbclient);
            $numcampos = dbase_numfields($dbclient);
            for ($i = 1; $i <= $numero_registros; $i++) 
            {
                if ($i==0) 
                    $i++;
                // procesar cada uno de los registros
                $fila = dbase_get_record_with_names($dbclient, $i);
                $clients[$i-1]=$fila;
                if(trim($fila['FECHANACI'])==NULL or trim($fila['FECHANACI'])=='')
                //if(empty($fila['FECHANACI']))
                    $fechanaci='1900/01/01';
                else
                    $fechanaci=$fila['FECHANACI'];
                //if($fecha['PROFESION']==NULL)
                $profesion=6;
                //if($fecha['ESTADOCI']==NULL)
                $estadocivil=0;

                //$fila['OBSERVA1']=str_replace("\xA5","Ñ", $fila['OBSERVA1']);

                //$fila['DIR_TER']=str_replace("#","No ", $fila['DIR_TER']);
                
                $confirmarcliente = Client::where('codeTer','=',$fila['CODIGO_TER'])->get();

                $cliente=
                [
                    'codeTer'=>$fila['CODIGO_TER'],
                    'nameTer'=>utf8_encode($fila['NOMBRE_TER']),
                    'typedocument_id'=>$fila['TIPODOCU'],
                    'nitTer'=>$fila['NIT_TER'],
                    'nitTer1'=>$fila['NIT_TER1'],
                    'dirTer'=>utf8_encode($fila['DIR_TER']),
                    'telTer'=>utf8_encode($fila['TEL_TER']),
                    'faxTer'=>utf8_encode($fila['FAX_TER']),
                    'city_id'=>$fila['CODIGO_CIU'],
                    'ciudad_ter'=>utf8_encode($fila['CIUDAD_TER']),
                    'state_id'=>$fila['CODIGO_DEP'],
                    'oficioTer'=>utf8_encode($fila['OFICIO_TER']),
                    'profession'=>utf8_encode($profesion),
                    'business'=>utf8_encode($fila['EMPRESA']),
                    'birthday'=>$fechanaci,
                    'civilStatus_id'=>$estadocivil,
                    'email'=>utf8_encode($fila['EMAIL']),
                    'tipotercero'=>$fila['TIPO_TER'],
                    'limit'=>$fila['LIMITE'],
                    'authorization'=>$fila['AUTORIZA'],
                    'discount'=>$fila['DESCUENTO'],
                    'activo'=>$fila['ACTIVO'],
                    'clasification'=>$fila['CLASIFI'],
                    'methodPayment'=>$fila['FORMAPAGO'],
                    'methodPaymentP'=>$fila['FORMAPAGOP'],
                    'retefte'=>$fila['RETEFTE'],
                    'ica'=>$fila['ICA'],
                    'regimen'=>$fila['REGIMEN'],
                    'list'=>$fila['LISTA'],
                    'zone_id'=>$fila['COD_ZONA'],
                    'zone'=>utf8_encode($fila['ZONA']),
                    'seller_id'=>$fila['VENDEDOR'],
                    'addressOffice'=>utf8_encode($fila['DIR_OFI']),
                    'phoneOffice'=>utf8_encode($fila['TEL_OFI']),
                    'gender'=>$fila['SEXO'],
                    'observation1'=>utf8_encode($fila['OBSERVA1']),
                    'observation2'=>utf8_encode($fila['OBSERVA2']),
                    'observation3'=>utf8_encode($fila['OBSERVA3']),
                    'name'=>utf8_encode($fila['PRIMERNOM']),
                    'lastname'=>utf8_encode($fila['PRIMERAPE']),
                    'businessName'=>utf8_encode($fila['RAZONSOCI']),
                    'reference1'=>$fila['REFE1'],
                    'reference2'=>$fila['REFE2'],
                    'reference3'=>$fila['REFE3'],
                    'reference4'=>$fila['REFE4'],
                    'reference5'=>$fila['REFE5'],
                    'discountFina'=>$fila['DCTOFINA'],
                    'termFunding'=>$fila['PLAZOFIN'],
                    'codePais'=>$fila['CODIGOPAI'],
                    'codePais2'=>$fila['CODIGO_PAI'],
                    'promp24'=>$fila['PROMP24'],
                    'promp12'=>$fila['PROMP12'],
                    'promp6'=>$fila['PROMP6'],
                    'cheqd24'=>$fila['CHEQD24'],
                    'cheqd12'=>$fila['CHEQD12'],
                    'cheqd6'=>$fila['CHEQD6'],
                    'status'=>$fila['ESTADO'],
                    'cree'=>$fila['CREE'],
                    'creevtas'=>$fila['CREEVTAS'],
                    'cifin'=>$fila['CIFIN']
                ];

                if(count($confirmarcliente) > 0)
                    $confirmacliente[0]->save();
                else
                    Client::create($cliente);

                //codigo para guardar en la tabla de sincronización del aplicativo movil, para obligar a que la informacion se actualice.
                $vendedor=Seller::where('code','=',$fila['VENDEDOR'])->get();
                $verificarupdate = ThereAreUpdate::where('id_vendedor','=',$vendedor[0]->id)->get();
                $hayactualizaciones = 
                [
                    'id_vendedor'=>$vendedor[0]->id,
                    'identification'=>$vendedor[0]->identification,
                    'cambio_cliente'=>1,
                    'cambio_talla'=>0,
                    'cambio_vendedores'=>0,
                    'cambio_listap'=>0,
                    'fecha_cambio'=>date("Y/m/d"),
                    'usuario_registra_cambio'=>Auth::user()->id
                ];
                
                if(count($verficarupdate) == 0)
                {

                    ThereAreUpdate::create($hayactualizaciones);
                    ThereAreHistoricUpdate::create($hayactualizaciones);
                }
                else
                {
                    $verificarupdate[0]->cambio_cliente = 1;
                    $verificarupdate[0]->fecha_cambio = date("Y/m/d");
                    $verificarupdate[0]->usuario_registra_cambio = Auth::user()->id;
                    $verificarupdate[0]->save();
                    ThereAreHistoricUpdate::create($hayactualizaciones);
                }
                //fin codigo para guardar sincronización
            }
        }
        return view('Client.mostrar', compact('clients','numcampos'));
        
        /*$clients = Client::All();
        return view('Client.mostrar2',compact('clients'));*/
    }
    
    /*public function index() //Páginando con el paginate.
    {
        $clients = Client::paginate(15);
        return view('Client.index', compact('clients'));
        //return view('Client.mostrar2',compact('clients'));
    }*/

    /*public function index(Request $request) //Páginando con ajax.
    {
        $clients = Client::paginate(15);
        if ($request->ajax() and $request->otro==1) {
            return response()->json(view('Client.clientes', compact('clients'))->render());
        }
        return view('Client.index', compact('clients'));
        //return view('Client.mostrar2',compact('clients'));
    }*/

    public function index()  //Sacando todos los datos para el datatable
    {
        $clients = Client::all();
        return view('Client.index', compact('clients'));
    }

    public function getTasks()
    {
        $clients = Client::select(['id','codeTer','nameTer','nitTer','dirTer','telTer','ciudad_ter']);
 
        return Datatables::of($clients)

        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $typedocument = typedocument::pluck('typedocument', 'id');
        $states = State::pluck('state', 'id');
        $civilstatuses = Civilstatus::pluck('civilstatus', 'id');
        $profesiones = Profession::pluck('profession','id');
        $clasificaciones = Clasification::pluck('clasification', 'id');
        $formas = FormaPago::pluck('forma','id');
        //$listasprecios = Lista::pluck('listname','id');
        //$listasprecios = Price::select('pricenumber')->groupBy('pricenumber')->get();

        $listasprecios = Price::pluck('pricenumber','pricenumber');
        //dd($listasprecios);
        $sellers = Seller::pluck('name','id');
        $creevtas = Creevtas::pluck('creevtas','id');
        $cifins = Cifin::pluck('cifin','id');
        return view('Client.create', compact('typedocument', 'states' , 'profesiones', 'civilstatuses','clasificaciones', 'formas', 'listasprecios','sellers','creevtas','cifins'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Se busca el cliente en la base de datos para verificar si ya existe.
        $cliente=Client::where('codeTer','=',$request->codeTer)->get();

        if(count($cliente) == 0)
        {
            $city_id=$request->city_id;
            $citycod = DB::table('cities')->select('cities.CountryCode')->where('cities.id',$city_id)->get();
            $request->city_id=$citycod;
            
            Client::create($request->all());
            Session::flash('message', 'cliente creado correctamente');

            //codigo para guardar en la tabla de sincronización del aplicativo movil, para obligar a que la informacion se actualice.
            $vendedor=Seller::find($request->codeTer);
            $verificarupdate = ThereAreUpdate::where('id_vendedor','=',$vendedor->id)->get();
            $hayactualizaciones = 
            [
                'id_vendedor'=>$vendedor->id,
                'identification'=>$vendedor->identification,
                'cambio_cliente'=>1,
                'cambio_talla'=>0,
                'cambio_vendedores'=>0,
                'cambio_listap'=>0,
                'fecha_cambio'=>date("Y/m/d"),
                'usuario_registra_cambio'=>Auth::user()->id
            ];
            
            if(count($verficarupdate) == 0)
            {
                ThereAreUpdate::create($hayactualizaciones);
                ThereAreHistoricUpdate::create($hayactualizaciones);
            }
            else
            {
                $verificarupdate[0]->cambio_cliente = 1;
                $verificarupdate[0]->fecha_cambio = date("Y/m/d");
                $verificarupdate[0]->usuario_registra_cambio = Auth::user()->id;
                $verificarupdate[0]->save();
                ThereAreHistoricUpdate::create($hayactualizaciones);
            }
            //fin codigo para guardar sincronización:
        }
        else
        {
            Session::flash('message', 'cliente ya existe');
        }
        
        return ['url'=>'Clients/'];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //public function show($id)
    public function show(Request $request)
    {
        $datos = $request->all();
        return view('Client.mostrar');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Client = Client::find($id);
        $typedocument = typedocument::pluck('typedocument', 'id');
        $states = State::pluck('state', 'id');
        $civilstatuses = Civilstatus::pluck('civilstatus', 'id');
        $profesiones = Profession::pluck('profession','id');
        $clasificaciones = Clasification::pluck('clasification', 'id');
        $formas = FormaPago::pluck('forma','id');
        //$listasprecios = Lista::pluck('listname','id');
        $listasprecios = Price::pluck('pricenumber','pricenumber');
        $sellers = Seller::pluck('name','id');
        $creevtas = Creevtas::pluck('creevtas','id');
        $cifins = Cifin::pluck('cifin','id');
        return view('Client.edit', ['client'=>$Client],compact('typedocument', 'states' , 'profesiones', 'civilstatuses','clasificaciones', 'formas', 'listasprecios','sellers','creevtas','cifins'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Client = Client::find($id);
        $Client->fill($request->all());
        $Client->save();

        //codigo para guardar en la tabla de sincronización del aplicativo movil, para obligar a que la informacion se actualice.
        $vendedor=Seller::find($request->codeTer);
        $verificarupdate = ThereAreUpdate::where('id_vendedor','=',$vendedor->id)->get();
        $hayactualizaciones = 
        [
            'id_vendedor'=>$vendedor->id,
            'identification'=>$vendedor->identification,
            'cambio_cliente'=>1,
            'cambio_talla'=>0,
            'cambio_vendedores'=>0,
            'cambio_listap'=>0,
            'fecha_cambio'=>date("Y/m/d"),
            'usuario_registra_cambio'=>Auth::user()->id
        ];
        
        if(count($verficarupdate) == 0)
        {
            ThereAreUpdate::create($hayactualizaciones);
            ThereAreHistoricUpdate::create($hayactualizaciones);
        }
        else
        {
            $verificarupdate[0]->cambio_cliente = 1;
            $verificarupdate[0]->fecha_cambio = date("Y/m/d");
            $verificarupdate[0]->usuario_registra_cambio = Auth::user()->id;
            $verificarupdate[0]->save();
            ThereAreHistoricUpdate::create($hayactualizaciones);
        }
        //fin codigo para guardar sincronización:
        
        Session::flash('message', 'cliente modificado correctamente');
        return ['url'=>'Clients/'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Client::destroy($id);
        Session::flash('message', 'cliente eliminado correctamente');
        return ['url'=>'Clients/'];
    }

}
