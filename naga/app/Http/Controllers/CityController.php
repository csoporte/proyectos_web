<?php

namespace naga\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
//use Input;
use Response;
use naga\City;
use DB;


class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function jsonCities()
    {
        $state_id = Input::get('state_id');    

        //$cities = City::where('state_id', $state_id)->pluck('Name', 'id');
        $cities = DB::select(DB::raw("SELECT cities.name, cities.id, cities.city_code from cities where cities.state_id = $state_id"));
        return Response::json($cities);
    }

    public function jsoncity()
    {
        $city_id = Input::get('city_id');    

        //$cities = City::where('state_id', $state_id)->pluck('Name', 'id');
        //$cities = DB::select(DB::raw("SELECT cities.name, cities.id, cities.city_code from cities where cities.state_id = $state_id"));
        $city = City::where('id',$city_id)->get();
        return Response::json($city);
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
