<?php

namespace naga\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Redirect;
use Session;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth', ['only'=>'index']);
    }

    public function index()
    {
        return view("Admin.index");
    }

    public function login()
    {
        if(Auth::check())
        {
            return Redirect::to('Admin');
        }
        else
        {
            return view('Front.index');
        }
    }

    public function logout()
    {
        Auth::logout();
        return Redirect::to('/');
    }   

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Auth::attempt(['user'=> $request['user'], 'password'=>$request['password']]))
        {       
            return Redirect::to('Admin');            
        }
        else
        {
            Session::flash('message-error', 'Sus datos son incorrectos');
            return Redirect::to('/');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
