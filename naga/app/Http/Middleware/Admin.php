<?php

namespace naga\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Session;
class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    public function handle($request, Closure $next)
    {
        if($this->auth->user()->typeuser_id == 2)
        {
            Session::flash('message-error', 'Sin privilegios');
            return redirect()->to('Admin');
        }
        return $next($request);
    }
}
