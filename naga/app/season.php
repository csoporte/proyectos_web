<?php

namespace naga;

use Illuminate\Database\Eloquent\Model;

class season extends Model
{
    protected $table = 'seasons';
	protected $fillable = ['name'];
}
