<?php

namespace naga;

use Illuminate\Database\Eloquent\Model;

class Civilstatus extends Model
{
    protected $table = 'civilstatuses';
	protected $fillable = ['civilstatus'];
}
