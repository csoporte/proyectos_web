<?php

namespace naga;

use Illuminate\Database\Eloquent\Model;

class Active extends Model
{
    protected $table = 'actives';
	protected $fillable = ['id', 'active'];
}
