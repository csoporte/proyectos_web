<?php

namespace naga;

use Illuminate\Database\Eloquent\Model;

class Codepai extends Model
{
    protected $table = 'codepais';
	protected $fillable = ['id', 'codepai'];
}
