<?php

namespace naga;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReferenciaColor extends Model
{
	use softdeletes;

    protected $table = 'referencia_colors';

    protected $fillable = ['idcolor','codreferencia','color'];

    protected $dates = ['deleted_at']; 
}
