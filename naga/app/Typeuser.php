<?php

namespace naga;

use Illuminate\Database\Eloquent\Model;

class Typeuser extends Model
{
    protected $table = 'typeusers';
	protected $fillable = ['typeuser'];
}
