<?php

namespace naga;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ThereAreHistoricUpdate extends Model
{
    use softdeletes;

    protected $table = 'there_are_historic_updates';

    protected $fillable = ['id_vendedor','identification','cambio_cliente','cambio_talla','cambio_vendedores','cambio_listap','fecha_cambio','usuario_registra_cambio'];

    protected $dates = ['deleted_at'];
}
