<?php

namespace naga;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'cities';
	protected $fillable = ['name','city_code','State_id', 'CountryCode','indicativo'];
}
