<?php

namespace naga;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    protected $table = 'prices';
    protected $fillable = ['referencia','refprice','pricenumber','price'];
}
