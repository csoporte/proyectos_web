<?php

namespace naga;

use Illuminate\Database\Eloquent\Model;

class ProductionType extends Model
{
    protected $table = 'production_types';
	protected $fillable = ['name'];
}
