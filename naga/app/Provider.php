<?php

namespace naga;

use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
	protected $table = 'providers';
	protected $fillable = ['idn', 'code', 'provider', 'nit', 'address', 'phone', 'city_id', 'tradename', 'product_id', 'email'];
}
