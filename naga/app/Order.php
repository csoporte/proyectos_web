<?php

namespace naga;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use softdeletes;

    protected $table = 'orders';

    protected $fillable = ['codcliente','codvendedor','codpedidomovil','totalprendas','costototal','precio','numparciales','fechaDespacho','fechaRecibe','notapedido','fechapedido','horapedido','dirPedido','codciudad','emailConfirmacion'];

    protected $dates = ['deleted_at'];
}