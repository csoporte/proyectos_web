<?php

namespace naga;

use Illuminate\Database\Eloquent\Model;

class tallaswebmobile extends Model
{
    protected $table = 'tallaswebmobiles';

    protected $fillable = ['mobilekey','datoasociado'];
}
