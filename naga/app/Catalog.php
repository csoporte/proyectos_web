<?php

namespace naga;

use Illuminate\Database\Eloquent\Model;

class Catalog extends Model
{
    protected $table = 'catalogs';
	protected $fillable = ['id', 'catalog'];
}
