<?php

namespace naga;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrdersDetailsFinal extends Model
{
    use softdeletes;

    protected $table = 'orders_details_finals';

    protected $fillable = ['id_detail_modificado','id_order','codpedidomovil','referencia','codcolor','color','codtalla','talla','cantidad','vlrUnitario','vlrTotal','nota'];

    protected $dates = ['deleted_at'];
}
