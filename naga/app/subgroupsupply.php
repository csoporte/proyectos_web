<?php

namespace naga;

use Illuminate\Database\Eloquent\Model;

class subgroupsupply extends Model
{
    protected $table = 'subgroupsupplies';
	protected $fillable = ['subgroup', 'subgroupName','group_id'];
}
