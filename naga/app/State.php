<?php

namespace naga;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $table = 'states';
	protected $fillable = ['id', 'state', 'Country_id'];
}
