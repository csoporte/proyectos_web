<?php

namespace naga;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CutProportion extends Model
{
	use softdeletes;

    protected $table = 'proorder_cut_proportions';

    protected $fillable = ['id_proporcion','id_proorder','id_referencia','referencia','name','respuesta','28','30','32','34','36S','38M','40L','42XL','44XXL','XXXL','total','gentipo','user_id'];

    protected $dates = ['deleted_at'];
}