<?php

namespace naga;

use Illuminate\Database\Eloquent\Model;

class Profession extends Model
{
    protected $table = 'professions';
	protected $fillable = ['id', 'profession'];
}
