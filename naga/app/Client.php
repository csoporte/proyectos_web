<?php

namespace naga;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $table = 'clients';
	protected $fillable = ['codeTer','nameTer', 'typedocument_id', 'nitTer', 'nitTer1', 'dirTer', 'telTer', 'faxTer', 'city_id', 'ciudad_ter', 'state_id', 'oficioTer','profession', 'business', 'birthday', 'civilStatus_id', 'email', 'tipotercero', 'limit', 'authorization', 'discount', 'activo', 'clasification', 'methodPayment', 'methodPaymentP', 'retefte', 'ica', 'regimen', 'list', 'zone_id', 'zone', 'seller_id', 'addressOffice', 'phoneOffice', 'gender', 'observation1', 'observation2', 'observation3', 'name', 'lastname', 'businessName', 'reference1', 'reference2', 'reference3', 'reference4', 'reference5', 'discountFina', 'termFunding', 'codePais', 'codePais2', 'promp24', 'promp12', 'promp6', 'cheqd24', 'cheqd12', 'cheqd6', 'status', 'cree', 'creevtas', 'cifin','calificacion_despacho','condiciones_despacho','condiciones_pago','contacto_cartera','contacto_compras','auditoria'];
}