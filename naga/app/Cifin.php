<?php

namespace naga;

use Illuminate\Database\Eloquent\Model;

class Cifin extends Model
{
    protected $table = 'cifins';
	protected $fillable = ['id', 'cifin'];
}
