<?php

namespace naga;

use Illuminate\Database\Eloquent\Model;

class measure extends Model
{
   	protected $table = 'measures';
	protected $fillable = ['name','shortname'];
}
