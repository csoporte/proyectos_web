<?php

namespace naga;

use Illuminate\Database\Eloquent\Model;

class Zone extends Model
{
    protected $table = 'zones';
	protected $fillable = ['id', 'zone'];
}
