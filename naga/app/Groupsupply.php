<?php

namespace naga;

use Illuminate\Database\Eloquent\Model;

class Groupsupply extends Model
{
    protected $table = 'groupsupplies';
	protected $fillable = ['group', 'groupName'];
}
