<?php

namespace naga;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderProductionColor extends Model
{
    use softdeletes;

    protected $table = 'order_production_colors';

    protected $fillable = ['id_proporcion','id_proorder','id_referencia','referencia','colores','hilo_ppal','hilo_combinado','hilo_boton','hilo_ojal','user_id'];

    protected $dates = ['deleted_at'];
}