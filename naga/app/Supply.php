<?php

namespace naga;

use Illuminate\Database\Eloquent\Model;

class Supply extends Model
{
	protected $table = 'supplies';
	protected $fillable = ['code', 'description', 'clasification', 'unitMeasurement', 'packagingUnit', 'package',  'provider1', 'provider2', 'provider3', 'price1', 'price2', 'price3', 'deliveryTime1', 'deliveryTime2', 'deliveryTime3', 'optionProvider', 'currentProvider', 'currentPrice', 'currentTime', 'monthlyConsumption', 'dailyConsumption', 'dailyProduction', 'rateProduction', 'rateStyle', 'rateColor', 'rateSize', 'garment1Consumption', 'hourComsumption', 'redStock', 'yellowStock', 'greenStock', 'blueStock', 'red2Stock', 'yellow2Stock', 'green2Stock', 'blue2Stock', 'supplyTipe', 'shirt', 'tShirt', 'Jean', 'bermudaShort', 'classic'];
}
