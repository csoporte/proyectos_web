<?php

namespace naga;

use Illuminate\Database\Eloquent\Model;

class Pricelist extends Model
{
	protected $table = 'pricelists';
	protected $fillable = ['list_id','reference', 'code', 'colection', 'season_id', 'shape', 'producttype_id', 'physicalsample', 'composition','vende','price1', 'price2', 'price3', 'pwarehouse', 'M', 'seller', 'color', 'sizes', 'soldout', 'comments', 'deliverydate', 'ticket','estado','sync','grupo','nro_colores'];
}
