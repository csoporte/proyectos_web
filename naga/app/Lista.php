<?php

namespace naga;

use Illuminate\Database\Eloquent\Model;

class Lista extends Model
{
	protected $table = 'lists';
	protected $fillable = ['listname'];
}
