<?php

namespace naga;

use Illuminate\Database\Eloquent\Model;

class Typedocument extends Model
{
    protected $table = 'typedocuments';
	protected $fillable = ['id', 'typedocument'];
}
