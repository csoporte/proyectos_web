<?php

namespace naga;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class reference extends Model
{
	use softdeletes;

    protected $table = 'references';

    protected $fillable = ['codigo','nombre','almacen','temporada','categoria','tipo','pieza','grupo','subgrupo','silueta','colores','tallas','formaventa','description','muestra','fecharef','composition','costohisto','costoinve','price','precio_tiquete','observation'];

    protected $dates = ['deleted_at'];

}



















