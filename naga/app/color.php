<?php

namespace naga;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class color extends Model
{
    use softdeletes;

    protected $table = 'colores';

    protected $fillable = ['codigo','codigointer','nombre','observaciones'];

    protected $dates = ['deleted_at'];
}
