<?php

namespace naga;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProOrder extends Model
{
    use softdeletes;

    protected $table = 'pro_orders';

    protected $fillable = ['fecha_produccion','semanaplan','gentipo','posicion_pieza','prenda','estilo','categoria','idreferencia','referencia','idreferenciatela','referenciatela','descriptela','preciotela','proveedortela','refproveedor','composiciontela','tallas','color','logoinf','otrobordado','bordado_cual','pretina','bolsillodelantero','bolsillotrasero','observabolsillotrasero','llevatapabolsillo','llevatapabolsillo_cuantas','botoninf','cantbotones','remaches','cantremaches','tiporemache','garrainf','logosup','cogotera','tela_banda_cuello','cinta_faya','entretela','cuello','manga','botonsup','carterader','carteraizq','espalda','botoncont','bolsillos','tapa','diseno_bolsillo','charretera','garrasup','num_ojales','color_ojales','ult_color_ojales','procesoind','procesoind_observacion','insumosespeciales','observaciones','metros','promedio','unidades','color_muestra','28','30','32','34','36S','38M','40L','42XL','44XXL','XXXL','total','id_proporcion_corte','id_colores','elaborado_por','aprobada','revisado'];

    protected $dates = ['deleted_at'];
}