<?php

namespace naga;

use Illuminate\Database\Eloquent\Model;

class Seller extends Model
{
    protected $table = 'sellers';
	protected $fillable = ['code', 'warehouse', 'identification', 'name', 'address', 'phone', 'cellphone', 'email', 'zone_id','rol','status_seller','secreto','iduser'];
}
