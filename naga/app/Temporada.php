<?php

namespace naga;

use Illuminate\Database\Eloquent\Model;

class Temporada extends Model
{
    protected $table = 'temporadas';
	protected $fillable = ['Name', 'Description','PeriodIni','PeriodFin'];
}
