<?php

namespace naga;

use Illuminate\Database\Eloquent\Model;

class Clasification extends Model
{
    protected $table = 'clasifications';
	protected $fillable = ['clasification'];
}
