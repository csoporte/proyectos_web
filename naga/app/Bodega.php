<?php

namespace naga;

use Illuminate\Database\Eloquent\Model;

class Bodega extends Model
{
    protected $table = 'bodegas';
	protected $fillable = ['Codigo', 'Nombre'];
}
