<?php

namespace naga;

use Illuminate\Database\Eloquent\Model;

class Creevtas extends Model
{
    protected $table = 'creevtas';
	protected $fillable = ['id', 'creevtas'];
}
