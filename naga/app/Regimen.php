<?php

namespace naga;

use Illuminate\Database\Eloquent\Model;

class Regimen extends Model
{
    protected $table = 'regimens';
	protected $fillable = ['id', 'regimen'];
}
