<?php

namespace naga;

use Illuminate\Database\Eloquent\Model;

class producttype extends Model
{
    protected $table = 'producttypes';
	protected $fillable = ['name'];
}
