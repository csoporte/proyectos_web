<?php

namespace naga;

use Illuminate\Database\Eloquent\Model;

class FormaPago extends Model
{
    protected $table = 'forma_pagos';
	protected $fillable = ['forma'];
}
