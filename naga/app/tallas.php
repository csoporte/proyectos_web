<?php

namespace naga;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class talla extends Model
{
	use softdeletes;
    
    protected $table = 'tallas';

    protected $fillable = ['sexo','area','talla'];
    //sexo: M,F 	area: Superior, inferior	
    
    protected $dates = ['deleted_at'];
}
