<?php

namespace naga;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrdersDetail extends Model
{
    use softdeletes;

    protected $table = 'orders_details';

    protected $fillable = ['id_order','codpedidomovil','referencia','codcolor','color','codtalla','talla','cantidad','vlrUnitario','vlrTotal','nota','balanceado'];

    protected $dates = ['deleted_at'];
}