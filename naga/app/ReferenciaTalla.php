<?php

namespace naga;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReferenciaTalla extends Model
{
	use softdeletes;

    protected $table = 'referencia_tallas';

    protected $fillable = ['idreferencia','codreferencia','talla_id','talla'];

    protected $dates = ['deleted_at']; 
}
