<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateThereAreUpdatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('there_are_updates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_vendedor')->nullable();
            $table->integer('identification')->nullable();
            $table->integer('cambio_cliente')->nullable();
            $table->integer('cambio_talla')->nullable();
            $table->integer('cambio_vendedores')->nullable();
            $table->integer('cambio_listap')->nullable();
            $table->date('fecha_cambio')->nullable();
            $table->integer('usuario_registra_cambio')->nullable();
            $table->date('fecha_sincronizacion')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('there_are_updates');
    }
}
