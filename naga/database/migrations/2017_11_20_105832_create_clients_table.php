<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codeTer')->nullable();
            $table->string('nameTer')->nullable();
            $table->integer('typedocument_id')->unsigned();
            $table->foreign('typedocument_id')->references('id')->on('typedocuments');
            $table->string('nitTer')->nullable();
            $table->string('nitTer1')->nullable();
            $table->string('dirTer')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('telTer')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('faxTer')->nullable();
            $table->string('city_id')->nullable();
            //$table->foreign('city_id')->references('ID')->on('cities');
            $table->string('ciudad_ter')->nullable();
            $table->integer('state_id')->unsigned();
            $table->foreign('state_id')->references('id')->on('states');
            $table->string('oficioTer')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->integer('profession')->unsigned();
            $table->foreign('profession')->references('id')->on('professions');
            $table->string('business')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->date('birthday')->nullable();
            $table->integer('civilStatus_id')->unsigned();
            $table->foreign('civilStatus_id')->references('id')->on('civilstatuses');
            $table->string('email')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('tipotercero')->nullable();
            $table->integer('limit')->default(0);
            $table->string('authorization')->nullable();
            $table->integer('discount')->default(0);
            $table->String('activo')->nullable();
            $table->String('clasification')->nullable();
            $table->string('methodPayment')->nullable();
            $table->string('methodPaymentP')->nullable();
            $table->integer('retefte')->default(0);
            $table->integer('ica')->default(0);
            $table->string('regimen')->nullable();
            $table->integer('list')->nullable();
            $table->string('zone_id')->nullable();
            $table->string('zone')->nullable();
            $table->string('seller_id')->nullable();
            $table->string('addressOffice')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('phoneOffice')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('gender')->nullable();
            $table->string('observation1')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('observation2')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('observation3')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('name')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('lastname')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('businessName')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('reference1')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('reference2')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('reference3')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('reference4')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('reference5')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->integer('discountFina')->default(0);
            $table->integer('termFunding')->default(0);
            $table->string('codePais')->nullable();
            $table->string('codePais2')->nullable();
            $table->integer('promp24')->default(0);
            $table->integer('promp12')->default(0);
            $table->integer('promp6')->default(0);
            $table->integer('cheqd24')->default(0);
            $table->integer('cheqd12')->default(0);
            $table->integer('cheqd6')->default(0);
            $table->string('status')->nullable();
            $table->integer('cree')->default(0);
            $table->string('creevtas')->nullable();
            $table->string('cifin')->nullable();
            $table->string('calificacion_despacho')->nullable();
            $table->text('condiciones_despacho')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->text('condiciones_pago')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('contacto_cartera')->nullable();
            $table->string('contacto_compras')->nullable();
            $table->string('auditoria')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
