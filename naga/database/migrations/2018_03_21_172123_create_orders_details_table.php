<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_order')->unsigned();
            $table->foreign('id_order')->references('id')->on('orders');
            $table->string('codpedidomovil')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('referencia')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->integer('codcolor')->nullable();
            $table->string('color')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->integer('codtalla')->nullable();
            $table->string('talla')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->integer('cantidad')->nullable();
            $table->integer('vlrUnitario')->nullable();
            $table->integer('vlrTotal')->nullable();
            $table->text('nota')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->integer('balanceado')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders_details');
    }
}
