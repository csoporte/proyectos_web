<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSellersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sellers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('code');
            $table->integer('warehouse')->nullable();
            $table->string('identification')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('name')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('address')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('phone')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('cellphone')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('email')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->integer('zone_id')->nullable();
            $table->integer('rol')->nullable();
            $table->string('status_seller')->nullable();
            $table->string('secreto')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->integer('iduser')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sellers');
    }
}
