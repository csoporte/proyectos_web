<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePricelistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pricelists', function (Blueprint $table) {
            $table->increments('id');
            $table->string('reference')->nullable;
            $table->string('code')->nullable();
            $table->string('colection')->nullable();
            $table->integer('season_id')->nullable();
            $table->string('shape')->nullable();
            $table->string('producttype_id')->nullable();
            $table->string('physicalsample')->nullable();
            $table->string('composition')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->integer('vende')->nullable();
            $table->integer('price1')->nullable();
            $table->integer('price2')->nullable();
            $table->integer('pwarehouse')->nullable();
            $table->integer('price3')->nullable();
            $table->string('M')->nullable();
            $table->string('seller')->nullable();
            $table->string('color')->nullable();
            $table->string('sizes')->nullable();
            $table->string('soldout')->nullable();
            $table->string('comments')->nullable();
            $table->string('deliverydate')->nullable();
            $table->integer('ticket')->nullable();
            $table->integer('estado')->nullable();
            $table->integer('sync')->nullable();
            $table->string('grupo')->nullable();
            $table->integer('nro_colores')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pricelists');
    }
}
