<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCutProportionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proorder_cut_proportions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_proporcion')->nullable();
            $table->integer('id_proorder')->nullable();
            $table->integer('id_referencia')->nullable();
            $table->string('referencia')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('name')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('respuesta')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->float('28')->nullable;
            $table->float('30')->nullable;
            $table->float('32')->nullable;
            $table->float('34')->nullable;
            $table->float('36S')->nullable;
            $table->float('38M')->nullable;
            $table->float('40L')->nullable;
            $table->float('42XL')->nullable;
            $table->float('44XXL')->nullable;
            $table->float('XXXL')->nullable;
            $table->float('total')->nullable;
            $table->string('gentipo')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->integer('user_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cut_proportions');
    }
}
