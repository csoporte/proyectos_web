<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pro_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fecha_produccion')->nullable();
            $table->integer('semanaplan')->nullable();
            $table->string('gentipo')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('posicion_pieza')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('prenda')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('estilo')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('categoria')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->integer('idreferencia')->nullable();
            $table->string('referencia')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->integer('idreferenciatela')->nullable();
            $table->string('referenciatela')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('descriptela')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->integer('preciotela')->nullable();
            $table->string('proveedortela')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('refproveedor')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('composiciontela')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('tallas')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->integer('color')->nullable();
            //parte inferior
            $table->string('logoinf')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('otrobordado')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('bordado_cual',500)->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('pretina')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('bolsillodelantero')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('bolsillotrasero')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('observabolsillotrasero')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('llevatapabolsillo')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->integer('llevatapabolsillo_cuantas')->nullable();
            $table->string('botoninf')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->integer('cantbotones')->nullable();
            $table->string('remaches')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->integer('cantremaches')->nullable();
            $table->string('tiporemache')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('garrainf')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            //parte superior
            $table->string('logosup')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('cogotera')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('tela_banda_cuello')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('cinta_faya')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('entretela')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('cuello')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('manga')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('botonsup')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('carterader')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('carteraizq')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('espalda')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('botoncont')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('bolsillos')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('tapa')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('diseno_bolsillo')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('charretera')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('garrasup')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->integer('num_ojales')->nullable();
            $table->string('color_ojales')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('ult_color_ojales')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            //proceso industrial
            $table->string('procesoind')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('procesoind_observacion',500)->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            //insumos especiales
            $table->text('insumosespeciales')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            //observaciones
            $table->text('observaciones')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            //proporcion corte
            $table->integer('metros')->nullable();
            $table->float('promedio')->nullable();
            $table->integer('unidades')->nullable();
            $table->string('color_muestra')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->float('28')->nullable;
            $table->float('30')->nullable;
            $table->float('32')->nullable;
            $table->float('34')->nullable;
            $table->float('36S')->nullable;
            $table->float('38M')->nullable;
            $table->float('40L')->nullable;
            $table->float('42XL')->nullable;
            $table->float('44XXL')->nullable;
            $table->float('XXXL')->nullable;
            $table->float('total')->nullable;
            $table->integer('id_proporcion_corte')->nullable();
            //Colores
            $table->integer('id_colores')->nullable();
            $table->string('elaborado_por')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('aprobada')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('revisado')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->integer('user_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pro_orders');
    }
}
