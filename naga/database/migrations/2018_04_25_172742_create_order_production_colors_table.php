<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderProductionColorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_production_colors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_proporcion')->nullable();
            $table->integer('id_proorder')->nullable();
            $table->integer('id_referencia')->nullable();
            $table->string('referencia')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('colores')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('hilo_ppal')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('hilo_combinado')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('hilo_boton')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('hilo_ojal')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->integer('user_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_production_colors');
    }
}
