<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prices', function (Blueprint $table) {
            $table->increments('id');//consecutivo incremental
            $table->string('referencia');//referencia de la prenda
            $table->integer('refprice');//valor incremental pero repetible, este es el que va asociado a la referencia en precios.
            $table->string('pricenumber');//el indice del precio ya sea 1,2...,10
            $table->string('price');//el valor del precio
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prices');
    }
}
