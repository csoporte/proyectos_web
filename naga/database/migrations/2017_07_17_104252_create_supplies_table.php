<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuppliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('code');
            $table->string('description');
            $table->integer('clasification');
            $table->integer('unitMeasurement')->unsigned();
            $table->foreign('unitMeasurement')->references('id')->on('measures');
            $table->integer('packagingUnit');
            $table->integer('package'); 
            $table->integer('provider1')->unsigned();
            $table->foreign('provider1')->references('id')->on('providers');
            $table->integer('provider2')->unsigned();
            $table->foreign('provider2')->references('id')->on('providers');
            $table->integer('provider3')->unsigned();
            $table->foreign('provider3')->references('id')->on('providers');
            $table->integer('price1');
            $table->integer('price2');
            $table->integer('price3');
            $table->integer('deliveryTime1');
            $table->integer('deliveryTime2');
            $table->integer('deliveryTime3');
            $table->integer('optionProvider');
            $table->integer('currentProvider')->unsigned();
            $table->foreign('currentProvider')->references('id')->on('providers');
            $table->integer('currentPrice');
            $table->integer('currentTime');
            $table->integer('monthlyConsumption');
            $table->integer('dailyConsumption');   
            $table->integer('dailyProduction');
            $table->float('rateProduction');
            $table->float('rateStyle');
            $table->float('rateColor');
            $table->float('rateSize');
            $table->integer('garment1Consumption');
            $table->float('hourComsumption');            
            $table->integer('redStock');
            $table->integer('yellowStock');
            $table->integer('greenStock');
            $table->integer('blueStock');
            $table->integer('red2Stock');
            $table->integer('yellow2Stock');
            $table->integer('green2Stock');
            $table->integer('blue2Stock');
            $table->integer('supplyType')->unsigned();
            $table->foreign('supplyType')->references('id')->on('production_types')
            $table->boolean('shirt');
            $table->boolean('tShirt');
            $table->boolean('Jean');
            $table->boolean('bermudaShort');
            $table->boolean('classic');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplies');
    }
}
