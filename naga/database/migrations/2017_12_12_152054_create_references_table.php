<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('references', function (Blueprint $table) {
            $table->increments('id');
            /*$table->string('codigo')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('nombre')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('tipo')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('estilo')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('moda')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('silueta')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->text('observaciones')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->integer('tallas')->nullable();
            $table->integer('colores')->nullable();*/
            $table->string('codigo')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('nombre')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->integer('almacen')->nullable();
            $table->integer('temporada')->nullable();
            $table->string('categoria')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('tipo')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('pieza')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('grupo')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('subgrupo')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('silueta')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->integer('colores')->nullable();
            $table->integer('tallas')->nullable();
            $table->string('formaventa')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('description')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('muestra')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->date('fecharef')->nullable();
            $table->string('composition')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->integer('costohisto')->nullable();
            $table->integer('costoinve')->nullable();
            $table->integer('price')->nullable();
            $table->integer('precio_tiquete')->nullable();
            $table->text('observation')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('references');
    }
}
