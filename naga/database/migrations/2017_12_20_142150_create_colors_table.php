<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('colores', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codigo')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('codigointer')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('nombre')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->text('observaciones')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('colores');
    }
}
