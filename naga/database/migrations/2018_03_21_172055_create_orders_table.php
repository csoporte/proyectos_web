<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codcliente')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('codvendedor')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->string('codpedidomovil')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->integer('totalprendas')->nullable();
            $table->integer('costototal')->nullable();
            $table->integer('precio')->nullable();
            $table->integer('numparciales')->nullable();
            $table->date('fechaDespacho')->nullable();
            $table->date('fechaRecibe')->nullable();
            $table->text('notapedido')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->date('fechapedido')->useCurrent()->nullable();
            $table->time('horapedido')->useCurrent()->nullable();
            $table->string('dirPedido')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->integer('codciudad')->nullable();
            $table->string('emailConfirmacion')->charset('utf8')->collation('utf8_unicode_ci')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
