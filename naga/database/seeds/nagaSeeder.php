<?php

use Illuminate\Database\Seeder;

class nagaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(naga\Active::class, 5)->create();
        factory(naga\Cifin::class, 5)->create();
        factory(naga\City::class, 5)->create();
        factory(naga\Civilstatus::class, 5)->create();
        factory(naga\Clasification::class, 5)->create();
        factory(naga\Codepai::class, 5)->create();
        factory(naga\Creevtas::class, 5)->create();
        factory(naga\Gender::class, 5)->create();
        factory(naga\Catalog::class, 5)->create();
        factory(naga\Profession::class, 5)->create();
        factory(naga\Regimen::class, 5)->create();
        factory(naga\Seller::class, 5)->create();
        factory(naga\State::class, 5)->create();
        factory(naga\Typedocument::class, 5)->create();
        factory(naga\Zone::class, 5)->create();
        factory(naga\User::class, 50)->create();
        factory(naga\Client::class, 50)->create();
    }
}
