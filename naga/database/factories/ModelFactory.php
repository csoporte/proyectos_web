<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(naga\Active::class, function (Faker\Generator $faker) {
    return [
        'active' => $faker->randomLetter,
    ];
});

$factory->define(naga\Cifin::class, function (Faker\Generator $faker) {
    return [
        'cifin' => $faker->randomLetter,
    ];
});

$factory->define(naga\City::class, function (Faker\Generator $faker) {
    return [
        'city' => $faker->randomLetter,
    ];
});

$factory->define(naga\Civilstatus::class, function (Faker\Generator $faker) {
    return [
        'civilstatus' => $faker->randomLetter,
    ];
});

$factory->define(naga\Clasification::class, function (Faker\Generator $faker) {
    return [
        'clasification' => $faker->randomLetter,
    ];
});

$factory->define(naga\Codepai::class, function (Faker\Generator $faker) {
    return [
        'codePai' => $faker->randomLetter,
    ];
});

$factory->define(naga\Creevtas::class, function (Faker\Generator $faker) {
    return [
        'creevtas' => $faker->randomLetter,
    ];
});

$factory->define(naga\Gender::class, function (Faker\Generator $faker) {
    return [
        'gender' => $faker->randomLetter,
    ];
});

$factory->define(naga\Catalog::class, function (Faker\Generator $faker) {
    return [
        'catalog' => $faker->randomLetter,
    ];
});

$factory->define(naga\Profession::class, function (Faker\Generator $faker) {
    return [
        'profession' => $faker->randomLetter,
    ];
});

$factory->define(naga\Regimen::class, function (Faker\Generator $faker) {
    return [
        'regimen' => $faker->randomLetter,
    ];
});

$factory->define(naga\Seller::class, function (Faker\Generator $faker) {
    return [
        'seller' => $faker->name,
    ];
});

$factory->define(naga\State::class, function (Faker\Generator $faker) {
    return [
        'state' => $faker->randomLetter,
    ];
});

$factory->define(naga\Typedocument::class, function (Faker\Generator $faker) {
    return [
        'typedocument' => $faker->randomLetter,
    ];
});

$factory->define(naga\Zone::class, function (Faker\Generator $faker) {
    return [
        'zone' => $faker->randomLetter,
    ];
});

$factory->define(naga\User::class, function (Faker\Generator $faker) {

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' =>  Hash::make('12345678'),
        'user' => $faker->userName,
        'typeuser_id' => $faker->numberBetween($min = 1, $max = 2),
        'identification' => $faker->numberBetween($min = 1080000000, $max = 1099999999),
        'remember_token' => str_random(10),
    ];
});

$factory->define(naga\Client::class, function (Faker\Generator $faker) {

    return [
        'nameTer' => $faker->company,
        'typedocument_id' => $faker->numberBetween($min = 1, $max = 5),
        'nitTer' => $faker->numberBetween($min = 100000, $max = 999999),
        'nitTer1' => $faker->numberBetween($min = 100000, $max = 999999),
        'dirTer' => $faker->address,
        'telTer' => $faker->phoneNumber,
        'faxTer' => $faker->phoneNumber,
        'city_id' => $faker->numberBetween($min = 1, $max = 5),
        'cityTer' => $faker->city,
        'state_id' => $faker->numberBetween($min = 1, $max = 5),
        'oficioTer' => $faker->jobTitle,
        'profession_id' => $faker->numberBetween($min = 1, $max = 5),
        'business' => $faker->company,
        'birthday' => $faker->date,
        'civilStatus_id' => $faker->numberBetween($min = 1, $max = 5),
        'email' => $faker->email,
        'typeTer' => $faker->numberBetween($min = 1, $max = 5),
        'limit' => $faker->numberBetween($min = 100000, $max = 100000000),
        'authorization' => $faker->boolean,
        'discount' => $faker->numberBetween($min = 0, $max = 100),
        'active_id' => $faker->numberBetween($min = 1, $max = 5),
        'clasification_id' => $faker->numberBetween($min = 1, $max = 5),
        'methodPayment' => $faker->numberBetween($min = 0, $max = 30),
        'methodPaymentP' => $faker->numberBetween($min = 0, $max = 200),
        'retefte' => $faker->numberBetween($min = 0, $max = 10),
        'ica' => $faker->numberBetween($min = 0, $max = 10),
        'regimen_id' => $faker->numberBetween($min = 1, $max = 5),
        'catalog_id' => $faker->numberBetween($min = 1, $max = 5),
        'zone_id' => $faker->numberBetween($min = 1, $max = 5),
        'zone' => $faker->name,
        'seller_id' => $faker->numberBetween($min = 1, $max = 5),
        'addressOffice' => $faker->address,
        'phoneOffice' => $faker->phoneNumber,
        'gender_id' => $faker->numberBetween($min = 1, $max = 5),
        'observation1' => $faker->sentence($nbWords = 10, $variableNbWords = true),
        'observation2' => $faker->sentence($nbWords = 10, $variableNbWords = true),
        'observation3' => $faker->sentence($nbWords = 10, $variableNbWords = true),
        'name' => $faker->name,
        'lastname' => $faker->lastname,
        'businessName' => $faker->company,
        'reference1' => $faker->numberBetween($min = 10000, $max = 99999),
        'reference2' => $faker->numberBetween($min = 10000, $max = 99999),
        'reference3' => $faker->numberBetween($min = 10000, $max = 99999),
        'reference4' => $faker->numberBetween($min = 10000, $max = 99999),
        'reference5' => $faker->numberBetween($min = 10000, $max = 99999),
        'discountFina' => $faker->numberBetween($min = 1, $max = 5),
        'termFunding' => $faker->numberBetween($min = 0, $max = 90),
        'codePai_id' => $faker->numberBetween($min = 1, $max = 5),
        'codePai2_id' => $faker->numberBetween($min = 1, $max = 5),
        'promp24' => $faker->numberBetween($min = 0, $max = 1000),
        'promp12' => $faker->numberBetween($min = 0, $max = 1000),
        'promp6' => $faker->numberBetween($min = 0, $max = 1000),
        'cheqd24' => $faker->numberBetween($min = 0, $max = 1000),
        'cheqd12' => $faker->numberBetween($min = 0, $max = 1000),
        'cheqd6' => $faker->numberBetween($min = 0, $max = 1000),
        'status' => $faker->numberBetween($min = 0, $max = 4),
        'cree' => $faker->numberBetween($min = 0, $max = 10),
        'creevtas_id' => $faker->numberBetween($min = 1, $max = 5),
        'cifin_id' => $faker->numberBetween($min = 1, $max = 5),
     ];
});


