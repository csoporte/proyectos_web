{!!Html::script('js/jquery.dataTables.min.js')!!}
@include('alerts.AlertsRequest')
@include('alerts.SuccessRequest')
@include('alerts.ErrorsRequest')
<aside class="col-md-12">
   <div id="cambio">
      <table class="display" id="tablaclientes">
         <thead>
            <th>Id</th>
            <th>Codigo</th>
            <th>Nombre</th>
            <th>Nit</th>
            <th>Dirección</th>
            <th>Telefóno</th>
            <th>Ciudad</th>
            <th>Acción</th>
         </thead>
      </table>
   </div>
</aside>
<script type="text/javascript">
   $(document).ready(function() {
      oTable = $('#tablaclientes').DataTable({
         "processing": true,
         "serverSide": true,
         "ajax": "{{ route('datatable.tablaclientes') }}",
         "columns": [
            {data: 'id', name: 'id'},
            {data: 'codeTer', name: 'codeTer'},
            {data: 'nameTer', name: 'nameTer'},
            {data: 'nitTer', name: 'nitTer'},
            {data: 'dirTer', name: 'dirTer'},
            {data: 'telTer', name: 'telTer'},
            {data: 'ciudad_ter', name: 'ciudad_ter'},
            {defaultContent: "<a class='btn btn-success'>Editar</a>"}
         ],
         "language" : {
            processing:     "Por favor espere...",
            search:         "Búsqueda:",
            lengthMenu:     "Mostrar _MENU_ registros",
            info:           "Mostrando del _START_ al _END_ de _TOTAL_ registros",
            infoFiltered:   "(Filtrado de _MAX_ registros)",
            infoPostFix:    "",
            loadingRecords: "Cargando...",
            paginate: {
               first: "Primera",
               previous: "Previo",
               next: "Siguiente",
               last: "&uacute;ltimo"
            }
         }
      });
      $('#tablaclientes tbody').on( 'click', 'a', function () {
         content = typeof content !== 'undefined' ? content : 'content';
         var datos = oTable.row( $(this).parents('tr') ).data();
         //window.location.href = "/Clients/"+datos['id']+"/edit";
         filename = "/Clients/"+datos['id']+"/edit";
         $.ajax({
            type: "get",
            url: filename,
            contentType: false,
            success: function (data) {
               $("#content").html(data);
            },
            error: function (xhr, status, error) {
               alert(xhr.responseText);
            }
         });
      });
      /*$('#tablaclientes tbody').on( 'click', 'a', function () {
         var datos = oTable.row( $(this).parents('tr') ).data();
         window.location.href = "/Clients/"+datos['id']+"/edit";
      });*/

      /*function ajaxLoad(filename, content)
      {
         content = typeof content !== 'undefined' ? content : 'content';
         $('.loading').show();
         $.ajax({
            type: "get",
            url: filename,
            contentType: false,
            success: function (data) {
               $("#" + content).html(data);
               $('.loading').hide();
            },
            error: function (xhr, status, error) {
               alert(xhr.responseText);
            }
         });
      }*/

   });

</script>