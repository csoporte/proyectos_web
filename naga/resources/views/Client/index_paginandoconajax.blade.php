@include('alerts.AlertsRequest')
@include('alerts.SuccessRequest')
@include('alerts.ErrorsRequest')
<aside class="col-md-12">
   <div id="cambio">
      <table class="display">
         <thead>
            <th>Nombre</th>
            <th>Nit</th>
            <th>Digito de verificación</th>
            <th>Dirección</th>
            <th>Telefóno</th>
            <th>Departamento</th>
            <th>Ciudad</th>
            <th>Profesión</th>
            <th>Empresa</th>
            <th>Birthday</th>
            <th>Estado Civil</th>
            <th>Email</th>
            <th>Lista</th>
            <th>Vendedor</th>
            <th>Dirección Oficina</th>
            <th>Teléfono Oficina</th>
            <th>Género</th>
            <th>Operación</th>
         </thead>
         @foreach($clients as $client)
            <tbody>
               <td>{{$client->nameTer}}</td>
               <td>{{$client->nitTer}}</td>
               <td>{{$client->nitTer1}}</td>
               <td>{{$client->dirTer}}</td>
               <td>{{$client->telTer}}</td>
               <td>{{$client->state_id}}</td>
               <td>{{$client->city_id}}</td>
               <td>{{$client->profession_id}}</td>
               <td>{{$client->business}}</td>
               <td>{{$client->birthday}}</td>
               <td>{{$client->civilStatus_id}}</td>
               <td>{{$client->email}}</td>
               <td>{{$client->catalog_id}}</td>
               <td>{{$client->seller_id}}</td>
               <td>{{$client->addressOffice}}</td>
               <td>{{$client->phoneOffice}}</td>
               <td>{{$client->gender_id}}</td>
               <td>
                  <a class="btn btn-primary" href="javascript:ajaxLoad('{{url('Clients/'.$client->id.'/edit')}}')">Editar</a>
               </td>
            </tbody>
         @endforeach       
      </table>
      {!!$clients->render()!!}
   </div>
      <!--Se agrega con doble signo de admiración --> 
</aside>