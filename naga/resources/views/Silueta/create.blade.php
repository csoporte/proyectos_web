@include('alerts.AlertsRequest')
@include('alerts.SuccessRequest')
@include('alerts.ErrorsRequest')
{!!Form::open(['route'=>'Silueta.store', 'id'=>'frm', 'method'=>'POST', 'autocomplete'=>'off'])!!}
	<div class="col-md-12 col-sm-12">
		<input id="_token" type="hidden" name="_token" value="{{ csrf_token() }}">
		@include('Forms.Silueta')
	</div>
{!!Form::close()!!}