<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <link href="../../../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
        
        <link href="../../../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
        
        <link href="../../../vendors/nprogress/nprogress.css" rel="stylesheet" />
        
        <link href="../../../vendors/iCheck/skins/flat/green.css" rel="stylesheet" />
        
        <link href="../../../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" />
        
        <link href="../../../vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet" />
        
        <link href="../../../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />
        
        <link href="../../../build/css/custom.min.css" rel="stylesheet" />
        
        <link href="../../../css/bootstrap-datetimepicker.min.css" rel="stylesheet" />  
        
        <link href="../../../css/dataTables.bootstrap.min.css" rel="stylesheet" />
        
        <link href="../../../css/datatables.bootstrap.css" rel="stylesheet" />
        
        <link href="../../../assets/css/paper-bootstrap-wizard.css" rel="stylesheet" />
        <link href="http://netdna.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.css" rel="stylesheet">
        <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
        <link href="../../../assets/css/themify-icons.css" rel="stylesheet">
        <style type="text/css">
            body {font-family: Arial, Helvetica, sans-serif;}

            #matrizpedido table {     font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
            font-size: 12px;    margin: 45px;     width: 480px; text-align: left;    border-collapse: collapse; }

            #matrizpedido th {     font-size: 13px;     font-weight: normal;     padding: 8px;     background: #b9c9fe;
            border-top: 4px solid #aabcfe;    border-bottom: 1px solid #fff; color: #039; }

            #matrizpedido td {    padding: 8px;     background: #e8edff;     border-bottom: 1px solid #fff;
            color: #669;    border-top: 1px solid transparent; }

            #matrizpedido tr:hover td { background: #d0dafd; color: #339; }
        </style>
    </head>

    <body class="nav-md">
        <div class="container body">
            <div class="main_container">
                <div id="content">
                    <!--<table border="0" cellspacing="0" cellpadding="0">
                        <thead>
                          <tr>
                            <th class="no">#</th>
                            <th class="desc">DESCRIPTION</th>
                            <th class="unit">UNIT PRICE</th>
                            <th class="total">TOTAL</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td class="no">{ 1 }}</td>
                            <td class="desc">{ 2 }}</td>
                            <td class="unit">{ 3 }}</td>
                            <td class="total">{ 4 }} </td>
                          </tr>
                 
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="2"></td>
                                <td >TOTAL</td>
                                <td>$6,500.00</td>
                            </tr>
                        </tfoot>
                    </table>-->
                    <div id="infopedido">
                        <table>
                            <tbody>
                                <tr>
                                    <th>
                                        {!!Form::label('Nro Orden: ')!!}
                                    </th>
                                    <th>
                                        {!!Form::label('Código pedido móvil: ')!!}    
                                    </th>
                                    <th>
                                        {!!Form::label('Total prendas pedido: ')!!}    
                                    </th>
                                    <th>
                                        {!!Form::label('Fecha despacho: ')!!}    
                                    </th>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        {!!Form::label($orden->id)!!}    
                                    </td>
                                    <td>
                                        {!!Form::label($orden->codpedidomovil)!!}    
                                    </td>
                                    <td class="text-center">
                                        {!!Form::label($orden->totalprendas)!!}
                                    </td>
                                    <td>
                                        {!!Form::label($orden->fechaDespacho)!!}    
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {!!Form::label('Dirección: ')!!}
                                    </th>
                                    <th>
                                        {!!Form::label('Nit Cliente: ')!!}
                                    </th>
                                    <th>
                                        {!!Form::label('Nombre cliente: ')!!}
                                    </th>
                                    <th>
                                        {!!Form::label('calificación: ')!!}
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        {!!Form::label($orden->dirPedido)!!}
                                    </td>
                                    <td>
                                        {!!Form::label($cliente->nitTer)!!}
                                    </td>
                                    <td>
                                        {!!Form::label($cliente->nameTer)!!}
                                    </td>
                                    <td>
                                        {!!Form::label($cliente->clasification)!!}
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4"></td>
                                </tr>
                                <tr>
                                    <th colspan="4">
                                        {!!Form::label('Nota pedido: ')!!}
                                    </th>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        {!!Form::label($orden->notapedido)!!}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div id="matrizpedido">
                        <table style="width: 100%">
                            <tr>
                                <th>&nbsp;</th>
                                <th>28</th>
                                <th>30</th>
                                <th>32</th>
                                <th>XS</th>
                                <th>S</th>
                                <th>M</th>
                                <th>L</th>
                                <th>XL</th>
                                <th>2XL</th>
                                <th>3XL</th>
                            </tr>
                            <tr>
                                <th>COLOR</th>
                                <th>6</th>
                                <th>8</th>
                                <th>10</th>
                                <th>34</th>
                                <th>36</th>
                                <th>38</th>
                                <th>40</th>
                                <th>42</th>
                                <th>44</th>
                                <th>46</th>
                            </tr>
                            <tr>
                                <th>&nbsp;</th>
                                <th>-</th>
                                <th>-</th>
                                <th>-</th>
                                <th>12</th>
                                <th>14</th>
                                <th>16</th>
                                <th>18</th>
                                <th>20</th>
                                <th>22</th>
                                <th>24</th>
                            </tr>
                            @for($i=0;$i<$numcolores;$i++)
                                <tr>
                                    <td>{{$i+1}}</td>
                                    @for($j = 0;$j < 10;$j++)
                                        @if($tempo[$i][$j]==null)
                                            <td>--</td>
                                        @else
                                            <td>
                                                {!!Form::label($tempo[$i][$j])!!}
                                            </td>
                                        @endif
                                    @endfor
                                </tr>
                            @endfor
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>