{!!Html::script('js/jquery.dataTables.min.js')!!}
@include('alerts.AlertsRequest')
@include('alerts.SuccessRequest')
@include('alerts.ErrorsRequest')
<aside class="col-md-12 col-sm-12">
   <div id="cambio">
      <table class="display" id="tablapedidoscrudos">
         <thead>
            <th>Id</th>
            <th>clientId</th>
            <th>NroOrdenMóvil</th>
            <th>Referencia</th>
            <th>Clasificación</th>
            <th>Cantidad</th>
            <th>Vendedor</th>
         </thead>
      </table>
   </div>
</aside>
<script type="text/javascript">
   $(document).ready(function() {
      oTable = $('#tablapedidoscrudos').DataTable({
         "processing": true,
         "serverSide": true,
         "ajax": "{{ route('datatable.tablapedidoscrudos') }}",
         "columns": [
            {data: 'id', name: 'id'},
            {data: 'clienteid', name: 'clienteid'},
            {data: 'nroOrden', name: 'nroOrden'},
            {data: 'referenciaId', name: 'referenciaId'},
            {data: 'clasificacion_producto', name: 'clasificacion_producto'},
            {data: 'cantidadItems', name: 'cantidadItems'},
            {data: 'vendedorId', name: 'vendedorId'},
            //{defaultContent: "<a class='btn btn-success'>Editar</a>"}
         ],
         "language" : {
            processing:     "Por favor espere...",
            search:         "Búsqueda:",
            lengthMenu:     "Mostrar _MENU_ registros",
            info:           "Mostrando del _START_ al _END_ de _TOTAL_ registros",
            infoFiltered:   "(Filtrado de _MAX_ registros)",
            infoPostFix:    "",
            loadingRecords: "Cargando...",
            paginate: {
               first: "Primera",
               previous: "Previo",
               next: "Siguiente",
               last: "&uacute;ltimo"
            }
         }
      });
      $('#tablapedidoscrudos tbody').on( 'click', 'a', function () {
         content = typeof content !== 'undefined' ? content : 'content';
         var datos = oTable.row( $(this).parents('tr') ).data();
         //window.location.href = "/Clients/"+datos['id']+"/edit";
         filename = "/Referencia/"+datos['id']+"/edit";
         $.ajax({
            type: "get",
            url: filename,
            contentType: false,
            success: function (data) {
               $("#content").html(data);
            },
            error: function (xhr, status, error) {
               alert(xhr.responseText);
            }
         });
      });
   });

</script>