<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        {!!Html::style('vendors/bootstrap/dist/css/bootstrap.min.css')!!}

        {!!Html::style('vendors/font-awesome/css/font-awesome.min.css')!!}

        {!!Html::style('vendors/nprogress/nprogress.css')!!}

        {!!Html::style('vendors/iCheck/skins/flat/green.css')!!}

        {!!Html::style('vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css')!!}

        {!!Html::style('vendors/jqvmap/dist/jqvmap.min.css')!!}

        {!!Html::style('vendors/bootstrap-daterangepicker/daterangepicker.css')!!}

        {!!Html::style('build/css/custom.min.css')!!}

        {!!Html::style('css/bootstrap-datetimepicker.min.css')!!}
         
        {!!Html::script('vendors/jquery/dist/jquery.min.js')!!}

        {!!Html::script('js/moment.min.js')!!} 

        {!!Html::script('js/bootstrap-datetimepicker.min.js')!!}  

        {!!Html::style('css/dataTables.bootstrap.min.css')!!}

        {!!Html::style('css/datatables.bootstrap.css')!!}

        <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png" />
        <link rel="icon" type="image/png" href="assets/img/favicon.png" />
        <link href="assets/css/paper-bootstrap-wizard.css" rel="stylesheet" />
        <link href="http://netdna.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.css" rel="stylesheet">
        <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
        <link href="assets/css/themify-icons.css" rel="stylesheet">
    </head>

    <body class="nav-md">
        <div class="container body">
            <div class="main_container">
                <div id="content">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                {!!Form::label('Nro Orden: ')!!}
                                {!!Form::text('id',$orden->id, ['id'=>'id','class'=>'form-control','readonly'=>true])!!}
                            </div>    
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                            </div>    
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                {!!Form::label('Código pedido móvil: ')!!}
                                {!!Form::text('codpedidomovil',$orden->codpedidomovil,['id'=>'codpedidomovil','class'=>'form-control','readonly'=>true])!!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                {!!Form::label('Total prendas pedido: ')!!}
                                {!!Form::text('totalprendas',$orden->totalprendas, ['id'=>'totalprendas','class'=>'form-control','readonly'=>true])!!}
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                {!!Form::label('Fecha despacho: ')!!}
                                {!!Form::text('fechaDespacho',$orden->fechaDespacho, ['id'=>'fechaDespacho','class'=>'form-control','readonly'=>true])!!}
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                {!!Form::label('Dirección: ')!!}
                                {!!Form::text('dirPedido',$orden->dirPedido, ['id'=>'dirPedido','class'=>'form-control','readonly'=>true])!!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                {!!Form::label('Nit Cliente: ')!!}
                                {!!Form::text('nit',$cliente->nitTer, ['id'=>'nit','class'=>'form-control','readonly'=>true])!!}
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                {!!Form::label('Nombre cliente: ')!!}
                                {!!Form::text('nomcliente',$cliente->nameTer, ['id'=>'nomcliente','class'=>'form-control','readonly'=>true])!!}
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group has-error">
                                {!!Form::label('calificación: ')!!}
                                {!!Form::text('score',$cliente->clasification, ['id'=>'score','class'=>'form-control','readonly'=>true])!!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                {!!Form::label('Nota pedido: ')!!}
                                {!!Form::textarea('notapedido',$orden->notapedido, ['id'=>'notapedido','class'=>'form-control','readonly'=>true])!!}
                            </div>
                        </div>
                    </div>
                    <!--div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                {!Form::label('Referencias: ')!!}
                                {!Form::select('referencia',$referencias,null,['id'=>'referencia','class'=>'form-control', 'placeholder'=>'Seleccione uno...'])!!}
                            </div>
                        </div>
                    </div-->
                </div>
            </div>
        </div>
    </body>
</html>

<html>
<head>
  <style>
    body{
      font-family: sans-serif;
    }
    @page {
      margin: 160px 50px;
    }
    header { position: fixed;
      left: 0px;
      top: -160px;
      right: 0px;
      height: 100px;
      background-color: #ddd;
      text-align: center;
    }
    header h1{
      margin: 10px 0;
    }
    header h2{
      margin: 0 0 10px 0;
    }
    footer {
      position: fixed;
      left: 0px;
      bottom: -50px;
      right: 0px;
      height: 40px;
      border-bottom: 2px solid #ddd;
    }
    footer .page:after {
      content: counter(page);
    }
    footer table {
      width: 100%;
    }
    footer p {
      text-align: right;
    }
    footer .izq {
      text-align: left;
    }
  </style>
<body>
  <header>
    <h1>Cabecera de mi documento</h1>
    <h2>DesarrolloWeb.com</h2>
  </header>
  <footer>
    <table>
      <tr>
        <td>
            <p class="izq">
              Desarrolloweb.com
            </p>
        </td>
        <td>
          <p class="page">
            Página
          </p>
        </td>
      </tr>
    </table>
  </footer>
  <div id="content">
    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                {!!Form::label('Nro Orden: ')!!}
                                {!!Form::text('id',$orden->id, ['id'=>'id','class'=>'form-control','readonly'=>true])!!}
                            </div>    
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                            </div>    
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                {!!Form::label('Código pedido móvil: ')!!}
                                {!!Form::text('codpedidomovil',$orden->codpedidomovil,['id'=>'codpedidomovil','class'=>'form-control','readonly'=>true])!!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                {!!Form::label('Total prendas pedido: ')!!}
                                {!!Form::text('totalprendas',$orden->totalprendas, ['id'=>'totalprendas','class'=>'form-control','readonly'=>true])!!}
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                {!!Form::label('Fecha despacho: ')!!}
                                {!!Form::text('fechaDespacho',$orden->fechaDespacho, ['id'=>'fechaDespacho','class'=>'form-control','readonly'=>true])!!}
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                {!!Form::label('Dirección: ')!!}
                                {!!Form::text('dirPedido',$orden->dirPedido, ['id'=>'dirPedido','class'=>'form-control','readonly'=>true])!!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                {!!Form::label('Nit Cliente: ')!!}
                                {!!Form::text('nit',$cliente->nitTer, ['id'=>'nit','class'=>'form-control','readonly'=>true])!!}
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                {!!Form::label('Nombre cliente: ')!!}
                                {!!Form::text('nomcliente',$cliente->nameTer, ['id'=>'nomcliente','class'=>'form-control','readonly'=>true])!!}
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group has-error">
                                {!!Form::label('calificación: ')!!}
                                {!!Form::text('score',$cliente->clasification, ['id'=>'score','class'=>'form-control','readonly'=>true])!!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                {!!Form::label('Nota pedido: ')!!}
                                {!!Form::textarea('notapedido',$orden->notapedido, ['id'=>'notapedido','class'=>'form-control','readonly'=>true])!!}
                            </div>
                        </div>
                    </div>
                    <!--div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                {!Form::label('Referencias: ')!!}
                                {!Form::select('referencia',$referencias,null,['id'=>'referencia','class'=>'form-control', 'placeholder'=>'Seleccione uno...'])!!}
                            </div>
                        </div>
                    </div-->
  </div>
</body>
</html>





<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <link href="../../../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
        
        <link href="../../../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
        
        <link href="../../../vendors/nprogress/nprogress.css" rel="stylesheet" />
        
        <link href="../../../vendors/iCheck/skins/flat/green.css" rel="stylesheet" />
        
        <link href="../../../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" />
        
        <link href="../../../vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet" />
        
        <link href="../../../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />
        
        <link href="../../../build/css/custom.min.css" rel="stylesheet" />
        
        <link href="../../../css/bootstrap-datetimepicker.min.css" rel="stylesheet" />  
        
        <link href="../../../css/dataTables.bootstrap.min.css" rel="stylesheet" />
        
        <link href="../../../css/datatables.bootstrap.css" rel="stylesheet" />
        
        <link href="../../../assets/css/paper-bootstrap-wizard.css" rel="stylesheet" />
        <link href="http://netdna.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.css" rel="stylesheet">
        <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
        <link href="../../../assets/css/themify-icons.css" rel="stylesheet">
        <style type="text/css">
            body {font-family: Arial, Helvetica, sans-serif;}

            #matrizpedido table {     font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
            font-size: 12px;    margin: 45px;     width: 480px; text-align: left;    border-collapse: collapse; }

            #matrizpedido th {     font-size: 13px;     font-weight: normal;     padding: 8px;     background: #b9c9fe;
            border-top: 4px solid #aabcfe;    border-bottom: 1px solid #fff; color: #039; }

            #matrizpedido td {    padding: 8px;     background: #e8edff;     border-bottom: 1px solid #fff;
            color: #669;    border-top: 1px solid transparent; }

            #matrizpedido tr:hover td { background: #d0dafd; color: #339; }
        </style>
    </head>

    <body class="nav-md">
        <div class="container body">
            <div class="main_container">
                <div id="content">
                    <!--<table border="0" cellspacing="0" cellpadding="0">
                        <thead>
                          <tr>
                            <th class="no">#</th>
                            <th class="desc">DESCRIPTION</th>
                            <th class="unit">UNIT PRICE</th>
                            <th class="total">TOTAL</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td class="no">{ 1 }}</td>
                            <td class="desc">{ 2 }}</td>
                            <td class="unit">{ 3 }}</td>
                            <td class="total">{ 4 }} </td>
                          </tr>
                 
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="2"></td>
                                <td >TOTAL</td>
                                <td>$6,500.00</td>
                            </tr>
                        </tfoot>
                    </table>-->
                    <div id="infopedido">
                        <table>
                            <tbody>
                                <tr>
                                    <th>
                                        {!!Form::label('Nro Orden: ')!!}
                                    </th>
                                    <th>
                                        {!!Form::label('Código pedido móvil: ')!!}    
                                    </th>
                                    <th>
                                        {!!Form::label('Total prendas pedido: ')!!}    
                                    </th>
                                    <th>
                                        {!!Form::label('Fecha despacho: ')!!}    
                                    </th>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        {!!Form::label($orden->id)!!}    
                                    </td>
                                    <td>
                                        {!!Form::label($orden->codpedidomovil)!!}    
                                    </td>
                                    <td class="text-center">
                                        {!!Form::label($orden->totalprendas)!!}
                                    </td>
                                    <td>
                                        {!!Form::label($orden->fechaDespacho)!!}    
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {!!Form::label('Dirección: ')!!}
                                    </th>
                                    <th>
                                        {!!Form::label('Nit Cliente: ')!!}
                                    </th>
                                    <th>
                                        {!!Form::label('Nombre cliente: ')!!}
                                    </th>
                                    <th>
                                        {!!Form::label('calificación: ')!!}
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        {!!Form::label($orden->dirPedido)!!}
                                    </td>
                                    <td>
                                        {!!Form::label($cliente->nitTer)!!}
                                    </td>
                                    <td>
                                        {!!Form::label($cliente->nameTer)!!}
                                    </td>
                                    <td>
                                        {!!Form::label($cliente->clasification)!!}
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4"></td>
                                </tr>
                                <tr>
                                    <th colspan="4">
                                        {!!Form::label('Nota pedido: ')!!}
                                    </th>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        {!!Form::label($orden->notapedido)!!}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div id="matrizpedido">
                        <table style="width: 100%">
                            <tr>
                                <th>COLOR</th>
                                <th>28</th>
                                <th>30</th>
                                <th>32</th>
                                <th>XS</th>
                                <th>S</th>
                                <th>M</th>
                                <th>L</th>
                                <th>XL</th>
                                <th>2XL</th>
                                <th>3XL</th>
                            </tr>
                            <tr>
                                <th>&nbsp;</th>
                                <th>6</th>
                                <th>8</th>
                                <th>10</th>
                                <th>34</th>
                                <th>36</th>
                                <th>38</th>
                                <th>40</th>
                                <th>42</th>
                                <th>44</th>
                                <th>46</th>
                            </tr>
                            <tr>
                                <th>&nbsp;</th>
                                <th>-</th>
                                <th>-</th>
                                <th>-</th>
                                <th>12</th>
                                <th>14</th>
                                <th>16</th>
                                <th>18</th>
                                <th>20</th>
                                <th>22</th>
                                <th>24</th>
                            </tr>
                            @for($i=0;$i<$numcolores;$i++)
                                <tr>
                                    <td>{{$i+1}}</td>
                                    @for($j = 0;$j < 10;$j++)
                                        @if($tempo[$i][$j]==null)
                                            <td>--</td>
                                        @else
                                            <td>
                                                {!!Form::label($tempo[$i][$j])!!}
                                            </td>
                                        @endif
                                    @endfor
                                </tr>
                            @endfor
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>