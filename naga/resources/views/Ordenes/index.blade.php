{!!Html::script('js/jquery.dataTables.min.js')!!}
@include('alerts.AlertsRequest')
@include('alerts.SuccessRequest')
@include('alerts.ErrorsRequest')
<aside class="col-md-12 col-sm-12">
   <div id="cambio">
      <table class="display" id="tablaordenes">
         <thead>
            <th>Num. Orden</th>
            <th>Cliente</th>
            <th>Cantidad prendas</th>
            <th>Dirección envío</th>
            <th>Fecha despacho</th>
            <th>Nota orden</th>
            <th>Vendedor</th>
            <th>Ver Orden</th>
         </thead>
      </table>
   </div>
</aside>
<script type="text/javascript">
   $(document).ready(function() {
      oTable = $('#tablaordenes').DataTable({
         "processing": true,
         "serverSide": true,
         "ajax": "{{ route('datatable.tablaordenes') }}",
         "columns": [
            {data: 'id', name: 'id'},
            {data: 'nameTer', name: 'nameTer'},
            {data: 'totalprendas', name: 'totalprendas'},
            {data: 'dirPedido', name: 'dirPedido'},
            {data: 'fechaDespacho', name: 'fechaDespacho'},
            {data: 'notapedido', name: 'notapedido'},
            {data: 'name', name: 'name'},
            {defaultContent: "<a class='btn btn-success'>Ver</a>"}
         ],
         "language" : {
            processing:     "Por favor espere...",
            search:         "Búsqueda:",
            lengthMenu:     "Mostrar _MENU_ registros",
            info:           "Mostrando del _START_ al _END_ de _TOTAL_ registros",
            infoFiltered:   "(Filtrado de _MAX_ registros)",
            infoPostFix:    "",
            loadingRecords: "Cargando...",
            paginate: {
               first: "Primera",
               previous: "Previo",
               next: "Siguiente",
               last: "&uacute;ltimo"
            }
         }
      });
      $('#tablaordenes tbody').on( 'click', 'a', function () {
         content = typeof content !== 'undefined' ? content : 'content';
         var datos = oTable.row( $(this).parents('tr') ).data();
         //window.location.href = "/Clients/"+datos['id']+"/edit";
         filename = "/Pedidos/"+datos['id']+"/edit";
         $.ajax({
            type: "get",
            url: filename,
            contentType: false,
            success: function (data) {
               //console.log(filename);
               $("#content").html(data);
            },
            error: function (xhr, status, error) {
               alert(xhr.responseText);
            }
         });
      });
   });

</script>