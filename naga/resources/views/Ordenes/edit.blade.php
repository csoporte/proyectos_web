@include('alerts.AlertsRequest')
@include('alerts.SuccessRequest')
@include('alerts.ErrorsRequest')
<!--{!Form::model($orden, ['route'=>['Pedidos.actualizarorderdetails',$orden->id], 'id'=>'frm', 'method'=>'GET', 'autocomplete'=>'off'])!!}-->
{!!Form::open(['route'=>'Pedidos.actualizarorderdetails', 'id'=>'frm', 'method'=>'POST', 'autocomplete'=>'off'])!!}
	<div class="col-md-12 col-sm-12">
		<input id="_token" type="hidden" name="_token" value="{{ csrf_token() }}">
		@include('Forms.ordenes.cabecera')
		
		@include('Forms.ordenes.matriz')
	</div>
	<div id="impresion" class="col-md-12 text-center" style="display:none;">
		{!!Form::button('imprimir',['id'=>'imprimir', 'class'=>'btn btn-primary text-center'])!!}
	</div>
{!!Form::close()!!}
<div class="col-md-12">
<br>
</div>