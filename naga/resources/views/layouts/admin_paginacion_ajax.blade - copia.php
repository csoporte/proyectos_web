<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ModalDia S.A.S | </title>

     <!-- Bootstrap -->
    {!!Html::style('vendors/bootstrap/dist/css/bootstrap.min.css')!!}
    <!-- Font Awesome -->
    {!!Html::style('vendors/font-awesome/css/font-awesome.min.css')!!}
    <!-- NProgress -->
     {!!Html::style('vendors/nprogress/nprogress.css')!!}
    <!-- iCheck -->
     {!!Html::style('vendors/iCheck/skins/flat/green.css')!!}
    <!-- bootstrap-progressbar -->
     {!!Html::style('vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css')!!}
    <!-- JQVMap -->
     {!!Html::style('vendors/jqvmap/dist/jqvmap.min.css')!!}
    <!-- bootstrap-daterangepicker -->
     {!!Html::style('vendors/bootstrap-daterangepicker/daterangepicker.css')!!}
    <!-- Custom Theme Style -->
     {!!Html::style('build/css/custom.min.css')!!}
     <!-- jQuery -->
    {!!Html::style('css/bootstrap-datetimepicker.min.css')!!}
     
     {!!Html::script('vendors/jquery/dist/jquery.min.js')!!}

      {!!Html::script('js/moment.min.js')!!} 

      {!!Html::script('js/bootstrap-datetimepicker.min.js')!!}  
     

  <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png" />
  <link rel="icon" type="image/png" href="assets/img/favicon.png" />


  <!-- CSS Files -->
  <link href="assets/css/paper-bootstrap-wizard.css" rel="stylesheet" />

  <!-- Fonts and Icons -->
  <link href="http://netdna.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.css" rel="stylesheet">
  <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
  <link href="assets/css/themify-icons.css" rel="stylesheet">

  </head>
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 10;">
              <a href="https://www.naga.com.co">
                <img class="img-circle" border="0" src="/images/naga.png" width="60" height="60">
                <span class="naga">NAGA</span>
              </a>
            </div>
            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile">
              <!--<div class="profile_pic">
                <img src="../images/metallica.jpg" alt="..." class="img-circle profile_img">
              </div>-->
              <!--<div class="profile_info">
                <span>Bienvenido,</span>
                <h2>Ing. Daniel Becerra</h2>
              </div>-->
            </div>
            <!-- /menu profile quick info -->
            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>ADMINISTRACIÓN</h3>                
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-tripadvisor" aria-hidden="true"></i> Parámetros<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      @if(Auth::user()->typeuser_id == 1)
                      <li><a><i class="fa fa-user" aria-hidden="true"></i> Clientes <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                          <li><a href="javascript:ajaxLoad('{{url('Clients/create')}}')">Agregar</a></li>
                          <li><a href="javascript:ajaxLoad('{{url('Clients')}}')">Listado de clientes</a></li>
                          <li><a href="javascript:ajaxLoad('{{url('Clients/prueba')}}')">Listado de clientes de la DBF</a></li>
                          <li><a href="javascript:ajaxLoad('{{url('Clients/sincronizar')}}')">Sincronizar Clientes</a></li>
                        </ul>
                      </li>
                      <li><a><i class="fa fa-users" aria-hidden="true"></i> Usuarios <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                          <li><a href="javascript:ajaxLoad('{{url('Users/create')}}')">Agregar</a></li>
                          <li><a href="javascript:ajaxLoad('{{url('Users')}}')">Listado de usuarios</a></li>
                        </ul>
                      </li>
                      @endif 
                      <li><a><i class="fa fa-bandcamp" aria-hidden="true"></i> Grupo de Insumos <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                          <li><a href="javascript:ajaxLoad('{{url('Groupsupplies/create')}}')">Agregar</a></li>
                          <li><a href="javascript:ajaxLoad('{{url('Groupsupplies')}}')">Listado de grupos de insumos</a></li>
                        </ul>
                      </li>
                      <li><a><i class="fa fa-bandcamp" aria-hidden="true"></i> Subgrupo de Insumos <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                          <li><a href="javascript:ajaxLoad('{{url('SubGroupsupplies/create')}}')">Agregar</a></li>
                          <li><a href="javascript:ajaxLoad('{{url('SubGroupsupplies')}}')">Listado de grupos de insumos</a></li>
                        </ul>
                      </li>
                      <li><a><i class="fa fa-archive" aria-hidden="true"></i> Insumos <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                          <li><a href="javascript:ajaxLoad('{{url('Supplies/create')}}')">Agregar</a></li>
                          <li><a href="javascript:ajaxLoad('{{url('Supplies')}}')">Listado de insumos</a></li>
                        </ul>
                      </li>
                      <li><a><i class="fa fa-money" aria-hidden="true"></i> Precios <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                          <li><a href="javascript:ajaxLoad('{{url('Pricelists/create')}}')">Agregar</a></li>
                          <li><a href="javascript:ajaxLoad('{{url('Pricelists')}}')">Listado de precios</a></li>
                        </ul>
                      </li>
                      <li><a><i class="fa fa-road" aria-hidden="true"></i> Proveedores <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                          <li><a href="javascript:ajaxLoad('{{url('Providers/create')}}')">Agregar</a></li>
                          <li><a href="javascript:ajaxLoad('{{url('Providers')}}')">Listado de proveedores</a></li>
                        </ul>
                      </li> 
                      <li><a><i class="fa fa-suitcase" aria-hidden="true"></i> Vendedores <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                          <li><a href="javascript:ajaxLoad('{{url('Sellers/create')}}')">Agregar</a></li>
                          <li><a href="javascript:ajaxLoad('{{url('Sellers')}}')">Listado de vendedores</a></li>
                        </ul>
                      </li>          
                      <li><a href="/Products">Listado de productos</a></li>
                      <li><a href="/Products">Códigos NAGA</a></li>
                      <li><a href="/Products">Referencias</a></li>
                      <li><a href="/Products">Costos Indirectos</a></li>
                      <li><a href="/Products">Ubicación en Bodega</a></li>
                      <li><a href="/Products">Transportadora</a></li>
                    </ul>
                  </li>
                </ul>
                @if(Auth::user()->typeuser_id == 1 || Auth::user()->typeuser_id == 2)
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-check-square-o" aria-hidden="true"></i> Val. de insumos<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a><i class="fa fa-user" aria-hidden="true"></i> Clientes <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                          <li><a href="javascript:ajaxLoad('{{url('Clients/create')}}')">Agregar</a></li>
                          <li><a href="javascript:ajaxLoad('{{url('Clients')}}')">Listado de clientes</a></li>
                        </ul>
                      </li>
                    </ul>
                  </li>
                </ul>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Orden de pedido<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a><i class="fa fa-user" aria-hidden="true"></i> Clientes <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                          <li><a href="javascript:ajaxLoad('{{url('Clients/create')}}')">Agregar</a></li>
                          <li><a href="javascript:ajaxLoad('{{url('Clients')}}')">Listado de clientes</a></li>
                        </ul>
                      </li>
                    </ul>
                  </li>
                </ul>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-handshake-o" aria-hidden="true"></i> Auditoría<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a><i class="fa fa-user" aria-hidden="true"></i> Clientes <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                          <li><a href="javascript:ajaxLoad('{{url('Clients/create')}}')">Agregar</a></li>
                          <li><a href="javascript:ajaxLoad('{{url('Clients')}}')">Listado de clientes</a></li>
                        </ul>
                      </li>
                    </ul>
                  </li>
                </ul>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-pencil-square" aria-hidden="true"></i> Pedido<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a><i class="fa fa-user" aria-hidden="true"></i> Clientes <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                          <li><a href="javascript:ajaxLoad('{{url('Clients/create')}}')">Agregar</a></li>
                          <li><a href="javascript:ajaxLoad('{{url('Clients')}}')">Listado de clientes</a></li>
                        </ul>
                      </li>
                    </ul>
                  </li>
                </ul>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-building-o" aria-hidden="true"></i> Almacén<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a><i class="fa fa-user" aria-hidden="true"></i> Clientes <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                          <li><a href="javascript:ajaxLoad('{{url('Clients/create')}}')">Agregar</a></li>
                          <li><a href="javascript:ajaxLoad('{{url('Clients')}}')">Listado de clientes</a></li>
                        </ul>
                      </li>
                    </ul>
                  </li>
                </ul>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-times" aria-hidden="true"></i> Reclamos<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a><i class="fa fa-user" aria-hidden="true"></i> Clientes <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                          <li><a href="javascript:ajaxLoad('{{url('Clients/create')}}')">Agregar</a></li>
                          <li><a href="javascript:ajaxLoad('{{url('Clients')}}')">Listado de clientes</a></li>
                        </ul>
                      </li>
                    </ul>
                  </li>
                </ul>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-shirtsinbulk" aria-hidden="true"></i> Insumos<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a><i class="fa fa-user" aria-hidden="true"></i> Clientes <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                          <li><a href="javascript:ajaxLoad('{{url('Clients/create')}}')">Agregar</a></li>
                          <li><a href="javascript:ajaxLoad('{{url('Clients')}}')">Listado de clientes</a></li>
                        </ul>
                      </li>
                    </ul>
                  </li>
                </ul>
                @endif
              </div>
            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>
              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="/images/naga.jpg" alt="">{!!Auth::user()->name!!}
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="javascript:;"> Profile</a></li>
                    <li>
                      <a href="javascript:;">
                        <span class="badge bg-red pull-right">50%</span>
                        <span>Settings</span>
                      </a>
                    </li>
                    <li><a href="javascript:;">Help</a></li>
                    <li><a href="{!!URL::to('/logout')!!}""><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>
                <!--<li role="presentation" class="dropdown">
                  <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-envelope-o"></i>
                    <span class="badge bg-green">6</span>
                  </a>
                  <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                    <li>
                      <a>
                        <span class="image"><img src="/images/naga.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class="image"><img src="/images/naga.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class="image"><img src="/images/naga.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class="image"><img src="/images/naga.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <div class="text-center">
                        <a>
                          <strong>See All Alerts</strong>
                          <i class="fa fa-angle-right"></i>
                        </a>
                      </div>
                    </li>
                  </ul>
                </li>-->
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <!-- top tiles -->
          <div id="content">
          
          </div>
          <!-- /top tiles @yield('content')-->
        
        </div>
        <!-- /page content -->
        
        <!-- footer content -->
        <footer>
          <div class="pull-right">
            M3Medios - Desarrolladores de software <a href="https://www.m3medios.com">M3Medios</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- Bootstrap -->
    {!!Html::script('vendors/bootstrap/dist/js/bootstrap.min.js')!!}
    <!-- FastClick -->
    {!!Html::script('vendors/fastclick/lib/fastclick.js')!!}
    <!-- NProgress -->
    {!!Html::script('vendors/nprogress/nprogress.js')!!}
    <!-- Chart.js -->
    {!!Html::script('vendors/Chart.js/dist/Chart.min.js')!!}
    <!-- gauge.js -->
    {!!Html::script('vendors/gauge.js/dist/gauge.min.js')!!}
    <!-- bootstrap-progressbar -->
    {!!Html::script('vendors/bootstrap-progressbar/bootstrap-progressbar.min.js')!!}
    <!-- iCheck -->
    {!!Html::script('vendors/iCheck/icheck.min.js')!!}
    <!-- Skycons -->
    {!!Html::script('vendors/skycons/skycons.js')!!}
    <!-- Flot -->
    {!!Html::script('vendors/Flot/jquery.flot.js')!!}
    {!!Html::script('vendors/Flot/jquery.flot.pie.js')!!}
    {!!Html::script('vendors/Flot/jquery.flot.time.js')!!}
    {!!Html::script('vendors/Flot/jquery.flot.stack.js')!!}
    {!!Html::script('vendors/Flot/jquery.flot.resize.js')!!}
    <!-- Flot plugins -->
    {!!Html::script('vendors/flot.orderbars/js/jquery.flot.orderBars.js')!!}
    {!!Html::script('vendors/flot-spline/js/jquery.flot.spline.min.js')!!}
    {!!Html::script('vendors/flot.curvedlines/curvedLines.js')!!}
    <!-- DateJS -->
    {!!Html::script('vendors/DateJS/build/date.js')!!}
    <!-- JQVMap -->
    {!!Html::script('vendors/jqvmap/dist/jquery.vmap.js')!!}
    {!!Html::script('vendors/jqvmap/dist/maps/jquery.vmap.world.js')!!}
    {!!Html::script('vendors/jqvmap/examples/js/jquery.vmap.sampledata.js')!!}
    <!-- bootstrap-daterangepicker -->
    {!!Html::script('vendors/moment/min/moment.min.js')!!}
    {!!Html::script('vendors/bootstrap-daterangepicker/daterangepicker.js')!!}
    <!-- Custom Theme Scripts -->
    {!!Html::script('build/js/custom.min.js')!!} 

    {!!Html::script('vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js')!!}

    {!!Html::script('js/script3.js')!!}  
     
<script>
    function ajaxLoad(filename, content)
  {
    content = typeof content !== 'undefined' ? content : 'content';
    $('.loading').show();
    $.ajax({
        type: "get",
        url: filename,
        contentType: false,
        success: function (data) {
            $("#" + content).html(data);
            $('.loading').hide();
        },
        error: function (xhr, status, error) {
            alert(xhr.responseText);
        }
    });
  }
  </script>
  </body>
</html>
