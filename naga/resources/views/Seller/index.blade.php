{!!Html::script('js/jquery.dataTables.min.js')!!}
@include('alerts.AlertsRequest')
@include('alerts.SuccessRequest')
@include('alerts.ErrorsRequest')
<aside class="col-md-12">
	<table class="table" id="tablavendedores">
		<thead>
			<th>Id</th>
			<th>Código</th>
			<th>Identificación</th>
			<th>Nombres</th>
			<th>Dirección</th>
			<th>Teléfono</th>
			<th>Celular</th>
			<th>Correo</th>
			<th>Operación</th>
		</thead>
	</table>
</aside>
<script type="text/javascript">
   $(document).ready(function() {
      oTable = $('#tablavendedores').DataTable({
         "processing": true,
         "serverSide": true,
         "ajax": "{{ route('datatable.tablavendedores') }}",
         "columns": [
         	{data: 'id', name: 'id'},
            {data: 'code', name: 'code'},
            {data: 'identification', name: 'identification'},
            {data: 'name', name: 'name'},
            {data: 'address', name: 'address'},
            {data: 'phone', name: 'phone'},
            {data: 'cellphone', name: 'cellphone'},
            {data: 'email', name: 'email'},
            {defaultContent: "<a class='btn btn-success'>Editar</a>"}
         ],
         "language" : {
            processing:     "Por favor espere...",
            search:         "Búsqueda:",
            lengthMenu:     "Mostrar _MENU_ registros",
            info:           "Mostrando del _START_ al _END_ de _TOTAL_ registros",
            infoFiltered:   "(Filtrado de _MAX_ registros)",
            infoPostFix:    "",
            loadingRecords: "Cargando...",
            paginate: {
               first: "Primera",
               previous: "Previo",
               next: "Siguiente",
               last: "&uacute;ltimo"
            }
         }
      });
      $('#tablavendedores tbody').on( 'click', 'a', function () {
         content = typeof content !== 'undefined' ? content : 'content';
         var datos = oTable.row( $(this).parents('tr') ).data();
         //window.location.href = "/Clients/"+datos['id']+"/edit";
         filename = "/Sellers/"+datos['id']+"/edit";
         $.ajax({
            type: "get",
            url: filename,
            contentType: false,
            success: function (data) {
               $("#content").html(data);
            },
            error: function (xhr, status, error) {
               alert(xhr.responseText);
            }
         });
      });
   });

</script>