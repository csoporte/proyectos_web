@include('alerts.AlertsRequest')
@include('alerts.SuccessRequest')
@include('alerts.ErrorsRequest')
{!!Form::open(['route'=>'Sellers.store', 'id'=>'frm', 'method'=>'POST', 'autocomplete'=>'off'])!!}

    <div class="row">
        <div class="col-sm-12">
            <div class="wizard-container">
				<div class="card wizard-card" data-color="orange" id="wizardProfile">
					<div class="col-md-12 col-sm-12">
						<input id="_token" type="hidden" name="_token" value="{{ csrf_token() }}">
						@include('Forms.Seller')
					</div>
					<!--<div class="col-md-4">		
						<div class="col-sm-12 col-sm-offset-1">
							<div class="picture-container">
								<div class="picture">
									<img src="assets/img/default-avatar.jpg" class="picture-src" id="wizardPicturePreview" title="" />
									<input type="file" id="wizard-picture">
								</div>
								<h6>Seleccione una foto</h6>
							</div>
						</div>
					</div>-->
				</div>
			</div>
		</div>
	</div>

{!!Form::close()!!}