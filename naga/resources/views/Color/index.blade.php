{!!Html::script('js/jquery.dataTables.min.js')!!}
@include('alerts.AlertsRequest')
@include('alerts.SuccessRequest')
@include('alerts.ErrorsRequest')
<aside class="col-md-12">
   <div id="cambio">
      <table class="display" id="tablacolores">
         <thead>
            <th>Id</th>
            <th>Código</th>
            <th>Código Internacional</th>
            <th>Nombre</th>
            <th>Observaciones</th>
            <th>Operación</th>
         </thead>
      </table>
   </div>
</aside>
<script type="text/javascript">
   $(document).ready(function() {
      oTable = $('#tablacolores').DataTable({
         "processing": true,
         "serverSide": true,
         "ajax": "{{ route('datatable.tablacolores') }}",
         "columns": [
            {data: 'id', name: 'id'},
            {data: 'codigo', name: 'codigo'},
            {data: 'codigointer', name: 'codigointer'},
            {data: 'nombre', name: 'nombre'},
            {data: 'observaciones', name: 'observaciones'},
            {defaultContent: "<a class='btn btn-success'>Editar</a>"}
         ],
         "language" : {
            processing:     "Por favor espere...",
            search:         "Búsqueda:",
            lengthMenu:     "Mostrar _MENU_ registros",
            info:           "Mostrando del _START_ al _END_ de _TOTAL_ registros",
            infoFiltered:   "(Filtrado de _MAX_ registros)",
            infoPostFix:    "",
            loadingRecords: "Cargando...",
            paginate: {
               first: "Primera",
               previous: "Previo",
               next: "Siguiente",
               last: "&uacute;ltimo"
            }
         }
      });
   });
</script>
