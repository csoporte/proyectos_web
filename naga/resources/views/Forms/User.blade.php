<br>
<div class="form-group">
{!!Form::label('Nombres y Apellidos: ')!!}
{!!Form::text('name',null, ['class'=>'form-control', 'placeholder'=>'Nombres y apellidos'])!!}
</div>
<div class="form-group">
{!!Form::label('Correo: ')!!}
{!!Form::text('email',null, ['class'=>'form-control', 'placeholder'=>'Correo electrónico'])!!}
</div>
<div class="form-group">
{!!Form::label('Usuario: ')!!}
{!!Form::text('user',null, ['class'=>'form-control', 'placeholder'=>'Usuario'])!!}
</div>
<div class="form-group">
{!!Form::label('Tipo de Usuario: ')!!}
{!!Form::text('typeuser_id',null, ['class'=>'form-control', 'placeholder'=>'Tipó de Usuario'])!!}
</div>
<div class="form-group">
{!!form::label('Contrase&ntilde;a: ')!!}
{!!form::password('password', ['class'=>'form-control', 'placeholder'=>'Ingresa la contrase&ntilde;a'])!!}
</div>
<div class="form-group">
{!!Form::label('Identificación: ')!!}
{!!Form::text('identification',null, ['class'=>'form-control', 'placeholder'=>'Identificación'])!!}
</div>
{!!Form::submit('Guardar',['id'=>'Guardar', 'class'=>'btn btn-primary'])!!}
<script type="text/javascript">
$("#frm").submit(function (event) {
    event.preventDefault();
    $('.loading').show();
    var form = $(this);
    var data = new FormData($(this)[0]);
    var url = form.attr("action");
    $.ajax({
        type: "POST",
        url: url,
        data: data,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
            if (data.fail) {
                $('#frm input.required, #frm textarea.required').each(function () {
                    index = $(this).attr('name');
                    if (index in data.errors) {
                        $("#form-" + index + "-error").addClass("has-error");
                        $("#" + index + "-error").html(data.errors[index]);
                    }
                    else {
                        $("#form-" + index + "-error").removeClass("has-error");
                        $("#" + index + "-error").empty();
                    }
                });
                $('#focus').focus().select();
            } else {
                $(".has-error").removeClass("has-error");
                $(".help-block").empty();
                $('.loading').hide();
                ajaxLoad(data.url, data.content);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            alert(errorThrown);
        }
    });
    return false;
});
</script>