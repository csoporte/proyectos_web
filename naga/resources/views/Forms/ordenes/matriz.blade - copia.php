{!!Html::script('vendors/jquery/dist/jquery.min.js')!!}
<div id="matriz">
    <!--<div class="row">
        <div class="col-sm-2 text-center">
            <div class="row">
                <h5></h5>
            </div>
            <div class="row">
                <h3>Color</h3>
            </div>
            <div class="row">
                <h5></h5>
            </div>
        </div>
        <div class="col-sm-1 text-center">
            <div class="row">
                <h5>28</h5>
            </div>
            <div class="row">
                <h5>6</h5>
            </div>
            <div class="row">
                <h5>-</h5>
            </div>
        </div>
        <div class="col-sm-1 text-center">
            <div class="row">
                <h5>30</h5>
            </div>
            <div class="row">
                <h5>8</h5>
            </div>
            <div class="row">
                <h5>-</h5>
            </div>
        </div>
        <div class="col-sm-1 text-center">
            <div class="row">
                <h5>32</h5>
            </div>
            <div class="row">
                <h5>10</h5>
            </div>
            <div class="row">
                <h5>-</h5>
            </div>
        </div>
        <div class="col-sm-1 text-center">
            <div class="row">
                <h5>XS</h5>
            </div>
            <div class="row">
                <h5>34</h5>
            </div>
            <div class="row">
                <h5>12</h5>
            </div>
        </div>
        <div class="col-sm-1 text-center">
            <div class="row">
                <h5>S</h5>
            </div>
            <div class="row">
                <h5>36</h5>
            </div>
            <div class="row">
                <h5>14</h5>
            </div>
        </div>
        <div class="col-sm-1 text-center">
            <div class="row">
                <h5>M</h5>
            </div>
            <div class="row">
                <h5>38</h5>
            </div>
            <div class="row">
                <h5>16</h5>
            </div>
        </div>
        <div class="col-sm-1 text-center">
            <div class="row">
                <h5>L</h5>
            </div>
            <div class="row">
                <h5>40</h5>
            </div>
            <div class="row">
                <h5>18</h5>
            </div>
        </div>
        <div class="col-sm-1 text-center">
            <div class="row">
                <h5>XL</h5>
            </div>
            <div class="row">
                <h5>42</h5>
            </div>
            <div class="row">
                <h5>20</h5>
            </div>
        </div>
        <div class="col-sm-1 text-center">
            <div class="row">
                <h5>2XL</h5>
            </div>
            <div class="row">
                <h5>44</h5>
            </div>
            <div class="row">
                <h5>22</h5>
            </div>
        </div>
        <div class="col-sm-1 text-center">
            <div class="row">
                <h5>3XL</h5>
            </div>
            <div class="row">
                <h5>46</h5>
            </div>
            <div class="row">
                <h5>24</h5>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-2 text-center">
            <div class="row">
                {!Form::label('1: ')!!}
            </div>
        </div>
        <div class="col-sm-1 text-center">
            <div class="row">
                {!Form::text('composition',null, ['class'=>'form-control', 'placeholder'=>'Composición'])!!}
            </div>
        </div>
        <div class="col-sm-1 text-center">
            <div class="row">
                {!Form::text('composition',null, ['class'=>'form-control', 'placeholder'=>'Composición'])!!}
            </div>
        </div>
        <div class="col-sm-1 text-center">
            <div class="row">
                {!Form::text('composition',null, ['class'=>'form-control', 'placeholder'=>'Composición'])!!}
            </div>
        </div>
        <div class="col-sm-1 text-center">
            <div class="row">
                {!Form::text('composition',null, ['class'=>'form-control', 'placeholder'=>'Composición'])!!}
            </div>
        </div>
        <div class="col-sm-1 text-center">
            <div class="row">
                {!Form::text('composition',null, ['class'=>'form-control', 'placeholder'=>'Composición'])!!}
            </div>
        </div>
        <div class="col-sm-1 text-center">
            <div class="row">
                {!Form::text('composition',null, ['class'=>'form-control', 'placeholder'=>'Composición'])!!}
            </div>
        </div>
        <div class="col-sm-1 text-center">
            <div class="row">
                {!Form::text('composition',null, ['class'=>'form-control', 'placeholder'=>'Composición'])!!}
            </div>
        </div>
        <div class="col-sm-1 text-center">
            <div class="row">
                {!Form::text('composition',null, ['class'=>'form-control', 'placeholder'=>'Composición'])!!}
            </div>
        </div>
        <div class="col-sm-1 text-center">
            <div class="row">
                {!Form::text('composition',null, ['class'=>'form-control', 'placeholder'=>'Composición'])!!}
            </div>
        </div>
        <div class="col-sm-1 text-center">
            <div class="row">
                {!Form::text('composition',null, ['class'=>'form-control', 'placeholder'=>'Composición'])!!}
            </div>
        </div>
    </div>-->
</div>

<script type="text/javascript">
    $('#referencia').on('change',function(e){
        var ref = e.target.value;
        var orden = $('#id').val();
        var orden_movil = $('#codpedidomovil').val();
        
        $.get('/llenarmatriz?ref='+ref+'&orden='+orden+'&codpedidomovil='+orden_movil, function(data){

            var auxiliar=data['datosmatriz'];
            var cantidadxref = data['cantidadxref'];
            
            $("#matriz").empty();

            $("#matriz").append('<div class="row"><div class="col-sm-6"><div class="form-group"><label>Cantidad prendas de la referencia:</label><input type="text" name="cantidadprendasxref" id="cantidadprendasxref" value="'+cantidadxref+'" class="form-control" readonly="readonly"></div></div><div class="col-sm-6"><div class="form-group"><label>Cantidad balanceada</label><input type="text" name="cantidadbalanceada" id="cantidadbalanceada" value="0" class="form-control" readonly="readonly"></div></div></div><hr>');
            
            $('#matriz').append('<div class="row"><div class="col-sm-2 text-center"><div class="row"><h5></h5></div><div class="row"><h3>Color</h3></div><div class="row"><h5></h5></div></div><div class="col-sm-1 text-center"><div class="row"><h5>28</h5></div><div class="row"><h5>6</h5></div><div class="row"><h5>-</h5></div></div><div class="col-sm-1 text-center"><div class="row"><h5>30</h5></div><div class="row"><h5>8</h5></div><div class="row"><h5>-</h5></div></div><div class="col-sm-1 text-center"><div class="row"><h5>32</h5></div><div class="row"><h5>10</h5></div><div class="row"><h5>-</h5></div></div><div class="col-sm-1 text-center"><div class="row"><h5>XS</h5></div><div class="row"><h5>34</h5></div><div class="row"><h5>12</h5></div></div><div class="col-sm-1 text-center"><div class="row"><h5>S</h5></div><div class="row"><h5>36</h5></div><div class="row"><h5>14</h5></div></div><div class="col-sm-1 text-center"><div class="row"><h5>M</h5></div><div class="row"><h5>38</h5></div><div class="row"><h5>16</h5></div></div><div class="col-sm-1 text-center"><div class="row"><h5>L</h5></div><div class="row"><h5>40</h5></div><div class="row"><h5>18</h5></div></div><div class="col-sm-1 text-center"><div class="row"><h5>XL</h5></div><div class="row"><h5>42</h5></div><div class="row"><h5>20</h5></div></div><div class="col-sm-1 text-center"><div class="row"><h5>2XL</h5></div><div class="row"><h5>44</h5></div><div class="row"><h5>22</h5></div></div><div class="col-sm-1 text-center"><div class="row"><h5>3XL</h5></div><div class="row"><h5>46</h5></div><div class="row"><h5>24</h5></div></div></div>');

            for (var i = 0; i < data['numcolores']; i++) 
            {
                $('#matriz').append('<div class="row"><div class="col-sm-2 text-center"><div class="row"><label>'+(i+1)+'</label></div></div><div class="col-sm-1 text-center"><div class="row"><input type="text" value="'+auxiliar[i][0]+'" class="form-control" name="cantidad_'+i+'_0" id="cantidad_'+i+'_0" placeholder="--"></div></div><div class="col-sm-1 text-center"><div class="row"><input type="text" value="'+auxiliar[i][1]+'" class="form-control" name="cantidad_'+i+'_1" id="cantidad_'+i+'_1" placeholder="--"></div></div><div class="col-sm-1 text-center"><div class="row"><input type="text" value="'+auxiliar[i][2]+'" class="form-control" name="cantidad_'+i+'_2" id="cantidad_'+i+'_2" placeholder="--"></div></div><div class="col-sm-1 text-center"><div class="row"><input type="text" value="'+auxiliar[i][3]+'"class="form-control" name="cantidad_'+i+'_3" id="cantidad_'+i+'_3" placeholder="--"></div></div><div class="col-sm-1 text-center"><div class="row"><input type="text" value="'+auxiliar[i][4]+'" class="form-control" name="cantidad_'+i+'_4" id="cantidad_'+i+'_4" placeholder="--"></div></div><div class="col-sm-1 text-center"><div class="row"><input type="text" value="'+auxiliar[i][5]+'" class="form-control" name="cantidad_'+i+'_5" id="cantidad_'+i+'_5" placeholder="--"></div></div><div class="col-sm-1 text-center"><div class="row"><input type="text" value="'+auxiliar[i][6]+'" class="form-control" name="cantidad_'+i+'_6" id="cantidad_'+i+'_6" placeholder="--"></div></div><div class="col-sm-1 text-center"><div class="row"><input type="text" value="'+auxiliar[i][7]+'" class="form-control" name="cantidad_'+i+'_7" id="cantidad_'+i+'_7" placeholder="--"></div></div><div class="col-sm-1 text-center"><div class="row"><input type="text" value="'+auxiliar[i][8]+'" class="form-control" name="cantidad_'+i+'_8" id="cantidad_'+i+'_8" placeholder="--"></div></div><div class="col-sm-1 text-center"><div class="row"><input type="text" value="'+auxiliar[i][9]+'" class="form-control" name="cantidad_'+i+'_9" id="cantidad_'+i+'_9" placeholder="--"></div></div></div>');
                if(auxiliar[i][0]==null)
                   $('#cantidad_'+i+'_0').val('');
                if(auxiliar[i][1]==null)
                   $('#cantidad_'+i+'_1').val('');
                if(auxiliar[i][2]==null)
                   $('#cantidad_'+i+'_2').val('');
                if(auxiliar[i][3]==null)
                   $('#cantidad_'+i+'_3').val('');
                if(auxiliar[i][4]==null)
                   $('#cantidad_'+i+'_4').val('');
                if(auxiliar[i][5]==null)
                   $('#cantidad_'+i+'_5').val('');
                if(auxiliar[i][6]==null)
                   $('#cantidad_'+i+'_6').val('');
                if(auxiliar[i][7]==null)
                   $('#cantidad_'+i+'_7').val('');
                if(auxiliar[i][8]==null)
                   $('#cantidad_'+i+'_8').val('');
                if(auxiliar[i][9]==null)
                   $('#cantidad_'+i+'_9').val('');
            }

            $('#matriz').append('<div class="row"><div class="col-sm-12"><div class="row"><label>Nota referencia</label></div></div></div><div class="row"><div class="col-sm-12"><div class="row"><textarea class="form-control" id="notaref" name="notaref">'+data['notarefer']+'</textarea></div></div></div><hr>');


            $('#matriz').append('<div class="row"><div class="col-sm-12 text-center"><input type="submit" value="Guardar" class="btn btn-primary"></div></div><br>');
        });
    });
</script>
<script type="text/javascript">
    $("#frm").submit(function (event) {
        event.preventDefault();
        $('.loading').show();
        var form = $(this);
        var aux = $(this)[0];
        var data = new FormData($(this)[0]);
        var url = form.attr("action");
        //console.log(data);
        //console.log(url);
        id= $('#id').val();
        filename = "/Pedidos/actualizarorderdetails/"+id;
        //filename = "/Pedidos/"+datos['id']+"/edit";
        //console.log("este es el id->"+id);
        $.ajax({
            type: "get",
            //url: url,
            url: filename,
            data: data,
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                //console.log(url);
                $("#content").html(data);
            },
            error: function (xhr, textStatus, errorThrown) {
                console.log("este es un error: "+error);
                alert(errorThrown);
            }
        });
        //return false;
    });
</script>