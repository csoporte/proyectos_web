<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            {!!Form::label('Nro Orden: ')!!}
            {!!Form::text('id',$orden->id, ['id'=>'id','class'=>'form-control','readonly'=>true])!!}
        </div>    
    </div>
    <div class="col-sm-4">
        <div class="form-group">
        </div>    
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {!!Form::label('Código pedido móvil: ')!!}
            {!!Form::text('codpedidomovil',$orden->codpedidomovil,['id'=>'codpedidomovil','class'=>'form-control','readonly'=>true])!!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            {!!Form::label('Total prendas pedido: ')!!}
            {!!Form::text('totalprendas',$orden->totalprendas, ['id'=>'totalprendas','class'=>'form-control','readonly'=>true])!!}
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {!!Form::label('Fecha despacho: ')!!}
            {!!Form::text('fechaDespacho',$orden->fechaDespacho, ['id'=>'fechaDespacho','class'=>'form-control','readonly'=>true])!!}
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {!!Form::label('Dirección: ')!!}
            {!!Form::text('dirPedido',$orden->dirPedido, ['id'=>'dirPedido','class'=>'form-control','readonly'=>true])!!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            {!!Form::label('Nit Cliente: ')!!}
            {!!Form::text('nit',$cliente->nitTer, ['id'=>'nit','class'=>'form-control','readonly'=>true])!!}
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {!!Form::label('Nombre cliente: ')!!}
            {!!Form::text('nomcliente',$cliente->nameTer, ['id'=>'nomcliente','class'=>'form-control','readonly'=>true])!!}
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group has-error">
            {!!Form::label('calificación: ')!!}
            {!!Form::text('score',$cliente->clasification, ['id'=>'score','class'=>'form-control','readonly'=>true])!!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            {!!Form::label('Nota pedido: ')!!}
            {!!Form::textarea('notapedido',$orden->notapedido, ['id'=>'notapedido','class'=>'form-control','readonly'=>true])!!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            {!!Form::label('Referencias: ')!!}
            {!!Form::select('referencia',$referencias,null,['id'=>'referencia','class'=>'form-control', 'placeholder'=>'Seleccione uno...'])!!}
        </div>
    </div>
</div>
<!--<div class="row"><div class="col-sm-8"><div class="form-group"><label>Cantidad prendas de la referencia:</label><input type="text" name="cantidadprendasxref" id="cantidadprendasxref" class="form-control" readonly="readonly"></div></div><div class="col-sm-8"><div class="form-group"><label>Cantidad balanceada</label><input type="text" name="cantidadbalanceada" id="cantidadbalanceada" class="form-control" readonly="readonly"></div></div></div>-->