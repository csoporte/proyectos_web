<?php
    $indice=1;                        
?>
<div class="tab-content">
    <div class="tab-pane" id="op1">
    	<div class="row">
        	<div class="col-sm-12">
            	<h5 class="info-text"></h5>
        	</div>
            <div class="col-sm-3">
                {!!Form::label('Fecha Producción: ')!!}
                <div class='col-sm-12 input-group date' id='fecha_produccion' style="margin-bottom: 0px;">
                    {!!Form::text('fecha_produccion', null, ['id'=>'fecha_produccion', 'class'=>'form-control', 'placeholder'=>'YYYY-MM-DD'])!!}
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
                    {!!Form::label('Orden de Producción: ')!!}
                    {!!Form::number('id',$idproorder, ['id'=>'id', 'class'=>'form-control', 'placeholder'=>'0'])!!}
                </div>
            </div>
            
            <div class="col-sm-3">
                <div class='form-group'>
                    {!!Form::label('Semana Planeación: ')!!}
                    {!!Form::number('semanaplan',null, ['id'=>'semanaplan', 'class'=>'form-control', 'placeholder'=>'0'])!!}
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    {!!Form::label('Tipo: ')!!}
                    {!!Form::select('gentipo',['M' =>'Masculino','F' => 'Femenino'],null, ['id'=>'gentipo','class'=>'form-control', 'placeholder'=>'Seleccione uno...', 'required'=>True])!!}
                </div>    
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    {!!Form::label('Pieza: ')!!}
                    {!!Form::select('posicion_pieza',['superior' => 'Superior','inferior' => 'Inferior'],null, ['id'=>'posicion_pieza','class'=>'form-control', 'placeholder'=>'Seleccionar uno ...', 'required'=>True, 'disabled'=>True])!!}
                </div>    
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
                    {!!Form::label('Prenda: ')!!}
                    {!!Form::text('prenda',null, ['id'=>'prenda', 'class'=>'form-control', 'placeholder'=>'prenda'])!!}
                </div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
                    {!!Form::label('Estilo: ')!!}
                    {!!Form::text('estilo',null, ['id'=>'estilo', 'class'=>'form-control', 'placeholder'=>'estilo'])!!}
                </div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
                    {!!Form::label('Categoria: ')!!}
                    {!!Form::text('categoria',null, ['id'=>'categoria', 'class'=>'form-control', 'placeholder'=>'categoria'])!!}
                </div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
                    {!!Form::label('Código Referencia: ')!!}
                    {!!Form::text('referencia',null, ['id'=>'referencia', 'class'=>'form-control', 'placeholder'=>'Código referencia'])!!}
                </div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
                    {!!Form::label('Referencia Tela: ')!!}
                    {!!Form::text('referenciatela',null, ['id'=>'referenciatela', 'class'=>'form-control', 'placeholder'=>'Referencia tela'])!!}
                </div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
                    {!!Form::label('Descripcion Tela: ')!!}
                    {!!Form::text('descriptela',null, ['id'=>'descriptela', 'class'=>'form-control', 'placeholder'=>'Descripción tela'])!!}
                </div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
                    {!!Form::label('Precio Tela: ')!!}
                    {!!Form::text('precio_tela',null, ['id'=>'preciotela', 'class'=>'form-control', 'placeholder'=>'Precio tela'])!!}
                </div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
                    {!!Form::label('Prov. Tela: ')!!}
                    {!!Form::text('proveedortela',null, ['id'=>'proveedortela', 'class'=>'form-control', 'placeholder'=>'Prov. tela'])!!}
                </div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
                    {!!Form::label('Ref. prov: ')!!}
                    {!!Form::text('refproveedor',null, ['id'=>'refproveedor', 'class'=>'form-control', 'placeholder'=>'Ref. prov'])!!}
                </div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
                    {!!Form::label('Composición Tela: ')!!}
                    {!!Form::text('composiciontela',null, ['id'=>'composicion_tela', 'class'=>'form-control', 'placeholder'=>'Composición tela'])!!}
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    {!!Form::label('Tallas: ')!!}
                    <div id="tallasdiv">
                        {!!Form::text('tallas',null, ['id'=>'tallas','class'=>'form-control', 'placeholder'=>'tallas'])!!}
                    </div>
                </div>
            </div>
            <!--<div class="col-sm-3">
                <div class="form-group">
                    {!Form::label('Colores: ')!!}<br>
                    foreach($colores as $color)
                        <?php
                            //if($indice==4)
                            {
                            ?>
                                {!Form::checkbox('colores[]',$color->id)!!}{!Form::label($color->nombre)!!}<br>
                            <?php
                                //$indice=1;
                            }
                            //else
                            {
                                //$indice=$indice+1;
                                ?>
                                {!Form::checkbox('colores[]',$color->id)!!}{!Form::label($color->nombre)!!}&nbsp;&nbsp;
                                <?php
                            }
                            ?>
                    endforeach
                </div>
            </div>-->
            <div class="col-sm-4">
                <div class="form-group">
                    {{Form::label('Colores')}}<br>
                    <select multiple class="chosen-tag" id="color" name="color[]">
                        @foreach($colores as $key => $value)
                            <option value="{{$key}}">{{$key." - ".$value}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
    	</div>
	</div>
    <div class="tab-pane" id="op2">
        <div class="row">
            <div class="col-sm-12">
                <h3 class="info-text" style="text-align: left;">INFERIOR</h3>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
                    {!!Form::label('Logo: ')!!}
                    {!!Form::text('logoinf',null, ['id'=>'logoinf', 'class'=>'form-control', 'placeholder'=>'....'])!!}
                </div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
                    {!!Form::label('Lleva otro bordado?: ')!!}
                    {!!Form::select('otrobordado',['NO' =>'NO','SI' => 'SI'],null, ['id'=>'otrobordado','class'=>'form-control', 'placeholder'=>'Seleccione uno...'])!!}
                </div>
            </div>
            <div class="col-sm-3" >
                <div id="bordado_cual_div" class='form-group'>
                    {!!Form::label('Cual?: ')!!}
                    {!!Form::text('bordado_cual',null, ['id'=>'bordado_cual', 'class'=>'form-control', 'placeholder'=>'....','disabled'=>true])!!}
                </div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
                    {!!Form::label('Pretina: ')!!}
                    {!!Form::text('pretina',null, ['id'=>'pretina', 'class'=>'form-control', 'placeholder'=>'Pretina'])!!}
                </div>
            </div>
            <div class="col-sm-2">
                <div class='form-group'>
                    <h5>{!!Form::label('BOLSILLOS: ')!!}</h5>
                </div>
            </div>
            <div class="col-sm-2">
                <div class='form-group'>
                    {!!Form::label('Delantero: ')!!}
                    {!!Form::text('bolsillodelantero',null, ['id'=>'bolsillodelantero', 'class'=>'form-control', 'placeholder'=>'....'])!!}
                </div>
            </div>
            <div class="col-sm-2">
                <div class='form-group'>
                    {!!Form::label('Trasero: ')!!}
                    {!!Form::text('bolsillotrasero',null, ['id'=>'bolsillotrasero', 'class'=>'form-control', 'placeholder'=>'....'])!!}
                </div>
            </div>
            <div class="col-sm-2">
                <div class='form-group'>
                    {!!Form::label('Cual: ')!!}
                    {!!Form::text('observabolsillotrasero',null, ['id'=>'observabolsillotrasero', 'class'=>'form-control', 'placeholder'=>'....'])!!}
                </div>
            </div>
            <div class="col-sm-2">
                <div class='form-group'>
                    {!!Form::label('Lleva bolsillo tra.? ')!!}
                    {!!Form::select('llevatapabolsillo',['NO' =>'NO','SI' => 'SI'],null, ['id'=>'llevatapabolsillo','class'=>'form-control', 'placeholder'=>'Seleccione uno...'])!!}
                </div>
            </div>
            <div class="col-sm-2">
                <div id="llevatapabolsillo_cuantas_div" class='form-group'>
                    {!!Form::label('Cuantas?: ')!!}
                    {!!Form::number('llevatapabolsillo_cuantas',null, ['id'=>'llevatapabolsillo_cuantas', 'class'=>'form-control', 'placeholder'=>'0','disabled'=>true])!!}
                </div>
            </div>
            <div class="col-sm-1">
                <div class='form-group'>
                    <h5>{!!Form::label('OTROS: ')!!}</h5>
                </div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
                    {!!Form::label('Botón: ')!!}
                    {!!Form::text('botoninf',null, ['id'=>'botoninf', 'class'=>'form-control', 'placeholder'=>'Botón'])!!}
                </div>
            </div>
            <div class="col-sm-1">
                <div class='form-group'>
                    {!!Form::label('#Cant.Botones: ')!!}
                    {!!Form::number('cantbotones',null, ['id'=>'cantbotones', 'class'=>'form-control', 'placeholder'=>'0'])!!}
                </div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
                    {!!Form::label('Remaches: ')!!}
                    {!!Form::select('remaches',['NO' =>'NO','SI' => 'SI'],null, ['id'=>'remaches','class'=>'form-control', 'placeholder'=>'Seleccione uno...'])!!}
                </div>
            </div> 
            <div class="col-sm-1">
                <div id="cantremaches_div" class='form-group'>
                    {!!Form::label('Cuántos: ')!!}
                    {!!Form::number('cantremaches',null, ['id'=>'cantremaches', 'class'=>'form-control', 'placeholder'=>'0','disabled'=>true])!!}
                </div>
            </div>
            <div class="col-sm-1">
                <div class='form-group'>
                    {!!Form::label('Tipo remache: ')!!}
                    {!!Form::text('tiporemache',null, ['id'=>'tiporemache', 'class'=>'form-control', 'placeholder'=>'0'])!!}
                </div>
            </div>
            <div class="col-sm-2">
                <div class='form-group'>
                    {!!Form::label('Garra: ')!!}
                    {!!Form::text('garrainf',null, ['id'=>'garrainf', 'class'=>'form-control', 'placeholder'=>'0'])!!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <hr/>
                <hr/>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <h3 class="info-text" style="text-align: left;">SUPERIOR</h3>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
                    {!!Form::label('Logo: ')!!}
                    {!!Form::text('logosup',null, ['id'=>'logosup', 'class'=>'form-control', 'placeholder'=>'....'])!!}
                </div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
                    {!!Form::label('Cogotera: ')!!}
                    {!!Form::text('cogotera',null, ['id'=>'cogotera', 'class'=>'form-control', 'placeholder'=>'--'])!!}
                </div>
            </div>
            <div class="col-sm-3" >
                <div class='form-group'>
                    {!!Form::label('Tela Banda Cuello ')!!}
                    {!!Form::text('tela_banda_cuello',null, ['id'=>'tela_banda_cuello', 'class'=>'form-control', 'placeholder'=>'--'])!!}
                </div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
                    {!!Form::label('Cinta Faya: ')!!}
                    {!!Form::text('cinta_faya',null, ['id'=>'cinta_faya', 'class'=>'form-control', 'placeholder'=>'--'])!!}
                </div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
                    {!!Form::label('Entretela: ')!!}
                    {!!Form::text('entretela',null, ['id'=>'entretela', 'class'=>'form-control', 'placeholder'=>'--'])!!}
                </div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
                    {!!Form::label('Cuello: ')!!}
                    {!!Form::text('cuello',null, ['id'=>'cuello', 'class'=>'form-control', 'placeholder'=>'--'])!!}
                </div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
                    {!!Form::label('Manga: ')!!}
                    {!!Form::text('manga',null, ['id'=>'manga', 'class'=>'form-control', 'placeholder'=>'--'])!!}
                </div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
                    {!!Form::label('Botón: ')!!}
                    {!!Form::text('botonsup',null, ['id'=>'botonsup', 'class'=>'form-control', 'placeholder'=>'--'])!!}
                </div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
                    {!!Form::label('Cartera Der: ')!!}
                    {!!Form::text('carterader',null, ['id'=>'carterader', 'class'=>'form-control', 'placeholder'=>'--'])!!}
                </div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
                    {!!Form::label('Cartera Izq: ')!!}
                    {!!Form::text('carteraizq',null, ['id'=>'carteraizq', 'class'=>'form-control', 'placeholder'=>'--'])!!}
                </div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
                    {!!Form::label('Espalda: ')!!}
                    {!!Form::text('espalda',null, ['id'=>'espalda', 'class'=>'form-control', 'placeholder'=>'--'])!!}
                </div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
                    {!!Form::label('Botón Cont: ')!!}
                    {!!Form::text('botoncont',null, ['id'=>'botoncont', 'class'=>'form-control', 'placeholder'=>'--'])!!}
                </div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
                    {!!Form::label('Bolsillos: ')!!}
                    {!!Form::text('bolsillos',null, ['id'=>'bolsillos', 'class'=>'form-control', 'placeholder'=>'--'])!!}
                </div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
                    {!!Form::label('Tapa: ')!!}
                    {!!Form::text('tapa',null, ['id'=>'tapa', 'class'=>'form-control', 'placeholder'=>'--'])!!}
                </div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
                    {!!Form::label('Diseño bolsillo: ')!!}
                    {!!Form::text('diseno_bolsillo',null, ['id'=>'diseno_bolsillo', 'class'=>'form-control', 'placeholder'=>'--'])!!}
                </div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
                    {!!Form::label('Charretera: ')!!}
                    {!!Form::text('charratera',null, ['id'=>'charratera', 'class'=>'form-control', 'placeholder'=>'--'])!!}
                </div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
                    {!!Form::label('Garra: ')!!}
                    {!!Form::text('garrasup',null, ['id'=>'garrasup', 'class'=>'form-control', 'placeholder'=>'--'])!!}
                </div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
                    {!!Form::label('N° de Ojales: ')!!}
                    {!!Form::text('num_ojales',null, ['id'=>'num_ojales', 'class'=>'form-control', 'placeholder'=>'--'])!!}
                </div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
                    {!!Form::label('Color de Ojal: ')!!}
                    {!!Form::text('color_ojales',null, ['id'=>'color_ojales', 'class'=>'form-control', 'placeholder'=>'--'])!!}
                </div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
                    {!!Form::label('Color Ult Ojal: ')!!}
                    {!!Form::text('ult_color_ojales',null, ['id'=>'ult_color_ojales', 'class'=>'form-control', 'placeholder'=>'--'])!!}
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane" id="opmed">
        <div class="row">
            <div class="col-sm-12">
                <h5 class="info-text"></h5>
            </div>
            <div class="col-sm-3">
                <h5>{!!Form::label('Lleva proceso industrial?: ')!!}</h5>    
            </div>
            <div class="col-sm-2">
                {!!Form::select('procesoind',['NO' =>'NO','SI' => 'SI'],null, ['id'=>'procesoind','class'=>'form-control', 'placeholder'=>'Seleccione uno...'])!!}    
            </div>
            <div class="col-sm-7">
                {!!Form::text('procesoind_observacion',null, ['id'=>'procesoind_observacion', 'class'=>'form-control', 'placeholder'=>'--','disabled'=>true])!!}
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <hr/>
                <hr/>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <h5>{!!Form::label('Insumos Especiales: ')!!}</h5>    
            </div>
            <div class="col-sm-9">
                {!!Form::text('insumosespeciales',null, ['id'=>'insumosespeciales', 'class'=>'form-control'])!!}
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <hr/>
                <hr/>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <h3 class="info-text" style="text-align: left;">OBSERVACIONES</h3>
            </div>
            <div class="col-sm-12">
                {!! Form::textarea('observaciones', null, ['id' => 'observaciones', 'class'=>'form-control']) !!}
            </div>
        </div>
    </div>
    <div class="tab-pane" id="opfin">
        <div class="row">
        	<div class="col-sm-12">
                <h3 class="info-text" style="text-align: left;">PROPORCIÓN DE CORTE</h3>
            </div>
            <div class="col-sm-6">
                <div class="col-sm-6">
                    <div class="form-group">
                        <h6>{!!Form::label('Mts ')!!}</h6>    
                        {!!Form::text('metros',null, ['id'=>'metros', 'class'=>'form-control'])!!}
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <h6>{!!Form::label('Prom. ')!!}</h6>
                        {!!Form::text('promedio',null, ['id'=>'promedio', 'class'=>'form-control'])!!}
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <h6>{!!Form::label('Und. ')!!}</h6>
                        {!!Form::text('unidades',null, ['id'=>'unidades', 'class'=>'form-control'])!!}
                    </div>
                </div>
                <div class="col-sm-6">

                    <div class="form-group">
                        <h6>{!!Form::label('Color Muestra ')!!}</h6>
                        {!!Form::text('color_muestra',null, ['id'=>'color_muestra', 'class'=>'form-control'])!!}
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div id="matriz">
                    <div class="row">
                        <div class="col-sm-1 text-center">
                            <div class="row">
                                <h6>Tallas</h6>
                            </div>
                        </div>
                        <div class="col-sm-1 text-center">
                            <div class="row">
                                <h6>28</h6>
                            </div>
                        </div>
                        <div class="col-sm-1 text-center">
                            <div class="row">
                                <h6>30</h6>
                            </div>
                        </div>
                        <div class="col-sm-1 text-center">
                            <div class="row">
                                <h6>32</h6>
                            </div>
                        </div>
                        <div class="col-sm-1 text-center">
                            <div class="row">
                                <h6>34</h5>
                            </div>
                        </div>
                        <div class="col-sm-1 text-center">
                            <div class="row">
                                <h6>36</h5>
                            </div>
                            <div class="row">
                                <h6>S</h5>
                            </div>
                        </div>
                        <div class="col-sm-1 text-center">
                            <div class="row">
                                <h6>38</h5>
                            </div>
                            <div class="row">
                                <h6>M</h5>
                            </div>
                        </div>
                        <div class="col-sm-1 text-center">
                            <div class="row">
                                <h6>40</h6>
                            </div>
                            <div class="row">
                                <h6>L</h6>
                            </div>
                        </div>
                        <div class="col-sm-1 text-center">
                            <div class="row">
                                <h6>42</h6>
                            </div>
                            <div class="row">
                                <h6>XL</h6>
                            </div>
                        </div>
                        <div class="col-sm-1 text-center">
                            <div class="row">
                                <h6>44</h6>
                            </div>
                            <div class="row">
                                <h6>2XL</h6>
                            </div>
                        </div>
                        <div class="col-sm-1 text-center">
                            <div class="row">
                                <h6>46</h6>
                            </div>
                            <div class="row">
                                <h6>3XL</h6>
                            </div>
                        </div>
                        <div class="col-sm-1 text-center">
                            <div class="row">
                                <h6>Total</h6>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-1 text-center">
                            <div class="row">
                                {!!Form::label('Proporción: ')!!}
                            </div>
                        </div>
                        <div class="col-sm-1 text-center">
                            <div class="row">
                                {!!Form::text('composition',null, ['class'=>'form-control', 'placeholder'=>'--'])!!}
                            </div>
                        </div>
                        <div class="col-sm-1 text-center">
                            <div class="row">
                                {!!Form::text('composition',null, ['class'=>'form-control', 'placeholder'=>'--'])!!}
                            </div>
                        </div>
                        <div class="col-sm-1 text-center">
                            <div class="row">
                                {!!Form::text('composition',null, ['class'=>'form-control', 'placeholder'=>'--'])!!}
                            </div>
                        </div>
                        <div class="col-sm-1 text-center">
                            <div class="row">
                                {!!Form::text('composition',null, ['class'=>'form-control', 'placeholder'=>'--'])!!}
                            </div>
                        </div>
                        <div class="col-sm-1 text-center">
                            <div class="row">
                                {!!Form::text('composition',null, ['class'=>'form-control', 'placeholder'=>'--'])!!}
                            </div>
                        </div>
                        <div class="col-sm-1 text-center">
                            <div class="row">
                                {!!Form::text('composition',null, ['class'=>'form-control', 'placeholder'=>'--'])!!}
                            </div>
                        </div>
                        <div class="col-sm-1 text-center">
                            <div class="row">
                                {!!Form::text('composition',null, ['class'=>'form-control', 'placeholder'=>'--'])!!}
                            </div>
                        </div>
                        <div class="col-sm-1 text-center">
                            <div class="row">
                                {!!Form::text('composition',null, ['class'=>'form-control', 'placeholder'=>'--'])!!}
                            </div>
                        </div>
                        <div class="col-sm-1 text-center">
                            <div class="row">
                                {!!Form::text('composition',null, ['class'=>'form-control', 'placeholder'=>'--'])!!}
                            </div>
                        </div>
                        <div class="col-sm-1 text-center">
                            <div class="row">
                                {!!Form::text('composition',null, ['class'=>'form-control', 'placeholder'=>'--'])!!}
                            </div>
                        </div>
                        <div class="col-sm-1 text-center">
                            <div class="row">
                                {!!Form::text('composition',null, ['class'=>'form-control', 'placeholder'=>'--'])!!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    	</div>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        $('#fechaordenproduccion').datetimepicker({
            viewMode: 'years',
            format: 'YYYY-MM-DD',
        });
    });

    jQuery('#fechaop').keypress(function(tecla) {
        if(tecla.keycode == 8 || tecla.keycode == 505)
            return false;
        else return false;
    });

    $('#gentipo').on('change', function(e)
    {
        var tipo = e.target.value;
        //console.log(tipo);
        $('#posicion_pieza').attr('disabled',false);
    });

    /*$('#pieza').on('change',function(e){
        var tipo= $('#tipo').val();
        var pieza = e.target.value;
        //console.log(tipo);
        //console.log(silueta);
        $.get('/Traertallas?tipo='+tipo+'&pieza='+pieza, function(data){
            //console.log(data);
            $("#tallasdiv").empty();
            $.each(data,function(index, talla){
                //console.log(talla);
                $('#tallasdiv').append('<input type="checkbox" name="talla[]" value="'+talla.id+'"/> ' + talla.talla + '<br />');
            });
        });
    });*/

    $('#posicion_pieza').on('change',function(e){
        var tipo= $('#gentipo').val();
        var pieza = e.target.value;
        //console.log(tipo);
        //console.log(silueta);
        $.get('/Traertallas?tipo='+tipo+'&pieza='+pieza, function(data){
            //console.log(data);
            helper=1;
            $("#tallasdiv").empty();
            $.each(data,function(index, talla){
                //console.log(talla);
                if(index==0)
                {
                    $('#tallasdiv').append('<input type="checkbox" name="tallas[]" value="'+talla.id+'"/> ' + talla.talla + '&nbsp;&nbsp;');       
                }
                else 
                {
                    if(index==4 || (index % 4)===0)
                    {
                        $('#tallasdiv').append('<input type="checkbox" name="tallas[]" value="'+talla.id+'"/> ' + talla.talla + '<br />');
                        index=1;    
                    }
                    else
                    {
                        $('#tallasdiv').append('<input type="checkbox" name="tallas[]" value="'+talla.id+'"/> ' + talla.talla + '&nbsp;&nbsp;');
                        index=index+1;
                    }
                }
            });
        });
    });

    $('#otrobordado').on('change',function(e){
        var valor = e.target.value;
        if(valor=='SI')
            $('#bordado_cual').prop('disabled',false);
        else
            $('#bordado_cual').prop('disabled',true);
    });

    $('#llevatapabolsillo').on('change',function(e){
        var valor = e.target.value;
        if(valor=='SI')
            $('#llevatapabolsillo_cuantas').prop('disabled',false);
        else
            $('#llevatapabolsillo_cuantas').prop('disabled',true);
    });

    $('#remaches').on('change',function(e){
        var valor = e.target.value;
        if(valor=='SI')
            $('#cantremaches').prop('disabled',false);
        else
            $('#cantremaches').prop('disabled',true);
    });

    $('#procesoind').on('change',function(e){
        var valor = e.target.value;
        if(valor=='SI')
            $('#procesoind_observacion').prop('disabled',false);
        else
            $('#procesoind_observacion').prop('disabled',true);
    });

</script>