<div class="tab-content">
    <div class="tab-pane" id="client1">
    	<div class="row">
        	<div class="col-sm-12">
            	<h5 class="info-text"></h5>
        	</div>
            <div class="col-sm-4">
                <div class='form-group'>
                    {!!Form::label('Código Tercero: ')!!}
                    {!!Form::text('codeTer',null, ['id'=>'codeTer', 'class'=>'form-control', 'placeholder'=>'Código Tercero'])!!}
                </div>
            </div>
            <div class="col-sm-4">
                <div class='form-group'>
					{!!Form::label('Nombre y/o Razón Social: ')!!}
					{!!Form::text('nameTer',null, ['id'=>'nameTer', 'class'=>'form-control', 'placeholder'=>'Nombre y/o Razón Social'])!!}
				</div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
					{!!Form::label('Tipo documento: ')!!}
                    {!!Form::select('typedocument_id', $typedocument, null, ['id'=>'typedocument_id', 'class'=>'form-control', 'placeholder' => 'Seleccione uno...'])!!}
				</div>
            </div>
            <div class= "col-sm-1" style="text-align:center">
                <div class='form-group'>
                    <a style="" class="btn btn-fill btn-primary" id="addCity"><i class="fa fa-plus"></i></a>
                </div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
					{!!Form::label('Nit: ')!!}
					{!!Form::text('nitTer',null, ['id'=>'nitTer', 'class'=>'form-control', 'placeholder'=>'Nit'])!!}
				</div>
            </div>
            <div class="col-sm-1">
            	<div class='form-group'>
					<div class='form-group'>
						{!!Form::label('Verificación: ')!!}
						{!!Form::text('nitTer1',null, ['id'=>'nitTer1', 'class'=>'form-control', 'placeholder'=>'0', 'disabled'=>true])!!}
					</div>
				</div>
            </div>
            <div class="col-sm-4">
                <div class='form-group'>
                    {!!Form::label('Departamento: ')!!}
                    {!!Form::select('state_id', $states, null, ['id'=>'state_id', 'class'=>'form-control', 'placeholder' => 'Seleccione uno...'])!!}
                </div>
            </div>
            <div class="col-sm-4">
                <div class='form-group'>
                    {!!Form::label('Ciudad: ')!!}
                    {!!Form::select('city_id', [], null, ['id'=>'city_id', 'class'=>'form-control', 'placeholder' => 'Seleccione uno...'])!!}
                </div>
            </div>
            <div class="col-sm-4">
                <div class='form-group'>
                    {!!Form::label('Ciudad Tercero: ')!!}
                    {!!Form::text('ciudad_ter',null, ['id'=>'ciudad_ter', 'class'=>'form-control', 'placeholder'=>'Ciudad Tercero'])!!}
                </div>
            </div>
            <div class="col-sm-4">
                <div class='form-group'>
					{!!Form::label('Celular: ')!!}
					{!!Form::text('telTer',null, ['id'=>'telTer', 'class'=>'form-control', 'placeholder'=>'Celular'])!!}
				</div>
            </div>
            <div class="col-sm-1">
                <div class='form-group'>
                    {!!Form::label('Indicativo: ')!!}
                    {!!Form::text('indicativo',null, ['id'=>'indicativo', 'class'=>'form-control', 'placeholder'=>'indicativo'])!!}
                </div>
            </div>
            <div class="col-sm-3">
            	<div class='form-group'>
					{!!Form::label('Teléfono: ')!!}
					{!!Form::text('faxTer',null, ['id'=>'faxTer', 'class'=>'form-control', 'placeholder'=>'Teléfono'])!!}
				</div>
            </div>
            <div class="col-sm-8">
                <div class='form-group'>
                    <div class='form-group'>
                        {!!Form::label('Dirección: ')!!}
                        {!!Form::text('dirTer',null, ['id'=>'dirTer', 'class'=>'form-control', 'placeholder'=>'Dirección'])!!}
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class='form-group'>
                    {!!Form::label('Email: ')!!}
                    {!!Form::email('email',null, ['id'=>'email', 'class'=>'form-control', 'placeholder'=>'Correo electrónico'])!!}
                </div>
            </div>
    	</div>
	</div>
    <div class="tab-pane" id="client2">
    	<div class="row">
        	<div class="col-sm-12">
            	<h5 class="info-text"> </h5>
        	</div>
            <div class="col-sm-3 col-sm-offset-1">
                <div class='form-group'>
					{!!Form::label('Profesión: ')!!}
					{!!Form::select('profession',$profesiones,null, ['id'=>'profession', 'class'=>'form-control', 'placeholder'=>'Seleccionar uno ...'])!!}
				</div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
                    {!!Form::label('Oficio: ')!!}
                    {!!Form::text('oficioTer',null, ['id'=>'oficioTer', 'class'=>'form-control', 'placeholder'=>'Oficio'])!!}
                </div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
					{!!Form::label('Nombre Almacen: ')!!}
					{!!Form::text('business',null, ['id'=>'business', 'class'=>'form-control', 'placeholder'=>'Almacen'])!!}
				</div>
            </div>
            <div class="col-sm-3 col-sm-offset-1">
                <div class='form-group'>
					{!!Form::label('Estado civil: ')!!}
					{!!Form::select('civilStatus_id', $civilstatuses, null, ['id'=>'civilStatus_id', 'class'=>'form-control', 'placeholder'=>'Seleccione uno ...'])!!}
				</div>
            </div>
            <div class="col-sm-3">
            	<div class='form-group'>
					{!!Form::label('Tipo Cliente: ')!!}
					{!!Form::text('tipotercero',null, ['id'=>'tipotercero', 'class'=>'form-control', 'placeholder'=>'Tipo Cliente'])!!}
				</div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
					{!!Form::label('límite: ')!!}
					{!!Form::number('limit',null, ['id'=>'limit', 'class'=>'form-control', 'placeholder'=>'0'])!!}
				</div>
            </div>
            <div class="col-sm-3 col-sm-offset-1">
            	<div class='form-group'>
					{!!Form::label('Autorización: ')!!}
					{!!Form::text('authorization',null, ['id'=>'authorization', 'class'=>'form-control', 'placeholder'=>'Autorización'])!!}
				</div>
            </div>
            <div class="col-sm-3">
            	<div class='form-group'>
					{!!Form::label('Descuento: ')!!}
					{!!Form::number('discount',null, ['id'=>'discount', 'class'=>'form-control', 'placeholder'=>'0'])!!}
				</div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
					{!!Form::label('Activo: ')!!}
					{!!Form::text('activo',null, ['id'=>'activo', 'class'=>'form-control', 'placeholder'=>'Activo'])!!}
				</div>
            </div>
            <div class="col-sm-3 col-sm-offset-1">
            	<div class='form-group'>
					{!!Form::label('Clasificación: ')!!}
					{!!Form::text('clasification',null, ['id'=>'clasification', 'class'=>'form-control', 'placeholder'=>'Clasificación'])!!}
				</div>
            </div>
            <div class="col-sm-3">
                {!!Form::label('Cumpleaños: ')!!}
                <div class='col-sm-12 input-group date' id='birthdayDiv'>
                    {!!Form::text('birthday', null, ['id'=>'birthday', 'class'=>'form-control', 'placeholder'=>'DD-MM-YYYY'])!!}
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
    	</div>
	</div>
    <div class="tab-pane" id="client3">
        <div class="row">
        	<div class="col-sm-12">
            	<h5 class="info-text"> </h5>
        	</div>
            <div class="col-sm-3 col-sm-offset-1">
                <div class='form-group'>
                    {!!Form::label('Método de pago: ')!!}
                    {!!Form::text('methodPayment',null, ['id'=>'methodPayment', 'class'=>'form-control', 'placeholder'=>'Método de pago'])!!}
                </div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
					{!!Form::label('Método de pago P: ')!!}
					{!!Form::text('methodPaymentP',null, ['id'=>'methodPaymentP', 'class'=>'form-control', 'placeholder'=>'Método de pago P'])!!}
				</div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
					{!!Form::label('Retefuente: ')!!}
					{!!Form::number('retefte',null, ['id'=>'retefte', 'class'=>'form-control', 'placeholder'=>'Retefuente'])!!}
				</div>
            </div>
            <div class="col-sm-3 col-sm-offset-1">
                <div class='form-group'>
					{!!Form::label('ICA: ')!!}
					{!!Form::number('ica',null, ['id'=>'ica', 'class'=>'form-control', 'placeholder'=>'ICA'])!!}
				</div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
					{!!Form::label('Régimen: ')!!}
					{!!Form::text('regimen',null, ['id'=>'regimen', 'class'=>'form-control', 'placeholder'=>'Régimen'])!!}
				</div>
            </div>
            <div class="col-sm-3">
            	<div class='form-group'>
					{!!Form::label('Lista: ')!!}
					<!--{!Form::text('list',null, ['id'=>'list', 'class'=>'form-control', 'placeholder'=>'Lista'])!!}-->
                    {!!Form::select('list', $listasprecios, null, ['id'=>'list', 'class'=>'form-control', 'placeholder'=>'Seleccione uno ...'])!!}
				</div>
            </div>
            <div class="col-sm-3  col-sm-offset-1">
            	<div class='form-group'>
					{!!Form::label('Zona: ')!!}
					{!!Form::text('zone_id',null, ['id'=>'zone_id', 'class'=>'form-control', 'placeholder'=>'Código Zona'])!!}
				</div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
					{!!Form::label('Zona: ')!!}
					{!!Form::text('zone',null, ['id'=>'zone', 'class'=>'form-control', 'placeholder'=>'Zona'])!!}
				</div>
            </div>
            <div class="col-sm-3">
            	<div class='form-group'>
					{!!Form::label('Vendedor: ')!!}
					{!!Form::text('seller_id',null, ['id'=>'seller_id', 'class'=>'form-control', 'placeholder'=>'Vendedor'])!!}
				</div>
            </div>
            <div class="col-sm-3 col-sm-offset-1">
            	<div class='form-group'>
					{!!Form::label('Dirección Oficina: ')!!}
					{!!Form::text('addressOffice',null, ['id'=>'addressOffice', 'class'=>'form-control', 'placeholder'=>'Dirección Oficina'])!!}
				</div>
            </div>

            <div class="col-sm-3">
                <div class='form-group'>
					{!!Form::label('Teléfono Oficina: ')!!}
					{!!Form::number('phoneOffice',null, ['id'=>'phoneOffice', 'class'=>'form-control', 'placeholder'=>'Teléfono Oficina'])!!}
				</div>
            </div>
            <div class="col-sm-3">
            	<div class='form-group'>
					{!!Form::label('Genero/ Sexo: ')!!}
					{!!Form::text('gender',null, ['id'=>'gender', 'class'=>'form-control', 'placeholder'=>'Genero/ Sexo'])!!}
				</div>
            </div>
            
    	</div>
    </div>
	<div class="tab-pane" id="client4">
        <div class="row">
        	<div class="col-sm-12">
            	<h5 class="info-text"> </h5>
        	</div>
            <div class="col-sm-3 col-sm-offset-1">
                <div class='form-group'>
                    {!!Form::label('Observación1: ')!!}
                    {!!Form::text('observation1',null, ['id'=>'observation1', 'class'=>'form-control', 'placeholder'=>'Observación1'])!!}
                </div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
					{!!Form::label('Observación2: ')!!}
					{!!Form::text('observation2',null, ['id'=>'observation2', 'class'=>'form-control', 'placeholder'=>'Observación2'])!!}
				</div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
					{!!Form::label('Observación3: ')!!}
					{!!Form::text('observation3',null, ['id'=>'observation3', 'class'=>'form-control', 'placeholder'=>'Observación3'])!!}
				</div>
            </div>
            <div class="col-sm-3 col-sm-offset-1">
                <div class='form-group'>
					{!!Form::label('Nombres: ')!!}
					{!!Form::text('name',null, ['id'=>'name', 'class'=>'form-control', 'placeholder'=>'Nombres'])!!}
				</div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
					{!!Form::label('Apellidos: ')!!}
					{!!Form::text('lastname',null, ['id'=>'lastname', 'class'=>'form-control', 'placeholder'=>'Apellidos'])!!}
				</div>
            </div>
            <div class="col-sm-3">
            	<div class='form-group'>
					{!!Form::label('Razón Social: ')!!}
					{!!Form::text('businessName',null, ['id'=>'businessName', 'class'=>'form-control', 'placeholder'=>'Razón Social'])!!}
				</div>
            </div>
            <div class="col-sm-3 col-sm-offset-1">
            	<div class='form-group'>
					{!!Form::label('Referencia1: ')!!}
					{!!Form::text('reference1',null, ['id'=>'reference1', 'class'=>'form-control', 'placeholder'=>'Referencia1'])!!}
				</div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
					{!!Form::label('Referencia2: ')!!}
					{!!Form::text('reference2',null, ['id'=>'reference2', 'class'=>'form-control', 'placeholder'=>'Referencia2'])!!}
				</div>
            </div>
            <div class="col-sm-3">
            	<div class='form-group'>
					{!!Form::label('Referencia3: ')!!}
					{!!Form::text('reference3',null, ['id'=>'reference3', 'class'=>'form-control', 'placeholder'=>'Referencia3'])!!}
				</div>
            </div>
            <div class="col-sm-3 col-sm-offset-1">
            	<div class='form-group'>
					{!!Form::label('Referencia4: ')!!}
					{!!Form::text('reference4',null, ['id'=>'reference4', 'class'=>'form-control', 'placeholder'=>'Referencia4'])!!}
				</div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
					{!!Form::label('Referencia5: ')!!}
					{!!Form::text('reference5',null, ['id'=>'reference5', 'class'=>'form-control', 'placeholder'=>'Referencia5'])!!}
				</div>
            </div>
            <div class="col-sm-3">
            	<div class='form-group'>
					{!!Form::label('Dto Financiación: ')!!}
					{!!Form::number('discountFina',null, ['id'=>'discountFina', 'class'=>'form-control', 'placeholder'=>'Descuento Financiación'])!!}
				</div>
            </div>
            
    	</div>
    </div>
    <div class="tab-pane" id="client5">
        <div class="row">
        	<div class="col-sm-12">
            	<h5 class="info-text"> </h5>
        	</div>
            <div class="col-sm-3 col-sm-offset-1">
                <div class='form-group'>
                    {!!Form::label('Plazo (días): ')!!}
                    {!!Form::number('termFunding',null, ['id'=>'termFunding', 'class'=>'form-control', 'placeholder'=>'Plazo'])!!}
                </div>
            </div>
        	<div class="col-sm-3">
                <div class='form-group'>
					{!!Form::label('País(Código)')!!}
					{!!Form::text('codePais',null, ['id'=>'codePais', 'class'=>'form-control', 'placeholder'=>'Código País'])!!}
				</div>
			</div>
            <div class="col-sm-3">
                <div class='form-group'>
					{!!Form::label('Código País 2: ')!!}
					{!!Form::text('codePais2',null, ['id'=>'codePais2', 'class'=>'form-control', 'placeholder'=>'Código País'])!!}
				</div>
            </div>
            <div class="col-sm-3 col-sm-offset-1">
                <div class='form-group'>
					{!!Form::label('Promp24: ')!!}
					{!!Form::number('promp24',null, ['id'=>'promp24', 'class'=>'form-control', 'placeholder'=>'Promp24'])!!}
				</div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
					{!!Form::label('Promp12: ')!!}
					{!!Form::number('promp12',null, ['id'=>'promp12', 'class'=>'form-control', 'placeholder'=>'Promp12'])!!}
				</div>
            </div>
            <div class="col-sm-3">
            	<div class='form-group'>
					{!!Form::label('Promp6: ')!!}
					{!!Form::number('promp6',null, ['id'=>'promp6', 'class'=>'form-control', 'placeholder'=>'Promp6'])!!}
				</div>
            </div>
            <div class="col-sm-3 col-sm-offset-1">
            	<div class='form-group'>
					{!!Form::label('Cheqd24: ')!!}
					{!!Form::number('cheqd24',null, ['id'=>'cheqd24', 'class'=>'form-control', 'placeholder'=>'Cheqd24'])!!}
				</div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
					{!!Form::label('Cheqd12: ')!!}
					{!!Form::number('cheqd12',null, ['id'=>'cheqd12', 'class'=>'form-control', 'placeholder'=>'Cheqd12'])!!}
				</div>
            </div>
            <div class="col-sm-3">
            	<div class='form-group'>
					{!!Form::label('Cheqd6: ')!!}
					{!!Form::number('cheqd6',null, ['id'=>'cheqd6', 'class'=>'form-control', 'placeholder'=>'Cheqd6'])!!}
				</div>
            </div>
            <div class="col-sm-3 col-sm-offset-1">
            	<div class='form-group'>
					{!!Form::label('Estado: ')!!}
					{!!Form::text('status',null, ['id'=>'status', 'class'=>'form-control', 'placeholder'=>'Estado'])!!}
				</div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
					{!!Form::label('cree: ')!!}
					{!!Form::number('cree',null, ['id'=>'cree', 'class'=>'form-control', 'placeholder'=>'Cree'])!!}
				</div>
            </div>
            <div class="col-sm-3">
            	<div class='form-group'>
					{!!Form::label('Creevtas: ')!!}
					{!!Form::text('creevtas',null, ['id'=>'creevtas', 'class'=>'form-control', 'placeholder'=>'Creevtas'])!!}
				</div>
            </div>
            <div class="col-sm-3 col-sm-offset-1">
            	<div class='form-group'>
					{!!Form::label('Cifin: ')!!}
					{!!Form::text('cifin',null, ['id'=>'cifin', 'class'=>'form-control', 'placeholder'=>'Cifin'])!!}
				</div>
            </div>
    	</div>
    </div>
</div>
<script type="text/javascript">

$(function () {
    $('#birthdayDiv').datetimepicker({
        viewMode: 'years',
        format: 'YYYY-MM-DD',
    });
});

jQuery('#birthday').keypress(function(tecla) {
    if(tecla.keycode == 8 || tecla.keycode == 505)
        return false;
    else return false;
});


$("#frm").submit(function (event) {
    event.preventDefault();
    $('.loading').show();
    var form = $(this);
    var data = new FormData($(this)[0]);
    var url = form.attr("action");
    $.ajax({
        type: "POST",
        url: url,
        data: data,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
            if (data.fail) {
                $('#frm input.required, #frm textarea.required').each(function () {
                    index = $(this).attr('name');
                    if (index in data.errors) {
                        $("#form-" + index + "-error").addClass("has-error");
                        $("#" + index + "-error").html(data.errors[index]);
                    }
                    else {
                        $("#form-" + index + "-error").removeClass("has-error");
                        $("#" + index + "-error").empty();
                    }
                });
                $('#focus').focus().select();
            } else {
                $(".has-error").removeClass("has-error");
                $(".help-block").empty();
                $('.loading').hide();
                ajaxLoad(data.url, data.content);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            alert(errorThrown);
        }
    });
    return false;
});

$('#typedocument_id').on('change',function(e){
    var type = e.target.value;
    $.get('/JsonTypes?type_id='+type,function(data){
        //console.log(data[0].typedocument);
        if (data[0].typedocument=='NIT') {
            //console.log($("#nitTer1").disable);
            document.getElementById('nitTer1').disabled = false;
            //$('#nitTer1').prop('disabled', false);
        }
        else{
            document.getElementById('nitTer1').disabled = true;
        }
    });
});

$('#state_id').on('change', function(e)
{
    var state_id = e.target.value;
    //console.log(state_id);
    $.get('/JsonCities?state_id='+state_id, function(data)
    {
        $("#city_id").empty();
        $('#city_id').attr('placeholder', 'Seleccione una opcion...');
        $('#city_id').append($('<option>', { 
                value: 0,
                text : '......' 
            }));
        $.each(data, function(index, cityObj)
        {
            //console.log(cityObj.id);
            $('#city_id').append($('<option>', { 
                value: cityObj.id,
                text : cityObj.name 
            }));
        });
    });
});

$('#city_id').on('change', function(e){
    var city = e.target.value;
    //console.log(city);
    $.get('/JsonCity?city_id='+city, function(data){
        console.log(data[0].Name);
        $('#ciudad_ter').val(data[0].Name);
        $('#indicativo').val(data[0].indicativo)
    });
});  
</script>