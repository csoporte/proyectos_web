<div class="tab-content">
    <div class="tab-pane" id="client1">
    	<div class="row">
        	<div class="col-sm-12">
            	<h5 class="info-text"> </h5>
        	</div>
            <div class="col-sm-3 col-sm-offset-1">
                <div class='form-group'>
					{!!Form::label('Referencia: ')!!}
					{!!Form::text('reference',null, ['id'=>'reference', 'class'=>'form-control', 'placeholder'=>'reference'])!!}
				</div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
					{!!Form::label('Codigo: ')!!}
					{!!Form::text('code',null, ['id'=>'code', 'class'=>'form-control', 'placeholder'=>'code'])!!}
				</div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
					{!!Form::label('Colección: ')!!}
					{!!Form::text('colection',null, ['id'=>'colection', 'class'=>'form-control', 'placeholder'=>'colection'])!!}
				</div>
            </div>
            <div class="col-sm-3 col-sm-offset-1">
                <div class='form-group'>
					{!!Form::label('Temporada: ')!!}
					{!!Form::text('season',null, ['id'=>'season', 'class'=>'form-control', 'placeholder'=>'season'])!!}
				</div>
            </div>
            <div class="col-sm-3">
            	<div class='form-group'>
					<div class='form-group'>
						{!!Form::label('Silueta: ')!!}
						{!!Form::text('shape',null, ['id'=>'shape', 'class'=>'form-control', 'placeholder'=>'shape'])!!}
					</div>
				</div>
            </div>
            <div class="col-sm-3">
            	<div class='form-group'>
					<div class='form-group'>
						{!!Form::label('Clasificación del producto: ')!!}
						{!!Form::text('productClasification',null, ['id'=>'productClasification', 'class'=>'form-control', 'placeholder'=>'productClasification'])!!}
					</div>
				</div>
            </div>
            <div class="col-sm-3 col-sm-offset-1">
                <div class='form-group'>
					{!!Form::label('Muestra Física: ')!!}
					{!!Form::text('physicalSample',null, ['id'=>'physicalSample', 'class'=>'form-control', 'placeholder'=>'physicalSampler'])!!}
				</div>
            </div>
            <div class="col-sm-3">
            	<div class='form-group'>
					{!!Form::label('Composición: ')!!}
					{!!Form::text('composition',null, ['id'=>'composition', 'class'=>'form-control', 'placeholder'=>'composition'])!!}
				</div>
            </div>
            <div class="col-sm-3">
            	<div class='form-group'>
					{!!Form::label('Precio1: ')!!}
					{!!Form::text('price1',null, ['id'=>'price1', 'class'=>'form-control', 'placeholder'=>'price1'])!!}
				</div>
            </div>

            <div class="col-sm-3 col-sm-offset-1">
                <div class='form-group'>
					{!!Form::label('Precio2: ')!!}
					{!!Form::text('price2',null, ['id'=>'price2', 'class'=>'form-control', 'placeholder'=>'price2'])!!}
				</div>
            </div>
            <div class="col-sm-3">
            	<div class='form-group'>
					{!!Form::label('Precio3: ')!!}
					{!!Form::text('price3',null, ['id'=>'price3', 'class'=>'form-control', 'placeholder'=>'price3'])!!}
				</div>
            </div>
            <div class="col-sm-3">
            	<div class='form-group'>
					{!!Form::label('Almacén: ')!!}
					{!!Form::text('warehouse',null, ['id'=>'warehouse', 'class'=>'form-control', 'placeholder'=>'warehouse'])!!}
				</div>
            </div>

    	</div>
	</div>
    <div class="tab-pane" id="client2">
    	<div class="row">
        	<div class="col-sm-12">
            	<h5 class="info-text"> </h5>
        	</div>
            <div class="col-sm-3 col-sm-offset-1">
                <div class='form-group'>
					{!!Form::label('m: ')!!}
					{!!Form::text('m',null, ['id'=>'m', 'class'=>'form-control', 'placeholder'=>'m'])!!}
				</div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
					{!!Form::label('Vende: ')!!}
					{!!Form::text('sell',null, ['id'=>'sell', 'class'=>'form-control', 'placeholder'=>'sell'])!!}
				</div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
					{!!Form::label('Color: ')!!}
					{!!Form::text('color',null, ['id'=>'color', 'class'=>'form-control', 'placeholder'=>'color'])!!}
				</div>
            </div>
            <div class="col-sm-3 col-sm-offset-1">
                <div class='form-group'>
					{!!Form::label('Tallas: ')!!}
					{!!Form::text('sizes',null, ['id'=>'sizes', 'class'=>'form-control', 'placeholder'=>'sizes'])!!}
				</div>
            </div>
            <div class="col-sm-3">
            	<div class='form-group'>
					{!!Form::label('Agotados: ')!!}
					{!!Form::text('soldout',null, ['id'=>'soldout', 'class'=>'form-control', 'placeholder'=>'soldout'])!!}
				</div>
            </div>
            <div class="col-sm-3">
            	<div class='form-group'>
					{!!Form::label('Observaciones: ')!!}
					{!!Form::text('comments',null, ['id'=>'comments', 'class'=>'form-control', 'placeholder'=>'comments'])!!}
				</div>
            </div>
            <div class="col-sm-3 col-sm-offset-1">
                <div class='form-group'>
					{!!Form::label('entrega: ')!!}
					{!!Form::text('deliveryDate',null, ['id'=>'limit', 'class'=>'form-control', 'placeholder'=>'deliveryDate'])!!}
				</div>
            </div>
            <div class="col-sm-3">
            	<div class='form-group'>
					{!!Form::label('ticket: ')!!}
					{!!Form::text('ticket',null, ['id'=>'ticket', 'class'=>'form-control', 'placeholder'=>'ticket'])!!}
				</div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$("#frm").submit(function (event) {
    event.preventDefault();
    $('.loading').show();
    var form = $(this);
    var data = new FormData($(this)[0]);
    var url = form.attr("action");
    $.ajax({
        type: "POST",
        url: url,
        data: data,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
            if (data.fail) {
                $('#frm input.required, #frm textarea.required').each(function () {
                    index = $(this).attr('name');
                    if (index in data.errors) {
                        $("#form-" + index + "-error").addClass("has-error");
                        $("#" + index + "-error").html(data.errors[index]);
                    }
                    else {
                        $("#form-" + index + "-error").removeClass("has-error");
                        $("#" + index + "-error").empty();
                    }
                });
                $('#focus').focus().select();
            } else {
                $(".has-error").removeClass("has-error");
                $(".help-block").empty();
                $('.loading').hide();
                ajaxLoad(data.url, data.content);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            alert(errorThrown);
        }
    });
    return false;
});
</script>