<br>
<div class="form-group">
    {!!Form::button('Asociar',['id'=>'agregar', 'class'=>'btn btn-primary'])!!}
</div>
<div class="span12"></div>
<div class="form-group">
    {!!Form::button('Quitar',['id'=>'borrar', 'class'=>'btn btn-primary'])!!}
</div>
<script type="text/javascript">
    $("#frm").submit(function (event) {
        event.preventDefault();
        $('.loading').show();
        var form = $(this);
        var data = new FormData($(this)[0]);
        var url = form.attr("action");
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.fail) {
                    $('#frm input.required, #frm textarea.required').each(function () {
                        index = $(this).attr('name');
                        if (index in data.errors) {
                            $("#form-" + index + "-error").addClass("has-error");
                            $("#" + index + "-error").html(data.errors[index]);
                        }
                        else {
                            $("#form-" + index + "-error").removeClass("has-error");
                            $("#" + index + "-error").empty();
                        }
                    });
                    $('#focus').focus().select();
                } else {
                    $(".has-error").removeClass("has-error");
                    $(".help-block").empty();
                    $('.loading').hide();
                    ajaxLoad(data.url, data.content);
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                alert(errorThrown);
            }
        });
        return false;
    });

    //funcion que se activa al dar click al boton agregar, el objetivo es tomar los datos de ambos selectores y se manda a una fucnion ajax donde se guardan los registros. Se genera un registro de la referencia por tercero. Luego se actualiza el select de los datos de la lista de precios, mostrando el ulitmo registro seleccionado.
    $("#agregar").on('click',function(e){
        e.preventDefault();
        //console.log("Doy click en el boton agregar");
        i=0;
        j=0;
        var referencias = [];
        var vendedores = [];
        //se capturan los datos seleccionados del select multiple y se guardan en una array.
        $('#referencetoload').each(function(){
            referencias[i]=$(this).val();
            i=i+1;
        });
        $('#vendedores').each(function(){
            vendedores[j]=$(this).val();
            j=j+1;
        });
        
        //se envian los datos del select a la funcion del controller para guardarlos en la tabla de la lista de precios, adicional se recibe los datos de la tabla lista de precios despues de haber generado los registros nuevos.
        $.get('/GuardarVendedoReferencia?referencias='+referencias+'&vendedores='+vendedores, function(data)
        {
            //se limpia el select.
            $("#referenciasloading").empty();
            //se genera un indice para que guarde el valor de la posicion del ultimo registro guardado
            var indice=0;
            $.each(data, function(index, refObj)
            {
                //se generan las opciones del select, con el valor y con la referencia y el tercero asociado a esta.
                $('#referenciasloading').append($('<option>', { 
                    value: refObj.id,
                    text : refObj.reference+" -- "+refObj.seller 
                }));
                indice=index;
            });
            // se captura el ultimo registro
            var ultimo=data[indice].id;

            //se pone por defecto el ultimo registro.
            $('#referenciasloading').val(ultimo);
        });
    });


    $("#borrar").on('click',function(e){
        e.preventDefault();
        console.log("Doy click en el boton borrar");
        $i=0;
        var refer_vende=[];
        $('#referenciasloading').each(function(){
            refer_vende[$i]=$(this).val();
            $i=$i+1;
        });

        $.get('/BorrarVendedoReferencia?refer_vende='+refer_vende,function(data){
            $("#referenciasloading").empty();
            $.each(data, function(index, refObj){
                //se generan las opciones del select, con el valor y con la referencia y el tercero asociado a esta.
                $('#referenciasloading').append($('<option>', { 
                    value: refObj.id,
                    text : refObj.reference+" -- "+refObj.seller 
                }));
                indice=index;
            });
        });
    });
</script>