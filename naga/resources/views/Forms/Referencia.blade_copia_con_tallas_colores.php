<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            {!!Form::label('Código: ')!!}
            {!!Form::text('codigo',null, ['class'=>'form-control', 'placeholder'=>'codigo'])!!}
        </div>    
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {!!Form::label('Nombre: ')!!}
            {!!Form::text('nombre',null, ['class'=>'form-control', 'placeholder'=>'nombre'])!!}
        </div>    
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {!!Form::label('Almacen:')!!}
            {!!Form::select('almacen',$bodegas,null,['id'=>'almacen','class'=>'form-control','placeholder'=>'Seleccionar uno...'])!!}
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {!!Form::label('Temporada:')!!}
            {!!Form::select('temporada',$temporadas,null,['id'=>'temporada','class'=>'form-control','placeholder'=>'Seleccionar uno...'])!!}
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {!!Form::label('Categoria:')!!}
            {!!Form::select('categoria',['coleccion'=>'Colección','linea'=>'Línea'],null,['id'=>'categoria','class'=>'form-control','placeholder'=>'Seleccionar...'])!!}
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {!!Form::label('Tipo: ')!!}
            {!!Form::select('tipo',['M' =>'Masculino','F' => 'Femenino'],null, ['id'=>'tipo','class'=>'form-control', 'placeholder'=>'Seleccione uno...', 'required'=>True])!!}
        </div>    
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {!!Form::label('Pieza: ')!!}
            {!!Form::select('pieza',['superior' => 'Superior','inferior' => 'Inferior'],null, ['id'=>'pieza','class'=>'form-control', 'placeholder'=>'Seleccionar uno ...', 'required'=>True, 'disabled'=>True])!!}
        </div>    
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {!!Form::label('Grupo o Prenda')!!}
            {!!Form::select('grupo',$grupos,null,['id'=>'grupo','class'=>'form-control','placeholder'=>'Seleccionar...'])!!}
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {!!Form::label('Subgrupo o Estilo')!!}
            {!!Form::select('subgrupo',$subgrupos,null,['id'=>'subgrupo','class'=>'form-control','placeholder'=>'Seleccionar...'])!!}
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {!!Form::label('Estilo: ')!!}
            {!!Form::text('estilo',null, ['class'=>'form-control', 'placeholder'=>'estilo'])!!}
        </div>    
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {!!Form::label('Moda: ')!!}
            {!!Form::text('moda',null, ['class'=>'form-control', 'placeholder'=>'moda'])!!}
        </div>    
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {!!Form::label('Silueta')!!}
            {!!Form::select('silueta',$siluetas,null,['id'=>'silueta','class'=>'form-control','placeholder'=>'Seleccionar...'])!!}
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {!!Form::label('Observaciones: ')!!}
            {!!Form::textarea('observaciones',null, ['class'=>'form-control', 'placeholder'=>'observaciones', 'rows'=>'3'])!!}
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {!!Form::label('Tallas: ')!!}
            <div id="tallasdiv">
                {!!Form::text('tallas',null, ['class'=>'form-control', 'placeholder'=>'tallas'])!!}
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {!!Form::label('Colores: ')!!}<br>
            @foreach($colores as $color)
                {!!Form::checkbox('colores[]',$color->id)!!}{!!Form::label($color->nombre)!!}<br>
            @endforeach
        </div>
    </div>
    <div class="col-sm-12">
        {!!Form::submit('Guardar',['id'=>'Guardar', 'class'=>'btn btn-primary'])!!}
    </div>
</div>
<script type="text/javascript">
    $("#frm").submit(function (event) {
        event.preventDefault();
        $('.loading').show();
        var form = $(this);
        var data = new FormData($(this)[0]);
        var url = form.attr("action");
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.fail) {
                    $('#frm input.required, #frm textarea.required').each(function () {
                        index = $(this).attr('name');
                        if (index in data.errors) {
                            $("#form-" + index + "-error").addClass("has-error");
                            $("#" + index + "-error").html(data.errors[index]);
                        }
                        else {
                            $("#form-" + index + "-error").removeClass("has-error");
                            $("#" + index + "-error").empty();
                        }
                    });
                    $('#focus').focus().select();
                } else {
                    $(".has-error").removeClass("has-error");
                    $(".help-block").empty();
                    $('.loading').hide();
                    ajaxLoad(data.url, data.content);
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                alert(errorThrown);
            }
        });
        return false;
    });

    $('#tipo').on('change', function(e)
    {
        var tipo = e.target.value;
        //console.log(tipo);
        $('#pieza').attr('disabled',false);
        /*$.get('/JsonCities?department_id='+department_id, function(data)
        {
            $("#city_id").empty();
            $('#city_id').attr('placeholder', 'Seleccione una opcion...');
            $.each(data, function(index, cityObj)
            {
                console.log(cityObj.id);
                $('#city_id').append($('<option>', { 
                    value: cityObj.id,
                    text : cityObj.name 
                }));
            });
        });*/
    });

    $('#pieza').on('change',function(e){
        var tipo= $('#tipo').val();
        var pieza = e.target.value;
        //console.log(tipo);
        //console.log(silueta);
        $.get('/Traertallas?tipo='+tipo+'&pieza='+pieza, function(data){
            //console.log(data);
            $("#tallasdiv").empty();
            $.each(data,function(index, talla){
                //console.log(talla);
                $('#tallasdiv').append('<input type="checkbox" name="talla[]" value="'+talla.id+'"/> ' + talla.talla + '<br />');
            });
        });
    });
</script>