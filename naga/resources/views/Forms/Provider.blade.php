<br>
<div class="form-group">
{!!Form::label('Codigo: ')!!}
{!!Form::text('code',null, ['class'=>'form-control', 'placeholder'=>'code'])!!}
</div>
<div class="form-group">
{!!Form::label('Proveedor: ')!!}
{!!Form::text('provider',null, ['class'=>'form-control', 'placeholder'=>'provider'])!!}
</div>
<div class="form-group">
{!!Form::label('nit: ')!!}
{!!Form::text('nit',null, ['class'=>'form-control', 'placeholder'=>'nit'])!!}
</div>
<div class="form-group">
{!!Form::label('Dirección: ')!!}
{!!Form::text('address',null, ['class'=>'form-control', 'placeholder'=>'address'])!!}
</div>
<div class="form-group">
{!!Form::label('Teléfono: ')!!}
{!!Form::text('phone',null, ['class'=>'form-control', 'placeholder'=>'phone'])!!}
</div>
<div class="form-group">
{!!Form::label('Ciudad: ')!!}
{!!Form::number('city_id',null, ['class'=>'form-control', 'placeholder'=>'0'])!!}
</div>
<div class="form-group">
{!!Form::label('Nombre Comercial: ')!!}
{!!Form::text('tradename',null, ['class'=>'form-control', 'placeholder'=>'tradename'])!!}
</div>
<div class="form-group">
{!!Form::label('Producto: ')!!}
{!!Form::number('product_id',null, ['class'=>'form-control', 'placeholder'=>'0'])!!}
</div>
<div class="form-group">
{!!Form::label('Correo electrónico: ')!!}
{!!Form::text('email',null, ['class'=>'form-control', 'placeholder'=>'email'])!!}
</div>
{!!Form::submit('Guardar',['id'=>'Guardar', 'class'=>'btn btn-primary'])!!}
<script type="text/javascript">
$("#frm").submit(function (event) {
    event.preventDefault();
    $('.loading').show();
    var form = $(this);
    var data = new FormData($(this)[0]);
    var url = form.attr("action");
    $.ajax({
        type: "POST",
        url: url,
        data: data,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
            if (data.fail) {
                $('#frm input.required, #frm textarea.required').each(function () {
                    index = $(this).attr('name');
                    if (index in data.errors) {
                        $("#form-" + index + "-error").addClass("has-error");
                        $("#" + index + "-error").html(data.errors[index]);
                    }
                    else {
                        $("#form-" + index + "-error").removeClass("has-error");
                        $("#" + index + "-error").empty();
                    }
                });
                $('#focus').focus().select();
            } else {
                $(".has-error").removeClass("has-error");
                $(".help-block").empty();
                $('.loading').hide();
                ajaxLoad(data.url, data.content);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            alert(errorThrown);
        }
    });
    return false;
});
</script>