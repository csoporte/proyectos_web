<div class='form-group'>
{!!Form::label('codigoTer: ')!!}
{!!Form::text('codeTer',null, ['id'=>'codeTer', 'class'=>'form-control', 'placeholder'=>'CodigoTer'])!!}
</div>
<div class='form-group'>
{!!Form::label('nombre Ter: ')!!}
{!!Form::text('nameTer',null, ['id'=>'nameTer', 'class'=>'form-control', 'placeholder'=>'nombre Ter'])!!}
</div>
<div class='form-group'>
{!!Form::label('Tipo documento: ')!!}
{!!Form::text('typeDocument_id',null, ['id'=>'typeDocument_id', 'class'=>'form-control', 'placeholder'=>'Tipo documento'])!!}
</div>
<div class='form-group'>
{!!Form::label('Nit Ter: ')!!}
{!!Form::text('nitTer',null, ['id'=>'nitTer', 'class'=>'form-control', 'placeholder'=>'Nit Ter'])!!}
</div>
<div class='form-group'>
{!!Form::label('Nit Ter1: ')!!}
{!!Form::text('nitTer1',null, ['id'=>'nitTer1', 'class'=>'form-control', 'placeholder'=>'Nit Ter1'])!!}
</div>
<div class='form-group'>
{!!Form::label('Dir Ter: ')!!}
{!!Form::text('dirTer',null, ['id'=>'dirTer', 'class'=>'form-control', 'placeholder'=>'Dir Ter'])!!}
</div>
<div class='form-group'>
{!!Form::label('Tel Ter: ')!!}
{!!Form::text('telTer',null, ['id'=>'telTer', 'class'=>'form-control', 'placeholder'=>'Tel Ter'])!!}
</div>
<div class='form-group'>
{!!Form::label('Fax Ter: ')!!}
{!!Form::text('faxTer',null, ['id'=>'faxTer', 'class'=>'form-control', 'placeholder'=>'Fax Ter'])!!}
</div>
<div class='form-group'>
{!!Form::label('Ciudad: ')!!}
{!!Form::text('city_id',null, ['id'=>'city_id', 'class'=>'form-control', 'placeholder'=>'Ciudad'])!!}
</div>
<div class='form-group'>
{!!Form::label('Ciudad Ter: ')!!}
{!!Form::text('cityTer',null, ['id'=>'cityTer', 'class'=>'form-control', 'placeholder'=>'Ciudad Ter'])!!}
</div>
<div class='form-group'>
{!!Form::label('Departamento Ter: ')!!}
{!!Form::text('state_id',null, ['id'=>'state_id', 'class'=>'form-control', 'placeholder'=>'Departamento Ter'])!!}
</div>
<div class='form-group'>
{!!Form::label('Oficio Ter: ')!!}
{!!Form::text('oficioTer',null, ['id'=>'oficioTer', 'class'=>'form-control', 'placeholder'=>'Oficio Ter'])!!}
</div>
<div class='form-group'>
{!!Form::label('Profesión: ')!!}
{!!Form::text('profession_id',null, ['id'=>'profession_id', 'class'=>'form-control', 'placeholder'=>'Profesión'])!!}
</div>
<div class='form-group'>
{!!Form::label('Empresa: ')!!}
{!!Form::text('business',null, ['id'=>'business', 'class'=>'form-control', 'placeholder'=>'Empresa'])!!}
</div>
<div class='form-group'>
{!!Form::label('Fecha de nacimiento: ')!!}
{!!Form::text('birthday',null, ['id'=>'birthday', 'class'=>'form-control', 'placeholder'=>'Fecha de nacimiento'])!!}
</div>
<div class='form-group'>
{!!Form::label('Estado civil: ')!!}
{!!Form::text('civilStatus_id',null, ['id'=>'civilStatus_id', 'class'=>'form-control', 'placeholder'=>'Estado civil'])!!}
</div>
<div class='form-group'>
{!!Form::label('Correo electrónico: ')!!}
{!!Form::text('email',null, ['id'=>'email', 'class'=>'form-control', 'placeholder'=>'Correo electrónico'])!!}
</div>
<div class='form-group'>
{!!Form::label('Tipo Ter: ')!!}
{!!Form::text('typeTer',null, ['id'=>'typeTer', 'class'=>'form-control', 'placeholder'=>'Tipo Ter'])!!}
</div>
<div class='form-group'>
{!!Form::label('límite: ')!!}
{!!Form::text('limit',null, ['id'=>'limit', 'class'=>'form-control', 'placeholder'=>'límite'])!!}
</div>
<div class='form-group'>
{!!Form::label('Autorización: ')!!}
{!!Form::text('authorization',null, ['id'=>'authorization', 'class'=>'form-control', 'placeholder'=>'Autorización'])!!}
</div>
<div class='form-group'>
{!!Form::label('Descuento: ')!!}
{!!Form::text('discount',null, ['id'=>'discount', 'class'=>'form-control', 'placeholder'=>'Descuento'])!!}
</div>
<div class='form-group'>
{!!Form::label('Activo: ')!!}
{!!Form::text('active_id',null, ['id'=>'active_id', 'class'=>'form-control', 'placeholder'=>'Activo'])!!}
</div>
<div class='form-group'>
{!!Form::label('Clasificación: ')!!}
{!!Form::text('clasification_id',null, ['id'=>'clasification_id', 'class'=>'form-control', 'placeholder'=>'Clasificación'])!!}
</div>
<div class='form-group'>
{!!Form::label('Método de pago: ')!!}
{!!Form::text('methodPayment',null, ['id'=>'methodPayment_id', 'class'=>'form-control', 'placeholder'=>'Método de pago'])!!}
</div>
<div class='form-group'>
{!!Form::label('Método de pago P: ')!!}
{!!Form::text('methodPaymentP',null, ['id'=>'methodPaymentP_id', 'class'=>'form-control', 'placeholder'=>'Método de pago P'])!!}
</div>
<div class='form-group'>
{!!Form::label('Retefte: ')!!}
{!!Form::text('retefte',null, ['id'=>'retefte', 'class'=>'form-control', 'placeholder'=>'Retefte'])!!}
</div>
<div class='form-group'>
{!!Form::label('ICA: ')!!}
{!!Form::text('ica',null, ['id'=>'ica', 'class'=>'form-control', 'placeholder'=>'ICA'])!!}
</div>
<div class='form-group'>
{!!Form::label('Régimen: ')!!}
{!!Form::text('regimen_id',null, ['id'=>'regimen_id', 'class'=>'form-control', 'placeholder'=>'Régimen'])!!}
</div>
<div class='form-group'>
{!!Form::label('Lista: ')!!}
{!!Form::text('list_id',null, ['id'=>'list_id', 'class'=>'form-control', 'placeholder'=>'Lista'])!!}
</div>
<div class='form-group'>
{!!Form::label('Zona: ')!!}
{!!Form::text('zone_id',null, ['id'=>'zone_id', 'class'=>'form-control', 'placeholder'=>'Zona'])!!}
</div>
<div class='form-group'>
{!!Form::label('Zona: ')!!}
{!!Form::text('zone',null, ['id'=>'zone', 'class'=>'form-control', 'placeholder'=>'Zona'])!!}
</div>
<div class='form-group'>
{!!Form::label('Vendedor: ')!!}
{!!Form::text('seller_id',null, ['id'=>'seller_id', 'class'=>'form-control', 'placeholder'=>'Vendedor'])!!}
</div>
<div class='form-group'>
{!!Form::label('Dirección Oficina: ')!!}
{!!Form::text('addressOffice',null, ['id'=>'addressOffice', 'class'=>'form-control', 'placeholder'=>'Dirección Oficina'])!!}
</div>
<div class='form-group'>
{!!Form::label('Teléfono Oficina: ')!!}
{!!Form::text('phoneOffice',null, ['id'=>'phoneOffice', 'class'=>'form-control', 'placeholder'=>'Teléfono Oficina'])!!}
</div>
<div class='form-group'>
{!!Form::label('Genero/ Sexo: ')!!}
{!!Form::text('gender_id',null, ['id'=>'gender_id', 'class'=>'form-control', 'placeholder'=>'Genero/ Sexo'])!!}
</div>
<div class='form-group'>
{!!Form::label('Observación1: ')!!}
{!!Form::text('observation1',null, ['id'=>'observation1', 'class'=>'form-control', 'placeholder'=>'Observación1'])!!}
</div>
<div class='form-group'>
{!!Form::label('Observación2: ')!!}
{!!Form::text('observation2',null, ['id'=>'observation2', 'class'=>'form-control', 'placeholder'=>'Observación2'])!!}
</div>
<div class='form-group'>
{!!Form::label('Observación3: ')!!}
{!!Form::text('observation3',null, ['id'=>'observation3', 'class'=>'form-control', 'placeholder'=>'Observación3'])!!}
</div>
<div class='form-group'>
{!!Form::label('Nombres: ')!!}
{!!Form::text('name',null, ['id'=>'name', 'class'=>'form-control', 'placeholder'=>'Nombres'])!!}
</div>
<div class='form-group'>
{!!Form::label('Apellidos: ')!!}
{!!Form::text('lastname',null, ['id'=>'lastname', 'class'=>'form-control', 'placeholder'=>'Apellidos'])!!}
</div>
<div class='form-group'>
{!!Form::label('Nombre de la empresa: ')!!}
{!!Form::text('businessName',null, ['id'=>'businessName', 'class'=>'form-control', 'placeholder'=>'Nombre de la empresa'])!!}
</div>
<div class='form-group'>
{!!Form::label('Referencia1: ')!!}
{!!Form::text('reference1',null, ['id'=>'reference1', 'class'=>'form-control', 'placeholder'=>'Referencia1'])!!}
</div>
<div class='form-group'>
{!!Form::label('Referencia2: ')!!}
{!!Form::text('reference2',null, ['id'=>'reference2', 'class'=>'form-control', 'placeholder'=>'Referencia2'])!!}
</div>
<div class='form-group'>
{!!Form::label('Referencia3: ')!!}
{!!Form::text('reference3',null, ['id'=>'reference3', 'class'=>'form-control', 'placeholder'=>'Referencia3'])!!}
</div>
<div class='form-group'>
{!!Form::label('Referencia4: ')!!}
{!!Form::text('reference4',null, ['id'=>'reference4', 'class'=>'form-control', 'placeholder'=>'Referencia4'])!!}
</div>
<div class='form-group'>
{!!Form::label('Referencia5: ')!!}
{!!Form::text('reference5',null, ['id'=>'reference5', 'class'=>'form-control', 'placeholder'=>'Referencia5'])!!}
</div>
<div class='form-group'>
{!!Form::label('Descuento Financiación: ')!!}
{!!Form::text('discountFina',null, ['id'=>'discountFina', 'class'=>'form-control', 'placeholder'=>'Descuento Financiación'])!!}
</div>
<div class='form-group'>
{!!Form::label('Término descuento: ')!!}
{!!Form::text('termFunding',null, ['id'=>'termFunding', 'class'=>'form-control', 'placeholder'=>'Término descuento'])!!}
</div>
<div class='form-group'>
{!!Form::label('Cod Pai: ')!!}
{!!Form::text('codePai_id',null, ['id'=>'codePai_id', 'class'=>'form-control', 'placeholder'=>'Cod Pai'])!!}
</div>
<div class='form-group'>
{!!Form::label('Cod Pai2: ')!!}
{!!Form::text('codePai2_id',null, ['id'=>'codePai2_id', 'class'=>'form-control', 'placeholder'=>'Cod Pai2'])!!}
</div>
<div class='form-group'>
{!!Form::label('Promp24: ')!!}
{!!Form::text('promp24',null, ['id'=>'promp24', 'class'=>'form-control', 'placeholder'=>'Promp24'])!!}
</div>
<div class='form-group'>
{!!Form::label('Promp12: ')!!}
{!!Form::text('promp12',null, ['id'=>'promp12', 'class'=>'form-control', 'placeholder'=>'Promp12'])!!}
</div>
<div class='form-group'>
{!!Form::label('Promp6: ')!!}
{!!Form::text('promp6',null, ['id'=>'promp6', 'class'=>'form-control', 'placeholder'=>'Promp6'])!!}
</div>
<div class='form-group'>
{!!Form::label('Cheqd24: ')!!}
{!!Form::text('cheqd24',null, ['id'=>'cheqd24', 'class'=>'form-control', 'placeholder'=>'Cheqd24'])!!}
</div>
<div class='form-group'>
{!!Form::label('Cheqd12: ')!!}
{!!Form::text('cheqd12',null, ['id'=>'cheqd12', 'class'=>'form-control', 'placeholder'=>'Cheqd12'])!!}
</div>
<div class='form-group'>
{!!Form::label('Cheqd6: ')!!}
{!!Form::text('cheqd6',null, ['id'=>'cheqd6', 'class'=>'form-control', 'placeholder'=>'Cheqd6'])!!}
</div>
<div class='form-group'>
{!!Form::label('Estado: ')!!}
{!!Form::text('status',null, ['id'=>'status', 'class'=>'form-control', 'placeholder'=>'Estado'])!!}
</div>
<div class='form-group'>
{!!Form::label('cree: ')!!}
{!!Form::text('cree',null, ['id'=>'cree', 'class'=>'form-control', 'placeholder'=>'Cree'])!!}
</div>
<div class='form-group'>
{!!Form::label('Creevtas: ')!!}
{!!Form::text('creevtas_id',null, ['id'=>'creevtas_id', 'class'=>'form-control', 'placeholder'=>'Creevtas'])!!}
</div>
<div class='form-group'>
{!!Form::label('Cifin: ')!!}
{!!Form::text('cifin_id',null, ['id'=>'cifin_id', 'class'=>'form-control', 'placeholder'=>'Cifin'])!!}
</div>
{!!Form::submit('Guardar',['id'=>'Guardar', 'class'=>'btn btn-primary'])!!}
<script type="text/javascript">
$("#frm").submit(function (event) {
    event.preventDefault();
    $('.loading').show();
    var form = $(this);
    var data = new FormData($(this)[0]);
    var url = form.attr("action");
    $.ajax({
        type: "POST",
        url: url,
        data: data,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
            if (data.fail) {
                $('#frm input.required, #frm textarea.required').each(function () {
                    index = $(this).attr('name');
                    if (index in data.errors) {
                        $("#form-" + index + "-error").addClass("has-error");
                        $("#" + index + "-error").html(data.errors[index]);
                    }
                    else {
                        $("#form-" + index + "-error").removeClass("has-error");
                        $("#" + index + "-error").empty();
                    }
                });
                $('#focus').focus().select();
            } else {
                $(".has-error").removeClass("has-error");
                $(".help-block").empty();
                $('.loading').hide();
                ajaxLoad(data.url, data.content);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            alert(errorThrown);
        }
    });
    return false;
});
</script>