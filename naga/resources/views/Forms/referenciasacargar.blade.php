<br>

<div class="col-md-12 col-sm-12">
	<div class="col-md-6 col-sm-6">
		<div class="form-group">
			{{Form::label('Vendedores')}}<br>
			<select multiple class="chosen-tag" id="vendedores" name="vendedores[]" required>
		      	@foreach($vendedores as $vendedor)
		        	<option value="{{$vendedor->code}}">{{$vendedor->name}}</option>
		      	@endforeach
		    </select>
		</div>
	</div>
	<div class="col-md-6 col-sm-6">
		<div class="form-group">
			{{Form::label('Referencias')}}<br>
			{{Form::select('referencias',$referencias,null,array('id'=>'referencetoload','multiple'=>'multiple','name'=>'referencias[]','width'=>'100'))}}
		</div>
	</div>
</div>
<script type="text/javascript">
	$("#frm").submit(function (event) {
	    event.preventDefault();
	    $('.loading').show();
	    var form = $(this);
	    var data = new FormData($(this)[0]);
	    var url = form.attr("action");
	    $.ajax({
	        type: "POST",
	        url: url,
	        data: data,
	        async: false,
	        cache: false,
	        contentType: false,
	        processData: false,
	        success: function (data) {
	            if (data.fail) {
	                $('#frm input.required, #frm textarea.required').each(function () {
	                    index = $(this).attr('name');
	                    if (index in data.errors) {
	                        $("#form-" + index + "-error").addClass("has-error");
	                        $("#" + index + "-error").html(data.errors[index]);
	                    }
	                    else {
	                        $("#form-" + index + "-error").removeClass("has-error");
	                        $("#" + index + "-error").empty();
	                    }
	                });
	                $('#focus').focus().select();
	            } 
	            else 
	            {
	                $(".has-error").removeClass("has-error");
	                $(".help-block").empty();
	                $('.loading').hide();
	                ajaxLoad(data.url, data.content);
	            }
	        },
	        error: function (xhr, textStatus, errorThrown) {
	            alert(errorThrown);
	        }
	    });
	    return false;
	});

	$("#referencetoload").css({'width':'70%','height':'250px'});
	$("#vendedores").css({'width':'70%','height':'250px'});
</script>