<div class="tab-content">
    <div class="tab-pane" id="supply1">
    	<div class="row">
        	<div class="col-sm-12">
            	<h5 class="info-text"> </h5>
        	</div>
            <div class="col-sm-3 col-sm-offset-1">
                <div class='form-group'>
                    {!!Form::label('Codigo: ')!!}
                    {!!Form::text('code',null, ['id'=>'code', 'class'=>'form-control', 'placeholder'=>'code'])!!}
                </div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
					{!!Form::label('Descripción: ')!!}
					{!!Form::text('description',null, ['id'=>'description', 'class'=>'form-control', 'placeholder'=>'description'])!!}
				</div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
					{!!Form::label('clasificación: ')!!}
					{!!Form::text('clasification',null, ['id'=>'clasification', 'class'=>'form-control', 'placeholder'=>'clasification'])!!}
				</div>
            </div>
            <div class="col-sm-3 col-sm-offset-1">
                <div class='form-group'>
					{!!Form::label('Unidad de medida: ')!!}
					{!!Form::select('unitMeasurement',$measures,null, ['id'=>'unitMeasurement', 'class'=>'form-control', 'placeholder'=>'Escoger uno...'])!!}
				</div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
					{!!Form::label('Unidad de empaque: ')!!}
					{!!Form::number('packagingUnit',null, ['id'=>'nitTer', 'class'=>'form-control', 'placeholder'=>'0'])!!}
				</div>
            </div>
            <div class="col-sm-3">
            	<div class='form-group'>
					<div class='form-group'>
						{!!Form::label('Empaque: ')!!}
						{!!Form::text('package',null, ['id'=>'package', 'class'=>'form-control', 'placeholder'=>'package'])!!}
					</div>
				</div>
            </div>
            <div class="col-sm-3 col-sm-offset-1">
            	<div class='form-group'>
					<div class='form-group'>
						{!!Form::label('Proveedor1: ')!!}
						{!!Form::text('provider1',null, ['id'=>'provider1', 'class'=>'form-control', 'placeholder'=>'provider1'])!!}
					</div>
				</div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
					{!!Form::label('Proveedor2: ')!!}
					{!!Form::text('provider2',null, ['id'=>'provider2', 'class'=>'form-control', 'placeholder'=>'provider2'])!!}
				</div>
            </div>
            <div class="col-sm-3">
            	<div class='form-group'>
					{!!Form::label('Proveedor3: ')!!}
					{!!Form::text('provider3',null, ['id'=>'provider3', 'class'=>'form-control', 'placeholder'=>'provider3'])!!}
				</div>
            </div>
            <div class="col-sm-3 col-sm-offset-1">
            	<div class='form-group'>
					{!!Form::label('Precio1: ')!!}
					{!!Form::text('price1',null, ['id'=>'price1', 'class'=>'form-control', 'placeholder'=>'price1'])!!}
				</div>
            </div>

            <div class="col-sm-3">
                <div class='form-group'>
					{!!Form::label('Precio2: ')!!}
					{!!Form::text('price2',null, ['id'=>'price2', 'class'=>'form-control', 'placeholder'=>'price2'])!!}
				</div>
            </div>
            <div class="col-sm-3">
            	<div class='form-group'>
					{!!Form::label('Precio3: ')!!}
					{!!Form::text('price3',null, ['id'=>'price3', 'class'=>'form-control', 'placeholder'=>'price3'])!!}
				</div>
            </div>
    	</div>
	</div>
    <div class="tab-pane" id="supply2">
    	<div class="row">
        	<div class="col-sm-12">
            	<h5 class="info-text"> </h5>
        	</div>
            <div class="col-sm-3 col-sm-offset-1">
                <div class='form-group'>
                    {!!Form::label('Tiempo de entrega 1: ')!!}
                    {!!Form::text('deliveryTime1',null, ['id'=>'deliveryTime1', 'class'=>'form-control', 'placeholder'=>'deliveryTime1'])!!}
                </div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
					{!!Form::label('Tiempo de entrega 2: ')!!}
					{!!Form::text('deliveryTime2',null, ['id'=>'deliveryTime2', 'class'=>'form-control', 'placeholder'=>'deliveryTime2'])!!}
				</div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
					{!!Form::label('Tiempo de entrega 3: ')!!}
					{!!Form::text('deliveryTime3',null, ['id'=>'deliveryTime3', 'class'=>'form-control', 'placeholder'=>'deliveryTime3'])!!}
				</div>
            </div>
            <div class="col-sm-3 col-sm-offset-1">
                <div class='form-group'>
					{!!Form::label('optionProvider: ')!!}
					{!!Form::text('optionProvider',null, ['id'=>'optionProvider', 'class'=>'form-control', 'placeholder'=>'optionProvider'])!!}
				</div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
					{!!Form::label('Proveedor actual: ')!!}
					{!!Form::text('currentProvider',null, ['id'=>'currentProvider', 'class'=>'form-control', 'placeholder'=>'currentProvider'])!!}
				</div>
            </div>
            <div class="col-sm-3">
            	<div class='form-group'>
					{!!Form::label('Precio actual: ')!!}
					{!!Form::text('currentPrice',null, ['id'=>'currentPrice', 'class'=>'form-control', 'placeholder'=>'currentPrice'])!!}
				</div>
            </div>
            <div class="col-sm-3 col-sm-offset-1">
            	<div class='form-group'>
					{!!Form::label('Tiempo de entrega actual: ')!!}
					{!!Form::text('currentTime',null, ['id'=>'currentTime', 'class'=>'form-control', 'placeholder'=>'currentTime'])!!}
				</div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
					{!!Form::label('Consumo mensual: ')!!}
					{!!Form::text('monthlyConsumption',null, ['id'=>'monthlyConsumption', 'class'=>'form-control', 'placeholder'=>'monthlyConsumption'])!!}
				</div>
            </div>
            <div class="col-sm-3">
            	<div class='form-group'>
					{!!Form::label('Consumo diario: ')!!}
					{!!Form::text('dailyConsumption',null, ['id'=>'dailyConsumption', 'class'=>'form-control', 'placeholder'=>'dailyConsumption'])!!}
				</div>
            </div>
            <div class="col-sm-3 col-sm-offset-1">
            	<div class='form-group'>
					{!!Form::label('Producción diaria: ')!!}
					{!!Form::text('dailyProduction',null, ['id'=>'dailyProduction', 'class'=>'form-control', 'placeholder'=>'dailyProduction'])!!}
				</div>
            </div>

            <div class="col-sm-3">
                <div class='form-group'>
					{!!Form::label('% de Producción: ')!!}
					{!!Form::text('rateProduction',null, ['id'=>'rateProduction', 'class'=>'form-control', 'placeholder'=>'rateProduction'])!!}
				</div>
            </div>
            <div class="col-sm-3">
            	<div class='form-group'>
					{!!Form::label('% de Estilo: ')!!}
					{!!Form::text('rateStyle',null, ['id'=>'rateStyle', 'class'=>'form-control', 'placeholder'=>'rateStyle'])!!}
				</div>
            </div>
    	</div>
	</div>
    <div class="tab-pane" id="supply3">
        <div class="row">
        	<div class="col-sm-12">
            	<h5 class="info-text"> </h5>
        	</div>
            <div class="col-sm-3 col-sm-offset-1">
                <div class='form-group'>
                    {!!Form::label('% de Color: ')!!}
                    {!!Form::text('rateColor',null, ['id'=>'rateColor', 'class'=>'form-control', 'placeholder'=>'rateColor'])!!}
                </div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
					{!!Form::label('% de Talla o medida: ')!!}
					{!!Form::text('rateSize',null, ['id'=>'rateSize', 'class'=>'form-control', 'placeholder'=>'rateSize'])!!}
				</div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
					{!!Form::label('Consumo de prenda 1: ')!!}
					{!!Form::text('garment1Consumption',null, ['id'=>'garment1Consumption', 'class'=>'form-control', 'placeholder'=>'garment1Consumption'])!!}
				</div>
            </div>
            <div class="col-sm-3 col-sm-offset-1">
                <div class='form-group'>
					{!!Form::label('Consumo hora: ')!!}
					{!!Form::text('hourComsumption',null, ['id'=>'hourComsumption', 'class'=>'form-control', 'placeholder'=>'hourComsumption'])!!}
				</div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
					{!!Form::label('Stock rojo: ')!!}
					{!!Form::text('redStock',null, ['id'=>'redStock', 'class'=>'form-control', 'placeholder'=>'redStock'])!!}
				</div>
            </div>
            <div class="col-sm-3">
            	<div class='form-group'>
					{!!Form::label('Stock amarillo: ')!!}
					{!!Form::text('yellowStock',null, ['id'=>'yellowStock', 'class'=>'form-control', 'placeholder'=>'yellowStock'])!!}
				</div>
            </div>
            <div class="col-sm-3 col-sm-offset-1">
            	<div class='form-group'>
					{!!Form::label('Stock verde: ')!!}
					{!!Form::text('greenStock',null, ['id'=>'greenStock', 'class'=>'form-control', 'placeholder'=>'greenStock'])!!}
				</div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
					{!!Form::label('Stock azul: ')!!}
					{!!Form::text('blueStock',null, ['id'=>'blueStock', 'class'=>'form-control', 'placeholder'=>'blueStock'])!!}
				</div>
            </div>
            <div class="col-sm-3">
            	<div class='form-group'>
					{!!Form::label('Stock rojo 2: ')!!}
					{!!Form::text('red2Stock',null, ['id'=>'red2Stock', 'class'=>'form-control', 'placeholder'=>'red2Stock'])!!}
				</div>
            </div>
            <div class="col-sm-3 col-sm-offset-1">
            	<div class='form-group'>
					{!!Form::label('Stock amarillo 2: ')!!}
					{!!Form::text('yellow2Stock',null, ['id'=>'yellow2Stock', 'class'=>'form-control', 'placeholder'=>'yellow2Stock'])!!}
				</div>
            </div>

            <div class="col-sm-3">
                <div class='form-group'>
					{!!Form::label('Stock verde 2: ')!!}
					{!!Form::text('green2Stock',null, ['id'=>'green2Stock', 'class'=>'form-control', 'placeholder'=>'green2Stock'])!!}
				</div>
            </div>
            <div class="col-sm-3">
            	<div class='form-group'>
					{!!Form::label('Stock azul 2: ')!!}
					{!!Form::text('blue2Stock',null, ['id'=>'blue2Stock', 'class'=>'form-control', 'placeholder'=>'blue2Stock'])!!}
				</div>
            </div>
    	</div>
    </div>
	<div class="tab-pane" id="supply4">
        <div class="row">
        	<div class="col-sm-12">
            	<h5 class="info-text"> </h5>
        	</div>
            <div class="col-sm-3 col-sm-offset-1">
                <div class='form-group'>
                    {!!Form::label('Tipo de Insumo: ')!!}
                    {!!Form::text('supplyTipe',null, ['id'=>'supplyTipe', 'class'=>'form-control', 'placeholder'=>'supplyTipe'])!!}
                </div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
					{!!Form::label('Camisa: ')!!}
					{!!Form::text('shirt',null, ['id'=>'shirt', 'class'=>'form-control', 'placeholder'=>'shirt'])!!}
				</div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
					{!!Form::label('Camiseta: ')!!}
					{!!Form::text('tShirt',null, ['id'=>'tShirt', 'class'=>'form-control', 'placeholder'=>'tShirt'])!!}
				</div>
            </div>
            <div class="col-sm-3 col-sm-offset-1">
                <div class='form-group'>
					{!!Form::label('Jean: ')!!}
					{!!Form::text('Jean',null, ['id'=>'Jean', 'class'=>'form-control', 'placeholder'=>'Jean'])!!}
				</div>
            </div>
            <div class="col-sm-3">
                <div class='form-group'>
					{!!Form::label('bermuda: ')!!}
					{!!Form::text('bermudaShort',null, ['id'=>'bermudaShort', 'class'=>'form-control', 'placeholder'=>'bermudaShort'])!!}
				</div>
            </div>
            <div class="col-sm-3">
            	<div class='form-group'>
					{!!Form::label('Clásico: ')!!}
					{!!Form::text('classic',null, ['id'=>'classic', 'class'=>'form-control', 'placeholder'=>'classic'])!!}
				</div>
            </div>
    	</div>
    </div>
</div>
<script type="text/javascript">
$("#frm").submit(function (event) {
    event.preventDefault();
    $('.loading').show();
    var form = $(this);
    var data = new FormData($(this)[0]);
    var url = form.attr("action");
    $.ajax({
        type: "POST",
        url: url,
        data: data,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
            if (data.fail) {
                $('#frm input.required, #frm textarea.required').each(function () {
                    index = $(this).attr('name');
                    if (index in data.errors) {
                        $("#form-" + index + "-error").addClass("has-error");
                        $("#" + index + "-error").html(data.errors[index]);
                    }
                    else {
                        $("#form-" + index + "-error").removeClass("has-error");
                        $("#" + index + "-error").empty();
                    }
                });
                $('#focus').focus().select();
            } else {
                $(".has-error").removeClass("has-error");
                $(".help-block").empty();
                $('.loading').hide();
                ajaxLoad(data.url, data.content);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            alert(errorThrown);
        }
    });
    return false;
});
</script>