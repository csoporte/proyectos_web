<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            {!!Form::label('Código: ')!!}
            {!!Form::text('codigo',null, ['class'=>'form-control', 'placeholder'=>'codigo'])!!}
        </div>    
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {!!Form::label('Codigo Internacional: ')!!}
            {!!Form::text('codigointer',null, ['class'=>'form-control', 'placeholder'=>'Codigo Int.'])!!}
        </div>    
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {!!Form::label('Nombre: ')!!}
            {!!Form::text('nombre',null, ['class'=>'form-control', 'placeholder'=>'nombre'])!!}
        </div>    
    </div>
    <div class="col-sm-12">
        <div class="form-group">
            {!!Form::label('Observaciones: ')!!}
            {!!Form::textarea('observaciones',null, ['class'=>'form-control', 'placeholder'=>'observaciones', 'rows'=>'9'])!!}
        </div>
    </div>
    <div class="col-sm-12">
        {!!Form::submit('Guardar',['id'=>'Guardar', 'class'=>'btn btn-primary'])!!}
    </div>
</div>
<script type="text/javascript">
    $("#frm").submit(function (event) {
        event.preventDefault();
        $('.loading').show();
        var form = $(this);
        var data = new FormData($(this)[0]);
        var url = form.attr("action");
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.fail) {
                    $('#frm input.required, #frm textarea.required').each(function () {
                        index = $(this).attr('name');
                        if (index in data.errors) {
                            $("#form-" + index + "-error").addClass("has-error");
                            $("#" + index + "-error").html(data.errors[index]);
                        }
                        else {
                            $("#form-" + index + "-error").removeClass("has-error");
                            $("#" + index + "-error").empty();
                        }
                    });
                    $('#focus').focus().select();
                } else {
                    $(".has-error").removeClass("has-error");
                    $(".help-block").empty();
                    $('.loading').hide();
                    ajaxLoad(data.url, data.content);
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                alert(errorThrown);
            }
        });
        return false;
    });
</script>