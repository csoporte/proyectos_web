<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            {!!Form::label('Código: ')!!}
            {!!Form::text('codigo',null, ['id'=>'codigo','class'=>'form-control', 'placeholder'=>'codigo'])!!}
        </div>    
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {!!Form::label('Nombre: ')!!}
            {!!Form::text('nombre',null, ['id'=>'nombre','class'=>'form-control', 'placeholder'=>'nombre'])!!}
        </div>    
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {!!Form::label('Almacen:')!!}
            {!!Form::select('almacen',$bodegas,null,['id'=>'almacen','class'=>'form-control','placeholder'=>'Seleccionar uno...'])!!}
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {!!Form::label('Temporada:')!!}
            {!!Form::select('temporada',$temporadas,null,['id'=>'temporada','class'=>'form-control','placeholder'=>'Seleccionar uno...'])!!}
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {!!Form::label('Categoria:')!!}
            {!!Form::select('categoria',['coleccion'=>'Colección','linea'=>'Línea'],null,['id'=>'categoria','class'=>'form-control','placeholder'=>'Seleccionar...'])!!}
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {!!Form::label('Tipo: ')!!}
            {!!Form::select('tipo',['M' =>'Masculino','F' => 'Femenino'],null, ['id'=>'tipo','class'=>'form-control', 'placeholder'=>'Seleccione uno...', 'required'=>True])!!}
        </div>    
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {!!Form::label('Pieza: ')!!}
            {!!Form::select('pieza',['superior' => 'Superior','inferior' => 'Inferior'],null, ['id'=>'pieza','class'=>'form-control', 'placeholder'=>'Seleccionar uno ...', 'required'=>True, 'disabled'=>True])!!}
        </div>    
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {!!Form::label('Grupo o Prenda')!!}
            {!!Form::select('grupo',$grupos,null,['id'=>'grupo','class'=>'form-control','placeholder'=>'Seleccionar...'])!!}
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {!!Form::label('Subgrupo o Estilo')!!}
            {!!Form::select('subgrupo',$subgrupos,null,['id'=>'subgrupo','class'=>'form-control','placeholder'=>'Seleccionar...'])!!}
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {!!Form::label('Silueta')!!}
            {!!Form::select('silueta',$siluetas,null,['id'=>'silueta','class'=>'form-control','placeholder'=>'Seleccionar...'])!!}
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {!!Form::label('Colores: ')!!}<br>
            {!!Form::text('colores',null, ['class'=>'form-control', 'placeholder'=>'# de colores'])!!}
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {!!Form::label('Tallas: ')!!}
            <div id="tallasdiv">
                @if($bandera==1)
                <?php
                    $i=0;
                ?>
                    @foreach($tallasfin as $talla)
                        @if($i==0)
                            {!!Form::checkbox('talla[]',$talla[0],$talla[2],['disabled'=>true])!!}&nbsp;{!!Form::label($talla[1])!!}
                            <?php
                                $i=$i+1;
                            ?>
                        @else
                            @if(($i==3) or ($i % 3===0))
                                {!!Form::checkbox('talla[]',$talla[0],$talla[2],['disabled'=>true])!!}&nbsp;{!!Form::label($talla[1])!!}<br> 
                                <?php
                                    $i=0;
                                ?>
                            @else
                                {!!Form::checkbox('talla[]',$talla[0],$talla[2],['disabled'=>true])!!}&nbsp;{!!Form::label($talla[1])!!}
                                <?php
                                    $i=$i+1;
                                ?>
                            @endif
                        @endif
                    @endforeach
                @else
                    {!!Form::text('tallas',null, ['class'=>'form-control', 'placeholder'=>'tallas', 'disabled'=>True])!!}
                @endif
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {!!Form::label('Se vende: ')!!}<br>
            Surtido&nbsp;{!!Form::radio('formaventa','surtido', true )!!}&nbsp;&nbsp;&nbsp;Xcolor&nbsp;{!!Form::radio('formaventa','xcolor', false)!!}
        </div>
    </div>
    <div class="col-sm-8">
        <div class="form-group">
            {!!Form::label('Descripción: ')!!}<br>
            {!!Form::text('description',null, ['id'=>'description','class'=>'form-control', 'placeholder'=>'Descripción'])!!}
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {!!Form::label('Muestra Física: ')!!}<br>
            Si&nbsp;{!!Form::radio('muestra',1, false )!!}&nbsp;&nbsp;&nbsp;No&nbsp;{!!Form::radio('muestra',0, false)!!}
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {!!Form::label('Fecha: ')!!}<br>
            {!!Form::date('fecharef',null, ['class'=>'form-control'])!!}
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {!!Form::label('Composición: ')!!}<br>
            {!!Form::text('composition',null, ['class'=>'form-control', 'placeholder'=>'Composición'])!!}
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {!!Form::label('Costo 1:')!!}<br>
            {!!Form::number('costohisto',null, ['id'=>'costohisto','class'=>'form-control', 'placeholder'=>'$$$'])!!}
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {!!Form::label('Costo 2:')!!}<br>
            {!!Form::number('costoinve',null, ['id'=>'costoinve','class'=>'form-control', 'placeholder'=>'$$$'])!!}
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {!!Form::label('Precio Tiquete:')!!}<br>
            {!!Form::number('precio_tiquete',null, ['id'=>'precio_tiquete','class'=>'form-control', 'placeholder'=>'$$$'])!!}
        </div>
    </div>
    <div class="col-sm-12">
        <div id="precios">
            <div id="fillprecios0">
                <div class="form-group">
                    @if($bandera==1)
                        <?php
                            $i=0;
                        ?>
                        @foreach($precios as $precio)
                            @if($i==0)
                                {!!Form::label('Precio: ', null, ['class'=>'control-label col-md-3 col-sm-3 col-xs-12','for'=>'namePrecio'])!!}
                                <div class="col-md-9 col-sm-9 col-xs-9">
                                    {!!Form::number('precio[]',$precio->price, ['class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'$$$'])!!}
                                </div>
                                <!--a class="btn btn-success" id="fl"><i class="fa fa-plus"></i> </a--><br>
                                <?php
                                    $i=$i+1;
                                ?>
                            @else
                                {!!Form::label('Precio: ', null, ['class'=>'control-label col-md-3 col-sm-3 col-xs-12','for'=>'namePrecio'])!!}
                                <div class="col-md-9 col-sm-9 col-xs-9">
                                    {!!Form::number('precio[]',$precio->price, ['class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'$$$'])!!}
                                </div>
                                <!--a class="btn btn-danger" id="fl" id='Subfl'><i class="fa fa-minus"></i> </a>--><br>
                            @endif
                        @endforeach
                    @else
                        {!!Form::label('Precio: ', null, ['class'=>'control-label col-md-3 col-sm-3 col-xs-12','for'=>'namePrecio'])!!}
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            {!!Form::number('precio[]',null, ['class'=>'form-control col-md-7 col-xs-12', 'placeholder'=>'$$$'])!!}
                         </div>
                        <a class="btn btn-success" id="fl"><i class="fa fa-plus"></i> </a>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12">
        <div class="form-group">
            {!!Form::label('Observaciones: ')!!}
            {!!Form::textarea('observation',null, ['class'=>'form-control', 'placeholder'=>'observaciones', 'rows'=>'3'])!!}
        </div>
    </div>
    <div class="col-sm-12">
        {!!Form::submit('Guardar',['id'=>'Guardar', 'class'=>'btn btn-primary'])!!}
    </div>
</div>
<script type="text/javascript">
    $("#frm").submit(function (event) {
        event.preventDefault();
        $('.loading').show();
        var form = $(this);
        var data = new FormData($(this)[0]);
        var url = form.attr("action");
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.fail) {
                    $('#frm input.required, #frm textarea.required').each(function () {
                        index = $(this).attr('name');
                        if (index in data.errors) {
                            $("#form-" + index + "-error").addClass("has-error");
                            $("#" + index + "-error").html(data.errors[index]);
                        }
                        else {
                            $("#form-" + index + "-error").removeClass("has-error");
                            $("#" + index + "-error").empty();
                        }
                    });
                    $('#focus').focus().select();
                } else {
                    $(".has-error").removeClass("has-error");
                    $(".help-block").empty();
                    $('.loading').hide();
                    ajaxLoad(data.url, data.content);
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                alert(errorThrown);
            }
        });
        return false;
    });

    $('#tipo').on('change', function(e)
    {
        var tipo = e.target.value;
        //console.log(tipo);
        $('#pieza').attr('disabled',false);
    });

    $('#pieza').on('change',function(e){
        var tipo= $('#tipo').val();
        var pieza = e.target.value;
        //console.log(tipo);
        //console.log(silueta);
        $.get('/Traertallas?tipo='+tipo+'&pieza='+pieza, function(data){
            //console.log(data);
            $("#tallasdiv").empty();
            /*$.each(data,function(index, talla){
                //console.log(talla);
                $('#tallasdiv').append('<input type="checkbox" name="talla[]" value="'+talla.id+'"/> ' + talla.talla + '<br />');
            });*/

            $.each(data,function(index, talla){
                //console.log(talla);
                if(index==0)
                {
                    $('#tallasdiv').append('<input type="checkbox" name="talla[]" value="'+talla.id+'"/> ' + talla.talla + '&nbsp;&nbsp;');       
                }
                else 
                {
                    if(index==4 || (index % 4)===0)
                    {
                        $('#tallasdiv').append('<input type="checkbox" name="talla[]" value="'+talla.id+'"/> ' + talla.talla + '<br />');
                        index=1;    
                    }
                    else
                    {
                        $('#tallasdiv').append('<input type="checkbox" name="talla[]" value="'+talla.id+'"/> ' + talla.talla + '&nbsp;&nbsp;');
                        index=index+1;
                    }
                }
            });
        });
    });

    $('#fl').click(function(e) 
    {
        $('#precios').append("<div id='fillprecios'><div class='form-group'><label class='control-label col-md-3 col-sm-3 col-xs-12' for='namePrecio'>Precio: </label><div class='col-md-6 col-sm-6 col-xs-12'><input class= 'form-control col-md-7 col-xs-12', placeholder='$$$' type='text' name='precio[]'></div><a class='btn btn-danger' id='Subfl'><i class='fa fa-minus'></i> </a></div></div>");
    });
    $(document).on('click', '#Subfl', function()
    {
        $(this).parents('div').eq(1).remove();
    });

    $('#codigo').on('change', function(e)
    {
        var tipo = e.target.value;
        $.get('/datosReferencia?idreferencia='+tipo, function(data){
            if(data.length > 0)
            {
                console.log("Si hay datos");
                console.log(data);
                $('#nombre').val(data[0].NOMBRE);
                $('#almacen').val(data[0].ALMACEN);
                if(data[0].COSTOINV != null)
                    $('#costoinve').val(data[0].COSTOINV);
                if(data[0].COSTOULCO != null)
                    $('#costohisto').val(data[0].COSTOULCO);
                $('#description').val(data[0].DESCRI1);   
            }
            else
            {
                console.log("no hay datos");
                console.log(data);
            }
        });
    });
</script>