<br>
<div class="form-group">
	<div id="referenciasactualizar">
        <!--{Form::label('Referencias')}}<br>
        {Form::select('referencias',$referencias,null,array('id'=>'referenceloading','multiple'=>'multiple','name'=>'referenciasloading[]'))}}-->
        <div class="col-md-12 col-sm-12">
            <div class="form-group">
                {{Form::label('Referencias asociadas')}}<br>
                <select multiple class="chosen-tag" id="referenciasloading" name="referenciasloading[]" required>
                    @foreach($priceslist as $pricelist)
                        <option value="{{$pricelist->id}}">{{$pricelist->reference}} -- {{$pricelist->seller}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $("#frm").submit(function (event) {
        event.preventDefault();
        $('.loading').show();
        var form = $(this);
        var data = new FormData($(this)[0]);
        var url = form.attr("action");
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.fail) {
                    $('#frm input.required, #frm textarea.required').each(function () {
                        index = $(this).attr('name');
                        if (index in data.errors) {
                            $("#form-" + index + "-error").addClass("has-error");
                            $("#" + index + "-error").html(data.errors[index]);
                        }
                        else {
                            $("#form-" + index + "-error").removeClass("has-error");
                            $("#" + index + "-error").empty();
                        }
                    });
                    $('#focus').focus().select();
                } else {
                    $(".has-error").removeClass("has-error");
                    $(".help-block").empty();
                    $('.loading').hide();
                    ajaxLoad(data.url, data.content);
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                alert(errorThrown);
            }
        });
        return false;
    });
    $("#referenciasloading").css({'width':'70%','height':'250px'});
</script>