<br>
<div class="col-md-4 col-sm-4">
    {!!Form::label('Código: ')!!}
    {!!Form::text('code',null, ['class'=>'form-control', 'placeholder'=>'Código'])!!}
</div>
<div class="col-md-4 col-sm-4">
    {!!Form::label('Bodega: ')!!}
    {!!Form::text('warehouse',null, ['class'=>'form-control', 'placeholder'=>'Bodega'])!!}
</div>
<div class="col-md-4 col-sm-4">
    {!!Form::label('Identificación: ')!!}
    {!!Form::text('identification',null, ['class'=>'form-control', 'placeholder'=>'Identificación'])!!}
</div>
<div class="col-md-4 col-sm-4">
    {!!Form::label('Nombres: ')!!}
    {!!Form::text('name',null, ['class'=>'form-control', 'placeholder'=>'name'])!!}
</div>
<div class="col-md-4 col-sm-4">
    {!!Form::label('Dirección: ')!!}
    {!!Form::text('address',null, ['class'=>'form-control', 'placeholder'=>'dirección'])!!}
</div>
<div class="col-md-4 col-sm-4">
    {!!Form::label('Teléfono: ')!!}
    {!!Form::text('phone',null, ['class'=>'form-control', 'placeholder'=>'Teléfono'])!!}
</div>
<div class="col-md-4 col-sm-4">
    {!!Form::label('Celular: ')!!}
    {!!Form::text('cellphone',null, ['class'=>'form-control', 'placeholder'=>'Celular'])!!}
</div>
<div class="col-md-4 col-sm-4">
    {!!Form::label('Correo: ')!!}
    {!!Form::text('email',null, ['class'=>'form-control', 'placeholder'=>'Correo'])!!}
</div>
<div class="col-md-4 col-sm-4">
    {!!Form::label('Zona: ')!!}
    {!!Form::text('zone_id',null, ['class'=>'form-control', 'placeholder'=>'Zona'])!!}
</div>
<div class="col-md-4 col-sm-4">
    {!!Form::label('Tipo usuario: ')!!}
    {!!Form::select('rol',$typeuser,null, ['class'=>'form-control', 'placeholder'=>'Seleccione uno...'])!!}
</div>
<div class="col-md-4 col-sm-4">
    {!!Form::label('Nombre usuario: ')!!}
    {!!Form::text('username',$nombreusuario, ['class'=>'form-control', 'placeholder'=>'username'])!!}
</div>
<div class="col-md-4 col-sm-4">
    {!!Form::label('Password: ')!!}
    {!!Form::password('clave', ['class'=>'form-control', 'placeholder'=>'Zona'])!!}
</div>
<div class="col-md-12 col-sm-12">
    <br>
</div>
<div class="col-md-12 col-sm-12">
    <div class="form-group">
        {!!Form::submit('Guardar',['id'=>'Guardar', 'class'=>'btn btn-primary'])!!}
    </div>
</div>
<script type="text/javascript">
$("#frm").submit(function (event) {
    event.preventDefault();
    $('.loading').show();
    var form = $(this);
    var data = new FormData($(this)[0]);
    var url = form.attr("action");
    $.ajax({
        type: "POST",
        url: url,
        data: data,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
            if (data.fail) {
                $('#frm input.required, #frm textarea.required').each(function () {
                    index = $(this).attr('name');
                    if (index in data.errors) {
                        $("#form-" + index + "-error").addClass("has-error");
                        $("#" + index + "-error").html(data.errors[index]);
                    }
                    else {
                        $("#form-" + index + "-error").removeClass("has-error");
                        $("#" + index + "-error").empty();
                    }
                });
                $('#focus').focus().select();
            } else {
                $(".has-error").removeClass("has-error");
                $(".help-block").empty();
                $('.loading').hide();
                ajaxLoad(data.url, data.content);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            alert(errorThrown);
        }
    });
    return false;
});
</script>