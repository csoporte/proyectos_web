<br>
<div class="form-group">
    {!!Form::label('Grupo: ')!!}
    {!!Form::select('group_id',$groupsupplies,null, ['id'=>'group_id','class'=>'form-control', 'placeholder'=>'Escoger uno...'])!!}
</div>
<div class="form-group">
    {!!Form::label('Código: ')!!}
    {!!Form::text('subgroup',null, ['id'=>'subgroup','class'=>'form-control', 'placeholder'=>'Código'])!!}
</div>
<div class="form-group">
    {!!Form::label('Nombre del subgrupo: ')!!}
    {!!Form::text('subgroupName',null, ['class'=>'form-control', 'placeholder'=>'Subgrupo'])!!}
</div>
    {!!Form::submit('Guardar',['id'=>'Guardar', 'class'=>'btn btn-primary'])!!}
<script type="text/javascript">
$("#frm").submit(function (event) {
    event.preventDefault();
    $('.loading').show();
    var form = $(this);
    var data = new FormData($(this)[0]);
    var url = form.attr("action");
    $.ajax({
        type: "POST",
        url: url,
        data: data,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
            if (data.fail) {
                $('#frm input.required, #frm textarea.required').each(function () {
                    index = $(this).attr('name');
                    if (index in data.errors) {
                        $("#form-" + index + "-error").addClass("has-error");
                        $("#" + index + "-error").html(data.errors[index]);
                    }
                    else {
                        $("#form-" + index + "-error").removeClass("has-error");
                        $("#" + index + "-error").empty();
                    }
                });
                $('#focus').focus().select();
            } else {
                $(".has-error").removeClass("has-error");
                $(".help-block").empty();
                $('.loading').hide();
                ajaxLoad(data.url, data.content);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            alert(errorThrown);
        }
    });
    return false;
});

$('#group_id').on('change', function(e){
    var grupo = e.target.value;
    $.get('/JsonCodGrupo?grupo_id='+grupo, function(data){
        //console.log(data[0].group);
        $('#subgroup').val(data[0].group);
    });
});

</script>