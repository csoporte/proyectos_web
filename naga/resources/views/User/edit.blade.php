@include('alerts.AlertsRequest')
@include('alerts.SuccessRequest')
@include('alerts.ErrorsRequest')
{!!Form::model($user, ['route'=>['Users.update',$user->id], 'id'=>'frm', 'method'=>'PUT', 'autocomplete'=>'off'])!!}
<div class="col-md-4">
	@include('Forms.User')
{!!Form::close()!!}
<br>
{!!Form::open(['route'=>['Users.destroy', $user->id], 'id'=>'frmDelete', 'method'=>'DELETE'])!!}
	<script type="text/javascript">
		$("#frmDelete").submit(function (event) {
		    event.preventDefault();
		    $('.loading').show();
		    var form = $(this);
		    var data = new FormData($(this)[0]);
		    var url = form.attr("action");
		    $.ajax({
		        type: "POST",
		        url: url,
		        data: data,
		        async: false,
		        cache: false,
		        contentType: false,
		        processData: false,
		        success: function (data) {
		            if (data.fail) {
		                $('#frm input.required, #frm textarea.required').each(function () {
		                    index = $(this).attr('name');
		                    if (index in data.errors) {
		                        $("#form-" + index + "-error").addClass("has-error");
		                        $("#" + index + "-error").html(data.errors[index]);
		                    }
		                    else {
		                        $("#form-" + index + "-error").removeClass("has-error");
		                        $("#" + index + "-error").empty();
		                    }
		                });
		                $('#focus').focus().select();
		            } else {
		                $(".has-error").removeClass("has-error");
		                $(".help-block").empty();
		                $('.loading').hide();
		                ajaxLoad(data.url, data.content);
		            }
		        },
		        error: function (xhr, textStatus, errorThrown) {
		            alert(errorThrown);
		        }
		    });
		    return false;
		});
	</script>
	{!!Form::submit('Eliminar',['id'=>'eliminar', 'class'=>'btn btn-danger'])!!}
{!!Form::close()!!}
</div>