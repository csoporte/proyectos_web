@section('content')	
@include('alerts.AlertsRequest')
@include('alerts.SuccessRequest')
@include('alerts.ErrorsRequest')
<aside class="col-md-12">
	<table class="table">
		<thead>
			<th>Nombre</th>
			<th>Correo</th>
			<th>Usuario</th>
			<th>Identificación</th>
			<th>Operación</th>
		</thead>
		@foreach($users as $user)
		<tbody>
			<td>{{$user->name}}</td>
			<td>{{$user->email}}</td>
			<td>{{$user->user}}</td>
			<td>{{$user->identification}}</td>
			<td>{!!link_to_route('Users.edit', $title = 'Editar', $parameters = $user->id, $attributes = ['class'=>'btn btn-primary']);!!}</td>
		</tbody>
		@endforeach		
	</table>
	{!!$users->render()!!}
</aside>