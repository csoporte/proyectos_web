@include('alerts.AlertsRequest')
@include('alerts.SuccessRequest')
@include('alerts.ErrorsRequest')

<script type="text/javascript">
	$(document).ready(function() {
    	setTimeout(function() {
        	$(".animationload").fadeOut(1500);
    	},1000);
	});
</script>

<div>
    <!--   Big container   -->
    <div class="container">
    	<!--div class="animationload">
            <div class="osahanloading"></div>
        </div-->
        <div class="row">
	        <div class="col-sm-12">
	            <!--      Wizard container        -->
	            <div class="wizard-container">
	                <div class="card wizard-card" data-color="blue" id="wizard">
	                    {!!Form::open(['route'=>'Clients.store', 'id'=>'frm', 'method'=>'POST', 'autocomplete'=>'off'])!!}
	                <!--        You can switch " data-color="azure" "  with one of the next bright colors: "blue", "green", "orange", "red"           -->

	                    	<div class="wizard-header">
	                        	<h3 class="wizard-title">Órden de Producción</h3>
	                        	<p class="category">Ingrese todos los campos que sean requeridos.</p>
	                    	</div>

							<div class="wizard-navigation">
								<div class="progress-with-circle">
								     <div class="progress-bar" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="3" style="width: 21%;"></div>
								</div>
								<ul>
		                            <li>
										<a href="#op1" data-toggle="tab">
											<div class="icon-circle">
												<i class="ti-arrow-right"></i>
											</div>
											Información General
										</a>
									</li>
									<li>
										<a href="#op2" data-toggle="tab">
											<div class="icon-circle">
												<i class="ti-arrow-right"></i>
											</div>
											Inferior/Superior
										</a>
									</li>
									<li>
										<a href="#opmed" data-toggle="tab">
											<div class="icon-circle">
												<i class="ti-arrow-right"></i>
											</div>
											Observaciones
										</a>
									</li>
									<li>
										<a href="#opfin" data-toggle="tab">
											<div class="icon-circle">
												<i class="ti-check"></i>
											</div>
											Finalizar
										</a>
									</li>
		                        </ul>
							</div>


								<input id="_token" type="hidden" name="_token" value="{{ csrf_token() }}">
								@include('Forms.op.ProOrder')

	                        <div class="wizard-footer">
	                        	<div class="pull-right">
	                                <input type='button' class='btn btn-next btn-fill btn-primary btn-wd' name='next' value='Siguiente' />
	                      			{!!Form::submit('Finalizar',['id'=>'Finalizar', 'class'=>'btn btn-finish btn-fill btn-primary btn-wd'])!!}
	                            </div>

	                            <div class="pull-left">
	                                <input type='button' class='btn btn-previous btn-default btn-wd' name='previous' value='Atrás' />
	                            </div>
	                            <div class="clearfix"></div>
	                        </div>
	                    {!!Form::close()!!}
	                </div>
	            </div> <!-- wizard container -->
	        </div>
        </div> <!-- row -->
    </div> <!--  big container -->
</div>

	<!--   Core JS Files   -->
	<script src="assets/js/jquery-2.2.4.min.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="assets/js/jquery.bootstrap.wizard.js" type="text/javascript"></script>

	<!--  Plugin for the Wizard -->
	<script src="assets/js/paper-bootstrap-wizard.js" type="text/javascript"></script>

	<!--  More information about jquery.validate here: http://jqueryvalidation.org/	 -->
	<script src="assets/js/jquery.validate.min.js" type="text/javascript"></script>