@include('alerts.AlertsRequest')
@include('alerts.SuccessRequest')
@include('alerts.ErrorsRequest')
<div>
    <!--   Big container   -->
    <div class="container">
        <div class="row">
	        <div class="col-sm-8 col-sm-offset-2">

	            <!--      Wizard container        -->
	            <div class="wizard-container">
	                <div class="card wizard-card" data-color="blue" id="wizard">
	                    {!!Form::model($pricelist, ['route'=>['Pricelists.update',$pricelist->id], 'id'=>'frm', 'method'=>'PUT', 'autocomplete'=>'off'])!!}
	                <!--        You can switch " data-color="azure" "  with one of the next bright colors: "blue", "green", "orange", "red"           -->

	                    	<div class="wizard-header">
	                        	<h3 class="wizard-title">Creación de Lista de precios</h3>
	                        	<p class="category">Ingrese todos los campos que sean requeridos.</p>
	                    	</div>

							<div class="wizard-navigation">
								<div class="progress-with-circle">
								     <div class="progress-bar" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="3" style="width: 21%;"></div>
								</div>
								<ul>
		                            <li>
										<a href="#client1" data-toggle="tab">
											<div class="icon-circle">
												<i class="ti-arrow-right"></i>
											</div>
											Details
										</a>
									</li>
		                            <li>
										<a href="#client2" data-toggle="tab">
											<div class="icon-circle">
												<i class="ti-arrow-right"></i>
											</div>
											Timetable
										</a>
									</li>
		                        </ul>
							</div>
								<input id="_token" type="hidden" name="_token" value="{{ csrf_token() }}">
								@include('Forms.PriceList')
	                        <div class="wizard-footer">
	                        	<div class="pull-right">
	                                <input type='button' class='btn btn-next btn-fill btn-primary btn-wd' name='next' value='Next' />
	                      			{!!Form::submit('finish',['id'=>'finish', 'class'=>'btn btn-finish btn-fill btn-primary btn-wd'])!!}
	                      			{!!Form::close()!!}
	                      			{!!Form::open(['route'=>['Pricelists.destroy', $pricelist->id], 'id'=>'frmDelete', 'method'=>'DELETE'])!!}
	                      				{!!Form::submit('Eliminar',['id'=>'eliminar', 'class'=>'btn btn-danger'])!!}
									{!!Form::close()!!}
	                            </div>

	                            <div class="pull-left">
	                                <input type='button' class='btn btn-previous btn-default btn-wd' name='previous' value='Previous' />
	                            </div>
	                            <div class="clearfix"></div>
	                        </div>
	                    
	                    
	                </div>
	            </div> <!-- wizard container -->
	        </div>
        </div> <!-- row -->
    </div> <!--  big container -->
</div>
	<!--   Core JS Files   -->
	<script src="assets/js/jquery-2.2.4.min.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="assets/js/jquery.bootstrap.wizard.js" type="text/javascript"></script>

	<!--  Plugin for the Wizard -->
	<script src="assets/js/paper-bootstrap-wizard.js" type="text/javascript"></script>

	<!--  More information about jquery.validate here: http://jqueryvalidation.org/	 -->
	<script src="assets/js/jquery.validate.min.js" type="text/javascript"></script>

	<script type="text/javascript">
		$("#frmDelete").submit(function (event) {
		    event.preventDefault();
		    $('.loading').show();
		    var form = $(this);
		    var data = new FormData($(this)[0]);
		    var url = form.attr("action");
		    $.ajax({
		        type: "POST",
		        url: url,
		        data: data,
		        async: false,
		        cache: false,
		        contentType: false,
		        processData: false,
		        success: function (data) {
		            if (data.fail) {
		                $('#frm input.required, #frm textarea.required').each(function () {
		                    index = $(this).attr('name');
		                    if (index in data.errors) {
		                        $("#form-" + index + "-error").addClass("has-error");
		                        $("#" + index + "-error").html(data.errors[index]);
		                    }
		                    else {
		                        $("#form-" + index + "-error").removeClass("has-error");
		                        $("#" + index + "-error").empty();
		                    }
		                });
		                $('#focus').focus().select();
		            } else {
		                $(".has-error").removeClass("has-error");
		                $(".help-block").empty();
		                $('.loading').hide();
		                ajaxLoad(data.url, data.content);
		            }
		        },
		        error: function (xhr, textStatus, errorThrown) {
		            alert(errorThrown);
		        }
		    });
		    return false;
		});
	</script>