@include('alerts.AlertsRequest')
@include('alerts.SuccessRequest')
@include('alerts.ErrorsRequest')
{!!Form::open(['route'=>'generarlista', 'id'=>'frm', 'method'=>'POST', 'autocomplete'=>'off'])!!}
	<div class="col-md-12 col-sm-12">
		<input id="_token" type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="col-sm-12">
            <h5 class="info-text">ASOCIAR VENDEDOR</h5>
        </div>
		<div class="col-md-5 col-sm-5">
			@include('Forms.referenciasacargar')
		</div>
		<div class="col-md-2 col-sm-2">
			@include('Forms.botones')
		</div>
		<div class="col-md-5 col-sm-5">
			@include('Forms.referenciascargadas')
		</div>
	</div>
{!!Form::close()!!}