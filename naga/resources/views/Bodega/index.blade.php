{!!Html::script('js/jquery.dataTables.min.js')!!}
@include('alerts.AlertsRequest')
@include('alerts.SuccessRequest')
@include('alerts.ErrorsRequest')
<aside class="col-md-12">
   <div id="cambio">
      <table class="table" id="tablabodega">
         <thead>
            <th>Id Bodega</th>
            <th>Código Bodega</th>
            <th>Nombre Bodega</th>
            <th>Operación</th>
         </thead>
      </table>
   </div>
</aside>
<script type="text/javascript">
   $(document).ready(function() {
      oTable = $('#tablabodega').DataTable({
         "processing": true,
         "serverSide": true,
         "ajax": "{{ route('datatable.tablabodegas') }}",
         "columns": [
            {data: 'id', name: 'id'},
            {data: 'Codigo', name: 'Codigo'},
            {data: 'Nombre', name: 'Nombre'},
            {defaultContent: "<a class='btn btn-success'>Editar</a>"}
         ],
         "language" : {
            processing:     "Por favor espere...",
            search:         "Búsqueda:",
            lengthMenu:     "Mostrar _MENU_ registros",
            info:           "Mostrando del _START_ al _END_ de _TOTAL_ registros",
            infoFiltered:   "(Filtrado de _MAX_ registros)",
            infoPostFix:    "",
            loadingRecords: "Cargando...",
            paginate: {
               first: "Primera",
               previous: "Previo",
               next: "Siguiente",
               last: "&uacute;ltimo"
            }
         }
      });
      $('#tablabodega tbody').on( 'click', 'a', function () {
         content = typeof content !== 'undefined' ? content : 'content';
         var datos = oTable.row( $(this).parents('tr') ).data();
         //window.location.href = "/Clients/"+datos['id']+"/edit";
         filename = "/Bodegas/"+datos['id']+"/edit";
         $.ajax({
            type: "get",
            url: filename,
            contentType: false,
            success: function (data) {
               $("#content").html(data);
            },
            error: function (xhr, status, error) {
               alert(xhr.responseText);
            }
         });
      });
   });
</script>