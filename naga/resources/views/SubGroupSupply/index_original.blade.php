@section('content')	
@include('alerts.AlertsRequest')
@include('alerts.SuccessRequest')
@include('alerts.ErrorsRequest')
<aside class="col-md-12">
	<table class="table">
		<thead>
			<th>subgroup</th>
			<th>subgroupName</th>
			<th>Operación</th>
		</thead>
		@foreach($subgroupsupplies as $subgroupsupply)
		<tbody>
			<td>{{$subgroupsupply->subgroup}}</td>
			<td>{{$subgroupsupply->subgroupName}}</td>
			<td>
                <a class="btn btn-primary" href="javascript:ajaxLoad('{{url('SubGroupsupplies/'.$subgroupsupply->id.'/edit')}}')">Editar</a>
			</td
		</tbody>
		@endforeach		
	</table>
	{!!$subgroupsupplies->render()!!}
</aside>