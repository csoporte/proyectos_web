@section('content')	
@include('alerts.AlertsRequest')
@include('alerts.SuccessRequest')
@include('alerts.ErrorsRequest')
<aside class="col-md-12">
	<table class="table">
		<thead>
			<th>code</th>
			<th>provider</th>
			<th>nit</th>
			<th>address</th>
			<th>phone</th>
			<th>city_id</th>
			<th>tradename</th>
			<th>product_id</th>
			<th>email</th>
			<th>Operación</th>
		</thead>
		@foreach($providers as $provider)
		<tbody>
			<td>{{$provider->code}}</td>
			<td>{{$provider->provider}}</td>
			<td>{{$provider->nit}}</td>
			<td>{{$provider->address}}</td>
			<td>{{$provider->phone}}</td>
			<td>{{$provider->city_id}}</td>
			<td>{{$provider->tradename}}</td>
			<td>{{$provider->product_id}}</td>
			<td>{{$provider->email}}</td>
			<td>
                <a class="btn btn-primary" href="javascript:ajaxLoad('{{url('Providers/'.$provider->id.'/edit')}}')">Editar</a>
			</td
		</tbody>
		@endforeach		
	</table>
	{!!$providers->render()!!}
</aside>