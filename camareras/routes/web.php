<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/
Route::get("/", "AdminController@login");
Route::get('logout', 'AdminController@logout');
Route::get("Rooms/asear","RoomsController@asear");
Route::get("Rooms/revision","RoomsController@revision");
Route::get("Rooms/registrarlimpieza/{room}","RoomsController@registrarlimpieza");
Route::get("Rooms/registrarmantenimiento/{room}","RoomsController@registrarmantenimiento");
Route::get("Rooms/observarhabitacion/{room}","RoomsController@observarhabitacion");
Route::post("Rooms/guardarregistrolimpieza","RoomsController@guardarregistrolimpieza")->name('Rooms.guardarlimpieza');
Route::post("Rooms/guardarregistromantenimiento","RoomsController@guardarregistromantenimiento")->name('Rooms.guardarmantenimiento');
Route::resource("Admin", "AdminController");
Route::resource("Camareras","CamarerasController");
Route::get("Camareras/editarmantenimiento/{idmante}","CamarerasController@editarmantenimiento");
Route::resource("Items","ItemsController");
Route::resource("Notas","NotasController");
Route::post("Notas/cargar","NotasController@cargar")->name('Notas.cargar');
Route::resource("Notificaciones","NotificacionesController");
Route::get("Notificaciones/actualizarnotif/{idnotificacion}","NotificacionesController@actualizarnotif");
Route::post("Notificaciones/registraractualizarnotif","NotificacionesController@registraractualizarnotif")->name('Notificaciones.actualizarnotificacion');
Route::resource("Rooms","RoomsController");
Route::resource("User","UserController");


//Route::get("/", "AdminController@login");
//Route::get('logout', 'AdminController@logout');
