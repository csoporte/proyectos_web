
{!!Form::model($camarera, ['route'=>['Camareras.update',$camarera['ID']], 'id'=>'frm', 'method'=>'PUT', 'autocomplete'=>'off'])!!}
<div class="col-md-12 col-sm-12">
	@include('Forms.Camarera')
{!!Form::close()!!}
<br>
{!!Form::open(['route'=>['Camareras.destroy', $camarera['ID']], 'id'=>'frmDelete', 'method'=>'DELETE'])!!}
	<script type="text/javascript">
		$("#frmDelete").submit(function (event) {
		    event.preventDefault();
		    $('.loading').show();
		    var form = $(this);
		    var data = new FormData($(this)[0]);
		    var url = form.attr("action");
		    $.ajax({
		        type: "POST",
		        url: url,
		        data: data,
		        async: false,
		        cache: false,
		        contentType: false,
		        processData: false,
		        success: function (data) {
		            if (data.fail) {
		                $('#frm input.required, #frm textarea.required').each(function () {
		                    index = $(this).attr('name');
		                    if (index in data.errors) {
		                        $("#form-" + index + "-error").addClass("has-error");
		                        $("#" + index + "-error").html(data.errors[index]);
		                    }
		                    else {
		                        $("#form-" + index + "-error").removeClass("has-error");
		                        $("#" + index + "-error").empty();
		                    }
		                });
		                $('#focus').focus().select();
		            } else {
		                $(".has-error").removeClass("has-error");
		                $(".help-block").empty();
		                $('.loading').hide();
		                ajaxLoad(data.url, data.content);
		            }
		        },
		        error: function (xhr, textStatus, errorThrown) {
		            alert(errorThrown);
		        }
		    });
		    return false;
		});
	</script>
{!!Form::close()!!}
</div>