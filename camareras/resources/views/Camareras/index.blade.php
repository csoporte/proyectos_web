@extends('layouts.admin')
@section('content')
   <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <h1 class="page-header">PERSONAL</h1>
      </div>
   </div>
   <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <div class="panel panel-default">
            <div class="panel-body">
               <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                  <thead>
                     <tr>
                        <th>Nombres</th>
                        <th>Teléfono</th>
                        <th>Perfil</th>
                        <th>Activo/Inactivo</th>
                        <th>Operación</th>
                     </tr>
                  </thead>
                  @foreach($camareras as $camarera)
                     <tbody>
                        <td>{{$camarera->NOMBRES}}</td>
                        <td>{{$camarera->TELEFONO}}</td>
                        <td>{{$camarera->PERFIL}}</td>
                        <td>
                           @if($camarera->ESTADO=='T')
                              ACTIVO
                           @else
                              INACTIVO
                           @endif
                        </td>
                        <td>
                           <a class="btn btn-primary" href="{{url('Camareras/'.$camarera->ID.'/edit')}}">Editar</a>
                        </td>
                     </tbody>
                  @endforeach
                  @foreach($mantenimientos as $mantenimiento)
                     <tbody>
                        <td>{{$mantenimiento->NOMBRES}}</td>
                        <td>{{$mantenimiento->TELEFONO}}</td>
                        <td>{{$mantenimiento->PERFIL}}</td>
                        <td>
                           @if($mantenimiento->ESTADO=='T')
                              ACTIVO
                           @else
                              INACTIVO
                           @endif
                        </td>
                        <td>
                           <a class="btn btn-primary" href="{{url('Camareras/editarmantenimiento/'.$mantenimiento->ID)}}">Editar</a>
                        </td>
                     </tbody>
                  @endforeach
               </table>
            </div>
         </div>
      </div>
   </div>
      <!--{!$groupsupplies->render()!!}-->
   {!!Form::open(['route'=>'Camareras.create', 'id'=>'frm', 'method'=>'GET', 'autocomplete'=>'off'])!!}
      {!!Form::submit('Nuevo registro',['id'=>'Guardar', 'class'=>'btn btn-primary'])!!}
   {!!Form::close()!!}
@endsection