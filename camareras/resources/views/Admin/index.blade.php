@extends('layouts.admin')
@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h1 class="page-header">NOTIFICACIONES - PERSONAL</h1>
        </div>
    </div>
    @if($numerodatos > 0)
        @foreach($notificaciones as $notificacion)
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            {{utf8_encode($notificacion->TITULONOTIFICACION)}}
                        </div>
                        <div class="panel-body">
                            <p>{{utf8_encode($notificacion->NOTIFICACION)}}</p>
                        </div>
                        <div class="panel-footer">
                            Administrador
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    @else
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Sin Notificaciones
                    </div>
                    <div class="panel-body">
                        <p>En este momento no existen notificaciones. Gracias !!</p>
                    </div>
                    <div class="panel-footer">
                        Administrador
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection