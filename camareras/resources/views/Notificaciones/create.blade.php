{!!Form::open(['route'=>'Notificaciones.store', 'id'=>'frm', 'method'=>'POST', 'autocomplete'=>'off'])!!}
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<input id="_token" type="hidden" name="_token" value="{{ csrf_token() }}">
		@include('Forms.Notificaciones')
	</div>
{!!Form::close()!!}