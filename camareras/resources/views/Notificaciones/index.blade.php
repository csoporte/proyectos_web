@extends('layouts.admin')
@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h1 class="page-header">NOTIFICACIONES</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                   Notificaciones
                </div>
                <div class="panel-body">
                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Asunto Notificación</th>
                                <th>Notificación</th>
                                <th>Operación</th>
                            </tr>
                        </thead>
                        @foreach($notificaciones as $notificacion)
                        <tbody>
                            <td>{{$notificacion->IDNOTIFICACION}}</td>
                            <td>{{utf8_encode($notificacion->TITULONOTIFICACION)}}</td>
                            <td>{{utf8_encode($notificacion->NOTIFICACION)}}</td>
                            <td>
                               <a class="btn btn-primary" href="{{url('Notificaciones/actualizarnotif/'.$notificacion->IDNOTIFICACION)}}">Editar</a>
                            </td>
                        </tbody>
                        @endforeach   
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection