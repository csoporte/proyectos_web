@extends('layouts.admin')
@section('content')
	{!!Form::open(['route'=>'Notificaciones.actualizarnotificacion', 'id'=>'frm','name'=>'editarnotificaciones', 'method'=>'POST', 'autocomplete'=>'off'])!!}
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<input id="_token" type="hidden" name="_token" value="{{ csrf_token() }}">
			@include('Forms.NotificacionesEdit')
		</div>
	{!!Form::close()!!}
	
@endsection