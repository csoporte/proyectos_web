@extends('layouts.admin')
@section('content')

   <div>
      <aside class="col-md-12 col-sm-12">
         <table class="table" id="table_bonita">
            <thead>
               <th>ID</th>
               <th>NOTA</th>
               <th>CAMARERA</th>
            </thead>
            @foreach($notas as $nota)
               <tbody>
                  <td>{{$nota->ID}}</td>
                  <td>{{$nota->NOTA}}</td>
                  <td>{{$nombrecamarera}}</td>
               </tbody>
            @endforeach
         </table>
         <!--{!$groupsupplies->render()!!}-->
         <div class="well">
            <a class="btn btn-default btn-lg btn-block" href="{{url('Notas')}}">Volver</a>
         </div>
      </aside>
   </div>
   
@endsection