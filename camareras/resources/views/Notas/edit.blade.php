
{!!Form::model($item, ['route'=>['Items.update',$item->ID], 'id'=>'frm', 'method'=>'PUT', 'autocomplete'=>'off'])!!}
<div class="col-md-4">
	@include('Forms.Item')
{!!Form::close()!!}
<br>
{!!Form::open(['route'=>['Items.destroy', $item->ID], 'id'=>'frmDelete', 'method'=>'DELETE'])!!}
	{!!Form::submit('Eliminar',['id'=>'eliminar', 'class'=>'btn btn-danger'])!!}
{!!Form::close()!!}
</div>