{!!Form::open(['route'=>'Notas.store', 'id'=>'frm', 'method'=>'POST', 'autocomplete'=>'off'])!!}
	<div class="col-md-12 col-sm-12">
		<input id="_token" type="hidden" name="_token" value="{{ csrf_token() }}">
		@include('Forms.Nota')
	</div>
{!!Form::close()!!}