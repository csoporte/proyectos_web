@section('content')	
@include('alerts.AlertsRequest')
@include('alerts.SuccessRequest')
@include('alerts.ErrorsRequest')
<aside class="col-md-12">
	<table class="table" id="table_bonita">
		<thead>
			<th>group</th>
			<th>groupName</th>
			<th>Operación</th>
		</thead>
		@foreach($groupsupplies as $groupsupply)
		<tbody>
			<td>{{$groupsupply->group}}</td>
			<td>{{$groupsupply->groupName}}</td>
			<td>
                <a class="btn btn-primary" href="javascript:ajaxLoad('{{url('Groupsupplies/'.$groupsupply->id.'/edit')}}')">Editar</a>
			</td
		</tbody>
		@endforeach		
	</table>
	<!--{!$groupsupplies->render()!!}-->
</aside>