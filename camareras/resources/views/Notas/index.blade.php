@extends('layouts.admin')
@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h1 class="page-header">NOTAS GENERALES</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default">
            	{!!Form::open(['route'=>'Notas.cargar', 'id'=>'frm', 'method'=>'POST', 'autocomplete'=>'off'])!!}
                <div class="panel-heading">
                    {!!Form::label('Fecha: ')!!}
                </div>
                <div class="panel-body">
                    {!!Form::date('fecharevision', \Carbon\Carbon::now())!!}
                </div>
                <div class="panel-footer">
                    {!!Form::submit('Ver notas',['id'=>'Guardar', 'class'=>'btn btn-primary'])!!}
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->
@endsection