@extends('layouts.admin')
@section('content')
   <aside class="col-md-12">
      <table class="table">
         <thead>
            <th>id</th>
            <th>Nombre usuario</th>
            <th>Perfil</th>
            <th>Operación</th>
         </thead>
         @foreach($usuarios as $usuario)
            <tbody>
               <td>{{$usuario->ID}}</td>
               <td>{{$usuario->NOMBRE_USUARIO}}</td>
               <td>{{$usuario->PERFIL}}</td>
               <td>
                  <a class="btn btn-primary" href="javascript:ajaxLoad('{{url('User/'.$usuario->id.'/edit')}}')">Editar</a>
               </td>
            </tbody>
         @endforeach    
      </table>
   </aside>
@endsection
