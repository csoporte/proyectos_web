<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Soho Camareras</title>

    <!-- Bootstrap Core CSS 
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">-->
    {!!Html::style('vendor/bootstrap/css/bootstrap.min.css')!!}

    <!-- MetisMenu CSS 
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">-->
    {!!Html::style('vendor/metisMenu/metisMenu.min.css')!!}

    <!-- Custom CSS 
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">-->
    {!!Html::style('dist/css/sb-admin-2.css')!!}

    <!-- Morris Charts CSS 
    <link href="../vendor/morrisjs/morris.css" rel="stylesheet">-->
    {!!Html::style('vendor/morrisjs/morris.css')!!}

    <!-- Custom Fonts 
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">-->
    {!!Html::style('vendor/font-awesome/css/font-awesome.min.css')!!}

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

    <!-- DataTables CSS 
    <link href="../vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
    {!Html::style('vendor/datatables-plugins/dataTables.bootstrap.css')!!}-->

    <!-- DataTables Responsive CSS 
    <link href="../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
    {!Html::style('vendor/datatables-responsive/dataTables.responsive.css')!!}-->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{url('Admin')}}">{{Session::get('nomempresa')}}</a>
                <img alt="logo" class="img" border="0" width="60" height="60" src="{{ asset('images/logo.png') }}">
            </div>
            <!-- /.navbar-header -->

            <!--div class="navbar-header" style="float: right;">
                SOHO - CAMARERAS V1.0
            </div-->
            <div class="navbar-header" style="float: right;">
                <h4>{!!Form::label(Auth::user()->NOMBRE_USUARIO)!!}</h4>
            </div>
            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <!--li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>-->
                        <li>
                            <a href="{!!URL::to('/logout')!!}"><i class="fas fa-sign-out-alt"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <!--<li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div>-->
                            <!-- /input-group -->
                        </li>
                        @if(Auth::user()->PERFIL == 'Administrador')
                            <li>
                                <a href="#"><i class="fa fa-cogs"></i> Parametrización<span class="fas fa-angle-down" data-fa-transform="right-10"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="{{url('Rooms')}}"><i class="fas fa-bed"></i> Habitaciones</a>
                                    </li>
                                    <li>
                                        <a href="{{url('Items')}}"><i class="fas fa-briefcase"></i> Items a revisar</a>
                                    </li>
                                    <li>
                                        <a href="{{url('Camareras')}}"><i class="fas fa-briefcase"></i> Camareras y Mantenimiento</a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fas fa-comment"></i>  Notificaciones <span class="fas fa-angle-down"></span></a>
                                        <ul class="nav nav-third-level">
                                            <li>
                                                <a href="{{url('Notificaciones')}}"><i class="fas fa-exclamation"></i> Ver Notificación</a>
                                            </li>
                                            <li>
                                                <a href="{{url('Notificaciones/create')}}"><i class="far fa-edit"></i> Crear Notificación</a>
                                            </li>
                                        </ul>
                                        <!-- /.nav-third-level -->
                                    </li>
                                </ul>
                                <!-- /.nav-second-level -->
                            </li>
                        @endif
                        <li>
                            <a href="#"><i class="fa fa-bed"></i> Manejo habitaciones<span class="fas fa-angle-down" data-fa-transform="right-10"></span></a>
                            <ul class="nav nav-second-level">
                                @if(Auth::user()->PERFIL == 'Camarera' OR Auth::user()->PERFIL == 'Mantenimiento')
                                    <li>
                                        <a href="{{url('Rooms/asear')}}"><i class="fa fa-exclamation-triangle"></i> Gestionar habitacion</a>
                                    </li>
                                @endif
                                @if(Auth::user()->PERFIL == 'Administrador')
                                    <li>
                                        <a href="{{url('Rooms/revision')}}"><i class="fa fa-exclamation-triangle"></i> Revisión de las habitaciones</a>
                                    </li>
                                @endif
                                <li>
                                    <a href="#"><i class="fas fa-comment"></i> Notas<span class="fas fa-angle-down" data-fa-transform="right-10"></span></a>
                                    <ul class="nav nav-third-level">
                                        @if(Auth::user()->PERFIL == 'Camarera' or Auth::user()->PERFIL == 'Mantenimiento')
                                        <li>
                                            <a href="{{url('Notas/create')}}"><i class="fas fa-pen-square"></i>  Crear nota</a>
                                        </li>
                                        @endif
                                        <li>
                                            <a href="{{url('Notas')}}"><i class="fas fa-list"></i>  Notas del día</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
		
        <div id="content">
        	<div id="page-wrapper">
        	    @yield('content')
        	</div>
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery 
    <script src="../vendor/jquery/jquery.min.js"></script>-->
    {!!Html::script('vendor/jquery/jquery.min.js')!!}

    <!-- Bootstrap Core JavaScript 
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>-->
    {!!Html::script('vendor/bootstrap/js/bootstrap.min.js')!!}

    <!-- Metis Menu Plugin JavaScript 
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>-->
    {!!Html::script('vendor/metisMenu/metisMenu.min.js')!!}

    <!-- Morris Charts JavaScript 
    <script src="../vendor/raphael/raphael.min.js"></script>
    <script src="../vendor/morrisjs/morris.min.js"></script>
    <script src="../data/morris-data.js"></script>-->
    {!!Html::script('vendor/raphael/raphael.min.js')!!}
    {!!Html::script('vendor/morrisjs/morris.min.js')!!}
    {!!Html::script('data/morris-data.js')!!}

    <!-- Custom Theme JavaScript 
    <script src="../dist/js/sb-admin-2.js"></script>-->
    {!!Html::script('dist/js/sb-admin-2.js')!!}

    <!-- DataTables JavaScript 
    <script src="../vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="../vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="../vendor/datatables-responsive/dataTables.responsive.js"></script>-->

    <!-- SCript de datatables
    {!Html::script('vendor/datatables/js/jquery.dataTables.min.js')!!}
    {!Html::script('vendor/datatables-plugins/dataTables.bootstrap.min.js')!!}
    {!Html::script('vendor/datatables-responsive/dataTables.responsive.js')!!}-->

</body>

</html>

