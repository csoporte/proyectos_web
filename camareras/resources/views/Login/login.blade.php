<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Soho Camareras</title>
        

        <!-- Bootstrap Core CSS 
        <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">-->
        {!!Html::style('vendor/bootstrap/css/bootstrap.min.css')!!}

        <!-- MetisMenu CSS 
        <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">-->
        {!!Html::style('vendor/metisMenu/metisMenu.min.css')!!}

        <!-- Custom CSS 
        <link href="../dist/css/sb-admin-2.css" rel="stylesheet">-->
        {!!Html::style('dist/css/sb-admin-2.css')!!}

        <!-- Custom Fonts 
        <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">-->
        {!!Html::style('vendor/font-awesome/css/font-awesome.min.css')!!}

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>
        @include('alerts.AlertsRequest')
        @include('alerts.SuccessRequest')
        @include('alerts.ErrorsRequest')
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="panel-heading text-center">
                        <img src="images/m3.png" alt="" width="50%" />
                    </div>
                </div>
                <div class="col-md-4 col-md-offset-4">
                    <div class="login-panel panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Iniciar Sesión</h3>
                        </div>
                        <div class="panel-body">
                            {!!Form::open(['route'=>'Admin.store', 'id'=>'frm', 'method'=>'POST', 'autocomplete'=>'off'])!!}
                                <fieldset>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Usuario" name="usuario" type="text" autofocus>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Contraseña" name="clave" type="password" value="">
                                    </div>
                                    <!-- Change this to a button or input when using this as a form -->
                                    <button class="btn btn-lg btn-info btn-block" type="submit" name="iniciosesion">Iniciar Sesi&oacute;n
                                </fieldset>

                                <div class="col-md-4">
                                    <input id="_token" type="hidden" name="_token" value="{{ csrf_token() }}">
                                </div>
                            {!!Form::close()!!}
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- jQuery 
        <script src="../vendor/jquery/jquery.min.js"></script>-->
        {!!Html::script('vendor/jquery/jquery.min.js')!!}

        <!-- Bootstrap Core JavaScript 
        <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>-->
        {!!Html::script('vendor/bootstrap/js/bootstrap.min.js')!!}

        <!-- Metis Menu Plugin JavaScript 
        <script src="../vendor/metisMenu/metisMenu.min.js"></script>-->
        {!!Html::script('vendor/metisMenu/metisMenu.min.js')!!}

        <!-- Custom Theme JavaScript 
        <script src="../dist/js/sb-admin-2.js"></script>-->
        {!!Html::script('dist/js/sb-admin-2.js')!!}

    </body>

</html>
