@extends('layouts.admin')
@section('content')
   <!-- DataTables CSS 
   <link href="../vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">-->
   {!!Html::style('vendor/datatables-plugins/dataTables.bootstrap.css')!!}

   <!-- DataTables Responsive CSS 
   <link href="../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">-->
   {!!Html::style('vendor/datatables-responsive/dataTables.responsive.css')!!}
   
   <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <h1 class="page-header">Habitaciones</h1>
      </div>
   </div>
   <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <div class="panel panel-default">
            <div class="panel-heading">
               Habitaciones
            </div>
            <div class="panel-body">
               <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                  <thead>
                     <tr>
                        <th>Habitación</th>
                        <th>Estado</th>
                        <th>Operación</th>
                     </tr>
                  </thead>
                  @foreach($rooms as $room)
                     <tbody>
                        <td>{{$room->HABITACION}}</td>
                        <td>
                           @if($room->ESTADO_LIMPIEZA=='T')
                              <div style="text-align: center;">
                                 <i class="fas fa-check-circle fa-2x" style="color:green"></i>   
                              </div>
                           @elseif($room->ESTADO=='6')
                              <div style="text-align: center;">
                                 <i class="fas fa-wrench fa-2x" style="color:tomato"></i>   
                              </div> 
                           @else
                              <div style="text-align: center;">
                                 <i class="fas fa-quidditch fa-2x" style="color:blue"></i>   
                              </div>
                           @endif
                        </td>
                        <td>
                           <a class="btn btn-primary" href="{{url('Rooms/'.$room->HABITACION.'/edit')}}">Editar</a>
                        </td>
                     </tbody>
                  @endforeach   
               </table>
            </div>
         </div>
      </div>
   </div>
   {!!Html::script('vendor/jquery/jquery.min.js')!!}
   {!!Html::script('vendor/datatables/js/jquery.dataTables.min.js')!!}
   {!!Html::script('vendor/datatables-plugins/dataTables.bootstrap.min.js')!!}
   {!!Html::script('vendor/datatables-responsive/dataTables.responsive.js')!!}
   <script>
      $(document).ready(function() {
         $('#dataTables-example').DataTable({
            responsive: true
         });
      });
   </script>
@endsection