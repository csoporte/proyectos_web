@extends('layouts.admin')
@section('content')
   <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <h1 class="page-header">REVISIÓN HABITACIONES</h1>
      </div>
   </div>
   <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <div class="panel panel-default">
            <div class="panel-body">
               <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                  <thead>
                     <tr>
                        <th>Habitación:</th>
                        <th>Asignada a:</th>
                        <th style="text-align: center;">Estado</th>
                        <th>Operación</th>
                     </tr>
                  </thead>
                  @foreach($rooms as $room)
                     <tbody>
                        <td>{{$room->HABITACION}}</td>
                        <td>{{$room->NOMBRES}}</td>
                        <td>
                           @if($room->ESTADO_LIMPIEZA=='T')
                              <div style="text-align: center;">
                                 <i class="fas fa-check-circle fa-2x" style="color:green"></i>   
                              </div> 
                           @else
                              <div style="text-align: center;">
                                 <i class="fas fa-times-circle fa-2x" style="color:Tomato"></i>   
                              </div>
                           @endif
                        </td>
                        <td>
                           @if($room->ESTADO_LIMPIEZA=='F')
                              <a class="btn btn-primary" href="#" disabled="disabled">Observar habitación</a>
                           @else
                              <a class="btn btn-primary" href="{{url('Rooms/observarhabitacion/'.$room->HABITACION)}}">Observar habitación</a>
                           @endif
                        </td>
                     </tbody>
                  @endforeach    
               </table>
            </div>
         </div>
      </div>
   </div>
@endsection