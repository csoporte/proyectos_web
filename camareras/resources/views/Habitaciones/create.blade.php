@include('alerts.AlertsRequest')
@include('alerts.SuccessRequest')
@include('alerts.ErrorsRequest')
{!!Form::open(['route'=>'Groupsupplies.store', 'id'=>'frm', 'method'=>'POST', 'autocomplete'=>'off'])!!}
	<div class="col-md-4">
		<input id="_token" type="hidden" name="_token" value="{{ csrf_token() }}">
		@include('Forms.GroupSupply')
	</div>
{!!Form::close()!!}