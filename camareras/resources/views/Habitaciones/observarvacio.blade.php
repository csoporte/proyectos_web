@extends('layouts.admin')
@section('content')
   <aside class="col-md-12">
      <div class="row">
         <div class="col-lg-12">
            <h1 class="page-header">Información</h1>
         </div>
         <!-- /.col-lg-12 -->
      </div>
      <!-- /.row -->
      <div class="row">
         <div class="col-lg-12">
            <div class="panel panel-default">
               <div class="panel-heading">
                  {{$room}}
               </div>
               <!-- /.panel-heading -->
               <div class="panel-body">
                  <!-- /.table-responsive -->
                  <div class="well">
                     <h1>Limpieza gestionada por el administrador o desde Soho Recepción.<h1><br><br>
                     <a class="btn btn-default btn-lg btn-block" href="{{url('Rooms/revision')}}">Volver</a>
                  </div>
               </div>
               <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
         </div>
          <!-- /.col-lg-12 -->
      </div>
      <!-- /.row -->
   </aside>
   <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>
@endsection