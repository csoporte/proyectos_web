@extends('layouts.admin')
@section('content')
	{!!Form::model($room, ['route'=>['Rooms.update',$room->HABITACION], 'id'=>'frm', 'method'=>'PUT', 'autocomplete'=>'off'])!!}
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		@include('Forms.Habitacion')
	{!!Form::close()!!}
	<br>
	{!!Form::open(['route'=>['Rooms.destroy', $room->HABITACION], 'id'=>'frmDelete', 'method'=>'DELETE'])!!}
		<br>
	{!!Form::close()!!}
	</div>
@endsection