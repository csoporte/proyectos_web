@extends('layouts.admin')
@section('content')
{!!Html::script('vendor/jquery/jquery.min.js')!!}
    <div class="row">
       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
           <h1 class="page-header">Notificaciones</h1>
       </div>
   </div>
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <div class="form-group">
               {!!Form::label('Fecha de publicacion: ')!!}
               {!!Form::date('fechainicial', \Carbon\Carbon::now())!!}
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <div class="form-group">
               {!!Form::label('Fecha de finalización: ')!!}
               {!!Form::date('fechafinal', \Carbon\Carbon::now())!!}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <div class="form-group">
               {!!Form::label('Titulo de la notificacion: ')!!}
               {!!Form::text('titulonotificacion',null, ['class'=>'form-control', 'placeholder'=>'Título'])!!}
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <div class="form-group">
                {!!Form::label('Personal a notificar: ')!!}<br>
                {!!Form::radio('personal', 'camareras',false,['id'=>'camarerarad'])!!} Camareras&nbsp;
                {!!Form::radio('personal', 'mantenimiento',false,['id'=>'mantenimientorad'])!!} Mantenimiento&nbsp;
                {!!Form::radio('personal', 'ambos',false,['id'=>'ambos'])!!} Ambos&nbsp;
                {!!Form::radio('personal', 'ninguno',false,['id'=>'ninguno'])!!} Ninguno
            </div>
        </div>
    </div>
    <div class="row" id="divcamarera" style="display: none;">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <div class="form-group">
                <h4>{!!Form::label('Camarera')!!}</h4>
                {!!Form::select('camarera',$camareras,null,['id'=>'camarera','class'=>'form-control','disabled'=>true])!!}
                
            </div>
        </div>
    </div>
    <div class="row" id="divmantenimiento" style="display: none;">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <div class="form-group">
                <h4>{!!Form::label('Personal mantenimiento')!!}</h4>
                {!!Form::select('mantenimiento',$mantenimientos,null,['id'=>'mantenimiento','class'=>'form-control','disabled'=>true])!!}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                {!!Form::label('Notificación: ')!!}<br>
                {!! Form::textarea('nota', null, ['id' => 'nota', 'rows' => 4]) !!}
            </div>  
        </div>
    </div>
    {!!Form::submit('Guardar',['id'=>'Guardar', 'class'=>'btn btn-primary'])!!}
    <script type="text/javascript">
        $(document).ready(function(){
            $('#camarerarad').click(function () {  
                $('#divcamarera').show();
                $('#camarera').prop('disabled', false);
                $('#divmantenimiento').hide();
                $('#mantenimiento').prop('disabled', true);
            });
            $('#mantenimientorad').click(function () {  
                $('#divcamarera').hide();
                $('#camarera').prop('disabled', true);
                $('#divmantenimiento').show();
                $('#mantenimiento').prop('disabled', false);
            });
            $('#ambos').click(function () {  
                $('#divcamarera').show();
                $('#camarera').prop('disabled', false);
                $('#divmantenimiento').show();
                $('#mantenimiento').prop('disabled', false);
            });
            $('#ninguno').click(function () {  
                $('#divcamarera').hide();
                $('#camarera').prop('disabled', true);
                $('#divmantenimiento').hide();
                $('#mantenimiento').prop('disabled', true);
            });
        });

    </script>
@endsection