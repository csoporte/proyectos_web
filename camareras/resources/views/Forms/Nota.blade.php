@extends('layouts.admin')
@section('content')
    <br>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h1 class="page-header">NOTAS GENERALES</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="form-group">
                       {!!Form::label('Fecha: ')!!}
                       {!!Form::date('fecha', \Carbon\Carbon::now(),['readonly'])!!}
                    </div>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        {!!Form::label('Nota: ')!!}<br>
                        {!! Form::textarea('nota', null, ['id' => 'nota', 'rows' => 4]) !!}
                    </div>
                </div>
                <div class="panel-footer">
                    {!!Form::submit('Guardar',['id'=>'Guardar', 'class'=>'btn btn-primary'])!!}
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->
    <script type="text/javascript">
@endsection