{!!Html::script('vendor/jquery/jquery.min.js')!!}

{!!Html::style('vendor/bootstrap-toggle-master/css/bootstrap-toggle.css')!!}
<br>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="form-group">
            <h4>{!!Form::label('Habitación: ')!!}</h4>
            {!!Form::text('HABITACION',null, ['class'=>'form-control', 'placeholder'=>'Habitacion', 'readonly'=>true])!!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="form-group">
            <h4>{!!Form::label('Enviar a limpieza: ')!!}</h4><br>
            @if($room->ESTADO_LIMPIEZA=='T')
                <input name="limpiar" id="limpiar" type="checkbox" data-toggle="toggle" data-on="Si" data-off="No">
            @else
                <input name="limpiar" id="limpiar" type="checkbox" checked="checked" data-toggle="toggle" data-on="Si" data-off="No">
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="form-group">
            <h4>{!!Form::label('Bloquear habitación: ')!!}</h4><br>
            @if($room->ESTADO==6)
                <input name="bloquear" id="bloquear" type="checkbox" data-toggle="toggle" checked="checked" data-on="Bloqueada" data-off="Desbloqueada">
            @else
                <input name="bloquear" id="bloquear" type="checkbox" data-toggle="toggle" data-on="Bloqueada" data-off="Desbloqueada">
            @endif
        </div>
    </div>
</div>
{!!Form::submit('Guardar',['id'=>'Guardar', 'class'=>'btn btn-primary'])!!}
{!!Html::script('vendor/bootstrap-toggle-master/js/bootstrap-toggle.js')!!}