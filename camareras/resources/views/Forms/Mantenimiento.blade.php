    {!!Html::script('vendor/jquery/jquery.min.js')!!}

    {!!Html::style('vendor/bootstrap-toggle-master/css/bootstrap-toggle.css')!!}
    
   
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h1 class="page-header">{{$nomhabitacion}}</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h3>En mantenimiento</h3>
                    <p>Esta habitación se ha enviado a mantenimiento por los siguientes motivos:</p>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>
                                        Item
                                    </th>
                                    <th>
                                        Nota
                                    </th>
                                </tr>
                            </thead>
                            @foreach($itemsxrevisar as $item)
                                <tbody>
                                    <tr>
                                        <th>{{$itemsall[$item->IDITEM]}}</th>
                                        <td>{{$item->NOTAITEM}}</td>
                                    </tr>
                                </tbody>
                            @endforeach
                        </table>
                    </div>
                    <p>{{$datosmantenimiento[0]->NOTA_CAMARERA}}</p>
                </div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                {!!Form::label('Bloquear Habitación: ')!!}
                <input name="bloquear" id="bloquear" type="checkbox" checked="checked" data-toggle="toggle" data-on="Bloqueada" data-off="Desbloqueada">
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                {!!Form::label('Nota de mantenimiento: ')!!}<br>
                <!--{! Form::textarea('nota_habitacion', null, ['id' => 'nota_habitacion', 'rows' => 4, 'cols' => , 'style' => 'resize:none']) !!}-->
                {!! Form::textarea('nota_mantenimiento', null, ['id' => 'nota_mantenimiento', 'class'=>'small-textarea', 'required' =>true]) !!}
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="form-group">
        {{ Form::hidden('idmantenimiento', $datosmantenimiento[0]->ID) }}
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                {!!Form::submit('Guardar',['id'=>'Guardar', 'class'=>'btn btn-primary'])!!}
            </div>
            
        </div>
        <!-- /.col-lg-12 -->
    </div>
    
    <script type="text/javascript">
        /*$('#bloquear').on('change',function(){
            if ($(this).is(':checked') ) {
                $("#nota_mantenimiento").prop('required',true);
                //$("#nota_habitacion").prop('disabled',true);
            }
            else{
                $("#nota_mantenimiento").prop('required',false);
                //$("#nota_habitacion").prop('disabled',false);
            }
        });*/
    </script>

    {!!Html::script('vendor/bootstrap-toggle-master/js/bootstrap-toggle.js')!!}
