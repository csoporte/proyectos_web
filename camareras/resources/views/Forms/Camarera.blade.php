@extends('layouts.admin')
@section('content')
    <br>
    <div class="col-md-6 col-sm-6">
        <div class="form-group">
            {!!Form::label('Nombres: ')!!}
            {!!Form::text('NOMBRES',null, ['class'=>'form-control', 'placeholder'=>'Nombres'])!!}
        </div>
        <div class="form-group">
            {!!Form::label('Nombre Usuario: ')!!}
            {!!Form::text('NOMBRE_USUARIO',null, ['class'=>'form-control', 'placeholder'=>'Usuario'])!!}
        </div>
        <div class="form-group">
            {!!Form::label('Contraseña: ')!!}<br>
            {!!Form::password('CLAVE',['class'=>'form-control'])!!}
        </div>
        <div class="form-group">
            {!!Form::label('Perfil: ')!!}
            <!--{!Form::text('PERFIL',"Camarera", ['class'=>'form-control', 'disabled'=>'disabled'])!!}-->
            {!!Form::select('PERFIL',['Camarera' =>'Camarera','Mantenimiento' => 'Mantenimiento'],null, ['id'=>'perfil','class'=>'form-control', 'placeholder'=>'Seleccione uno...', 'required'=>True])!!}
        </div>
        <div class="form-group">
            {!!Form::label('Dirección: ')!!}
            {!!Form::text('DIRECCION',null, ['class'=>'form-control', 'placeholder'=>'Dirección'])!!}
        </div>
        <div class="form-group">
            {!!Form::label('Teléfono: ')!!}
            {!!Form::number('TELEFONO',null, ['class'=>'form-control', 'placeholder'=>'Teléfono'])!!}
        </div>
        <div class="form-group">
            {!!Form::label('Estado: ')!!}
            {{ Form::checkbox('ESTADO', 'T', ['class' => 'field']) }}
        </div>
        
        {!!Form::submit('Guardar',['id'=>'Guardar', 'class'=>'btn btn-primary'])!!}
        {!!Form::submit('Eliminar',['id'=>'eliminar', 'class'=>'btn btn-danger'])!!}
    </div>
    <div class="col-md-6 col-sm-6"></div>
    <script type="text/javascript">
    $("#frm").submit(function (event) {
        event.preventDefault();
        $('.loading').show();
        var form = $(this);
        var data = new FormData($(this)[0]);
        var url = form.attr("action");
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.fail) {
                    $('#frm input.required, #frm textarea.required').each(function () {
                        index = $(this).attr('name');
                        if (index in data.errors) {
                            $("#form-" + index + "-error").addClass("has-error");
                            $("#" + index + "-error").html(data.errors[index]);
                        }
                        else {
                            $("#form-" + index + "-error").removeClass("has-error");
                            $("#" + index + "-error").empty();
                        }
                    });
                    $('#focus').focus().select();
                } else {
                    $(".has-error").removeClass("has-error");
                    $(".help-block").empty();
                    $('.loading').hide();
                    ajaxLoad(data.url, data.content);
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                alert(errorThrown);
            }
        });
        return false;
    });
    </script>
@endsection