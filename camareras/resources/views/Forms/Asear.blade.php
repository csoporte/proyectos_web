@extends('layouts.admin')
@section('content')

    {!!Html::script('vendor/jquery/jquery.min.js')!!}

    {!!Html::style('vendor/bootstrap-toggle-master/css/bootstrap-toggle.css')!!}
    <br>
    <?php
        $i=0;
    ?>
    <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
            <div class="form-group">
                <h4>{!!Form::label('Habitacion: ')!!}</h4>
                {!!Form::text('habitacion',$nomhabitacion, ['id'=>'habit','class'=>'form-control', 'placeholder'=>'Habitacion', 'readonly'=>true])!!}
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h4>{!!Form::label('Artículos a revisar: ')!!}</h4>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <th></th>
                                <th>Item</th>
                                <th>Nota</th>    
                            </thead>
                            @foreach($itemsxrevisar as $item)
                                <tbody>
                                   <td>{!!Form::checkbox('itemsxrevisar_'.$i,$item->ID,null,['class'=>'custom-control-input','onchange'=>'revisar(this,'.$i.')'])!!}</td>
                                   <!--<td>{!Form::checkbox('itemsxrevisar[]',$item->ID,null,['class'=>'form-control'])!!}</td>-->
                                   <td>{!!Form::label($item->ITEM)!!}</td>
                                   <td>{!!Form::text('notas_'.$i,null, ['id'=>'notas_'.$i,'class'=>'form-control', 'placeholder'=>'nota', 'disabled'=>'disabled'])!!}</td>
                                   <!--<td>{!Form::text('notas[]',null, ['id'=>'notas[]','class'=>'form-control', 'placeholder'=>'nota', 'disabled'=>'disabled'])!!}</td>-->
                                </tbody>      
                                <?php
                                    $i=$i+1;
                                ?>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--<div class="form-group">
        {!Form::label('Artículos a revisar: ')!!}<br>
        <1aside class="col-md-8 col-sm-8 col-xs-8">
            <table class="table">
                <thead>
                    <th>Seleccionar</th>
                    <th>Item</th>
                    <th>Nota</th>
                </thead>
                @foreach($itemsxrevisar as $item)
                    <tbody>
                       <td>{!Form::checkbox('itemsxrevisar_'.$i,$item->ID,null,['class'=>'custom-control-input','onchange'=>'revisar(this,'.$i.')'])!!}</td>
                       <td>{!Form::label($item->ITEM)!!}</td>
                       <td>{!Form::text('notas_'.$i,null, ['id'=>'notas_'.$i,'class'=>'form-control', 'placeholder'=>'nota', 'disabled'=>'disabled'])!!}</td>
                    </tbody>      
                    <?php
                        $i=$i+1;
                    ?>
                @endforeach
            </table>
        </aside>
    </div>-->
    <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
            <div class="form-group">
                <h4>{!!Form::label('Bloquear Habitación por Mantenimiento: ')!!}</h4><br>
                <input name="bloquear" id="bloquear" type="checkbox" data-toggle="toggle">
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
            <div class="form-group">
                <h4>{!!Form::label('Encargada')!!}</h4>
                {!!Form::select('camarera',$camareras,null,['id'=>'camarera','class'=>'form-control','required'=>True,'readonly'=>True])!!}
            </div>
        </div>
    </div>
    <hr>
    @if($ordenes > 0)
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                <div class="form-group">
                    <h4>{!!Form::label('Gestionar Minibar: ')!!}</h4><br>
                    <input name="gestionminibar" id="gestionminibar" type="checkbox" data-toggle="toggle" data-on="Si" data-off="No">
                </div>
            </div>
        </div>
        <hr>
    @endif
    <div class="row" id="minibar" style="display:none;">
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h4>{!!Form::label('Artículos de minibar: ')!!}</h4>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <th class="text-center">Producto</th>
                                <th class="text-center">Cantidad Fija</th>    
                                <th>Nueva cantidad</th>
                            </thead>
                            @foreach($invhabitacion as $inv)
                                <tbody>
                                   <!--<td>{!Form::checkbox('itemsxrevisar[]',$item->ID,null,['class'=>'form-control'])!!}</td>-->
                                   <td class="text-center">{!!Form::label($servicios[$inv->SERVICIO])!!}</td>
                                   <td class="text-center">{{ $inv->CANTIDAD }}</td>
                                   <td>{!!Form::text('nuevacantidad_'.$inv->ID,null, ['class'=>'form-control nuevacantidad', 'placeholder'=>'Digite nueva cantidad','disabled'=>true])!!}</td>
                                   <!--<td>{!Form::text('notas[]',null, ['id'=>'notas[]','class'=>'form-control', 'placeholder'=>'nota', 'disabled'=>'disabled'])!!}</td>-->
                                </tbody>
                            @endforeach

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
            <div class="form-group">
                <h4>{!!Form::label('Nota: ')!!}</h4><br>
                <!--{! Form::textarea('nota_habitacion', null, ['id' => 'nota_habitacion', 'rows' => 4, 'cols' => , 'style' => 'resize:none']) !!}-->
                {!! Form::textarea('nota_habitacion', null, ['id' => 'nota_habitacion', 'class'=>'small-textarea']) !!}
            </div>
        </div>
    </div>
    <div class="form-group">
        {{ Form::hidden('indices', $i) }}
    </div>
    <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
            <div class="form-group">
                {!!Form::submit('Guardar',['id'=>'Guardar', 'class'=>'btn btn-primary'])!!}
            </div>
        </div>
    </div>
    <script type="text/javascript">
        /*if($("checkbox[name*='itemsxrevisar']").prop('checked'))
        {
            console.log("holamundo");
        }*/
        /*var items = document.formabonos.elements["itemsxrevisar[]"];

        $('#itemsxrevisar[0]').on('click',function(e){
            //var type = e.target.value;
            console.log("hola");
            $("#habit").prop('disabled', true);
            //console.log(data[0].typedocument);
            //$("notas[]").prop('disabled', true);
        });*/

        /*$('#camarera').on('change',function(e){
            //var type = e.target.value;
            console.log("Si funciona");
            //console.log(data[0].typedocument);
            //$("notas[]").prop('disabled', true);
        });*/

        function revisar(formulario,dato)
        {
            //console.log(dato);
            //console.log("notas_"+dato);
            
            //document.getElementsByName('notas_'+dato).disabled=false;
            //$("notas_"+dato).prop('disabled', false);
            if(formulario.checked)
                $("#notas_"+dato).prop('disabled', false);
            else
                $("#notas_"+dato).prop('disabled', true);
                
                
            //$("#habit").prop('disabled', true);
            //mutli_education = document.form_name.elements[“education[]”];
            //var abonos = document.formabonos.elements["abono[]"];
        }

        $('#bloquear').on('change',function(){
            if ($(this).is(':checked') ) {
                $("#nota_habitacion").prop('required',true);
                $('#gestionminibar').prop('disabled',true);
                //$("#nota_habitacion").prop('disabled',true);
            }
            else{
                $("#nota_habitacion").prop('required',false);
                $('#gestionminibar').prop('disabled',false);
                //$("#nota_habitacion").prop('disabled',false);
            }
        });

        $('#gestionminibar').on('change',function(){
            if($(this).is(':checked'))
            {
                $('#minibar').show();
                $(".nuevacantidad").prop('disabled', false);
            }
            else
            {
                $('#minibar').hide();
                $(".nuevacantidad").prop('disabled', true);
            } 
        });
    </script>

    {!!Html::script('vendor/bootstrap-toggle-master/js/bootstrap-toggle.js')!!}
@endsection
