@extends('layouts.admin')
@section('content')
    {!!Html::script('vendor/jquery/jquery.min.js')!!}
    <br>
    <?php
        $i=0;
    ?>
    <div class="form-group">
        {!!Form::label('Habitacion: ')!!}
        {!!Form::text('habitacion',$nomhabitacion, ['id'=>'habit','class'=>'form-control', 'placeholder'=>'Habitacion'])!!}
    </div>
    <div class="form-group">
        {!!Form::label('Artículos a revisar: ')!!}<br>
        <aside class="col-md-12">
            <table class="table">
                <thead>
                    <th>Seleccionar</th>
                    <th>Item</th>
                    <th>Nota</th>
                </thead>
                @foreach($itemsxrevisar as $item)
                    <tbody>
                       <!--<td>{!Form::checkbox('itemsxrevisar[]',$item->ID,null,['class'=>'form-control','onclick'=>'revisar('.$i.')'])!!}</td>-->
                       <td>{!!Form::checkbox('itemsxrevisar[]',$item->ID,null,['class'=>'form-control'])!!}</td>
                       <td>{!!Form::label($item->ITEM)!!}</td>
                       <td>{!!Form::text('notas[]',null, ['id'=>'notas[]','class'=>'form-control', 'placeholder'=>'nota', 'disabled'=>'disabled'])!!}</td>
                    </tbody>      
                    <?php
                        $i=$i+1;
                    ?>
                @endforeach
            </table>
        </aside>
    </div>
    <div class="form-group">
        {!!Form::label('Encargada')!!}
        {!!Form::select('camarera',$camareras,null,['id'=>'camarera','class'=>'form-control','placeholder'=>'Seleccionar...','required'=>True])!!}
    </div>

    <div class="form-group">
        {!!Form::label('prueba')!!}
        {!!Form::checkbox('prueba',1)!!}
    </div>
    <div class="form-group">
        {{ Form::hidden('idasear', $room) }}
    </div>

    {!!Form::submit('Guardar',['id'=>'Guardar', 'class'=>'btn btn-primary'])!!}
    <script type="text/javascript">
        if($("checkbox[name*='itemsxrevisar']").prop('checked'))
        {
            console.log("holamundo");
        }
        /*var items = document.formabonos.elements["itemsxrevisar[]"];

        $('#itemsxrevisar[0]').on('click',function(e){
            //var type = e.target.value;
            console.log("hola");
            $("#habit").prop('disabled', true);
            //console.log(data[0].typedocument);
            //$("notas[]").prop('disabled', true);
        });*/

        /*$('#camarera').on('change',function(e){
            //var type = e.target.value;
            console.log("Si funciona");
            //console.log(data[0].typedocument);
            //$("notas[]").prop('disabled', true);
        });*/

        /*function revisar(dato)
        {
            console.log(dato);
            
            document.getElementsByName('notas[]').disabled=false;
            $("#notas[0]").prop('disabled', false);
            $("#habit").prop('disabled', true);
            //mutli_education = document.form_name.elements[“education[]”];
            //var abonos = document.formabonos.elements["abono[]"];
        }*/
    </script>
@endsection
