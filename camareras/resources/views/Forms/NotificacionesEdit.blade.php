{!!Html::script('vendor/jquery/jquery.min.js')!!}
{!!Html::style('vendor/bootstrap-toggle-master/css/bootstrap-toggle.css')!!}  
<div class="row">
   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
       <h1 class="page-header">Notificaciones</h1>
   </div>
</div>
<div class="row">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
        <div class="form-group">
           {!!Form::label('Fecha de publicacion: ')!!}
           {!!Form::date('fechainicial', $notificacion->FECHAINICIAL,['readonly'=>true])!!}
        </div>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
        <div class="form-group">
           {!!Form::label('Fecha de finalización: ')!!}
           {!!Form::date('fechafinal', $notificacion->FECHAFINAL,['readonly'=>true])!!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
        <div class="form-group">
           {!!Form::label('Titulo de la notificacion: ')!!}
           {!!Form::text('titulonotificacion',utf8_encode($notificacion->TITULONOTIFICACION), ['class'=>'form-control', 'placeholder'=>'Título','readonly'=>true])!!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="form-group">
            {!!Form::label('Notificación: ')!!}<br>
            {!! Form::textarea('nota', utf8_encode($notificacion->NOTIFICACION), ['id' => 'nota','readonly'=>true]) !!}
        </div>  
    </div>
</div>
<div class="row">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
        <div class="form-group">
            <h4>{!!Form::label('Desactivar notificacion: ')!!}</h4><br>
            <input name="notiacti" id="notiacti" type="checkbox" checked="checked" data-toggle="toggle" data-on="Activada" data-off="Desactivada">
        </div>
    </div>
</div>
<div class="form-group">
    {{ Form::hidden('idnotificacion', $notificacion->IDNOTIFICACION) }}
</div>
{!!Form::submit('Guardar',['id'=>'Guardar', 'class'=>'btn btn-primary'])!!}
{!!Html::script('vendor/bootstrap-toggle-master/js/bootstrap-toggle.js')!!}