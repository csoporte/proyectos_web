@extends('layouts.admin')
@section('content')
    <br>
    <div class="form-group">
        {!!Form::label('Item: ')!!}
        {!!Form::text('ITEM',null, ['class'=>'form-control', 'placeholder'=>'Item'])!!}
    </div>
    <div class="form-group">
        {!!Form::label('Marca: ')!!}
        {!!Form::text('MARCA',null, ['class'=>'form-control', 'placeholder'=>'Marca'])!!}
    </div>
    <div class="form-group">
        {!!Form::label('Característica: ')!!}
        {!!Form::textarea('CARACTERISTICA',null, ['class'=>'form-control', 'placeholder'=>'Caracteristicas...', 'rows'=>'3'])!!}
    </div>
    <div class="form-group">
        {!!Form::label('Actvo: ')!!}
        {{ Form::checkbox('ACTIVO', 'T', null, ['class' => 'field']) }}
    </div>

    {!!Form::submit('Guardar',['id'=>'Guardar', 'class'=>'btn btn-primary'])!!}
@endsection