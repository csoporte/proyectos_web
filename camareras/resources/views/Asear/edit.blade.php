@extends('layouts.admin')
@section('content')
	<aside class="col-md-12">
		{!!Form::model($room, ['route'=>['Rooms.update',$room->HABITACION], 'id'=>'frm', 'method'=>'PUT', 'autocomplete'=>'off'])!!}
		<div class="col-md-4">
			@include('Forms.Habitacion')
		{!!Form::close()!!}
		<br>
		{!!Form::open(['route'=>['Rooms.destroy', $room->HABITACION], 'id'=>'frmDelete', 'method'=>'DELETE'])!!}
			<br>
			{!!Form::submit('Eliminar',['id'=>'eliminar', 'class'=>'btn btn-danger'])!!}
		{!!Form::close()!!}
		</div>
	</aside>
@endsection