@extends('layouts.admin')
@section('content')
   <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <h1 class="page-header">Habitaciones</h1>
      </div>
        <!-- /.col-lg-12 -->
   </div>
   <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <div class="panel panel-default">
            <div class="panel-body">
               <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                  <thead>
                     <tr>
                        <th>Habitación</th>
                        <th>Fecha llegada</th>
                        <th>Fecha Salida</th>
                        <th>Fecha Reserva</th>
                        <th>Huesped</th>
                        <th style="text-align: center;">Estado</th>
                        <th>Operación</th>
                     </tr>
                  </thead>
                  @foreach($roomsasear as $room)
                     <tbody>
                        <td>{{$room->HABITACION}}</td>
                        <td>{{$room->FECHA_LLEGADA}}</td>
                        <td>{{$room->FECHA_SALIDA}}</td>
                        <td>{{$room->FECHARESERVA}}</td>
                        <td>{{utf8_encode($room->NCLIENTE)}}</td>
                        <td>
                           @if($room->ESTADO_LIMPIEZA=='T')
                              <div style="text-align: center;">
                                 <i class="fas fa-check-circle fa-2x" style="color:green"></i>   
                              </div>
                           @elseif($room->ESTADO=='6')
                              <div style="text-align: center;">
                                 <i class="fas fa-wrench fa-2x" style="color:tomato"></i>   
                              </div> 
                           @else
                              <div style="text-align: center;">
                                 <i class="fas fa-quidditch fa-2x" style="color:blue"></i>   
                              </div>
                           @endif
                        </td>
                        <td>
                           @if($room->ESTADO_LIMPIEZA=='T')
                              <a class="btn btn-primary" href="#" disabled="disabled">Registrada</a>
                           @elseif($room->ESTADO=='6')
                              <a class="btn btn-primary" href="{{url('Rooms/registrarmantenimiento/'.$room->HABITACION)}}">Gestionar mantenimiento</a>
                           @else
                              <a class="btn btn-primary" href="{{url('Rooms/registrarlimpieza/'.$room->HABITACION)}}">Registrar</a>
                           @endif
                        </td>
                     </tbody>
                  @endforeach    
               </table>
            </div>
         </div>
      </div>
   </div>
@endsection