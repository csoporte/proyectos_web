@extends('layouts.admin')
@section('content')
   <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <h1 class="page-header">Items por revisar</h1>
      </div>
   </div>
   <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <div class="panel panel-default">
            <div class="panel-body">
               <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                  <thead>
                     <tr>
                        <th>Item</th>
                        <th>Marca</th>
                        <th>Activo/Inactivo</th>
                        <th>Operación</th>
                     </tr>
                  </thead>
                   @foreach($items as $item)
                     <tbody>
                        <td>{{$item->ITEM}}</td>
                        <td>{{$item->MARCA}}</td>
                        <td>
                           @if($item->ACTIVO=='T')
                              ACTIVO
                           @else
                              INACTIVO
                           @endif
                        </td>
                        <td>
                           <a class="btn btn-primary" href="{{url('Items/'.$item->ID.'/edit')}}">Editar</a>
                        </td>
                     </tbody>
                  @endforeach    
               </table>
            </div>
         </div>
      </div>
   </div>
   {!!Form::open(['route'=>'Items.create', 'id'=>'frm', 'method'=>'GET', 'autocomplete'=>'off'])!!}
      {!!Form::submit('Nuevo registro',['id'=>'Guardar', 'class'=>'btn btn-primary'])!!}
   {!!Form::close()!!}
@endsection