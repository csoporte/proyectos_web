@extends('layouts.admin')
@section('content')
	{!!Form::open(['route'=>'Rooms.guardarmantenimiento', 'id'=>'frm','name'=>'asear', 'method'=>'POST', 'autocomplete'=>'off'])!!}
		<div class="col-md-10 col-sm-10 col-xs-10">
			<input id="_token" type="hidden" name="_token" value="{{ csrf_token() }}">
			@include('Forms.Mantenimiento')
		</div>
		<div class="col-md-2 col-sm-1 col-xs-2"></div>
	{!!Form::close()!!}
@endsection