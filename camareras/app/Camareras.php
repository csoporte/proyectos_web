<?php

namespace camareras;

use Illuminate\Database\Eloquent\Model;

class Camareras extends Model
{
    protected $table = 'CAMARERA';

    protected $primaryKey = 'ID';

    public $incrementing = false;

    protected $fillable = ['NOMBRES','DIRECCION','TELEFONO','ESTADO','IDUSUARIO'];

    public $timestamps = false;
}
