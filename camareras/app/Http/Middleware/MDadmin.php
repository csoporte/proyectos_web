<?php

namespace camareras\Http\Middleware;

use Illuminate\Contracts\Auth\Guard;
use Closure;
use Session;


class MDadmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    
     public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    public function handle($request, Closure $next)
    {
        if($this->auth->user()->PERFIL != 'Camarera' && $this->auth->user()->PERFIL != 'Mantenimiento' && $this->auth->user()->PERFIL != 'Administrador')
        {
            Session::flash('message-error', 'Sin privilegios');
            return redirect()->to('Admin');
        }
        return $next($request);
    }
}
