<?php

namespace camareras\Http\Controllers;

use Illuminate\Http\Request;
use camareras\Camareras;
use camareras\Mantenimiento;
use camareras\Notificacion;
use DB;
use Response;
use Redirect;
use DateTime;
use Auth;
use Session;

class NotificacionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notificaciones = Notificacion::where('ACTIVA','T')->distinct()->get();
        return view('Notificaciones.index', compact('notificaciones'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {  
        $data['camareras']=Camareras::pluck('NOMBRES','IDUSUARIO')->prepend('TODOS','0');

        $data['mantenimientos'] = Mantenimiento::pluck('NOMBRES','IDUSUARIO')->prepend('TODOS','0');

        return view('Notificaciones.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $datoaux = Notificacion::all()->last();
        if($datoaux==null)
            $idnotificacion=1;
        else
            $idnotificacion=$datoaux->IDNOTIFICACION+1;

        switch($request->personal)
        {
            case 'camareras':
                if($request->camarera == 0)
                {
                    $camareras = Camareras::where('ESTADO','T')->get();
                    foreach($camareras as $camarera)
                    {
                        $dato = 
                        [
                            'IDNOTIFICACION'=>$idnotificacion,
                            'FECHAINICIAL'=>$request->fechainicial,
                            'FECHAFINAL'=>$request->fechafinal,
                            'TITULONOTIFICACION'=>utf8_decode($request->titulonotificacion),
                            'USUARIONOTIFICADO'=>$camarera->IDUSUARIO,
                            'NOTIFICACION'=>utf8_decode($request->nota),
                            'FECHACREACION'=>date("d.m.Y H:i:s"),
                            'USUARIOCREA'=>Auth::user()->ID,
                            'ACTIVA'=>'T',
                        ];
                        Notificacion::create($dato);
                    }
                }
                else
                {
                    $dato = 
                    [
                        'IDNOTIFICACION'=>$idnotificacion,
                        'FECHAINICIAL'=>$request->fechainicial,
                        'FECHAFINAL'=>$request->fechafinal,
                        'TITULONOTIFICACION'=>utf8_decode($request->titulonotificacion),
                        'USUARIONOTIFICADO'=>$request->camarera,
                        'NOTIFICACION'=>utf8_decode($request->nota),
                        'FECHACREACION'=>date("d.m.Y H:i:s"),
                        'USUARIOCREA'=>Auth::user()->ID,
                        'ACTIVA'=>'T',
                    ];
                    
                    Notificacion::create($dato);
                }
            break;

            case 'mantenimiento':
                if($request->mantenimiento == 0)
                {
                    $mantenimientos = Mantenimiento::where('ESTADO','T')->get();
                    foreach($mantenimientos as $mantenimiento)
                    {
                        $dato = 
                        [
                            'IDNOTIFICACION'=>$idnotificacion,
                            'FECHAINICIAL'=>$request->fechainicial,
                            'FECHAFINAL'=>$request->fechafinal,
                            'TITULONOTIFICACION'=>$request->titulonotificacion,
                            'USUARIONOTIFICADO'=>$mantenimiento->IDUSUARIO,
                            'NOTIFICACION'=>$request->nota,
                            'FECHACREACION'=>date("d.m.Y H:i:s"),
                            'USUARIOCREA'=>Auth::user()->ID,
                            'ACTIVA'=>'T',
                        ];
                        Notificacion::create($dato);
                    }
                }
                else
                {
                    $dato = 
                    [
                        'IDNOTIFICACION'=>$idnotificacion,
                        'FECHAINICIAL'=>$request->fechainicial,
                        'FECHAFINAL'=>$request->fechafinal,
                        'TITULONOTIFICACION'=>$request->titulonotificacion,
                        'USUARIONOTIFICADO'=>$request->mantenimiento,
                        'NOTIFICACION'=>$request->nota,
                        'FECHACREACION'=>date("d.m.Y H:i:s"),
                        'USUARIOCREA'=>Auth::user()->ID,
                        'ACTIVA'=>'T',
                    ];
                    
                    Notificacion::create($dato);
                }
            break;

            case 'ambos':
                if($request->camarera == 0)
                {
                    $camareras = Camareras::where('ESTADO','T')->get();
                    foreach($camareras as $camarera)
                    {
                        $dato = 
                        [
                            'IDNOTIFICACION'=>$idnotificacion,
                            'FECHAINICIAL'=>$request->fechainicial,
                            'FECHAFINAL'=>$request->fechafinal,
                            'TITULONOTIFICACION'=>$request->titulonotificacion,
                            'USUARIONOTIFICADO'=>$camarera->IDUSUARIO,
                            'NOTIFICACION'=>$request->nota,
                            'FECHACREACION'=>date("d.m.Y H:i:s"),
                            'USUARIOCREA'=>Auth::user()->ID,
                            'ACTIVA'=>'T',
                        ];
                        Notificacion::create($dato);
                    }
                }
                else
                {
                    $dato = 
                    [
                        'IDNOTIFICACION'=>$idnotificacion,
                        'FECHAINICIAL'=>$request->fechainicial,
                        'FECHAFINAL'=>$request->fechafinal,
                        'TITULONOTIFICACION'=>$request->titulonotificacion,
                        'USUARIONOTIFICADO'=>$request->camarera,
                        'NOTIFICACION'=>$request->nota,
                        'FECHACREACION'=>date("d.m.Y H:i:s"),
                        'USUARIOCREA'=>Auth::user()->ID,
                        'ACTIVA'=>'T',
                    ];
                    
                    Notificacion::create($dato);
                }
                if($request->mantenimiento == 0)
                {
                    $mantenimientos = Mantenimiento::where('ESTADO','T')->get();
                    foreach($mantenimientos as $mantenimiento)
                    {
                        $dato = 
                        [
                            'IDNOTIFICACION'=>$idnotificacion,
                            'FECHAINICIAL'=>$request->fechainicial,
                            'FECHAFINAL'=>$request->fechafinal,
                            'TITULONOTIFICACION'=>$request->titulonotificacion,
                            'USUARIONOTIFICADO'=>$mantenimiento->IDUSUARIO,
                            'NOTIFICACION'=>$request->nota,
                            'FECHACREACION'=>date("d.m.Y H:i:s"),
                            'USUARIOCREA'=>Auth::user()->ID,
                            'ACTIVA'=>'T',
                        ];
                        Notificacion::create($dato);
                    }
                }
                else
                {
                    $dato = 
                    [
                        'IDNOTIFICACION'=>$idnotificacion,
                        'FECHAINICIAL'=>$request->fechainicial,
                        'FECHAFINAL'=>$request->fechafinal,
                        'TITULONOTIFICACION'=>$request->titulonotificacion,
                        'USUARIONOTIFICADO'=>$request->mantenimiento,
                        'NOTIFICACION'=>$request->nota,
                        'FECHACREACION'=>date("d.m.Y H:i:s"),
                        'USUARIOCREA'=>Auth::user()->ID,
                        'ACTIVA'=>'T',
                    ];
                    
                    Notificacion::create($dato);
                }
            break;
        }
        return Redirect::to('Notificaciones');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function actualizarnotif($idnotificacion)
    {
        $notificacion = Notificacion::where('IDNOTIFICACION',$idnotificacion)->get()->last();
        //dd($notificacion->IDNOTIFICACION);
        return view('Notificaciones.edit',compact('notificacion'));
    }

    public function registraractualizarnotif(Request $request)
    {
        //dd("llega aqui");

        if($request->notiacti==null)
        {
            Notificacion::where('IDNOTIFICACION', $request->idnotificacion)
            ->update(['ACTIVA' => 'F']);
        }

        return Redirect::to('Notificaciones');
    }
}
