<?php

namespace camareras\Http\Controllers;

use camareras\Camareras;
use camareras\User;
use camareras\Mantenimiento;
use Illuminate\Http\Request;
use Session;
use Redirect;
use DB;
use Response;

class CamarerasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('adminmd');
    }

    public function index()
    {
        //$camareras = Camareras::all();

        $camareras = DB::table('USUARIO')
                        ->join('CAMARERA','USUARIO.ID','=','CAMARERA.IDUSUARIO')
                        ->select('CAMARERA.ID','NOMBRES','DIRECCION','PERFIL','CAMARERA.ESTADO','TELEFONO')
                        ->get();

        $mantenimientos = DB::table('USUARIO')
                        ->join('MANTENIMIENTO','USUARIO.ID','=','MANTENIMIENTO.IDUSUARIO')
                        ->select('MANTENIMIENTO.ID','NOMBRES','DIRECCION','PERFIL','MANTENIMIENTO.ESTADO','TELEFONO')
                        ->get();
        

        return view("camareras.index", compact('camareras','mantenimientos'));
        //dd($camarerasonly,$mantenimiento);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Camareras.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //NEW.ID = GEN_ID(GEN_CAMARERA_ID,1);

        $revision = DB::select(DB::raw("select first 1 GEN_ID(GEN_USUARIO_ID, 1) FROM USUARIO"));
        //dd($revision);
        $idusuario = (int) $revision[0]->GEN_ID;

        //SELECT GEN_ID(PRODUCTOS_PRD_IDENTI_GEN, 0) FROM RDB$DATABASE

        $auxpassdecode = DB::select(DB::raw("select DATOOUT FROM CODIFICAR('E','".$request->CLAVE."')"));
        $clave=$auxpassdecode[0]->DATOOUT;

        $usuarioexiste = User::where('NOMBRE_USUARIO','=',$request->NOMBRE_USUARIO)->get();
        if(count($usuarioexiste) > 0)
            $existe=1;
        else
        {
            $usuario=[
                'ID'=>$idusuario,
                'CLAVE'=>$clave,
                'PERFIL'=>$request->PERFIL,
                'NOMBRE_USUARIO'=>$request->NOMBRE_USUARIO,
                'CODIGO'=>"HOTEL",
                'ESTADO'=>'True',
                'SUPERUSUARIO'=>'F'
            ];
            //$user = User::create($usuario);
            User::create($usuario);
        }

        if($request->PERFIL == 'Camarera')
        {
            $camareraexiste = Camareras::where('NOMBRES', '=',$request->NOMBRES)->get();

            if(count($camareraexiste) > 0)
                $existe = 1;
            else
            {
                $camarera =
                [
                    'NOMBRES'=>$request->NOMBRES,
                    'DIRECCION'=>$request->DIRECCION,
                    'TELEFONO'=>$request->TELEFONO,
                    'ESTADO'=>'T',
                    'IDUSUARIO'=>$idusuario
                ];
            
                Camareras::create($camarera);
            }
        }
        else
        {
            $mantenimientoexiste = Mantenimiento::where('NOMBRES', '=',$request->NOMBRES)->get();
            if(count($mantenimientoexiste) > 0)
                $existe = 1;
            else
            {
                $mantenimiento =
                [
                    'NOMBRES'=>$request->NOMBRES,
                    'DIRECCION'=>$request->DIRECCION,
                    'TELEFONO'=>$request->TELEFONO,
                    'ESTADO'=>'T',
                    'IDUSUARIO'=>$idusuario
                ];
                
                Mantenimiento::create($mantenimiento);
            }
        }
        
        //Session::flash('message', 'grupo de suministro creado correctamente');
        return Redirect::to('Camareras');
    }

    /**
     * Display the specified resource.
     *
     * @param  \camareras\Camareras  $camareras
     * @return \Illuminate\Http\Response
     */
    public function show(Camareras $camareras)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \camareras\Camareras  $camareras
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $camareradef = Camareras::find($id);
        //dd($camareradef);
        $usuario = User::find($camareradef->IDUSUARIO);
        $auxpassdecode = DB::select(DB::raw("select DATOOUT FROM CODIFICAR('','".$usuario->CLAVE."')"));
        $clave=$auxpassdecode[0]->DATOOUT;

        $camarera=[
            'ID'=>$camareradef->ID,
            'NOMBRES'=>$camareradef->NOMBRES,
            'DIRECCION'=>$camareradef->DIRECCION,
            'TELEFONO'=>$camareradef->TELEFONO,
            'ESTADO'=>'T',
            'NOMBRE_USUARIO'=>$usuario->NOMBRE_USUARIO,
            'CLAVE'=>$clave,
            'PERFIL'=>$usuario->PERFIL
        ];
        //dd($camarera['ID']);
        return view('Camareras.edit', ['camarera'=>$camarera]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \camareras\Camareras  $camareras
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        if($request->PERFIL=="Camarera")
        {


            $camarera = Camareras::find($id);

            $auxpassdecode = DB::select(DB::raw("select DATOOUT FROM CODIFICAR('E','".$request->CLAVE."')"));
            $clave=$auxpassdecode[0]->DATOOUT;
            $usuario = User::find($camarera->IDUSUARIO);
            $usuario->fill([
                'NOMBRE_USUARIO'=>$request->NOMBRE_USUARIO,
                'CLAVE'=>$clave

            ]);
            $usuario->save();
            $camarera->fill([
                'NOMBRES'=>$request->NOMBRES,
                'DIRECCION'=>$request->DIRECCION,
                'TELEFONO'=>$request->TELEFONO,
            ]);
            //$camarera->fill($request->all());
            $camarera->save();
        }
        else
        {
            $mantenimiento = Mantenimiento::find($id);

            $auxpassdecode = DB::select(DB::raw("select DATOOUT FROM CODIFICAR('E','".$request->CLAVE."')"));
            $clave=$auxpassdecode[0]->DATOOUT;
            $usuario = User::find($mantenimiento->IDUSUARIO);
            $usuario->fill([
                'NOMBRE_USUARIO'=>$request->NOMBRE_USUARIO,
                'CLAVE'=>$clave

            ]);
            $usuario->save();
            $mantenimiento->fill([
                'NOMBRES'=>$request->NOMBRES,
                'DIRECCION'=>$request->DIRECCION,
                'TELEFONO'=>$request->TELEFONO,
            ]);
            //$camarera->fill($request->all());
            $mantenimiento->save();
        }
        //Session::flash('message', 'grupo de suministro modificado correctamente');
        return Redirect::to('Camareras');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \camareras\Camareras  $camareras
     * @return \Illuminate\Http\Response
     */
    public function destroy(Camareras $camareras)
    {
        //
    }

    public function editarmantenimiento($idmante)
    {
        $mantenimientoaux = Mantenimiento::find($idmante);
        //dd($camareradef);
        $usuario = User::find($mantenimientoaux->IDUSUARIO);
        $auxpassdecode = DB::select(DB::raw("select DATOOUT FROM CODIFICAR('','".$usuario->CLAVE."')"));
        $clave=$auxpassdecode[0]->DATOOUT;

        $mantenimiento=[
            'ID'=>$mantenimientoaux->ID,
            'NOMBRES'=>$mantenimientoaux->NOMBRES,
            'DIRECCION'=>$mantenimientoaux->DIRECCION,
            'TELEFONO'=>$mantenimientoaux->TELEFONO,
            'ESTADO'=>'T',
            'NOMBRE_USUARIO'=>$usuario->NOMBRE_USUARIO,
            'CLAVE'=>$clave,
            'PERFIL'=>$usuario->PERFIL
        ];
        //dd($camarera['ID']);
        return view('Camareras.edit', ['camarera'=>$mantenimiento]);
    }
}
