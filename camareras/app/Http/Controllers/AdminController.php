<?php

namespace camareras\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Response;
use Redirect;
use DateTime;
use Auth;
use Session;
use camareras\User;
use camareras\Notificacion;
class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuario = Auth::user()->ID;

        $nomempresa = DB::table('LOCAL')
            ->where('LOCAL.CODIGO', '=', 'HOTEL')
            ->where('LOCAL.ACTIVO', '=', 'True')
            ->select('LOCAL.NOMBRE')
            ->get();

        //dd($nomempresa[0]->NOMBRE);
        Session::put('nomempresa', $nomempresa[0]->NOMBRE);

        $notificaciones = DB::select(DB::raw("SELECT * from NOTIFICACIONES where USUARIONOTIFICADO = $usuario and ACTIVA='T' and CURRENT_DATE between FECHAINICIAL AND FECHAFINAL"));

        $numerodatos = count($notificaciones);
        
        return view("Admin.index",compact('notificaciones','numerodatos'));
    }

    public function login()
    {
        return view('Login.login');
    }

    public function logout()
    {
        Auth::logout();
        Session::flush();
        return Redirect::to('/');
    }  

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
    /*public function store(Request $request)
    {
    
        //decidi usar first para que me retornara un array lineal, asi no tengo problemas para usar los metodods de AUTH
        $user = User::where('NOMBRE_USUARIO','=',$request->usuario)->first(); 
        
        //Verifico si con el nombre de usuario viene algun registro, si es cero es porque no existe ese nombre de usuario. Si es diferente de cero es porque existe.
        if($user==null OR $user->count()==0)
        {
            return Redirect::back()->withErrors('Este usuario no existe')->withInput();
            //Session::flash('message-error', 'Sus nombre de usuario no se encuentra registrado');
        }
        else
        {
            dd($user->CLAVE);
            //Se llama el metodo para decodificar la contraseña del usuario, se manda al procedimiento el campo vacio. Con E codifoca, con diferente a E decodifica.
            $auxpassdecode = DB::select(DB::raw("select DATOOUT FROM CODIFICAR('','".$user->CLAVE."')"));
            dd($auxpassdecode);
            $passdecode=$auxpassdecode[0]->DATOOUT;
            //Se verifican que las contraseñas sean iguales, si son iguales la autorizacion genera el token, si no son iguales, se manda a que la digite nuevamente.
            if($request->clave == $passdecode)
            {
                //Se usa login para simular que las credenciales son correctas y se asocia el objeto del modelo logueado.
                Auth::login($user);
                //if(Auth::user()->PERFIL=='Camarera')
                    //return Redirect::to('Rooms/asear');
                //if(Auth::user()->PERFIL=='Administrador')
                    //return Redirect::to('Rooms/revision');
                //dd("Si es igual");
                return Redirect::to('Admin');
            }
            else
            {
                //Session::flash('message-error', 'Sus contraseña no corresonde, por favor digitela nuevamente');
                return Redirect::back()->withErrors('Contraseña no corresponde, por favor vuelvala a digitar')->withInput();
            }
        }
    }*/

    public function store(Request $request)
    {
        //decidi usar first para que me retornara un array lineal, asi no tengo problemas para usar los metodods de AUTH
        $user = User::where('NOMBRE_USUARIO','=',$request->usuario)->first(); 
        
        //Verifico si con el nombre de usuario viene algun registro, si es cero es porque no existe ese nombre de usuario. Si es diferente de cero es porque existe.
        if($user==null OR $user->count()==0)
        {
            return Redirect::back()->withErrors('Este usuario no existe')->withInput();
            //Session::flash('message-error', 'Sus nombre de usuario no se encuentra registrado');
        }
        else
        {
            //Se llama el metodo para decodificar la contraseña del usuario, se manda al procedimiento el campo vacio. Con E codifoca, con diferente a E decodifica.
            $auxpasscode = DB::select(DB::raw("select DATOOUT FROM CODIFICAR('E','".$request->clave."')"));

            //Se busca el registro con el usuario y la contraseña digitada por el usuario ya codificada.
            $user2 = User::where('NOMBRE_USUARIO','=',$request->usuario)->where('CLAVE','=',$auxpasscode[0]->DATOOUT)->first();
            
            //Se verifican que el usuario exista y la contraseña sea igual a la registrada, si son iguales la autorizacion genera el token, si no son iguales, se manda a que la digite nuevamente.
            if($user->count() > 0)
            {
                //Se usa login para simular que las credenciales son correctas y se asocia el objeto del modelo logueado.
                Auth::login($user);
                //if(Auth::user()->PERFIL=='Camarera')
                    //return Redirect::to('Rooms/asear');
                //if(Auth::user()->PERFIL=='Administrador')
                    //return Redirect::to('Rooms/revision');
                //dd("Si es igual");
                return Redirect::to('Admin');
            }
            else
            {
                //Session::flash('message-error', 'Sus contraseña no corresonde, por favor digitela nuevamente');
                return Redirect::back()->withErrors('Contraseña no corresponde, por favor vuelvala a digitar')->withInput();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
