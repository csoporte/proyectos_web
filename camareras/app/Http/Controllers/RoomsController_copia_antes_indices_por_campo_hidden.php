<?php

namespace camareras\Http\Controllers;

use Illuminate\Http\Request;
use camareras\Room;
use camareras\Asear;
use camareras\HabitacionAseada;
use camareras\Item;
use camareras\ControlRevisionAseo;
use camareras\Camarera;
use DB;
use Response;
use Redirect;
use DateTime;

class RoomsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function asear()
    {
        //$rooms = Asear::where('ACTIVO','=','True')->get();
        
        $roomsasear = DB::table('ASEAR')
            ->join('HABITACION', 'ASEAR.HABITACION', '=', 'HABITACION.HABITACION')
            ->where('HABITACION.ACTIVA', '=', 'T')
            ->where('ASEAR.ACTIVO', '=', 'True')
            ->select('ASEAR.*')
            ->get();
        //dd($roomsasear);
        return view('Asear.index', compact('roomsasear'));
    }

    public function registrarlimpieza($room)
    {
        //$datos = DB::select("BUSCAR_CLIENTE('C','98')");
        //$datos = DB::select(DB::raw("select * FROM BUSCAR_CLIENTE('C','98')"));
        
        //Debo capturar el id de la persona en sesion, el nombre, el nombre del huesped y el número de la orden si la habitación esta ocupada y enviarlo a la vista asear.
        $datosasear = Asear::find($room);
        $nomhabitacion = $datosasear->HABITACION;

        $itemsxrevisar = Item::where('ACTIVO','=','T')->get();
        $camareras = Camarera::where('ESTADO','=','T')->pluck('NOMBRES','ID');
        return view('Asear.create',compact('room','nomhabitacion','itemsxrevisar','camareras'));
    }

    public function guardarregistrolimpieza(Request $request)
    {
        dd($request);
        //$indices = $request->indices-1;
        $dato = ControlRevisionAseo::all()->last();
        if($dato==null)
            $idrevisioncamarera=1;
        else
            $idrevisioncamarera=$dato->IDREVISIONCAMARERA+1;
        for($i = 0; $i < count($request->itemsxrevisar); $i++)
        {
            $reg=
            [
                'IDREVISIONCAMARERA'=>$idrevisioncamarera,
                'IDITEM'=>$request->itemsxrevisar[$i],
                'NOTAITEM'=>$request->notas[$i],
                'IDASEAR'=>$request->idasear
            ];
            ControlRevisionAseo::create($reg);
        }

        //Debo capturar el id de la persona en sesion, el nombre, el nombre del huesped y el número de la orden si la habitación esta ocupada y guardarlo.
       
        $camarera = Camarera::find($request->camarera);
        $nombrecamarera = $camarera->NOMBRES;

        $registro=
        [
            'HABITACION'=>$request->habitacion,
            'ORDEN'=>'0',
            'FECHALIMPIEZA'=>date("d.m.Y"),
            'HUESPED'=>'huesped',
            'USUARIO'=>1,
            'CAMARERA'=>$request->camarera,
            'ID_ITEMS_CHECK'=>$idrevisioncamarera,
            'HORALIMPIEZA'=>date("H:i:s"),
        ];
        //HabitacionAseada::create($registro);

        $habitacionasear = Asear::find($request->idasear);
        $habitacionasear->FECHACONFIRMACION=date("d.m.Y H:i:s");
        $habitacionasear->HORACONFIRMACION=date("d.m.Y H:i:s");
        $habitacionasear->ASEADOR=$nombrecamarera;
        $habitacionasear->ACTIVO='False';
        //$habitacionasear->save();

        return Redirect::to('Rooms/asear');
    }


    public function index()
    {
        $rooms = Room::all();
        //dd($rooms);
        return view('Habitaciones.index', compact('rooms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $room = Room::find($id);
        //dd($room);
        return view('Habitaciones.edit',['room'=>$room]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $room = Room::find($id);
        $room->fill($request->all());
        $room->save();
        //dd($request);
        //return ['url'=>'Rooms/'];
        return Redirect::to('Rooms');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
