<?php

namespace camareras\Http\Controllers;

use camareras\User;
use Illuminate\Http\Request;
use DB;
use Response;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //$usuarios = User::select(['ID','NOMBRE_USUARIO'])->get();
        $usuarios = User::all();
        //$usuarios = User::paginate(10);
        //$usuarios= User::all()->pluck('NOMBRE_USUARIO','ID'); //obtiene una llave tipo 1->administrador.
        // dd($usuarios);
        return view('Usuarios.index', compact('usuarios'));
        //return view('Usuarios.index')->withUser($usuarios);

        /*$arrayusuario=[
            "ID"=>$usuarios[0]->ID,
            "NOMBRE_USUARIO"=>$usuarios[0]->NOMBRE_USUARIO
        ];
        
        //return view('Usuarios.index', compact('usuarios'));
        
        //dd($usuarios[0]->NOMBRE_USUARIO);
        

        // codigo utlizando INTEBASE.DLL
        /*$host = 'localhost:C:\M3Medios\HOTELES\TIKIPALOMINO\HOTEL.FDB';
        $nombre_usuario='SYSDBA';
        $contrasenya='m3medios';

        $gestor_db = ibase_connect($host, $nombre_usuario, $contrasenya);
        $sentencia = 'select nombre_usuario from usuario';
        $gestor_sent = ibase_query($gestor_db, $sentencia);
        while ($fila = ibase_fetch_row($gestor_sent)) {
            echo $fila[0], "\n";
        }
        ibase_free_result($gestor_sent);
        ibase_close($gestor_db);*/

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
