<?php

namespace camareras\Http\Controllers;

use Illuminate\Http\Request;
use camareras\Notas;
use camareras\Camarera;
use DB;
use Response;
use Redirect;
use DateTime;
use Auth;
use Session;


class NotasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('adminmd');
    }


    public function index()
    {
        //$notas = Notas::all();
        return view('Notas.index');
    }

    public function cargar(Request $request)
    {
        $notas = Notas::where('FECHANOTA','=',$request->fecharevision)->get();
        $camarera = Camarera::where('ID','=',$notas[0]->CAMARERA)->get();
        $nombrecamarera = $camarera[0]->NOMBRES;
        //dd($notas);
        return view('Notas.mostrar',compact('notas'),compact('nombrecamarera')) ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Notas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $camarera = Camarera::where('IDUSUARIO','=',Auth::user()->ID)->get();
        //dd($camarera[0]);
        $nota=
        [
            'FECHANOTA'=>$request->fecha,
            'USUARIO'=>Auth::user()->ID,
            'CAMARERA'=>$camarera[0]->ID,
            'NOTA'=>$request->nota
        ];
        Notas::create($nota);
        Session::flash('message', 'grupo de suministro creado correctamente');
        return Redirect::to('Notas');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $nota = Notas::find($id);
        return view('Notas.edit', ['nota'=>$nota]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $notas = Notas::find($id);
        $notas->fill($request->all());
        $notas->save();
        Session::flash('message', 'grupo de suministro modificado correctamente');
        return ['url'=>'Notas/'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
