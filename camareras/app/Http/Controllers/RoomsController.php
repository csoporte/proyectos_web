<?php

namespace camareras\Http\Controllers;

use Illuminate\Http\Request;
use camareras\Room;
use camareras\Asear;
use camareras\HabitacionAseada;
use camareras\Item;
use camareras\ControlRevisionAseo;
use camareras\Camareras;
use camareras\Bloqueada;
use camareras\enmantenimiento;
use camareras\InvHabitacion;
use camareras\Servicio;
use camareras\consumoxhabitacion;
use DB;
use Response;
use Redirect;
use DateTime;
use Auth;

class RoomsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('adminmd');
    }


    public function asear()
    {
        //$rooms = Asear::where('ACTIVO','=','True')->get();
        
        /*$roomsasear = DB::table('ASEAR')
            ->join('HABITACION', 'ASEAR.HABITACION', '=', 'HABITACION.HABITACION')
            ->where('HABITACION.ACTIVA', '=', 'T')
            ->where('ASEAR.ACTIVO', '=', 'True')
            ->select('ASEAR.*')
            ->get();*/

        if(Auth::User()->PERFIL =='Camarera')
        {
            $roomsasear = DB::table('HABITACION')
                ->join('CAMARERA','HABITACION.CAMARERA','=','CAMARERA.ID')
                ->join('USUARIO', 'CAMARERA.IDUSUARIO','=','USUARIO.ID')
                ->where('USUARIO.ID','=',Auth::User()->ID)
                ->where('HABITACION.ACTIVA', '=', 'T')
                //->where('HABITACION.ESTADO_LIMPIEZA', '=', 'F')
                ->select('HABITACION.*')
                ->get();
            //dd($roomsasear);
        }

        if(Auth::User()->PERFIL =='Mantenimiento')
        {
            $roomsasear = DB::table('HABITACION')
                ->join('EN_MANTENIMIENTO','HABITACION.HABITACION','=','EN_MANTENIMIENTO.HABITACION')
                ->where('HABITACION.ACTIVA', '=', 'T')
                ->where('HABITACION.ESTADO','=','6')
                ->where('EN_MANTENIMIENTO.REALIZADO','=','F')
                ->select('HABITACION.*')
                ->get();
        }

        if(count($roomsasear) > 0)
            return view('Asear.index', compact('roomsasear'));
        else
            return view('layouts.sindatos');
    }

    public function registrarlimpieza($room)
    {   
        //Ejemplo de como llamar un procedimiento de la base de datos de firebird.
        //$datos = DB::select(DB::raw("select * FROM BUSCAR_CLIENTE('C','98')"));
        
        
        $datosasear = Room::find($room);
        $nomhabitacion = $datosasear->HABITACION;
        
        //Proceso donde se carga de base de datos los elementos a cargar en la habitación para que sean gestionados o revisados.
        $itemsxrevisar = Item::where('ACTIVO','=','T')->get();

        //Proceso para seleccionar la camarera en sesion.
        $camareras = Camareras::where('ESTADO','=','T')->where('IDUSUARIO',Auth::User()->ID)->pluck('NOMBRES','ID');

        //proceso para seleccionar los articulos o elementos asociados a la habitación.
        $invhabitacion = InvHabitacion::where('HABITACION',$room)->get();

        //Proceso para capturar todos los servicios y articulos en forma de llave=>valor.
        $servicios = Servicio::where('ACTIVO','True')->where('TIPO','False')->pluck('NOMBRE','CODIGO');

        //Se llama la vista que carga el formulario para gestionar la limpieza de la habitación.
        //return view('Asear.create',compact('room','nomhabitacion','itemsxrevisar','camareras'));

        $datos = DB::select(DB::raw("select * FROM CONSULTAORDEN('".$nomhabitacion."',null,'0',null)"));
        $ordenes = count($datos);

        return view('Asear.create',compact('room','nomhabitacion','itemsxrevisar','camareras','invhabitacion','servicios','ordenes'));
    }

    public function guardarregistrolimpieza(Request $request)
    {   
        if($request->bloquear=='on')
            $enviarmantenimiento="T";
        else
            $enviarmantenimiento="F";
        
        $indices = $request->indices;

        //variable contadora auxiliar la cual guardará el numero de datos de los items a revisar que no sean null. Si la variable al final del ciclo sigue en cero significa que ningun ítem fue seleccionado por tal motivo la variable idrevisioncamarera debe ser null ya que no tiene datos asosciados dentro de la tabla ControlRevisionAseo
        $contadoraux=0;

        //Se hace el registro de todos los elementos a revisar dentro de la habitación. DE aqui se saca un consecutivo unico para todos los elementos y este se registrará posteriormente en el registro de aseo de la habitación por parte de la camarera.
        $dato = ControlRevisionAseo::all()->last();
        if($dato==null)
            $idrevisioncamarera=1;
        else
            $idrevisioncamarera=$dato->IDREVISIONCAMARERA+1;
        
        for($i = 0; $i < $indices; $i++)
        {
            //dd($request->input('itemsxrevisar_'.$i));
            if($request->input('itemsxrevisar_'.$i)!=null)
            {
                $reg=
                [
                    'IDREVISIONCAMARERA'=>$idrevisioncamarera,
                    'IDITEM'=>$request->input('itemsxrevisar_'.$i),
                    'NOTAITEM'=>$request->input('notas_'.$i),
                ];
                ControlRevisionAseo::create($reg);
                $contadoraux=$contadoraux+1;
            }
        }

        if($contadoraux==0)
            $idrevisioncamarera=null;
       
        $camarera = Camareras::find($request->camarera);
        $nombrecamarera = $camarera->NOMBRES;


        //Se selecciona la habitación correspondiente.
        $room = Room::find($request->habitacion);

        // Se guarda un registro del aseo por parte de la camarera en la tabla Habitacion_aseada
        $registro=
        [
            'HABITACION'=>$request->habitacion,
            'ORDEN'=>$room->ORDEN,
            'FECHALIMPIEZA'=>date("d.m.Y"),
            'HUESPED'=>$room->NCLIENTE,
            'USUARIO'=>Auth::user()->ID,
            'CAMARERA'=>$request->camarera,
            'ID_ITEMS_CHECK'=>$idrevisioncamarera,
            'HORALIMPIEZA'=>date("H:i:s"),
            'NOTA_HABITACION'=>$request->nota_habitacion,
            'ENVIARAMANTENIMIENTO'=>$enviarmantenimiento
        ];
        HabitacionAseada::create($registro);

        $idhabitacionaseada = HabitacionAseada::all()->last();

        if($request->bloquear=='on')
        {
            $bloqueada = 
            [
                'HABITACION'=>$request->habitacion,
                'MOTIVO'=>$request->nota_habitacion,
                'FECHA'=>date("d.m.Y H:i:s"),
                'ESTADO'=>"T",
                'ESTADO_MOTORWEB'=>"0"
            ];
            Bloqueada::create($bloqueada);

            $infobloqueo = 
            [
                'HABITACION'=>$request->habitacion,
                'CAMARERA'=>$nombrecamarera,
                'IDHABITACIONASEADA'=>$idhabitacionaseada->ID,
                'ID_ITEMS_CHECK_CAMARERA'=>$idrevisioncamarera,
                'NOTA_CAMARERA'=>$request->nota_habitacion,
                'REALIZADO'=>'F'
            ];

            enmantenimiento::create($infobloqueo);
        }
        else
        {
            //Se verifica el estado de la habitación en la tabla habitacion. 1=Para limpieza, 2=libre, 3=reserva, 4=debe pero tiene anticipos, 5=debe, 6=bloqueado. Si se encuentra para limpieza significa que la tablaa asear ya tiene un registro, por lo cual se busca y se carga este registro para llenar la información necesaria. Si no esta en el estado de aseo, entonces se genera un nuevo registro en la tabla asear.
            if($room->ESTADO==1)
            {
                $habitacionasear =DB::table('ASEAR')
                ->where('ASEAR.HABITACION', '=', $room->HABITACION)
                ->where('ASEAR.ACTIVO','=','True')
                ->select('ASEAR.*')
                ->get();

                $habitacionasear->FECHACONFIRMACION=date("d.m.Y H:i:s");
                $habitacionasear->HORACONFIRMACION=date("d.m.Y H:i:s");
                $habitacionasear->ASEADOR=$nombrecamarera;
                $habitacionasear->ACTIVO='False';
                $habitacionasear->save();
            }
            else
            {
                $habitacionasear=
                [
                    'HABITACION'=>$request->habitacion,
                    'FECHA'=>date("d.m.Y H:i:s"),
                    'HORA'=>date("d.m.Y H:i:s"),
                    'FECHACONFIRMACION'=>date("d.m.Y H:i:s"),
                    'HORACONFIRMACION'=>date("d.m.Y H:i:s"),
                    'ASEADOR'=>$nombrecamarera,
                    'ACTIVO'=>'False'
                ];

                Asear::create($habitacionasear);
            }
            //Se actualiza el estado de la habitación en la tabla habitacion
            $room->ESTADO_LIMPIEZA='T';
            $room->save();
        }
        if($request->gestionminibar=="on")
        {
            //proceso para seleccionar los articulos o elementos asociados a la habitación.
            $invhabitacion = InvHabitacion::where('HABITACION',$request->habitacion)->get();

            //proceso para moverse por todos los articulos asociados a la habitacion
            foreach($invhabitacion as $inv)
            {
                //Se accede al input con el nombre del input concatenada con el id del registro asociado en la habitacion, asi se aseegura que el producto que carga en el formulario es el mismo que esta como registro de la tabla inv_inventario. Además se verifica que si se haya 
                
                if($request->input('nuevacantidad_'.$inv->ID)==null)
                    $cantidadfinal=0;
                else
                    $cantidadfinal=$request->input('nuevacantidad_'.$inv->ID);

                $cantidadconsumida=$inv->CANTIDAD - $cantidadfinal;

                //Proceso donde se consultan las ordenes activas mediante un procedimiento de base de datos. Solicita el nombre de la habitacion, el numero de orden para realizar la busqueda si por casualidad no hay #orden entonces se deja en blanco, el otro campo es el tipo de busqueda, con cero significa por habitacion por 1 busca de acuerdo a la habitacion
                $datos = DB::select(DB::raw("select * FROM CONSULTAORDEN('".$request->habitacion."',null,'0',null)"));
                //se cuenta cuantas ordenes activas viene de la consulta y se resta una para seleccionar la ultima orden del objeto.
                $indiceordenes = count($datos)-1;

                //Se buscan todos los datos del artículo.
                $service = Servicio::find($inv->SERVICIO);
                
                //se genera el registro de control del articulo consumido. Siempre que haya existido consumo.
                if($cantidadconsumida > 0)
                {
                    $datoconsumo =
                    [
                        'CODIGOPRODUCTO'=>$inv->SERVICIO,
                        'HABITACION'=>$request->habitacion,
                        'ORDENACTIVA'=>$datos[$indiceordenes]->N_ORDEN,
                        'HUESPED'=>$room->NCLIENTE,
                        'CANTIDADINICIAL'=>$inv->CANTIDAD,
                        'CANTIDADFINAL'=>$cantidadfinal,
                        'CANTIDADCONSUMIDA'=>$cantidadconsumida,
                        'FECHAANOTACION'=>date("d.m.Y"),
                        'HORAANOTACION'=>date("H:i:s"),
                        'CAMARERAANOTA'=>$nombrecamarera,
                        'IDUSUARIOANOTA'=>Auth::user()->ID
                    ];
                    consumoxhabitacion::create($datoconsumo);

                    //Si la diferencia entre lo que deberia haber y lo registrado por la camarera es mayor a cero significa que hubo consumos por lo que se registra con el procedimiento y se asocia a la habitacion.
                    DB::select(DB::raw("EXECUTE PROCEDURE VENTAPAQUETE(current_timestamp,current_timestamp,0,".$datos[$indiceordenes]->N_ORDEN.",".$service->VALOR.",".$inv->SERVICIO.",".Auth::user()->ID.",".$cantidadconsumida.",null,null)"));
                }
            }
        }
        return Redirect::to('Rooms/asear');
    }

    public function revision()
    {
        $rooms = DB::table('HABITACION')
            ->join('CAMARERA','HABITACION.CAMARERA','=','CAMARERA.ID')
            ->where('HABITACION.ACTIVA', '=', 'T')
            ->select('HABITACION.*','CAMARERA.*')
            ->get();
        //dd($rooms);
        return view('Habitaciones.revision', compact('rooms'));
    }

    public function observarhabitacion($room)
    {
        //dd($room);
        $habitacion = HabitacionAseada::where('HABITACION','=',$room)->where('FECHALIMPIEZA','=',date("d.m.Y"))->get()->last();
        //dd($habitacion->HABITACION);
        if($habitacion == null)
        {
            $habitacion = Asear::where('HABITACION','=',$room)->where('FECHACONFIRMACION','=',date("d.m.Y"))->where('ACTIVO','=','False')->get()->last();
            if($habitacion==null)
            {
                $revisiones = new ControlRevisionAseo();
                $existendatos=0;

                return view('Habitaciones.observarvacio',compact('room'));
            }
            else
            {
                $revisiones = DB::table('CONTROLREVISIONASEO')
                ->join('ITEMSXREVISARCAMARERAS','CONTROLREVISIONASEO.IDITEM','=','ITEMSXREVISARCAMARERAS.ID')
                ->where('CONTROLREVISIONASEO.IDHABITACIONASEADA','=',$habitacion->ID)
                ->select('CONTROLREVISIONASEO.*','ITEMSXREVISARCAMARERAS.ITEM')
                ->get(); 

                if(count($revisiones) > 0)
                    return view('Habitaciones.observaritems',compact('room','revisiones'));
                else
                    return view('Habitaciones.observarvacio',compact('room'));
            }
            //dd($habitacion);
        }
        else
        {
            //$revisiones = ControlRevisionAseo::where('IDHABITACIONASEADA','=',$habitacion->ID)->get();
            $revisiones = DB::table('CONTROLREVISIONASEO')
            ->join('ITEMSXREVISARCAMARERAS','CONTROLREVISIONASEO.IDITEM','=','ITEMSXREVISARCAMARERAS.ID')
            ->where('CONTROLREVISIONASEO.IDHABITACIONASEADA','=',$habitacion->ID)
            ->select('CONTROLREVISIONASEO.*','ITEMSXREVISARCAMARERAS.ITEM')
            ->get();

            if(count($revisiones) > 0)
                return view('Habitaciones.observar', compact('habitacion','revisiones'));
            else
                return view('Habitaciones.observaritemsvacio', compact('habitacion'));
        }
        
    }

    public function registrarmantenimiento($room)
    {
        $datoshabitacion = Room::find($room);
        $nomhabitacion = $datoshabitacion->HABITACION;

        $datosmantenimiento = enmantenimiento::where('HABITACION',$room)->where('REALIZADO','=','F')->get();

        $infohabitacionaseada = HabitacionAseada::where('ID',$datosmantenimiento[0]->IDHABITACIONASEADA)->get();

        $itemsall = Item::pluck('ITEM','ID');

        $itemsxrevisar = ControlRevisionAseo::where('IDHABITACIONASEADA',$datosmantenimiento[0]->IDHABITACIONASEADA)->get();

        //dd($items['2']);
        //dd($datosmantenimiento[0]);

        return view('Mantenimiento.create',compact('room','nomhabitacion','datosmantenimiento','infohabitacionaseada','itemsall','itemsxrevisar'));
    }

    
    public function guardarregistromantenimiento(Request $request)
    {
        $regenmantenimiento = enmantenimiento::find($request->idmantenimiento);
        //dd($request->nota_mantenimiento,$request->bloquear, $request->idmantenimiento);
        $regenmantenimiento->PERSONA_MANTENIMIENTO=Auth::User()->NOMBRE_USUARIO;
        $regenmantenimiento->ID_USUARIO=Auth::User()->ID;
        $regenmantenimiento->FECHAMANTENIMIENTO=date("d.m.Y H:i:s");
        $regenmantenimiento->HORAMANTENIMIENTO=date("d.m.Y H:i:s");
        if($request->bloquear==null)
        {
            $regenmantenimiento->REALIZADO="T";
            $bloqueada = Bloqueada::where('HABITACION',$regenmantenimiento->HABITACION)->where('ESTADO','=','T')->get();
            $bloqueada[0]->ESTADO = "F";
            $bloqueada[0]->FECHAFIN = date("d.m.Y H:i:s");
            $bloqueada[0]->save();
            //dd($bloqueada[0]);
        }
        $regenmantenimiento->NOTA_MANTENIMIENTO=$request->nota_mantenimiento;
        $regenmantenimiento->save();

        return Redirect::to('Rooms/asear');
    }


    public function index()
    {
        $rooms = Room::where('ACTIVA','=','T')->get()->sortBy('HABITACION');
        //dd($rooms);
        return view('Habitaciones.index', compact('rooms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $room = Room::find($id);
        //dd($room);
        return view('Habitaciones.edit',['room'=>$room]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $room = Room::find($id);
        //$room->fill($request->all());
        if($request->limpiar=='on')
        {
            $habitacionasear=
            [
                'HABITACION'=>$request->HABITACION,
                'FECHA'=>date("d.m.Y H:i:s"),
                'HORA'=>date("d.m.Y H:i:s"),
                'ACTIVO'=>'True'
            ];

            Asear::create($habitacionasear);
            $room->ESTADO='1';
            $room->ESTADO_LIMPIEZA='F';
        }
        else
        {
            $dato = Asear::where('HABITACION',$request->HABITACION)->where('ACTIVO','=','True')->get();
            if(count($dato) > 0)
            {
                $dato[0]->FECHACONFIRMACION = date("d.m.Y H:i:s");
                $dato[0]->HORACONFIRMACION = date("d.m.Y H:i:s");
                $dato[0]->ASEADOR = "ADMINISTRADOR";
                $dato[0]->ACTIVO = "False";

                $dato[0]->save();

            }

            if($room->NCLIENTE==null or $room->NCLIENTE=="")
                $room->ESTADO='2';
            else
                $room->ESTADO='5';
            
            $room->ESTADO_LIMPIEZA='T';
        }

        if($request->bloquear=='on')
        {
            $bloqueada = 
            [
                'HABITACION'=>$request->HABITACION,
                'MOTIVO'=>"Habitación bloqueada por administrador",
                'FECHA'=>date("d.m.Y H:i:s"),
                'ESTADO'=>"T",
                'ESTADO_MOTORWEB'=>"0"
            ];
            Bloqueada::create($bloqueada);

            $room->ESTADO='6';
            $room->ESTADO_LIMPIEZA='F';
        }
        else
        {
            $dato = Bloqueada::where('HABITACION',$request->HABITACION)->where('ESTADO','=','T')->get();
            if(count($dato) > 0)
            {
                $dato[0]->FECHAFIN = date("d.m.Y H:i:s");
                $dato[0]->ESTADO = 'F';
                $dato[0]->save();
            }
            /*if($request->limpiar=="on")
                $room->ESTADO_LIMPIEZA='F';
            else    */
        }
        
        $room->save();

        //dd($room);
        //dd($request);
        //return ['url'=>'Rooms/'];
        return Redirect::to('Rooms');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
