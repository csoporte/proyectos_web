<?php

namespace camareras;

use Illuminate\Database\Eloquent\Model;

class enmantenimiento extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     * 
     */
    
    protected $table = 'EN_MANTENIMIENTO';

    protected $primaryKey = 'ID';

    public $incrementing = false;

    protected $fillable = ['HABITACION','CAMARERA','IDHABITACIONASEADA','ID_ITEMS_CHECK_CAMARERA','NOTA_CAMARERA','PERSONA_MANTENIMIENTO','ID_USUARIO','NOTA_MANTENIMIENTO','FECHAMANTENIMIENTO','HORAMANTENIMIENTO','REALIZADO'];

    public $timestamps = false;
}
