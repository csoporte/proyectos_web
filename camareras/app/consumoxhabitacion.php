<?php

namespace camareras;

use Illuminate\Database\Eloquent\Model;

class consumoxhabitacion extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     * 
     */
    
    protected $table = 'CONSUMO_X_HABITACION';

    protected $primaryKey = 'ID';

    public $incrementing = false;

    protected $fillable = ['CODIGOPRODUCTO','HABITACION','ORDENACTIVA','HUESPED','CANTIDADINICIAL','CANTIDADFINAL','CANTIDADCONSUMIDA','FECHAANOTACION','HORAANOTACION','CAMARERAANOTA','IDUSUARIOANOTA'];

    public $timestamps = false;
}
