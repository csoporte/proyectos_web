<?php

namespace camareras;

use Illuminate\Database\Eloquent\Model;

class Notificacion extends Model
{
    protected $table = 'NOTIFICACIONES';

    protected $primaryKey = 'ID';

    public $incrementing = false;

    protected $fillable = ['IDNOTIFICACION','FECHAINICIAL','FECHAFINAL','TITULONOTIFICACION','USUARIONOTIFICADO','NOTIFICACION','FECHACREACION','USUARIOCREA','ACTIVA'];

    public $timestamps = false;
}
