<?php

namespace camareras;

use Illuminate\Database\Eloquent\Model;

class ControlRevisionAseo extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     * 
     */
    
    protected $table = 'CONTROLREVISIONASEO';

    protected $primaryKey = 'ID';

    public $incrementing = false;

    //public $keyType = 'string';

    protected $fillable = ['IDREVISIONCAMARERA','IDITEM','NOTAITEM','IDHABITACIONASEADA'];

    public $timestamps = false;
}
