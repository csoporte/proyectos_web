<?php

namespace camareras;

use Illuminate\Database\Eloquent\Model;

class Camarera extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     * 
     */
    
    protected $table = 'CAMARERA';

    protected $primaryKey = 'ID';

    public $incrementing = false;

    //public $keyType = 'string';

    protected $fillable = ['NOMBRES','DIRECCION','TELEFONO','ESTADO'];

    public $timestamps = false;
}
