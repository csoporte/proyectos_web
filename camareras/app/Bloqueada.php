<?php

namespace camareras;

use Illuminate\Database\Eloquent\Model;

class Bloqueada extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     * 
     */
    
    protected $table = 'BLOQUEADA';

    protected $primaryKey = 'ID';

    public $incrementing = false;

    protected $fillable = ['HABITACION','MOTIVO','FECHA','ESTADO','ESTADO_MOTORWEB'];

    public $timestamps = false;
}
