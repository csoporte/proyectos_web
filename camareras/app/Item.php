<?php

namespace camareras;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $table = 'ITEMSXREVISARCAMARERAS';

    protected $primaryKey = 'ID';

    public $incrementing = false;

    protected $fillable = ['ITEM','MARCA','CARACTERISTICA','ACTIVO'];

    public $timestamps = false;
}
