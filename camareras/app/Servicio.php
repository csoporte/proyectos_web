<?php

namespace camareras;

use Illuminate\Database\Eloquent\Model;

class Servicio extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     * 
     */
    
    protected $table = 'SERVICIO';

    protected $primaryKey = 'CODIGO';

    public $incrementing = false;

    protected $fillable = ['CODIGO','NOMBRE','VALOR','IVA','OBSERVACIONES','TIPO','GRUPO','ACTIVO','MEDIDA','COSTO','CODIGOB','MINIMO','IDPLAN','FOTO','DOLAR','CLASE','TEMPORADA','DESCRIPCION','DESCUENTO','FECHA_INICIO','FECHA_FIN','STOCK','TSELECCION','TDESCUENTO','ACOMPANANTE','TIENEA','DIAS','SUBGRUPO','REFERENCIA','COLOR','TALLA','MANEJARSERIALGARANTIA','USUARIO','STOCKMAXIMO','MAXIMO','ESTADO_MOTORWEB'];

    public $timestamps = false;
}
