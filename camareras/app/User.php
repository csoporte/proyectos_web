<?php

namespace camareras;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     * 
     */
    
    protected $table = 'USUARIO';

    protected $primaryKey = 'ID';

    public $incrementing = false;

    //public $keyType = 'string';

    protected $fillable = ['ID', 'CLAVE', 'PERFIL','NOMBRE_USUARIO','ESTADO','MENU','CODIGO','DESCUENTO','BORRAR','BORRARDIA','BITACORA','AUDITORIA','PREAUDITORIA','STANDBY','ANULAFACTURA','MODIFICARCUENTA','DESOCUPAR','MENUCHEF','DESCUENTOALOJA','PALOJAMIENTO','CONTAEDITAR','CONTAANULAR','MENUCONTA','CONTAELIMINAR','AUTORIZACXC','FCOMPRA','ELIMINARCOMPRA','EDITARCLIENTE','MODIFICARGRUPO','FECHA_FACTURA','FECHA_ANTERIOR','PDESCUENTO','PMALOJAMIENTO','PCLIENTE','SACARSTD','CAMBIARTIEMPOCXC','MAYORVALOR','NOBREGISTRO','ENCRI','MPAQUETE','ANULARDIA','MENUPOS','OBSERVACIONES','FACTURAR','MENUEVENTOS','MENUNOMINA','USUARIO_CORREO','CLAVE_CORREO','CAMBIARFORMAPAGO','EDITAOBSERVACIONES','SUPERUSUARIO','ARQUEO'
    ];

    public $timestamps = false;
    /*protected $fillable = ['id', 'perfil','nombre_usuario','estado','codigo','descuento','borrar','borrardia','bitacora','auditoria','preauditoria','standby','anulafactura','modificarcuenta','desocupar','descuentoaloja','palojamiento','contaeditar','contaanular','contaeliminar','autorizacxc','fcompra','eliminarcompra','editarcliente','modificargrupo','fecha_factura','fecha_anterior','pdescuento','pmalojamiento','pcliente','sacarstd','cambiartiempocxc','mayorvalor','nobregistro','encri','mpaquete','anulardia','menupos','observaciones','facturar','usuario_correo','clave_correo','cambiarformapago','editaobservaciones','superusuario','arqueo'
    ];*/

    /*protected $hidden = [
        'password', 'remember_token',
    ];*/

    public function setAttribute($key, $value)
    {
        $isRememberTokenAttribute = $key == $this->getRememberTokenName();
        if (!$isRememberTokenAttribute)
        {
          parent::setAttribute($key, $value);
        }
    }
}
