<?php

namespace camareras;

use Illuminate\Database\Eloquent\Model;

class HabitacionAseada extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     * 
     */
    
    protected $table = 'HABITACION_ASEADA';

    protected $primaryKey = 'ID';

    public $incrementing = false;

    //public $keyType = 'string';

    protected $fillable = ['HABITACION','ORDEN','FECHALIMPIEZA','HUESPED','USUARIO','CAMARERA','ID_ITEMS_CHECK','HORALIMPIEZA','NOTA_HABITACION','ENVIARAMANTENIMIENTO'];

    public $timestamps = false;
}
