<?php

namespace camareras;

use Illuminate\Database\Eloquent\Model;

class Notas extends Model
{
    protected $table = 'NOTASXHABITACIONDIARIA';

    protected $primaryKey = 'ID';

    public $incrementing = false;

    //public $keyType = 'string';

    protected $fillable = ['FECHANOTA','USUARIO','CAMARERA','NOTA'];

    public $timestamps = false;
}
