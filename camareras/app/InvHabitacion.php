<?php

namespace camareras;

use Illuminate\Database\Eloquent\Model;

class InvHabitacion extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     * 
     */
    
    protected $table = 'INV_HABITACION';

    protected $primaryKey = 'ID';

    public $incrementing = false;

    protected $fillable = ['ID','SERVICIO','HABITACION','CANTIDAD'];

    public $timestamps = false;

    protected $casts = ['CANTIDAD' => 'integer'];
}
