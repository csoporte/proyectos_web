<?php

namespace camareras;

use Illuminate\Database\Eloquent\Model;

class Mantenimiento extends Model
{
    protected $table = 'MANTENIMIENTO';

    protected $primaryKey = 'ID';

    public $incrementing = false;

    protected $fillable = ['NOMBRES','DIRECCION','TELEFONO','ESTADO','IDUSUARIO'];

    public $timestamps = false;
}
