<?php

namespace camareras;

use Illuminate\Database\Eloquent\Model;

class Asear extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     * 
     */
    
    protected $table = 'ASEAR';

    protected $primaryKey = 'ID';

    public $incrementing = false;

    protected $fillable = ['HABITACION','FECHA','HORA','FECHACONFIRMACION','HORACONFIRMACION','ASEADOR','ACTIVO'];

    public $timestamps = false;
}
