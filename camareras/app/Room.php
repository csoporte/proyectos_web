<?php

namespace camareras;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     * 
     */
    
    protected $table = 'HABITACION';

    protected $primaryKey = 'HABITACION';

    //public $incrementing = false;

    public $keyType = 'string';

    protected $fillable = ['HABITACION','CLASE','EXTENSION','FOTO','LOCAL','CAMARERA','ACTIVA','MOSTRAR_RESERVAS','NOMBRE_RESERVAS','MANTENIMIENTO','ESTADO','NCLIENTE','FECHA_LLEGADA','FECHA_SALIDA','FECHARESERVA','CGRUPO','ORDEN','TARJETA','ID_CERRADURA','EDIFICIO','PISO','OBSERVACIONES','ESTADO_LIMPIEZA','ESTADO_MOTORWEB'];

    public $timestamps = false;
}
